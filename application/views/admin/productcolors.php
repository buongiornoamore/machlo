<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title><? echo COMPANY_NAME . ' - ADMIN'; ?></title>
    <meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' />
    <meta name="viewport" content="width=device-width" />
    <? include('include/header_admin.php'); ?>
    <!-- color picker -->
    <link href="<? echo ASSETS_ROOT_FOLDER_ADMIN_CSS; ?>/bootstrap-colorpicker.min.css" rel="stylesheet" />
    <!-- JS / CSS added to GROCERY CRUD template -->
    <?php
    // import grocery and codigniter css
    foreach($css_files as $file): ?>
        <link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
    <?php endforeach; ?>
    <?php foreach($js_files as $file): ?>
        <script src="<?php echo $file; ?>"></script>
    <?php endforeach; ?>
    <!-- END JS / CSS added to GROCERY CRUD template -->
</head>
<body>
	<div class="wrapper">
        <? include('include/left_menu.php'); ?>
        <div class="main-panel">
            <? include('include/navbar_top.php'); ?>
            <!-- GROCERY CRUD output
    		<div class="content">
                <div class="container-fluid">
                	<?php// echo $output; ?>
                </div>
            </div> -->
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
            			<div class="col-md-12">
                            <div class="card">
                            	<div class="card-header card-header-icon" data-background-color="rose">
                                    <i class="material-icons">color_lens</i>
                                </div>
                                <div class="card-content">
									<h4 class="card-title">Colori prodotti</h4>
	                                <div class="table-responsive">
                                    	<?php echo $output; ?>
                                    </div>
                                 </div>
                        	</div>
                        </div>
                    </div>
                 </div>
            </div>
     		<? include('include/footer.php'); ?>
        </div>
    </div>
</body>
<? include('include/footer_js_admin.php'); ?>
<!-- color picker -->
<script src="<? echo ASSETS_ROOT_FOLDER_ADMIN_JS; ?>/bootstrap-colorpicker.min.js" type="text/javascript"></script>
<script type="text/javascript">
    $(document).ready(function() {
		$('#field-codice_colore').colorpicker({
				hexNumberSignPrefix: false,
				format: 'hex'
			});
/**/
    });
</script>
</html>
