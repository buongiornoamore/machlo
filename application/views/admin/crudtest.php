<!doctype html>
<html>
<head>
<meta charset="utf-8">
<title>Documento senza titolo</title>
	<?php 
	// import grocery and codigniter css
	foreach($css_files as $file): ?>
		<link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
	<?php endforeach; ?>
	<?php foreach($js_files as $file): ?>
		<script src="<?php echo $file; ?>"></script>
	<?php endforeach; ?>
</head>

<body>
	 <!-- GROCERY CRUD output -->
    <div class="content">
        <div class="container-fluid">
            <?php echo $output; ?>
        </div>
    </div>       
</body>
</html>