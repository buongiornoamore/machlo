<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title><? echo COMPANY_NAME . ' - ADMIN'; ?></title>
    <meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' />
    <meta name="viewport" content="width=device-width" />
    <? include('include/header_admin.php'); ?>
<!-- JS / CSS added to GROCERY CRUD template -->
<?php 
// import grocery and codigniter css
foreach($css_files as $file): ?>
    <link type="text/css" rel="stylesheet" href="<?php echo $file; ?>" />
<?php endforeach; ?>
<?php foreach($js_files as $file): ?>
    <script src="<?php echo $file; ?>"></script>
<?php endforeach; ?>
<!-- END JS / CSS added to GROCERY CRUD template --> 
</head>
<body>
	<div class="wrapper">
        <? include('include/left_menu.php'); ?>
        <div class="main-panel">
            <? include('include/navbar_top.php'); ?>  
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
            			<div class="col-md-12">
                            <div class="card">
                            	<div class="card-header card-header-icon" data-background-color="rose">
                                    <i class="material-icons">shopping_cart</i>
                                </div>
                                <div class="card-content">
									<h4 class="card-title"><?php echo $data['curr_function_title'];?>&nbsp;<a href="<?php echo site_url('admin/orders')?>" class="btn btn-danger btn-simple btn-little-icon" title="Ritorna agli ordini">
                                            <i class="material-icons">arrow_back</i>
                                        </a>
                                    </h4>    
                                    <!-- mettere una dropdown per cambiare prodotto -->
	                                <div class="table-responsive">
                                    	<?php echo $output; ?>
                                    </div>
                                 </div>   
                        	</div>          
                        </div>    
                    </div>
                 </div>
            </div>        
     		<? include('include/footer.php'); ?>
        </div>
    </div>
</body>
<? include('include/footer_js_admin.php'); ?>
<script type="text/javascript">
    $(document).ready(function() {
		var crudState = '<?php echo $data['curr_state'];?>';
		// Nasconde il pulsante per cancellare immagine
		$('.delete-anchor').hide();
		
		if($('#field-id_prodotto').length) {
			$("#field-id_prodotto").chosen().change(function() {
				// caricare dinamicamnte in ajax la select delle varianti prodotto
				var code = $("#field-id_prodotto option:selected").text().split("-");
				console.log((code[0]).trim());
				loadDependantSelectVarianti((code[0]).trim(), $('#field-id_variante').val());
			});

			// disabilita in partenza
			if(!$('#field-id_variante').val()) {
				$('#field-id_variante').empty();
				$('#field-id_variante').prop('disabled', true).trigger("chosen:updated");
			} else {
				var code = $("#field-id_prodotto option:selected").text().split("-");
				console.log((code[0]).trim());
				loadDependantSelectVarianti((code[0]).trim(), $('#field-id_variante').val());
			}
		}
		
    	if($('#field-id_variante').length) {
			$("#field-id_variante").chosen().change(function() {
				console.log("#field-id_variante " + $("#field-id_variante option:selected").data('prezzo'));
				// cambia dinamicamente
				$('#field-prezzo').html($("#field-id_variante option:selected").data('prezzo'));
				$('#field-prezzo_scontato').val($("#field-id_variante option:selected").data('prezzoscontato'));
				$('#field-colore_prodotto').html($("#field-id_variante option:selected").data('colore'));
				$('#field-taglia').html($("#field-id_variante option:selected").data('taglia'));
				$('#field-url_immagine').html('<a href="http://localhost/machlo/assets/assets-frontend/img/shop/'+$("#field-id_variante option:selected").data('img')+'" class="image-thumbnail"><img src="http://localhost/machlo/assets/assets-frontend/img/shop/'+$("#field-id_variante option:selected").data('img')+'" height="150px"></a>');
				// scrivere in gcrud qualcosa che sostituisca i dati effettivamente in storico carrello e
				// ricalcola tutto ordine... anche storico immagine in shop
			});
		}
	});
	
	function loadDependantSelectVarianti(prod_code, variante_id) {
		//console.log('p ' + prod_id + ' v ' + variante_id);
		$( ".loader" ).remove();
		$('#field-id_variante').prop('disabled', true).trigger("chosen:updated");
		$('#field-id_variante').next().after('&nbsp;<img style="width:15px !important;" src="<? echo ASSETS_ROOT_FOLDER_ADMIN_IMG; ?>/field_loader.gif" class="loader" />');
		if(prod_code) {
			$.ajax({
				url: '<? echo base_url(); ?>admin/Orders/loadVariantiAjax/'+prod_code,
				type: "GET",
				dataType: "json",
				success:function(data) {
					$('#field-id_variante').empty();
					$('#field-id_variante').append('<option value=""></option>');
					$.each(data, function(key, value) {
						$('#field-id_variante').append('<option value="'+ value.id_variante +'" data-prezzo="'+value.prezzo+'" data-prezzoscontato="'+value.prezzo_scontato+'" data-colore="'+value.colore+'" data-taglia="'+value.taglia+'" data-img="'+value.url_img_grande+'">'+ value.codice + ' - ' + value.nome + '</option>');
					});
					if(data.length > 0) {	
						$('#field-id_variante').prop('disabled', false).trigger("chosen:updated");
						if(variante_id != null && variante_id != '' && variante_id > 0)
							$('#field-id_variante').val(variante_id).trigger("chosen:updated");
						else 
							$('#field-id_variante').val('').trigger('chosen:updated');		
					} else {	
						$('#field-id_variante').prop('disabled', true).trigger("chosen:updated");
					}
					$('.loader').hide();
				}
			});
		}else{
			$('#field-id_variante').empty();
			$('#field-id_variante').val('').trigger('chosen:updated');	
			$('#field-id_variante').prop('disabled', true).trigger("chosen:updated");
		}
	}
</script>
</html>