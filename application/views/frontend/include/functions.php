<?
/* generare un hash da mettere nel cookie per identificare l'utente agli accessi succesivi
dallo stesso dispositivo */
function makeUnique($md5) {
 $start_time = uniqid(microtime(1));
 $duration = sprintf('%0.24f', $start_time);
 if($md5)
 	return md5(str_replace('.', '', date('dmYHis').$duration));
 else
 	return str_replace('.', '', date('dmYHis').$duration);
}

?>