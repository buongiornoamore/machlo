<!-- Item -->
<div class="col-md-4 col-sm-6">
  <div class="shop-item">
    <div class="shop-thumbnail">
      <span class="shop-label text-danger">Offerta</span>
      <a href="shop-single.html" class="item-link"></a>
      <img src="<? echo ASSETS_ROOT_FOLDER_FRONTEND_IMG; ?>/shop/ins_001.jpg" alt="Filetti pomodoro al naturale">
      <div class="shop-item-tools">
        <a href="#" class="add-to-whishlist" data-toggle="tooltip" data-placement="top" title="Aggiungi alla Whishlist">
          <i class="material-icons favorite_border"></i>
        </a>
        <a href="#" class="add-to-cart">
          <em><i class="material-icons add"></i> Aggiungi</em>
          <svg x="0px" y="0px" width="32px" height="32px" viewBox="0 0 32 32">
            <path stroke-dasharray="19.79 19.79" stroke-dashoffset="19.79" fill="none" stroke="#FFFFFF" stroke-width="2" stroke-linecap="square" stroke-miterlimit="10" d="M9,17l3.9,3.9c0.1,0.1,0.2,0.1,0.3,0L23,11"/>
          </svg>
        </a>
      </div>
    </div>
    <div class="shop-item-details">
      <h3 class="shop-item-title"><a href="shop-single.html">Filetti pomodoro</a></h3>
      <span class="shop-item-price">
        <span class="old-price">€5,00</span>
        €3,00
      </span>
    </div>
  </div><!-- .shop-item -->
</div><!-- .col-md-4.col-sm-6 -->
<!-- Item -->
<div class="col-md-4 col-sm-6">
  <div class="shop-item">
    <div class="shop-thumbnail">
      <span class="item-rating text-warning">
        <i class="material-icons star"></i>
        <i class="material-icons star"></i>
        <i class="material-icons star"></i>
        <i class="material-icons star_half"></i>
        <i class="material-icons star_border"></i>
      </span>
      <a href="shop-single.html" class="item-link"></a>
      <img src="<? echo ASSETS_ROOT_FOLDER_FRONTEND_IMG; ?>/shop/faella_023.jpg" alt="Paccheri di Gragnano">
      <div class="shop-item-tools">
        <a href="#" class="add-to-whishlist" data-toggle="tooltip" data-placement="top" title="Aggiungi alla Whishlist">
          <i class="material-icons favorite_border"></i>
        </a>
        <a href="#" class="add-to-cart">
          <em><i class="material-icons add"></i> Aggiungi</em>
          <svg x="0px" y="0px" width="32px" height="32px" viewBox="0 0 32 32">
            <path stroke-dasharray="19.79 19.79" stroke-dashoffset="19.79" fill="none" stroke="#FFFFFF" stroke-width="2" stroke-linecap="square" stroke-miterlimit="10" d="M9,17l3.9,3.9c0.1,0.1,0.2,0.1,0.3,0L23,11"/>
          </svg>
        </a>
      </div>
    </div>
    <div class="shop-item-details">
      <h3 class="shop-item-title"><a href="shop-single.html">Paccheri di Gragnano</a></h3>
      <span class="shop-item-price">
        €0,90
      </span>
    </div>
  </div><!-- .shop-item -->
</div><!-- .col-md-4.col-sm-6 -->
<!-- Item -->
<div class="col-md-4 col-sm-6">
  <div class="shop-item">
    <div class="shop-thumbnail">
     <!-- <span class="shop-label text-danger">Offerta</span>-->
      <a href="shop-single.html" class="item-link"></a>
      <img src="<? echo ASSETS_ROOT_FOLDER_FRONTEND_IMG; ?>/shop/ins_002.jpg" alt="Pomodorini gialli">
      <div class="shop-item-tools">
        <a href="#" class="add-to-whishlist" data-toggle="tooltip" data-placement="top" title="Aggiungi alla Whishlist">
          <i class="material-icons favorite_border"></i>
        </a>
        <a href="#" class="add-to-cart">
          <em><i class="material-icons add"></i> Aggiungi</em>
          <svg x="0px" y="0px" width="32px" height="32px" viewBox="0 0 32 32">
            <path stroke-dasharray="19.79 19.79" stroke-dashoffset="19.79" fill="none" stroke="#FFFFFF" stroke-width="2" stroke-linecap="square" stroke-miterlimit="10" d="M9,17l3.9,3.9c0.1,0.1,0.2,0.1,0.3,0L23,11"/>
          </svg>
        </a>
      </div>
    </div>
    <div class="shop-item-details">
      <h3 class="shop-item-title"><a href="shop-single.html">Pomodorini gialli</a></h3>
      <span class="shop-item-price">
        <!--<span class="old-price">€5,00</span>-->
        €3,00
      </span>
    </div>
  </div><!-- .shop-item -->
</div><!-- .col-md-4.col-sm-6 -->
<!-- Item -->
<div class="col-md-4 col-sm-6">
  <div class="shop-item">
    <div class="shop-thumbnail">
      <span class="item-rating text-warning">
        <i class="material-icons star"></i>
        <i class="material-icons star"></i>
        <i class="material-icons star"></i>
        <i class="material-icons star"></i>
        <i class="material-icons star_border"></i>
      </span>
      <a href="shop-single.html" class="item-link"></a>
      <img src="<? echo ASSETS_ROOT_FOLDER_FRONTEND_IMG; ?>/shop/faella_024.jpg" alt="Rigatoni 1kg">
      <div class="shop-item-tools">
        <a href="#" class="add-to-whishlist" data-toggle="tooltip" data-placement="top" title="Aggiungi alla Whishlist">
          <i class="material-icons favorite_border"></i>
        </a>
        <a href="#" class="add-to-cart">
          <em><i class="material-icons add"></i> Aggiungi</em>
          <svg x="0px" y="0px" width="32px" height="32px" viewBox="0 0 32 32">
            <path stroke-dasharray="19.79 19.79" stroke-dashoffset="19.79" fill="none" stroke="#FFFFFF" stroke-width="2" stroke-linecap="square" stroke-miterlimit="10" d="M9,17l3.9,3.9c0.1,0.1,0.2,0.1,0.3,0L23,11"/>
          </svg>
        </a>
      </div>
    </div>
    <div class="shop-item-details">
      <h3 class="shop-item-title"><a href="shop-single.html">Rigatoni 1kg</a></h3>
      <span class="shop-item-price">
        €2,50
      </span>
    </div>
  </div><!-- .shop-item -->
</div><!-- .col-md-4.col-sm-6 -->
<!-- Item -->
<div class="col-md-4 col-sm-6">
  <div class="shop-item">
    <div class="shop-thumbnail">
      <span class="shop-label text-warning">Più venduto</span>
      <a href="shop-single.html" class="item-link"></a>
      <img src="<? echo ASSETS_ROOT_FOLDER_FRONTEND_IMG; ?>/shop/ins_003.jpg" alt="Friarielli napoletani">
      <div class="shop-item-tools">
        <a href="#" class="add-to-whishlist" data-toggle="tooltip" data-placement="top" title="Aggiungi alla Whishlist">
          <i class="material-icons favorite_border"></i>
        </a>
        <a href="#" class="add-to-cart">
          <em><i class="material-icons add"></i> Aggiungi</em>
          <svg x="0px" y="0px" width="32px" height="32px" viewBox="0 0 32 32">
            <path stroke-dasharray="19.79 19.79" stroke-dashoffset="19.79" fill="none" stroke="#FFFFFF" stroke-width="2" stroke-linecap="square" stroke-miterlimit="10" d="M9,17l3.9,3.9c0.1,0.1,0.2,0.1,0.3,0L23,11"/>
          </svg>
        </a>
      </div>
    </div>
    <div class="shop-item-details">
      <h3 class="shop-item-title"><a href="shop-single.html">Friarielli</a></h3>
      <span class="shop-item-price">
        <!--<span class="old-price">€5,00</span>-->
        €5,80
      </span>
    </div>
  </div><!-- .shop-item -->
</div><!-- .col-md-4.col-sm-6 -->
<!-- Item -->
<div class="col-md-4 col-sm-6">
  <div class="shop-item">
    <div class="shop-thumbnail">
      <a href="shop-single.html" class="item-link"></a>
      <img src="<? echo ASSETS_ROOT_FOLDER_FRONTEND_IMG; ?>/shop/faella_025.jpg" alt="Caserecce 1kg">
      <div class="shop-item-tools">
        <a href="#" class="add-to-whishlist" data-toggle="tooltip" data-placement="top" title="Aggiungi alla Whishlist">
          <i class="material-icons favorite_border"></i>
        </a>
        <a href="#" class="add-to-cart">
          <em><i class="material-icons add"></i> Aggiungi</em>
          <svg x="0px" y="0px" width="32px" height="32px" viewBox="0 0 32 32">
            <path stroke-dasharray="19.79 19.79" stroke-dashoffset="19.79" fill="none" stroke="#FFFFFF" stroke-width="2" stroke-linecap="square" stroke-miterlimit="10" d="M9,17l3.9,3.9c0.1,0.1,0.2,0.1,0.3,0L23,11"/>
          </svg>
        </a>
      </div>
    </div>
    <div class="shop-item-details">
      <h3 class="shop-item-title"><a href="shop-single.html">Caserecce 1kg</a></h3>
      <span class="shop-item-price">
        €2,50
      </span>
    </div>
  </div><!-- .shop-item -->
</div><!-- .col-md-4.col-sm-6 -->