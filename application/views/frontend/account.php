<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Account<? echo ' | ' . SITE_TITLE_NAME; ?></title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
  <? require_once('include/common_header_css.php'); ?> <!-- Import css -->
</head>
<body class="page-preloading">
  <? require_once('include/common_preloader.php'); ?> <!-- Page Pre-Loader -->
  <!-- Page Wrapper -->
  <div class="page-wrapper">
    <? require_once('include/header_navbar.php'); ?> <!-- Header Navbar and Menu -->
    <!-- Content -->
    <section class="container padding-top-3x" style="min-height:700px">
      <h1 class="mobile-center"><? echo $this->ion_auth->user()->row()->first_name; ?>, <span class="text-semibold"><? echo $this->ion_auth->user()->row()->last_name; ?></span></h1>
      <div class="row">
        <div class="col-sm-9 padding-bottom-2x">
          <!-- Nav Tabs -->
          <ul class="nav-tabs mobile-center profile-tabs" role="tablist">
            <li class="active">
            	<a href="#profile" role="tab" data-toggle="tab">
              		<i class="material-icons person"></i>
              		<? echo lang("LABEL_PROFILE"); ?>
            	</a>
            </li>
            <li>
            	<a href="#addresses" role="tab" data-toggle="tab">
              		<i class="material-icons local_shipping"></i>
              		<? echo lang("LABEL_ADDRESSES"); ?> (<span id="indirizzi-count-span">0</span>)
            	</a>
            </li>
            <li>
            	<a href="#orders" role="tab" data-toggle="tab">
              		<i class="material-icons shopping_cart"></i>
              		<? echo lang("LABEL_ORDERS"); ?> (<?php echo count($ordini) ?>)
            	</a>
            </li>
        	<li>
            	<a href="#wishlist" role="tab" data-toggle="tab">
              		<i class="material-icons favorite"></i>
              		<? echo lang("LABEL_WHISHLIST"); ?> (<span id="wishlist-count-span">0</span>)
            	</a>
            </li>
          </ul><!-- .nav-tabs -->
          <!-- Tab Panes -->
          <div class="tab-content">
            <div role="tabpanel" class="tab-pane transition fade scale in active" id="profile">
              <form method="post"  action="<? echo site_url('it/salva_account');?>" accept-charset="utf-8">
                <div class="row">
                  <div class="col-sm-6">
                    <div class="form-element">
                    	 <label for="first_name"><? echo lang("LABEL_NAME"); ?></label>	
 	                     <input type="text" id="first_name" name="first_name" class="form-control" value="<? echo $this->ion_auth->user()->row()->first_name; ?>" placeholder="<? echo lang("LABEL_NAME"); ?>" >
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-element">
                    	<label for="last_name"><? echo lang("LABEL_SURNAME"); ?></label>	
                   	    <input type="text" id="last_name" name="last_name" class="form-control" value="<? echo $this->ion_auth->user()->row()->last_name; ?>"  placeholder="<? echo lang("LABEL_SURNAME"); ?>" >
                    </div>
                  </div>
                </div>

                <div class="row">
                  <div class="col-sm-6">
                    <div class="form-element">
                    	<label for="codice_fiscale"><? echo lang("create_user_cod_fiscale_label"); ?></label>
                     	<input type="text" id="codice_fiscale" name="codice_fiscale" class="form-control" value="<? echo $cliente->codice_fiscale; ?>" placeholder="<? echo lang("create_user_cod_fiscale_label"); ?>">
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-element">
                    	<label for="partita_iva"><? echo lang("create_user_partita_iva_label"); ?></label>
	                    <input type="text" id="partita_iva" name="partita_iva" class="form-control" value="<? echo $cliente->partita_iva; ?>" placeholder="<? echo lang("create_user_partita_iva_label"); ?>" >

                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-6">
                    <div class="form-element">
                    	<label for="email"><? echo lang("LABEL_EMAIL"); ?></label>
                        <input type="email" id="email" name="email"class="form-control" value="<? echo $this->ion_auth->user()->row()->email; ?>" placeholder="<? echo lang("LABEL_EMAIL"); ?>" readonly >
                    </div>
                  </div>
                  <div class="col-sm-6">
                    <div class="form-element">
                    	<label for="telefono"><? echo lang("LABEL_PHONE"); ?></label>
                        <input type="text" id="telefono" name="telefono" class="form-control" value="<? echo $cliente->telefono; ?>" placeholder="<? echo lang("LABEL_PHONE"); ?>" required>
                    </div>
                  </div>
                </div>
                <div class="row">
                    <div class="form-element col-sm-9">
                    	<label for="indirizzo"><? echo lang("LABEL_BILLING_ADDRESS"); ?></label>
                      	<input type="text" id="indirizzo" name="indirizzo" class="form-control" value="<? echo $cliente->indirizzo_fatt; ?>"  placeholder="<? echo lang("LABEL_BILLING_ADDRESS"); ?>" required />
                    </div>
                    <div class="form-element col-sm-3">
                    	<label for="civico"><? echo lang("LABEL_CIVICO"); ?></label>
                    	<input type="text" id="civico" name="civico" class="form-control" value="<? echo $cliente->civico_fatt; ?>" placeholder="<? echo lang("LABEL_CIVICO"); ?>" >
                     </div>
                </div>
                <div class="row">
	                <div class="col-sm-3">
		            	<div class="form-element">
                          	<label for="citta"><? echo lang("LABEL_CITY"); ?></label>  	
		                    <input type="text" id="citta"  name="citta" class="form-control" value="<? echo $cliente->citta_fatt; ?>" placeholder="<? echo lang("LABEL_CITY"); ?>" required />
	                    </div>
	                 </div>
	                  <div class="col-sm-3">
		                <div class="form-element">
                        	<label for="citta"><? echo lang("LABEL_POSTAL_CODE"); ?></label>  
		                	<input type="text" id="cap"  name="cap" class="form-control" value="<? echo $cliente->cap_fatt; ?>" placeholder="<? echo lang("LABEL_POSTAL_CODE"); ?>" required />
		                </div>
	                  </div>
	                 <div class="col-sm-6">
                     	<div class="form-element form-select form-select-withlabel">
                        	<label for="nazione"><? echo lang("LABEL_COUNTRY"); ?></label>  
                            <select name="nazione" class="form-control" id="nazione" required>
                                <option value=""><? echo lang("LABEL_COUNTRY"); ?></option>
                                <?
                                // carica tutti gli indirizzi escluso quello di default
                                foreach ($countries as $country) {
                                    echo '<option value="'.$country->country_code.'" '.($country->country_code == $cliente->nazione_fatt ? 'selected' : '').'>'.$country->country_name.'</option>';
                                }
                                ?>
                            </select>
						</div>
	                </div>
                </div>
                <div class="row">
                  <div class="col-sm-12 mobile-center" >
	                <div class="form-element">
                    	<label for="riferimento"><? echo lang("LABEL_ADDRESS_REF_FATT"); ?></label>  
	                    <input type="text" id="riferimento" name="riferimento" class="form-control" value="<? echo $cliente->riferimento_fatt; ?>" placeholder="<? echo lang("LABEL_ADDRESS_REF_FATT"); ?>" >
	                </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-12 mobile-center">
                  	<label for="note"><? echo lang("LABEL_ADDRESS_NOTES"); ?></label>  
                    <input type="text" id="note" name="note" class="form-control" value="<? echo $cliente->note_fatt; ?>" placeholder="<? echo lang("LABEL_ADDRESS_NOTES"); ?>" >
                  </div>
                </div>
                <div class="row">
                  <div class="col-sm-6">
                    <label class="checkbox space-top">
                      <input type="checkbox"  id="newsletter" name="newsletter" <? echo $cliente->newsletter == 0 ? '' : 'checked'; ?> > <? echo lang("MSG_SAVE_NEWSLETTER"); ?>
                    </label>
                  </div>
                  <div class="col-sm-6 text-right mobile-center">
                    <button type="submit" class="btn btn-primary waves-effect waves-light"><? echo lang("LABEL_UPDATE"); ?></button>
                  </div>
                </div>
              </form>
            </div><!-- .tab-pane#profile -->
            <div role="tabpanel" class="tab-pane transition fade scale" id="orders">
              <div>
              <?php
              if(count($ordini) > 0) { 
			  ?>	   
                <table class="table-responsive table-striped" >
                  <thead>
                    <tr>
                      <th style="text-align:center;">Ordine #</th>
                      <th>Data</th>
                      <th>Stato Pagamento</th>
                      <th>Stato Ordine</th>
                      <th>Totale</th>
                    </tr>
                  </thead>
                  <tbody>
				<?php 
					foreach($ordini as $row) {
				?>
                    <tr>
	                    <td>
							<button type="button" class="btn btn-primary btn_id_ordine" name="btn_id_ordine" style="height: 20px;line-height:10px; margin:0;width:100%;" data-id="<?php echo $row->id_ordine; ?>">
							  <?php echo $row->id_ordine; ?>
							</button>
						</td align="center">
                        <td><?php echo formattaData($row->data_ordine, 'd/m/Y'); ?></td>
                        <td><span class="<?php echo (isset($row->class_stato_pagamento) ? $row->class_stato_pagamento : ''); ?> no-border"><?php echo $row->desc_stato_pagamento ?></span></td>
                        <td><span class="<?php echo (isset($row->class_stato_ordine) ? $row->class_stato_ordine : ''); ?> no-border"><?php echo $row->desc_stato_ordine; ?></span></td>
                        <td><?php echo stampaValutaHtml($row->totale_ordine, true, true); ?></td>
                    </tr>	
				<?php
					}
				?>
                  </tbody>
                </table>
                <?php
				} else {
					echo '<div class="col-sm-12" align="center"><b>'.lang('MSG_NO_ELEMENTS').'</b></div>';
				}
				?>
              </div>
            </div><!-- .tab-pane#orders -->
			<div role="tabpanel" class="tab-pane transition fade scale" id="addresses" >
            	<div align="center" class="row">
                    <div id="addresses-table-div"> 
                        <table class="table-responsive table-striped" >
                          <thead>
                            <tr>
                              <th><?php echo lang('LABEL_ADDRESSES');?></th>
                              <th><?php echo lang('LABEL_CIVICO');?></th>
                              <th><?php echo lang('LABEL_POSTAL_CODE');?></th>
                              <th><?php echo lang('LABEL_CITY');?></th>
                              <th><?php echo lang('LABEL_COUNTRY');?></th>
                              <th><?php echo lang('LABEL_REFCODE');?></th>
                              <th></th>
                            </tr>
                           </thead>
                           <tbody id="addresses-list-div">
                           </tbody>
                          </table>
                    </div>
                    <div id="addresses-table-div-msg" class="col-sm-12" align="center"><b><? echo lang('MSG_NO_ELEMENTS'); ?></b></div>
                </div>    
			</div><!-- .tab-pane#addresses -->
            <div role="tabpanel" class="tab-pane transition fade scale" id="wishlist" >
            	<div class="row" id="wishlist-row">
                </div>
                <div id="wishlist-row-msg" class="col-sm-12" align="center"><b><? echo lang('MSG_NO_ELEMENTS'); ?></b></div>
			</div><!-- .tab-pane#wishlist -->
          </div><!-- .tab-content -->
        </div><!-- .col-sm-8 -->

        <!-- Sidebar -->
        <div class="col-sm-3 padding-bottom-2x">
          <aside class="mobile-center">
            <? if(POINTS_ENABLED > 0) { ?>
            <!-- POINTS -->
            <h3><span class="h5"><? echo lang("LABEL_GOT"); ?></span> <span class="text-semibold"><? echo $cliente->punti; ?></span> <span class="h5"><? echo lang("LABEL_POINTS"); ?></span></h3>
            <p class="text-sm text-gray"><? echo lang("LABEL_POINTS_DESC"); ?></p>
          <!--  
          	@TODO gestione punti da parte del cliente
            <a href="points" class="btn btn-default btn-block icon-left btn-block">
                <i class="material-icons receipt"></i>
           		<?// echo lang("LABEL_MANAGE_POINTS"); ?>
            </a> -->
            <?
            	} else { 
			 		echo '<br><br><br>';
				}
			?>	
            
			<button type="button" id="aggiungi_indirizzo_btn" class="btn btn-ghost btn-block space-top-none"><i class="material-icons add_box"></i> <? echo lang('LABEL_NEW_ADDRESS'); ?></button>
                                       
            <a href="<? echo site_url(lang('PAGE_SHOP_URL')); ?>" class="btn btn-ghost btn-block space-top-none">
            	<i class="material-icons shopping_basket"></i>
              	<? echo lang("LABEL_BACK_SHOP"); ?>
            </a>
            <a href="<? echo site_url(lang('PAGE_LOGOUT_URL'));?>" class="btn btn-primary btn-block icon-left btn-block">
            	<i class="material-icons arrow_back"></i>
            	<? echo lang("LABEL_LOGOUT"); ?>
            </a>
          </aside>
        </div><!-- .col-sm-4 -->
      </div><!-- .row -->
    </section><!-- .container -->
    <!-- PAGE CONTENT -->
    <? require_once('include/footer.php'); ?> <!-- Footer -->
  </div><!-- .page-wrapper -->
  
  	<!-- START MODAL DETTAGLIO -->
    <div id="modale-dettaglio-ordine" class="modal fade" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg">
        <!-- Modal content modal-lg-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title" align="center" style="font-weight:800"><? echo lang('LABEL_DETAIL'); ?> #<span id="order-id-span"></span></h4>
          </div>
          <div class="modal-body" id="popup_dettaglio_ordine">
            
          </div>
          <div class="modal-footer text-center" align="center">
            <button type="button" class="btn btn-default waves-effect waves-light margin-zero" data-dismiss="modal" onClick="cleanIndirizzoSpedizioneForm();"><i class="material-icons close"></i></button>
          </div>
        </div>
      </div>
    </div>  
    <!-- END MODAL DETTAGLIO -->
            
    <!-- Modal shipModal -->
    <div id="shipModal" class="modal fade" role="dialog">
      <div class="modal-dialog">
        <!-- Modal content modal-lg-->
        <div class="modal-content">
          <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title" align="center" style="font-weight:800"><? echo lang('LABEL_SHIPPING_ADDRESS'); ?></h4>
          </div>
          <div class="modal-body">
          	<form method="post" action="<? echo site_url('salvaIndirizzoSpedizione');?>" accept-charset="utf-8" id="indirizzo-spedizione-form">
                <div class="row">
                    <div class="col-sm-9">
                      <input type="hidden" id="id_indirizzo_sped" name="id_indirizzo_sped" class="form-control" value="" />
                      <label for="indirizzo_sped"><? echo lang("LABEL_ADDRESS"); ?></label>
                      <input type="text" id="indirizzo_sped" name="indirizzo_sped" class="form-control" value="" placeholder="<? echo lang("create_user_address_label"); ?>" required />
                    </div>
                    <div class="col-sm-3">
                      <label for="civico_sped"><? echo lang("LABEL_CIVICO"); ?></label>	
                      <input type="text" id="civico_sped" name="civico_sped" class="form-control" value="" placeholder="<? echo lang("create_user_street_number_label"); ?>" required /><br/>
                    </div>
                </div>
                <div class="row">
                     <div class="col-sm-5">
                     	 <label for="citta_sped"><? echo lang("LABEL_CITY"); ?></label>
                         <input type="text" id="citta_sped" name="citta_sped" class="form-control" value="" placeholder="<? echo lang("create_user_city_label"); ?>" required />
                     </div>
                     <div class="col-sm-3">
                     	<label for="cap_sped"><? echo lang("LABEL_POSTAL_CODE"); ?></label>
                        <input type="text" id="cap_sped" name="cap_sped" class="form-control" value="" placeholder="<? echo lang("create_user_cap_label"); ?>" required />
                     </div>
                     <div class="col-sm-4">
                        <div class="form-element form-select form-select-withlabel">
                        	<label for="nazione_sped"><? echo lang("LABEL_COUNTRY"); ?></label>
                            <select name="nazione_sped" class="form-control" id="nazione_sped" required>
                                <option value=""><? echo lang("LABEL_COUNTRY"); ?></option>
                                <?
                                // carica tutti gli indirizzi escluso quello di default
                                foreach ($countries as $country) {
                                    //if($ind->flag_predefinito_sped == 0)
                                    echo '<option value="'.$country->country_code.'">'.$country->country_name.'</option>';
                                }
                                ?>
                            </select>
                        </div>
                     </div>
                </div>
                <div class="row">
                    <div class="col-sm-12">
                    	<label for="riferimento_sped"><? echo lang("LABEL_ADDRESS_REF"); ?></label>
                        <input type="text" id="riferimento_sped" name="riferimento_sped" class="form-control" value="" placeholder="<? echo lang("LABEL_ADDRESS_REF"); ?>" required />
                    </div>
                </div> 
                <div class="row">
                    <div class="col-sm-12">
                    	<label class="checkbox">
                        	<input type="checkbox" id="predefinito_sped" name="predefinito_sped"> <? echo lang('LABEL_DEFAULT_ADDRESS'); ?>
                    	</label>
                    </div>
                </div> 
          </form>
          </div>
          <div class="modal-footer text-center" align="center">
            <button type="button" id="aggiungi_indirizzo_salva" class="btn btn-primary waves-effect waves-light margin-zero"><? echo lang('LABEL_UPDATE'); ?></button>
            <button type="button" class="btn btn-default waves-effect waves-light margin-zero" data-dismiss="modal" onClick="cleanIndirizzoSpedizioneForm();"><? echo lang('LABEL_CANCEL'); ?></button>
          </div>
        </div>
      </div>
    </div>  
  
  <? require_once('include/common_header_js.php'); ?> <!-- Import js -->
</body><!-- <body> -->
<script type="text/javascript">
  	$(window).load(function() {
  		//console.log("Siamo nella pagina di account");
		loadCartDropdown(true, false, false);
		loadAddresses();
		loadWishlist();
    });
	var activeTab = '#profile';
    $(document).ready(function() {	 	
		//nascondi pulsante aggiungi indirizzo
		$('#aggiungi_indirizzo_btn').hide();
		
		$('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
		  	var target = $(e.target).attr("href") // activated tab
			activeTab = target;
		  	if(target == '#addresses') {
				var addressesCount = $('#addresses-list-div tr').length;				
				if(addressesCount > 4) {
					$("#aggiungi_indirizzo_btn").hide();
				} else {
					$("#aggiungi_indirizzo_btn").show();
				}
			} else {
				$("#aggiungi_indirizzo_btn").hide();
			}	
		});

		$("#aggiungi_indirizzo_btn").click(function(){     
		 $('#shipModal').modal('show');
		});
	 
     	$(".btn_id_ordine").click(function(event){	
			event.preventDefault();    
			$(".se-pre-con").show();
			var id_ordine =  $(this).data('id'); 	 
			$.ajax({  
				  type: 'POST',
				  url: '<? echo base_url();?>frontend/Account/dettaglio_ordine',  
				  data: 'id_ordine=' + id_ordine,
				  dataType: 'html',
				  async: true,
				  success: function(risposta) {  
					$(".se-pre-con").delay(200).fadeOut("slow"); 
					$("#popup_dettaglio_ordine").html(risposta);  
					$('#modale-dettaglio-ordine').modal('show');
					$("#order-id-span").html(id_ordine);  	
				  },
				  error: function(){
					swal({
					  position: 'center',
					  type: 'error',
					  title: "<?php echo lang('MSG_PAYPAL_ERROR'); ?>",
					  showConfirmButton: false,
					  timer: 3000
					});
					return msg;
				  } 
			 }); 
   		});

     	$(".btn_modify_indirizzo_sped").click(function(){
		   	 console.log("Siamo in fase di modifica dell'indirizzo " + $(this).val() );
		   	 var id_indirizzo_sped = $(this).val(); 
		
		   	 var res = id_indirizzo_sped.split(",");
		   	 var riga = res[1];

		   	 $('#id_indirizzo_sped').val(res[0]);
		   	 $('#indirizzo_sped').val(indirizzi_sped[riga][0]);
		   	 $('#civico_sped').val(indirizzi_sped[riga][1]);
		   	 $('#cap_sped').val(indirizzi_sped[riga][2]);
		   	 $('#citta_sped').val(indirizzi_sped[riga][3]);
		   	 $('#nazione_sped').val(indirizzi_sped[riga][4]);
		   	 $('#riferimento_sped').val(indirizzi_sped[riga][5]);
		   	 
		     $("#button_aggiungi_indirizzo").hide();
		     $("#div_inserisci_indirizzo").show();
		
    	});
		
		$('#aggiungi_indirizzo_salva').click(function(event){	
			event.preventDefault();   
			// verifica form 
			if(validateIndirizzoSpedizioneForm())
				salvaIndirizzoSpedizione();
		});
	
		// start vaidation for indirizzo-spedizione-form
		$("#indirizzo_sped").on("keyup blur", function(){validateText('indirizzo_sped');});
		$("#civico_sped").on("keyup blur", function(){validateText('civico_sped');});
		$("#cap_sped").on("keyup blur", function(){validateText('cap_sped');});
		$("#citta_sped").on("keyup blur", function(){validateText('citta_sped');});
		$("#nazione_sped").on("change blur", function(){validateSelectText('nazione_sped', '');});
		$("#riferimento_sped").on("keyup blur", function(){validateText('riferimento_sped');});

		/*
		if( '1' == '< ? echo isset($fase_registrazione); ?>< ? echo ( isset($fase_registrazione) ? $fase_registrazione : ''); ?>' ){
			swal({
				  position: 'center',
				  type: 'success',
				  title: "Grazie per esserti registrato completa le informazioni del tuo profilo.",
				  showConfirmButton: false,
				  timer: 6000
			});
		}*/
		
    });
	
	function loadAddresses(){
		//$(".se-pre-con").show();
		console.log('loadAddresses');
		return $.ajax({
			url: '<? echo base_url();?>frontend/Account/loadAddresses',
			type: 'POST',
			dataType: "HTML",
			async: true,
			data: {},
			error: function(msg){
			//	$(".se-pre-con").delay(200).fadeOut("slow"); 
				swal({
				  position: 'center',
				  type: 'error',
				  title: "<?php echo lang('MSG_PAYPAL_ERROR'); ?>",
				  showConfirmButton: false,
				  timer: 3000
				});
				return msg;
			},
			success: function(html){
				//$(".se-pre-con").delay(200).fadeOut("slow"); 
				$("#addresses-list-div").html(html);	
				var addressesCount = $('#addresses-list-div tr').length;
				$("#indirizzi-count-span").html(addressesCount);
				
				// nascondi tabella
				if(addressesCount == 0){
					$("#addresses-table-div").hide();
					$("#addresses-table-div-msg").show();
				} else {
					$("#addresses-table-div").show();
					$("#addresses-table-div-msg").hide();
				}
				
				if(addressesCount > 4 || activeTab != '#addresses') {
					$("#aggiungi_indirizzo_btn").hide();
				} else {
					$("#aggiungi_indirizzo_btn").show();
				}
				
				// REMOVE ADDRESS ACTION
				$('.remove_address').click(function(event){	
					event.preventDefault();   
					console.log('remove ' + $(this).data('value'));
					deleteAddress($(this).data('value'));
				});
				
				// EDIT ADDRESS ACTION
				$('.edit_address').click(function(event){	
					event.preventDefault();   
					console.log('edit ' + $(this).data('value'));
					$('#id_indirizzo_sped').val($(this).data('value'));
					$('.se-pre-con').show();
					$.ajax({
						url: '<? echo base_url();?>frontend/Account/loadAddressJson/'+$(this).data('value'),
						type: 'GET',
						dataType: "JSON",
						async: true,
						cache: false,
						success: function(response) {
							$('#indirizzo_sped').val(response.indirizzo_sped);
							$('#civico_sped').val(response.civico_sped);
							$('#cap_sped').val(response.cap_sped);
							$('#citta_sped').val(response.citta_sped);
							$('#nazione_sped').val(response.nazione_sped);
							$('#riferimento_sped').val(response.riferimento_sped);
							if(response.flag_predefinito_sped > 0) {
								$('.icheckbox').addClass('checked');
								$('#predefinito_sped').prop('checked', true); // check it*/
							} else {
								$('.icheckbox').removeClass('checked');
								$('#predefinito_sped').prop('checked', false); // Unchecks it*/
							}
							$(".se-pre-con").delay(200).fadeOut("slow"); 
							$('#shipModal').modal('show');
						},
						error: function () {
							$(".se-pre-con").delay(200).fadeOut("slow"); 
							swal({
							  position: 'center',
							  type: 'error',
							  title: "<?php echo lang('MSG_SERVICE_FAILURE'); ?>",
							  showConfirmButton: false,
							  timer: 3000
							});
						}
					});
				});
					
				return true;
			}
		});
	} 
	
	function deleteAddress(id_indirizzo) {
		swal({
		  title: 'Sei sicuro?',
		  text: "Vuoi eliminare definitivamente l'indirizzo selezionato ?",
		  type: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Si, procedi!',
		  cancelButtonText: 'No, fermati'
		}).then(function () {
			$('.se-pre-con').show();
			$.ajax({
				url: '<? echo base_url();?>frontend/Account/deleteAdrress/'+id_indirizzo,
				type: 'GET',
				dataType: "JSON",
				async: true,
				cache: false,
				success: function(response) {
					loadAddresses();
					$(".se-pre-con").delay(200).fadeOut("slow"); 
					swal({
						title: response.title,
						text: response.text,
						type: response.type,
						showConfirmButton: false,
						timer: 3000
					}).catch(swal.noop);
				},
				error: function () {
					$(".se-pre-con").delay(200).fadeOut("slow"); 
					swal({
					  position: 'center',
					  type: 'error',
					  title: "<?php echo lang('MSG_SERVICE_FAILURE'); ?>",
					  showConfirmButton: false,
					  timer: 3000
					});
				}
			});
		}).catch(swal.noop);
	}
		
	function validateIndirizzoSpedizioneForm() {
		if(
			validateText('indirizzo_sped') &
			validateText('civico_sped') &
			validateText('cap_sped') &
			validateText('citta_sped') &
			validateSelectText('nazione_sped', '') &
			validateText('riferimento_sped')
		)
		{
			return true;
		} else {
			return false;
		}
	}
	
	function salvaIndirizzoSpedizione(){
		$(".se-pre-con").show();
		$('#shipModal').modal('toggle');
		var formData = $('#indirizzo-spedizione-form').serializeArray();
	
		return $.ajax({
			url: '<? echo base_url();?>frontend/Account/salvaIndirizzoSpedizione',
			type: 'POST',
			cache: false,
			data: formData,
			dataType: "JSON",
			async: true,
			error: function(response){
				$(".se-pre-con").delay(200).fadeOut("slow"); 
				swal({
				  position: 'center',
				  type: 'error',
				  title: "<?php echo lang('MSG_SERVICE_FAILURE'); ?>",
				  showConfirmButton: false,
				  timer: 3000
				});
				return response;
			},
			success: function(response){
				console.log(response);
				loadAddresses();
				$(".se-pre-con").delay(200).fadeOut("slow"); 
				swal({
					title: response.title,
					text: response.text,
					type: response.type,
					showConfirmButton: false,
					timer: 3000
				}).catch(swal.noop);

				return true;
			}
		});
	}
	
	function cleanIndirizzoSpedizioneForm() {
		$('#id_indirizzo_sped').val('');
		$('#indirizzo_sped').val('');
		$("#indirizzo_sped").removeClass('has-error');
		$('#civico_sped').val('');
		$("#civico_sped").removeClass('has-error');
		$('#cap_sped').val('');
		$("#cap_sped").removeClass('has-error');
		$('#citta_sped').val('');
		$("#citta_sped").removeClass('has-error');
		$('#nazione_sped').val('');
		$("#nazione_sped").removeClass('has-error');
		$('#riferimento_sped').val('');
		$("#riferimento_sped").removeClass('has-error');
		$('.icheckbox').removeClass('checked');
		$('#predefinito_sped').prop('checked', false); // Unchecks it
	}
	
	function loadWishlist(){
		//$(".se-pre-con").show();
		console.log('loadWishlist');
		return $.ajax({
			url: '<? echo base_url();?>frontend/Account/loadWishlist',
			type: 'POST',
			dataType: "HTML",
			async: true,
			data: {},
			error: function(msg){
			//	$(".se-pre-con").delay(200).fadeOut("slow"); 
				swal({
				  position: 'center',
				  type: 'error',
				  title: "<?php echo lang('MSG_PAYPAL_ERROR'); ?>",
				  showConfirmButton: false,
				  timer: 3000
				});
				return msg;
			},
			success: function(html){
				$("#wishlist-row").html(html);	
				$("#wishlist-count-span").html($('.wishlist-item').length);
				
				// nascondi tabella
				if($('.wishlist-item').length == 0){
					$("#wishlist-row").hide();
					$("#wishlist-row-msg").show();
				} else {
					$("#wishlist-row").show();
					$("#wishlist-row-msg").hide();
				}
	
				// REMOVE ACTION
				$('.remove-from-whishlist').click(function(event){	
					event.preventDefault();   
					console.log('remove ' + $(this).data('value'));
					deleteFromWhislist($(this).data('value'));
				});
				
				// ADD ACTION
				$('.add-to-cart').click(function(event){	
					event.preventDefault();   
					console.log('add ' + $(this).data('productid') + ' ' + $(this).data('variantid') + ' ' + $(this).data('size'));
					addToCart($(this).data('productid'), $(this).data('size'), 0, 1, $(this).data('variantid'), $(this).data('wishlistid'));
				});
				
				return true;
			}
		});
	} 
	
	function deleteFromWhislist(id) {
		swal({
		  title: 'Sei sicuro?',
		  text: "Vuoi eliminare definitivamente l'indirizzo selezionato ?",
		  type: 'warning',
		  showCancelButton: true,
		  confirmButtonColor: '#3085d6',
		  cancelButtonColor: '#d33',
		  confirmButtonText: 'Si, procedi!',
		  cancelButtonText: 'No, fermati'
		}).then(function () {
			$('.se-pre-con').show();
			$.ajax({
				url: '<? echo base_url();?>frontend/Account/deleteFromWishlist/'+id,
				type: 'GET',
				dataType: "JSON",
				async: true,
				cache: false,
				success: function(response) {
					loadWishlist();
					$(".se-pre-con").delay(200).fadeOut("slow"); 
					swal({
						title: response.title,
						text: response.text,
						type: response.type,
						showConfirmButton: false,
						timer: 3000
					}).catch(swal.noop);
				},
				error: function () {
					$(".se-pre-con").delay(200).fadeOut("slow"); 
					swal({
					  position: 'center',
					  type: 'error',
					  title: "<?php echo lang('MSG_SERVICE_FAILURE'); ?>",
					  showConfirmButton: false,
					  timer: 3000
					});
				}
			});
		}).catch(swal.noop);
	}
	
	function addToCart(product_id, size, size_id, qty, variant_id, wishlist_id){
		console.log('add to cart prod id: ' + product_id + ' size: ' + size + ' variant_id: ' + variant_id);
		$(".se-pre-con").show();
		return $.ajax({
			url: '<? echo base_url();?>frontend/Cart/addToCart',
			type: 'POST',
			dataType: "HTML",
			async: true,
			data: {"product_id": product_id, "size": size, "size_id": size_id, "qty": qty, "variant_id": variant_id},
			error: function(msg){
				console.log('error');
				swal({
				  position: 'center',
				  type: 'error',
				  title: "<?php echo lang('MSG_SERVICE_FAILURE'); ?>",
				  showConfirmButton: false,
				  timer: 3000
				}).catch(swal.noop);
				return msg;
			},
			success: function(html){
				loadCartDropdown(true, true, false);
				// remove from wishlist
				$.ajax({
					url: '<? echo base_url();?>frontend/Account/deleteFromWishlist/'+wishlist_id,
					type: 'GET',
					dataType: "JSON",
					async: true,
					cache: false,
					success: function(response) {
						$(".se-pre-con").delay(200).fadeOut("slow"); 
					/*	swal({
							title: response.title,
							text: response.text,
							type: response.type,
							showConfirmButton: false,
							timer: 3000
						}).catch(swal.noop);*/
						loadWishlist();
						// messagge
						swal({
						  position: 'center',
						  type: 'success',
						  title: "<?php echo lang('MSG_CART_ADDED'); ?>",
						  showConfirmButton: false,
						  timer: 2000
						}).catch(swal.noop);
					},
					error: function () {
						$(".se-pre-con").delay(200).fadeOut("slow"); 
						swal({
						  position: 'center',
						  type: 'error',
						  title: "<?php echo lang('MSG_SERVICE_FAILURE'); ?>",
						  showConfirmButton: false,
						  timer: 3000
						});
					}
				});
				return true;
			}
		});

	}
  </script>
</html>
