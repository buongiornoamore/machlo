<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title><? echo lang('PAGE_ABOUT_TITLE') . ' | ' . SITE_TITLE_NAME; ?></title>
  <meta name="description" content="<? echo lang('PAGE_ABOUT_META_DESCRIPTION'); ?>" />
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
  <? require_once('include/common_header_css.php'); ?> <!-- Import css -->
</head>
<body class="page-preloading">

  <? require_once('include/common_preloader.php'); ?> <!-- Page Pre-Loader -->

  <!-- Page Wrapper -->
  <div class="page-wrapper">
    <? require_once('include/header_navbar.php'); ?> <!-- Header Navbar and Menu -->
    <!-- Container -->
    <? if(lang('PAGE_ABOUT_IMAGE') != "") { ?>
    <!-- Featured Image -->
    <div class="featured-image" style="background-image: url(<? echo ASSETS_ROOT_FOLDER_FRONTEND_IMG; ?>/featured-image/<? echo lang('PAGE_ABOUT_IMAGE'); ?>);"></div>
    <? } ?> 
    <!-- Content -->
    <section class="container padding-top-3x padding-bottom-3x">
      <h1><? echo lang('PAGE_ABOUT_TITLE'); ?></h1>
      <div class="row padding-top">
        <div class="col-md-12 col-sm-12 padding-bottom">
          <p class="space-top" style="text-align:justify"><? echo lang('PAGE_ABOUT_DESCRIPTION'); ?></p>
        </div><!-- .col-md-5.col-sm-6 -->
      </div><!-- .row -->
    </section><!-- .container -->

    <? require_once('include/footer.php'); ?> <!-- Footer -->

  </div><!-- .page-wrapper -->

  <? require_once('include/common_header_js.php'); ?> <!-- Import js -->

</body><!-- <body> -->
<script type="text/javascript">
	$(window).load(function() {
		loadCartDropdown(true, false, false);
    });
</script>
</html>
