<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Login<? echo ' | ' . SITE_TITLE_NAME; ?></title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
  <? require_once('include/common_header_css.php'); ?> <!-- Import css -->
   <? require_once('include/common_header_js.php'); ?> <!-- Import js -->
</head>
<body class="page-preloading">
  <? require_once('include/common_preloader.php'); ?> <!-- Page Pre-Loader -->
  <!-- Page Wrapper -->
  <div class="page-wrapper">
    <? require_once('include/header_navbar.php'); ?> <!-- Header Navbar and Menu -->
    <!-- Featured Image
    <div class="featured-image" style="background-image: url(< ? echo ASSETS_ROOT_FOLDER_FRONTEND_IMG; ?>/featured-image/faq.jpg);"></div>
     -->
    <!-- Content -->
    <section class="container padding-top-3x padding-bottom-2x">
      <!-- <h1>< ? echo lang('LABEL_USER_ACCOUNT'); ?></h1> -->

      <div class="row padding-top">
        <div class="col-md-5 col-md-offset-1"  id="div-register-form" >
          <div id="infoMessage" class="text-danger"><?php echo $this->session->flashdata('register_message');?></div>
          <h3><?php echo lang('LABEL_CHANGE_PASSWORD');?></h3>
          <!-- <div id="infoMessage" class="text-danger">< ?php echo $this->session->flashdata('message');?></div> -->
          <div class="inner">
            <form method="post" id="register-form" action="<? echo site_url();?>frontend/reset_password/<?php echo isset($code) ? $code : '' ;?>" accept-charset="utf-8">
              <input type="password" id="password" name="password" class="form-control" placeholder="<?php echo lang('create_user_password_label');?>" required>
              <input type="password" id="password_confirm" name="password_confirm" class="form-control" placeholder="<?php echo lang('create_user_password_confirm_label');?>" required>
                   <?php echo form_input($user_id);?>
                   <?php echo form_hidden($csrf); ?>
              <div class="form-footer">
                <div class="rememberme"></div>
                <div class="form-submit">
<!--                  <button type="submit" class="btn btn-primary btn-block waves-effect waves-light">SIGNAP - Submit</button>   -->
                  <button type="submit" class="btn btn-primary btn-block waves-effect waves-light"><?php echo lang('reset_password_submit_btn');?></button>
                </div>
              </div>
            </form><!-- .login-form -->
          </div><!-- .inner -->
        </div><!-- .col-md-4.col-md-offset-1 -->
      </div><!-- .row -->
    </section><!-- .container -->
	<? require_once('include/footer.php'); ?> <!-- Footer -->
  </div><!-- .page-wrapper -->
</body><!-- <body> -->	

</html>
