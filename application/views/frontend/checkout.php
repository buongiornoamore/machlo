<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>Checkout<? echo ' | ' . SITE_TITLE_NAME; ?></title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
  <? require_once('include/common_header_css.php'); ?> <!-- Import css -->
</head>
<body class="page-preloading">
  <? require_once('include/common_preloader.php'); ?> <!-- Page Pre-Loader -->
  <!-- Page Wrapper -->
  <div class="page-wrapper maximized-container">
    <? require_once('include/header_navbar.php'); ?> <!-- Header Navbar and Menu -->
    <!-- Container -->
    <div id="checkout-message-div" class="col-sm-12 padding-bottom" style="display:none;font-size:24px;padding-top:50px;text-align:center;">
    </div>
    <form method="post" class="container padding-top-4x" id="checkout-form">
        <h1 class="space-top-half"><? echo lang("LABEL_CHECKOUT"); ?></h1>
        <div class="row">
            <div class="col-sm-8 padding-bottom"><!-- Modulo -->
            <? if(!$this->ion_auth->logged_in()) { ?>
                <p align="center" style="color:#979797"><? echo lang('LABEL_USER_ALREADY_REGISTERED'); ?></p>
                <div class="row">
                    <div class="col-sm-6">
                        <input type="text" class="form-control" id="identity" name="identity" placeholder="<? echo lang('login_identity_label'); ?>" required />
                    </div>
                    <div class="col-sm-6">
                      <input type="password" class="form-control" id="password_login" name="password_login" placeholder="<? echo lang('login_password_label'); ?>" required />
                    </div>
                 </div>
                 <div class="row">
                    <div class="col-sm-offset-4 col-sm-4">
                        <button type="button" id="btn_accedi" name="btn_accedi" class="btn btn-primary btn-block waves-effect waves-light"><?php echo lang('login_submit_btn');?></button>
                    </div>
                 </div>
                 <br/>
                 <div align="center" style="color:#979797"><? echo lang('LABEL_USER_NOTREGISTERED'); ?></div>    
                 <? if(lang('POINTS_ENABLED') != "0") { ?>	  
                 <div align="center" style="color:#979797;font-size:12px;font-style:italic;color:orange;padding-bottom:20px;"><? echo lang('LABEL_USER_NOTREGISTERED_POINTS'); ?></div> 
                 <? } ?>
            <? } ?>
            <? $readonly_fatt = ($this->ion_auth->logged_in() && ($cliente->id_indirizzo_fatturazione != NULL && $cliente->id_indirizzo_fatturazione > 0) ? 'readonly' : ''); ?>
                <div class="row">
                    <div class="col-sm-6">
                    	<label for="co_f_name"><? echo lang("LABEL_NAME"); ?></label>
                     	<input type="text" class="form-control" name="co_f_name" id="co_f_name" value="<? echo $this->ion_auth->logged_in() ? $this->ion_auth->user()->row()->first_name : ''; ?>" placeholder="<? echo lang("LABEL_NAME"); ?>" <? echo $readonly_fatt; ?>>
                    </div>
                    <div class="col-sm-6"> 
                    	<label for="co_l_name"><? echo lang("LABEL_SURNAME"); ?></label>
                        <input type="text" class="form-control" name="co_l_name" id="co_l_name" value="<? echo $this->ion_auth->logged_in() ? $this->ion_auth->user()->row()->last_name : ''; ?>" placeholder="<? echo lang("LABEL_SURNAME"); ?>" <? echo $readonly_fatt; ?>> 
                    </div>
                </div>  
 				<div class="row">
                	<div class="col-sm-6">
                    	<label for="co_email"><? echo lang("LABEL_EMAIL"); ?></label>
                    	<input type="text" class="form-control" name="co_email" id="co_email" value="<? echo $this->ion_auth->logged_in() ? $this->ion_auth->user()->row()->email : ''; ?>" placeholder="<? echo lang("LABEL_EMAIL"); ?>" <? echo $readonly_fatt; ?>>
                	</div>
                	<div class="col-sm-6">   
                    	<label for="co_phone"><? echo lang("LABEL_PHONE"); ?></label>
                  		<input type="text" class="form-control" name="co_phone" id="co_phone" value="<? echo $this->ion_auth->logged_in() ? $cliente->telefono : ''; ?>" placeholder="<? echo lang("LABEL_PHONE"); ?>" <? echo $readonly_fatt; ?>>
                	</div>
              	</div>
                <!-- INDIRIZZO FATTURAZIONE -->
                <div class="row">
                	<div class="col-sm-12"  style="font-weight:800;color:#979797;padding-bottom:5px;"><? echo lang('LABEL_BILLING_ADDRESS');?></div>
                	<div class="col-sm-12">
                    	<label for="co_ref"><? echo lang("LABEL_ADDRESS_REF_FATT"); ?></label>
                  		<input type="text" class="form-control" name="co_ref" id="co_ref" value="<? echo $this->ion_auth->logged_in() ? $cliente->riferimento_fatt : ''; ?>" placeholder="<? echo lang("LABEL_ADDRESS_REF_FATT"); ?>" <? echo $readonly_fatt; ?>>
                	</div>
                	<div class="col-sm-8">
                    	<label for="co_address"><? echo lang("LABEL_ADDRESS"); ?></label>
                  		<input type="text" class="form-control" name="co_address" id="co_address" value="<? echo $this->ion_auth->logged_in() ? $cliente->indirizzo_fatt : ''; ?>" placeholder="<? echo lang("LABEL_ADDRESS"); ?>" <? echo $readonly_fatt; ?>>
                	</div>
                	<div class="col-sm-2">
                    	<label for="co_civico"><? echo lang("LABEL_CIVICO"); ?></label>
                  		<input type="text" class="form-control" name="co_civico" id="co_civico" value="<? echo $this->ion_auth->logged_in() ? $cliente->civico_fatt : ''; ?>" placeholder="<? echo lang("LABEL_CIVICO"); ?>" <? echo $readonly_fatt; ?>>
                	</div>
                	<div class="col-sm-2">
                    	<label for="co_postalcode"><? echo lang("LABEL_POSTAL_CODE"); ?></label>
                    	<input type="text" class="form-control" name="co_postalcode" id="co_postalcode" value="<? echo $this->ion_auth->logged_in() ? $cliente->cap_fatt : ''; ?>" placeholder="<? echo lang("LABEL_POSTAL_CODE"); ?>" <? echo $readonly_fatt; ?>>
                	</div>
                    <div class="col-sm-6">
                    	<label for="co_country"><? echo lang("LABEL_COUNTRY"); ?></label>
                        	<? if($readonly_fatt == 'readonly') {?>
                        	<div class="form-element">
                            	<input type="text" readonly class="form-control" name="co_country" id="co_country" value="<? echo $cliente->nazione_fatt; ?>">
							<? } else { ?>
                        <div class="form-element form-select">    
                            <select name="co_country" class="form-control" id="co_country">
                                <option value=""><? echo lang("LABEL_COUNTRY"); ?></option>
                                <?
                                // carica tutti gli indirizzi escluso quello di default
                                foreach ($countries as $country) {
                                    //if($ind->flag_predefinito_sped == 0)
                                    echo '<option value="'.$country->country_code.'" '.($this->ion_auth->logged_in() && $cliente->nazione_fatt == $country->country_code ? 'selected' : '').'>'.$country->country_name.'</option>';
                                }
                                ?>
                            </select>
                            <? } ?>
                    	</div>
                    </div>
                    <div class="col-sm-6">
                    	<label for="co_city"><? echo lang("LABEL_CITY"); ?></label>
                        <input type="text" class="form-control" name="co_city" id="co_city" value="<? echo $this->ion_auth->logged_in() ? $cliente->citta_fatt : ''; ?>" placeholder="<? echo lang("LABEL_CITY"); ?>" <? echo $readonly_fatt; ?>>
                    </div>	
                    <div class="col-sm-12">
                    	<label for="co_note_order"><? echo lang("LABEL_ORDER_NOTES"); ?></label>
                   		<textarea rows="3" class="form-control" style="resize:none" name="co_note_order" id="co_note_order" placeholder="<? echo lang("LABEL_ORDER_NOTES");?>"></textarea>
                	</div>
            	</div>
              	<!-- end INDIRIZZO FATTURAZIONE -->
                <div class="row">
                	<div class="col-sm-12">
                    	<div class="form-group">
                        	<label class="radio radio-inline shipping-radio">
                          		<input type="radio" name="co_shipping" value="current" class="radio-ship" checked> <? echo lang('LABEL_SHIPPING_THIS'); ?>
                        	</label>
                        	<label class="radio radio-inline shipping-radio">
                          		<input type="radio" name="co_shipping" value="other" class="radio-ship"> <? echo lang('LABEL_SHIPPING_OTHER'); ?>
                        	</label>
                      	</div>
                    </div> 
                </div>
                <!-- INDIRIZZO ALTERNATIVO -->
              	<div id="alternative_address_div">
                	<p align="center" style="font-size:14px;font-style:italic;color:#979797"><? echo($this->ion_auth->logged_in() ? lang('MSG_ALTERNATE_ADDRESS_LOGGED') : lang('MSG_ALTERNATE_ADDRESS_NOTLOGGED')); ?></p>
                 	<? if($this->ion_auth->logged_in()) { ?>       
                 	<div class="form-element form-select">
                    	<select class="form-control" name="co_address_registered" id="co_address_registered">
                        	<? 
							if(count($indirizzi_spedizione) < 5) {
							?>
                        	<option value="0"><? echo lang('LABEL_NEW_ADDRESS');?></option> <!-- metti label traduzione -->
                        	<?
							}
							
                        	// carica tutti gli indirizzi escluso quello di default
                        	foreach ($indirizzi_spedizione as $ind) {
                            	//if($ind->flag_predefinito_sped == 0)
                            	echo '<option value="'.$ind->id_indirizzo_spedizione.'" data-ref="'.$ind->riferimento_sped.'" data-address="'.$ind->indirizzo_sped.'" data-civico="'.$ind->civico_sped.'" data-zip="'.$ind->cap_sped.'" data-city="'.$ind->citta_sped.'" data-country="'.$ind->nazione_sped.'" data-note="'.$ind->note_sped.'" data-predefinito="'.$ind->flag_predefinito_sped.'">'.$ind->riferimento_sped.' | '.$ind->indirizzo_sped.', '.$ind->civico_sped.' - '.$ind->cap_sped.' '.$ind->citta_sped.' ('.$ind->nazione_sped.') '.($ind->flag_predefinito_sped > 0 ? '[Default]' : '').'</option>';
                        	}
                        	?>
                    	</select>
                  	</div>
                    <? } ?>
                    <div class="form-element">
                    	<label for="co_alt_ref"><? echo lang("LABEL_ADDRESS_REF"); ?></label>
                    	<input type="text" class="form-control" name="co_alt_ref" id="co_alt_ref" placeholder="<? echo lang("LABEL_ADDRESS_REF"); ?>">
                  	</div>
                  	<div class="row">
                    	<div class="col-sm-6">
                        	<label for="co_alt_country"><? echo lang("LABEL_COUNTRY"); ?></label>
                        	<div class="form-element form-select">
                            	<select name="co_alt_country" class="form-control" id="co_alt_country">
                                	<option value=""><? echo lang("LABEL_COUNTRY"); ?></option>
                                	<?
                                	// carica tutti gli indirizzi escluso quello di default
                                	foreach ($countries as $country) {
                                    	//if($ind->flag_predefinito_sped == 0)
                                    	echo '<option value="'.$country->country_code.'">'.$country->country_name.'</option>';
                                	}
                                	?>
                            	</select>
                        	</div>
                    	</div>
                    	<div class="col-sm-6">
                        	<label for="co_alt_city"><? echo lang("LABEL_CITY"); ?></label>
                        	<input type="text" class="form-control" name="co_alt_city" id="co_alt_city" value="" placeholder="<? echo lang("LABEL_CITY"); ?>">
                    	</div>
                  	</div>
                  	<div class="row">
                    	<div class="col-sm-8">
                        	<label for="co_alt_address"><? echo lang("LABEL_ADDRESS"); ?></label>
                        	<input type="text" class="form-control" name="co_alt_address" id="co_alt_address" value="" placeholder="<? echo lang("LABEL_ADDRESS"); ?>">
                    	</div>
                    	<div class="col-sm-2">
                        	<label for="co_alt_civico"><? echo lang("LABEL_CIVICO"); ?></label>
                        	<input type="text" class="form-control" name="co_alt_civico" id="co_alt_civico" value="" placeholder="<? echo lang("LABEL_CIVICO"); ?>">
                    	</div>
                    	<div class="col-sm-2">
                        	<label for="co_alt_postalcode"><? echo lang("LABEL_POSTAL_CODE"); ?></label>
                        	<input type="text" class="form-control" name="co_alt_postalcode" id="co_alt_postalcode" value="" placeholder="<? echo lang("LABEL_POSTAL_CODE"); ?>">
                    	</div>
                  	</div>
                  	<div class="row">
                    	<div class="col-sm-12">
                        	<label for="co_alt_note_address"><? echo lang("LABEL_ADDRESS_NOTES"); ?></label>
                      		<input type="text" class="form-control" name="co_alt_note_address" id="co_alt_note_address" placeholder="<? echo lang("LABEL_ADDRESS_NOTES"); ?>">
                    	</div>
                  	</div>
              	</div>  
                <div class="row"><!-- pagamento --> 
                	<div class="col-sm-12">
                    	<label for="co_payment"><? echo lang("LABEL_PAYMENT_METHOD"); ?></label>
                    	<div class="form-element form-select">
                            <select class="form-control" name="co_payment" id="co_payment">
                                <option value="0"><? echo lang("LABEL_PAYMENT_METHOD"); ?></option>
                                <option value="paypal"><? echo lang("LABEL_PAYMENT_METHOD_PAYPAL"); ?></option>
                                <option value="stripe"><? echo lang("LABEL_PAYMENT_METHOD_CC"); ?></option>
                            </select>
                	  </div>
                	</div>
            	</div><!-- end pagamento -->     
            </div><!-- end Modulo --> 
            <div class="col-md-3 col-md-offset-1 col-sm-4 padding-bottom">	<!-- Sidebar -->
              <aside>
                <h3><? echo lang("LABEL_TOTAL_ORDER"); ?></h3>
                <div id="cart-total-show-div">
                    <img style="width: 45px;" src="<? echo ASSETS_ROOT_FOLDER_FRONTEND_IMG.'/field_loader.gif'; ?>" />
                </div>
                <p class="text-sm text-gray"><? echo lang("LABEL_TOTAL_ORDER_NOTES"); ?></p>
                <a href="<? echo site_url(lang('PAGE_CART_URL')); ?>" class="btn btn-default btn-ghost icon-left btn-block">
                  <i class="material-icons arrow_back"></i>
                  <? echo lang("LABEL_BACK_TO_CART"); ?>
                </a>
                <button type="button" id="submit-btn" class="btn btn-primary btn-block waves-effect waves-light space-top-none" data-desc="<? echo lang("LABEL_STRIPE_DESC"); ?>" data-amount="1000" data-email=""><? echo lang("LABEL_CONFIRM"); ?></button>
                <div id="paypal-button-loader" style="margin-top:25px;display:none" align="center"><img src="<?= ASSETS_ROOT_FOLDER_FRONTEND_IMG; ?>/preloader.gif" /></div>
                <div id="paypal-button-wrapper" style="margin-top:25px;" align="center"></div>
              </aside>
            </div><!-- end Sidebar -->
        </div><!-- end row -->    
        <input type="hidden" name="co_payment_status" id="co_payment_status" value="1">
        <input type="hidden" name="co_total_order" id="co_total_order" value="0">
        <input type="hidden" name="token_id_hidden" id="token_id_hidden" value="">
        <input type="hidden" name="coupon_code_hidden" id="coupon_code_hidden" value="<? echo ($this->session->userdata('validated_coupon') != NULL ? $this->session->userdata('validated_coupon') : '');?>">
        <input type="hidden" name="coupon_value_hidden" id="coupon_value_hidden" value="<? echo ($this->session->userdata('validated_coupon_value') != NULL ? $this->session->userdata('validated_coupon_value') : '');?>">
        <input type="hidden" name="id_fatturazione_hidden" id="id_fatturazione_hidden" value="<? echo ($this->ion_auth->logged_in() && $cliente->id_indirizzo_fatturazione != NULL ? $cliente->id_indirizzo_fatturazione : 0); ?>">
        <input type="hidden" name="co_address_default_id" id="co_address_default_id" value="<? echo ($this->ion_auth->logged_in() && count($indirizzi_spedizione) > 0 ? $indirizzi_spedizione[0]->id_indirizzo_spedizione : 0); ?>">
    </form><!-- .container -->
    <? require_once('include/footer.php'); ?> <!-- Footer -->
  </div><!-- .page-wrapper -->
  <? require_once('include/common_header_js.php'); ?> <!-- Import js -->
  <script src="https://checkout.stripe.com/checkout.js"></script>
  <script src="https://www.paypalobjects.com/api/checkout.js"></script>
</body><!-- <body> -->
<script type="text/javascript">
	var curr_address_choice = 'current';
	$(window).load(function() {
		loadCartDropdown(false, false, false);
		$('.cart-dropdown').hide();
		/* stripe */
		var handler = StripeCheckout.configure({
			key: '<?= STRIPE_PK; ?>',
			image: '<?= ASSETS_ROOT_FOLDER_FRAMEWORK_IMG; ?>/logo_128x128.jpg',
			locale: 'auto',
			token: function(token) {
			   console.log('token utile per il pagamento: ' + token.id);
			   $('#token_id_hidden').val(token.id);
			   checkoutOrder();
			}
		});
		// Close Checkout on page navigation
		$(window).on('popstate', function() {
			handler.close();
		});
		$('#submit-btn').on('click', function(e) {
			e.preventDefault();
			$("#password_login").val('');
			$("#password_login").removeClass('has-error');
			$("#identity").val('');
			$("#identity").removeClass('has-error');
			if(validateCheckout())
			{
				console.log('Start payment: ' + $('#co_payment').val());
				if($('#co_payment').val() == 'stripe') {
					$('#paypal-button-wrapper').empty();
					// STRIPE
					handler.open({
					  name: 'Ma Chlò',
					  description: $(this).data('desc'),
					  currency: "eur",
					  amount: $("#cart-total-float-hidden").val() * 100,
					  locale: 'it',
					  email: $("#co_email").val(),
					  allowRememberMe: false
					});
				} else if($('#co_payment').val() == 'paypal') {
					loadPaypalBtn();
				} else {
					$('#paypal-button-wrapper').empty();
				}
			} else {
				$('#paypal-button-wrapper').empty();
				return false;
			}
		});
		$("#co_f_name").on("keyup blur", function(){validateText('co_f_name');});
		$("#co_l_name").on("keyup blur", function(){validateText('co_l_name');});
		$("#co_email").on("keyup blur", function(){validateEmail('co_email');});
		$("#co_phone").on("keyup blur", function(){validateNumeric('co_phone', 1, true);});
		$("#co_country").on("change blur", function(){validateSelectText('co_country', '');});
		$("#co_city").on("keyup blur", function(){validateText('co_city');});
		$("#co_ref").on("keyup blur", function(){validateText('co_ref');});
		$("#co_address").on("keyup blur", function(){validateText('co_address');});
		$("#co_civico").on("keyup blur", function(){validateText('co_civico');});
		$("#co_postalcode").on("keyup blur", function(){validateNumeric('co_postalcode', 1, true);});
	//	$("#co_payment").on("change blur", function(){validateSelectText('co_payment', '0');});
			
		$("#co_payment").on("change blur", function(){
			//validateSelectText('co_payment', '0');
			if(validateCheckout())
			{
				console.log('Start payment: ' + $('#co_payment').val());
				if($('#co_payment').val() == 'stripe') {
					$('#paypal-button-wrapper').empty();
					$('#submit-btn').show();
					// STRIPE
					handler.open({
					  name: 'Ma Chlò',
					  description: $(this).data('desc'),
					  currency: "eur",
					  amount: $("#cart-total-float-hidden").val() * 100,
					  locale: 'it',
					  email: $("#co_email").val(),
					  allowRememberMe: false
					});
				} else if($('#co_payment').val() == 'paypal') {
					loadPaypalBtn();
				} else {
					$('#paypal-button-wrapper').empty();
					$('#submit-btn').show();
				}
			} else {
				$('#paypal-button-wrapper').empty();
				$('#submit-btn').show();
			}
		});

		$('#alternative_address_div').toggle();

		$('.shipping-radio').click(function () {
			//console.log('change shipping ' + $(this).find('input').val() + ' curr ' +curr_address_choice);
			if($(this).find('input').val() != curr_address_choice) {
				curr_address_choice = $(this).find('input').val();
				$('#alternative_address_div').slideToggle( "slow" );
				if($("#co_address_registered option:first").val() != 0) {
					$("#co_address_registered").val($("#co_address_registered option:first").val());
					$("#co_address_registered").trigger("change");
				} else {
					cleanCoAddressRegistered(true);
				}
			} 
		});
		$('.iCheck-helper').click(function () {
		//	console.log('change check ' + $(this).prev().val() + ' curr ' +curr_address_choice);
			if($(this).prev().val() != curr_address_choice) {
				curr_address_choice = $(this).prev().val();	
				$('#alternative_address_div').slideToggle( "slow" );
				cleanCoAddressRegistered(true);
			}
		});
		$('#co_address_registered').on('change', function() {
			//console.log($(this).find(':selected').data('zip'));
			if($(this).val() != 0 && $(this).val() != '') {
				$('#co_alt_ref').val($(this).find(':selected').data('ref'));
				$('#co_alt_country').val($(this).find(':selected').data('country'));
				$('#co_alt_city').val($(this).find(':selected').data('city'));
				$('#co_alt_address').val($(this).find(':selected').data('address'));
				$('#co_alt_civico').val($(this).find(':selected').data('civico'));
				$('#co_alt_postalcode').val($(this).find(':selected').data('zip'));
				$('#co_alt_note_address').val($(this).find(':selected').data('note'));
				// set readonly
				$('#co_alt_ref').attr('readonly', true);
				$('#co_alt_country').attr('readonly', true);
				$('#co_alt_country').attr('disabled', true);
				$('#co_alt_city').attr('readonly', true);
				$('#co_alt_address').attr('readonly', true);
				$('#co_alt_civico').attr('readonly', true);
				$('#co_alt_postalcode').attr('readonly', true);
				$('#co_alt_note_address').attr('readonly', true);
			} else {
				$('#co_alt_ref').val('');
				$('#co_alt_country').val('');
				$('#co_alt_city').val('');
				$('#co_alt_address').val('');
				$('#co_alt_civico').val('');
				$('#co_alt_postalcode').val('');
				$('#co_alt_note_address').val('');
				// set readonly
				$('#co_alt_ref').attr('readonly', false);
				$('#co_alt_country').attr('readonly', false);
				$('#co_alt_country').attr('disabled', false);
				$('#co_alt_city').attr('readonly', false);
				$('#co_alt_address').attr('readonly', false);
				$('#co_alt_civico').attr('readonly', false);
				$('#co_alt_postalcode').attr('readonly', false);
				$('#co_alt_note_address').attr('readonly', false);
			}
			validateAddressOther();
		});
		
		$("#btn_accedi").click(function(){    
			$("#co_f_name").val('');
			$("#co_f_name").removeClass('has-error');
			$("#co_l_name").val('');
			$("#co_l_name").removeClass('has-error');
			$("#co_email").val('');
			$("#co_email").removeClass('has-error');
			$("#co_phone").val('');
			$("#co_phone").removeClass('has-error');
			$("#co_ref").val('');
			$("#co_ref").removeClass('has-error');
			$("#co_address").val('');
			$("#co_address").removeClass('has-error');
			$("#co_civico").val('');
			$("#co_civico").removeClass('has-error');
			$("#co_postalcode").val('');
			$("#co_postalcode").removeClass('has-error');
			$("#co_country").val('');
			$("#co_country").removeClass('has-error');
			$("#co_city").val('');
			$("#co_city").removeClass('has-error');
			$("#co_note_order").val('');
			$("#co_note_order").removeClass('has-error');
			$("#co_payment").val(0);
			$("#co_payment").removeClass('has-error');
			cleanCoAddressRegistered(false);
			if(
				validateEmail('identity') &
				validateText('password_login')
			)
			{
				$.ajax({  
				  type: 'POST',
				  url: '<? echo site_url(lang('PAGE_LOGIN_URL'));?>',  
				  data: {"identity": $("#identity").val(), "password_login": $("#password_login").val()},
				  dataType: 'html',
				  async: true,
				  success: function(risposta) {  
					console.log("Siamo nella pagina di login : risposta " + risposta);
					if(risposta!='ok'){
						swal({
							  position: 'center',
							  type: 'error',
							  html:	risposta , 
							  animation: false
						});
						//$(window.location).attr('href', '<?// echo site_url(lang('PAGE_CHECKOUT_URL'));?>');
					} else {
						$(window.location).attr('href', '<? echo site_url(lang('PAGE_CHECKOUT_URL'));?>');
					}
				
				  },
				  error: function(){
					console.log("Siamo nella pagina di login : error " );
					alert("Chiamata fallita!!!");
				  } 
				});	
				return true;
			} else {
				return false;
			}
		});
		$("#password_login").on("keyup blur", function(){validateText('password_login');});
		$("#identity").on("keyup blur", function(){validateEmail('identity');});
	});
	
	function checkoutOrder(){
	//	console.log('checkout-form');
		$(".se-pre-con").show();
		$('#co_total_order').val($('#cart-total-float-hidden').val());
		var formData = $('#checkout-form').serializeArray();
		formData.push({name: 'curr_address_choice', value: curr_address_choice});
	
		return $.ajax({
			url: '<? echo base_url();?>frontend/Cart/checkoutOrder',
			type: 'POST',
			cache: false,
			data: formData,
			error: function(msg){
				$(".se-pre-con").delay(200).fadeOut("slow"); 
				swal({
				  position: 'center',
				  type: 'error',
				  title: "<?php echo lang('MSG_SERVICE_FAILURE'); ?>",
				  showConfirmButton: false,
				  timer: 3000
				});
				return msg;
			},
			success: function(messJson){
				console.log(messJson);
				var json = $.parseJSON(messJson);
				loadCartDropdown(false, false, false);
				$(".se-pre-con").delay(200).fadeOut("slow");  
				// messagge
				swal({
				  type: json['type'],
				  title: json['title'],
				  html: json['message'],
				  confirmButtonText: '<?php echo lang('LABEL_BACK_SHOP'); ?>',
				  showCancelButton: false
				}).then(function () {
      				// Redirect the user
      				window.location.href = "<?php echo site_url(lang('PAGE_SHOP_URL')); ?>";
    			});
				
				return true;
			}
		});
	}
	function loadPaypalBtn() {
		console.log('loadPaypalBtn');
		if($('#paypal-button-container').length == 0) {
			// PAYPAL
			$('#paypal-button-loader').show();
			$('#paypal-button-wrapper').hide();
			$('#paypal-button-wrapper').empty();
			$('#paypal-button-wrapper').html('<div id="paypal-button-container"></div>');
			paypal.Button.render({
				env: '<?= PAYPAL_ENV; ?>', // sandbox | production
				// Specify the style of the button
				style: {
					label: 'checkout',
					size:  'responsive',    // small | medium | large | responsive
					shape: 'pill',     // pill | rect
					color: 'blue'      // gold | blue | silver | black
				},
				locale: '<? echo lang('LANGUAGE_LOCALE_PAYPAL'); ?>',
				client: {
					sandbox:    '<?= PAYPAL_SANDBOX_CLIENT_ID; ?>',
					production: '<?= PAYPAL_LIVE_CLIENT_ID; ?>'
				},
				// Show the buyer a 'Pay Now' button in the checkout flow
				commit: true,
				// payment() is called when the button is clicked
				payment: function(data, actions) {
					// Make a call to the REST api to create the payment
					return actions.payment.create({
						payment: {
							transactions: [
								{
									amount: { total: $("#cart-total-float-hidden").val(), currency: 'EUR' },
									description: $("#co_f_name").val() + ' ' + $("#co_l_name").val() + ' | ' + $("#co_email").val(),
									item_list: {
										shipping_address: {
										  recipient_name: $("#co_f_name").val() + ' ' + $("#co_l_name").val(),
										  line1: $("#co_address").val(),
										  city: $("#co_city").val(),
										  country_code: $("#co_country").val(),
										  postal_code: $("#co_postalcode").val(),
										  phone: $("#co_phone").val(),
										  state: "IT"
										}
									}
								}
							],
							"note_to_payer": "<? echo lang('MSG_PAYPAL_NOTETOPAYER'); ?>"
						}
					});
				},
	
				// onAuthorize() is called when the buyer approves the payment
				onAuthorize: function(data, actions) {
					// Make a call to the REST api to execute the payment
					return actions.payment.execute().then(function() {
						console.log('Payment Paypal Complete!');
						$('#token_id_hidden').val('paypal');
						checkoutOrder();
					});
				},
				onError: function(err) {
					// messagge
					swal({
					  position: 'center',
					  type: 'error',
					  title: "<?php echo lang('MSG_PAYPAL_ERROR'); ?>",
					  showConfirmButton: false,
					  timer: 3000
					});
				},
				onCancel: function(data, actions) {
					swal({
					  position: 'center',
					  type: 'warning',
					  title: "<?php echo lang('MSG_PAYPAL_CANCEL'); ?>",
					  showConfirmButton: false,
					  timer: 3000
					});
				}
			}, '#paypal-button-container');
			setTimeout(function(){
			  $('#submit-btn').hide();
			  $('#paypal-button-loader').hide();
			  $('#paypal-button-wrapper').show();
			  $('#co_payment').blur();
			}, 2000);
		}
	}
	function validateAddressOther() {
		console.log('validateAddressOther' + curr_address_choice);
		if(curr_address_choice == 'other') {
			if(
				validateText('co_alt_ref') &
				validateSelectText('co_alt_country', '') &
				validateText('co_alt_city') &
				validateText('co_alt_address') &
				validateNumeric('co_alt_postalcode', 1, true) &
				validateText('co_alt_civico')
			)
			{
				return true;
			} else {
				$("#co_alt_ref").on("keyup blur", function(){validateText('co_alt_ref');});
				$("#co_alt_country").on("change blur", function(){validateSelectText('co_alt_country', '');});
				$("#co_alt_city").on("keyup blur", function(){validateText('co_alt_city');});
				$("#co_alt_address").on("keyup blur", function(){validateText('co_alt_address');});
				$("#co_alt_postalcode").on("keyup blur", function(){validateNumeric('co_alt_postalcode', 1, true);});
				$("#co_alt_civico").on("keyup blur", function(){validateText('co_alt_civico');});
				return false;
			}
		} else {
			return true;
		}
	}	
	function validateCheckout() {
		if(
			validateText('co_f_name') &
			validateText('co_l_name') &
			validateEmail('co_email') &
			validateNumeric('co_phone', 1, true) &
			validateSelectText('co_country', '') &
			validateText('co_city') &
			validateText('co_ref') &
			validateText('co_address') &
			validateNumeric('co_postalcode', 1, true) &
			validateSelectText('co_payment', '0') &
			validateText('co_civico') & 
			validateAddressOther()
		)
		{
			return true;
		} else {
			return false;
		}
	}
	function cleanCoAddressRegistered(validate) {
		if($('#co_address_registered option[data-predefinito="1"]').length !== 0) {
			$('#co_address_registered').val($('#co_address_registered option[data-predefinito="1"]').val());
			$('#co_address_registered').trigger("change");
		} else {
			$('#co_address_registered').val(0);
			$('#co_alt_ref').val('');
			$("#co_alt_ref").removeClass('has-error');
			$('#co_alt_country').val('');
			$("#co_alt_country").removeClass('has-error');
			$('#co_alt_city').val('');
			$("#co_alt_city").removeClass('has-error');
			$('#co_alt_address').val('');
			$("#co_alt_address").removeClass('has-error');
			$('#co_alt_civico').val('');
			$("#co_alt_civico").removeClass('has-error');
			$('#co_alt_postalcode').val('');
			$("#co_alt_postalcode").removeClass('has-error');
			$('#co_alt_note_address').val('');
			$("#co_alt_note_address").removeClass('has-error');
		}
		if(validate)
			validateCheckout();
	}
</script>
</html>
