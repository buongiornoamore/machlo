<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends Frontend_Controller {

	public function __construct() 
	{
        parent::__construct();   
	}
	
	public function index()
	{
		$data = array('productsContainerClass' => 'container-fluid');	
		// fullwidth: container-fluid | standard: container	
		$this->show_view_with_menu('frontend/index', $data);
	}
	
	public function about()
	{
		$data = array();
		$this->show_view_with_menu('frontend/about', $data);	
	}

	public function contacts()
	{
		$data = array();
		$this->show_view_with_menu('frontend/contacts', $data);	
	}
	
	public function privacy()
	{
		$data = array();
		$this->show_view_with_menu('frontend/privacy', $data);
	}
	
	public function shipping()
	{
		$data = array();
		$this->show_view_with_menu('frontend/shipping', $data);
	}
	
	public function rules()
	{
		$data = array();
		$this->show_view_with_menu('frontend/rules', $data);
	}
	
	public function shop($category_url = '')
	{
		// fullwidth: container-fluid | standard: container	
		$data = array('productsContainerClass' => 'container-fluid', 'category_url' => $category_url);		
		$this->show_view_with_menu('frontend/shop', $data);
	}
	
	public function gallery()
	{
		// fullwidth: container-fluid | standard: container	
		$data = array();
		$data = $this->loadGallery($data);
		$data['productsContainerClass'] = 'container-fluid';
		$this->show_view_with_menu('frontend/gallery', $data);
	}
	
	public function loadGallery($data)
	{
		// caricare categorie gallery
		$this->db->select('*');
		$this->db->from('categorie_gallery');
		$this->db->join('categorie_gallery_traduzioni', 'categorie_gallery_traduzioni.id_categoria_gallery = categorie_gallery.id_categoria_gallery', 'left');
		$this->db->where('categorie_gallery.stato_categoria_gallery', 1);
		$this->db->where('categorie_gallery_traduzioni.id_lingua', lang('LANGUAGE_ID'));
		$query_cats = $this->db->get();
		$data['gallery_cats'] = $query_cats->result();
		
		// caricare immagini galleria
		$this->db->select('*');
		$this->db->from('immagini_gallery');
		$this->db->join('immagini_gallery_traduzioni', 'immagini_gallery_traduzioni.id_ig = immagini_gallery.id_ig', 'left');
		$this->db->join('stato_descrizione', 'stato_descrizione.id_stato_descrizione = immagini_gallery.stato_ig');
		$this->db->join('categorie_gallery', 'categorie_gallery.id_categoria_gallery = immagini_gallery.id_categoria_ig');
		$this->db->join('categorie_gallery_traduzioni', 'categorie_gallery_traduzioni.id_categoria_gallery = categorie_gallery.id_categoria_gallery');
		$this->db->where('immagini_gallery.stato_ig', 1);
		$this->db->where('immagini_gallery_traduzioni.id_lingua', lang('LANGUAGE_ID'));
		$this->db->where('categorie_gallery_traduzioni.id_lingua', lang('LANGUAGE_ID'));
		$this->db->order_by("immagini_gallery.ordine_ig", "DESC");		
		$query_immagini = $this->db->get();
		$data['gallery_images'] = $query_immagini->result();

		return $data;
	}
}