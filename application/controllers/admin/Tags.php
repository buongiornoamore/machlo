<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Tags extends Admin_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
	}
	
	public function crud()
	{
		$this->checkUserPermissions();
		//CRUD 
		try{
			$crud = new grocery_CRUD();
			// tema
			$crud->set_theme('bootstrap');
			// tabella
			$crud->set_table('tags');
			// nome in tabella
			$crud->display_as('nome_tag', 'Nome tag');
			// campi obbligatori
			$crud->required_fields('nome_tag');
			// regole validazione campi
			//$crud->set_rules('prezzo', 'Prezzo', 'required|numeric'); // escludere tutto quello che non è alfanumerico - _
			//$crud->set_rules('prezzo_scontato', 'Prezzo scontato', 'numeric');
			// campi per add
			//$crud->add_fields('id_categoria', 'id_tipo_prodotto', 'codice', 'nome', 'prezzo', 'prezzo_scontato', 'stato', 'id_colori_prodotti');
			// colonne da mostrare
			$crud->columns('nome_tag');
			// unset delete action
			$crud->unset_delete();
	
			$output = $crud->render();

			$data['curr_page'] = 'ADMIN-TAGS';
			$data['curr_page_title'] = 'Prodotti';
			$data['collapseParentMenu'] = 'prodotti';
			$data['resourcetype'] = 'CRUD';
			$output->data = $data;
			$this->load->view('admin/tags',(array)$output);
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
}
