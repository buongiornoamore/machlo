<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Categories extends Admin_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
	}
	
	public function crud()
	{
		$this->checkUserPermissions();
		//CRUD Categorie
		try{
			$crud = new grocery_CRUD();
			// tema
			$crud->set_theme('bootstrap');
			// tabella
			$crud->set_table('categorie');
			// nome in tabella
			$crud->display_as('url_categorie', 'Url');
			$crud->display_as('nome', 'Nome');
			$crud->display_as('descrizione', 'Descrizione');
			$crud->display_as('immagine', 'Immagine');
			$crud->display_as('label_color_class', 'Colore');
			$crud->display_as('stato', 'Stato');
			$crud->display_as('prodotti', 'Prodotti');
			// realazioni join
			$crud->set_relation('stato', 'stato_prodotti', 'stato_prodotti_desc');
			$crud->set_relation('label_color_class', 'colori_classi', 'colore_classe_nome');
			$crud->set_relation_n_n('prodotti', 'prodotti_categorie', 'prodotti', 'id_categoria', 'id_prodotto', 'stato', 'id_prodotto', array('stato'=>1));
	
			// file upload
			$crud->set_field_upload('immagine', 'assets/assets-frontend/img/categories');
			// campi obbligatori
			$crud->required_fields('url_categorie', 'nome', 'descrizione', 'label_color_class', 'stato');
			// campi per add
			$crud->add_fields('url_categorie', 'nome', 'descrizione', 'label_color_class', 'stato', 'ordine');
			// campi per edit
			$crud->edit_fields('url_categorie', 'nome', 'descrizione', 'label_color_class', 'stato', 'ordine', 'immagine');
			// colonne da mostrare
			$crud->columns('immagine', 'url_categorie', 'nome', 'stato', 'ordine', 'prodotti');
			$crud->add_action('Traduzioni categoria', '', '', 'fa-flag', array($this, 'load_traduzioni'));
			// callbacks
			$crud->callback_before_insert(array($this,'formatCategoryUrl'));
			$crud->callback_before_update(array($this,'formatCategoryUrl'));
			$crud->callback_column('prodotti', function($value, $row) {
				$counter_active = 0;
				$counter_disabled = 0;
				if(strlen($value)) {
					$arr_values = explode(',', $value);
					foreach ($arr_values as $stato) {
						if($stato == 1)
							$counter_active++;
						else 		
							$counter_disabled++;
					}
				} 
				
				return '<b>Attivi</b> ' . $counter_active . '<br><b>Sospesi</b> ' . $counter_disabled;
			});
			
			$crud->set_rules('ordine', 'Ordine', 'numeric|greater_than[0]');
			// unset delete action
			$crud->unset_delete();
			
			$crud->order_by('stato','desc');
			$crud->order_by('ordine','asc');
			$output = $crud->render();

			$data['curr_page'] = 'ADMIN-CATEGORIES';
			$data['curr_page_title'] = 'Prodotti';
			$data['collapseParentMenu'] = 'prodotti';
			$data['resourcetype'] = 'CRUD';
			$output->data = $data;
			$this->load->view('admin/categories',(array)$output);
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
	
	function load_traduzioni($primary_key , $row)
	{
		return site_url('admin/categories/trad/'.$row->id_categorie);
	}
	
	function formatCategoryUrl($post_array) {
		$post_array['url_categorie'] = formattaUrlCodice($post_array['url_categorie'], '-');
		return $post_array;
	}
	
	public function traductions($category_id)
	{
		//CRUD varianti
		try {
			$crud = new grocery_CRUD();
			// tema
			$crud->set_theme('bootstrap');
			// tabella
			$crud->set_table('categorie_traduzioni');
			$crud->where('id_categoria', $category_id);
			// nome in tabella
			$crud->display_as('nome_categoria_trad', 'Nome');
			$crud->display_as('id_lingua', 'Lingua');
			// realazioni join
			$crud->set_relation('id_lingua', 'lingue', 'nome_lingue');
			// campi obbligatori
			$crud->required_fields('nome_categoria_trad', 'id_lingua');
			$crud->edit_fields('id_categoria', 'nome_categoria_trad', 'id_lingua');
			$crud->add_fields('id_categoria', 'nome_categoria_trad', 'id_lingua');
			// colonne da mostrare
			$crud->columns('nome_categoria_trad', 'id_lingua');
			// unset delete action
			//$crud->unset_delete();
			$crud->field_type('id_categoria', 'hidden', $category_id);
			$output = $crud->render();

			$data['curr_page'] = 'ADMIN-CATEGORIES';
			$data['curr_page_title'] = 'Prodotti';
			$data['collapseParentMenu'] = 'prodotti';
			$data['curr_function_title'] = 'Traduzioni per categoria <b>' . $category_id . '</b>';
			$data['resourcetype'] = 'CRUD';
			$output->data = $data;
			$this->load->view('admin/categories_trad',(array)$output);
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}

}
