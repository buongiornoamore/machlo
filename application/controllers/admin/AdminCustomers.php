<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class AdminCustomers extends Admin_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
	}
	
	public function crud()
	{
		$this->checkUserPermissions();
		//CRUD ordini
		try{
			$crud = new grocery_CRUD();
			// tema
			$crud->set_theme('bootstrap');
			// tabella
			$crud->set_table('clienti');
			// nome in tabella
			$crud->display_as('id_lingua', 'Lingua');
			$crud->display_as('id_cliente', 'ID cliente');
			$crud->display_as('user_id', 'ID utente');
			// realazioni join
			$crud->set_relation('id_lingua', 'lingue', 'nome_lingue');
			$crud->set_relation('user_id', 'users', 'id');
			// colonne da mostrare
			$crud->required_fields('cognome', 'nome', 'email', 'id_lingua');
			// campi per add
			$crud->add_fields('id_cliente', 'cognome', 'nome', 'email', 'telefono', 'partita_iva', 'codice_fiscale', 'id_indirizzo_fatturazione',  'id_lingua', 'newsletter', 'punti');
			// campi per edit
			$crud->edit_fields('id_cliente', 'user_id', 'cognome', 'nome', 'email', 'telefono', 'partita_iva', 'codice_fiscale', 'id_indirizzo_fatturazione', 'id_lingua', 'newsletter', 'punti');
			// colonne da mostrare
			$crud->columns('id_cliente', 'user_id', 'cognome', 'nome', 'email', 'telefono', 'id_lingua');
			$crud->add_action('Indirizzo fatturazione', '', '', 'fa-map', array($this, 'load_fatturazione'));
			$crud->add_action('Indirizzi spedizione', '', '', 'fa-map-marker', array($this, 'load_gestione_indirizzi'));
			
			$crud->unset_delete();
			if($crud->getState() == 'add'){
				$crud->change_field_type('id_cliente', 'hidden');
				$crud->change_field_type('id_indirizzo_fatturazione', 'hidden');
				$crud->change_field_type('punti', 'hidden');
				$crud->change_field_type('newsletter', 'true_false');
			}else if($crud->getState() == 'edit')
			{
				// carica cliente dal db
				$this->db->where('id_cliente', $crud->getStateInfo()->primary_key);
				$this->db->from('clienti');
				$query_if = $this->db->get();
				$customer_if = $query_if->row();
				
				$crud->change_field_type('id_cliente', 'readonly');
				$crud->change_field_type('user_id', 'readonly');
				$crud->change_field_type('newsletter', 'true_false');
				$crud->set_relation('id_indirizzo_fatturazione', 'indirizzo_fatturazione', '{indirizzo_fatt},{civico_fatt} - {citta_fatt} {cap_fatt} [{nazione_fatt}]');
				$crud->change_field_type('id_indirizzo_fatturazione', 'readonly');
				//, array('id_indirizzo_fatturazione' => $customer_if->id_indirizzo_fatturazione)
			}
			// callbacks
			$crud->callback_before_delete(array($this,'_callback_delete_customer'));
			
			$output = $crud->render();

			$data['curr_page'] = 'ADMIN-CUSTOMERS';
			$data['curr_page_title'] = 'Clienti';
			$data['collapseParentMenu'] = 'Clienti';
			$data['resourcetype'] = 'CRUD';
			$output->data = $data;
			$this->load->view('admin/customers',(array)$output);
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
	
	// se si cancella un cliente cancello anche l'utente da users e i suoi indirizzi da fatt e sped
	function _callback_delete_customer($primary_key) {
		$query_cliente = $this->db->where('id_cliente', $primary_key)->get('clienti');
		$cliente = $query_cliente->row();
		$this->db->delete('users', array('id' => $cliente->user_id)); 
		return true;	
	}
	
	function load_gestione_indirizzi($primary_key, $row)
	{	
		return site_url('admin/customers/shipping/'.$row->id_cliente);
	}
	
	function load_fatturazione($primary_key, $row)
	{	
		// load cliente da ordine
		$this->db->from('clienti');
		$this->db->where('id_cliente', $row->id_cliente);
		$cliente = $this->db->get()->row();
		
		if($cliente->id_indirizzo_fatturazione == 0) {
			return site_url('admin/customers/billing/'.$row->id_cliente.'/add');
		} else {
			return site_url('admin/customers/billing/'.$row->id_cliente.'/edit/'.$cliente->id_indirizzo_fatturazione);
		}
	}
	
	public function shipping($cliente_id) {
		$this->checkUserPermissions();
		// CRUD indirizzo_spedizione
		try {
			// load cliente da ordine
			$this->db->from('clienti');
			$this->db->where('id_cliente', $cliente_id);
			$cliente = $this->db->get()->row();
		
			$crud = new grocery_CRUD();

			// tema
			$crud->set_theme('bootstrap');
			// tabella
			$crud->set_table('indirizzo_spedizione');
			$crud->where('id_cliente', $cliente_id);
			// nome in tabella
			$crud->display_as('indirizzo_sped', 'Indirizzo');
			$crud->display_as('civico_sped', 'Civico');
			$crud->display_as('cap_sped', 'Cap');
			$crud->display_as('citta_sped', 'Città');
			$crud->display_as('nazione_sped', 'Nazione');
			$crud->display_as('riferimento_sped', 'Riferimento');
			$crud->display_as('note_sped', 'Note');
			$crud->display_as('flag_predefinito_sped', 'Indirizzo predefinito');
			// realazioni join
			$crud->set_relation('nazione_sped', 'countries', '{country_name} [{country_code}]');
			// colonne da mostrare
			$crud->columns('indirizzo_sped', 'civico_sped', 'cap_sped', 'citta_sped', 'nazione_sped', 'flag_predefinito_sped');
			$crud->add_fields('id_cliente', 'indirizzo_sped', 'civico_sped', 'cap_sped', 'citta_sped', 'nazione_sped', 'riferimento_sped', 'note_sped', 'flag_predefinito_sped');
			// campi per edit
			$crud->edit_fields('id_cliente', 'indirizzo_sped', 'civico_sped', 'cap_sped', 'citta_sped', 'nazione_sped', 'riferimento_sped', 'note_sped', 'flag_predefinito_sped');
			$crud->required_fields('indirizzo_sped', 'civico_sped', 'cap_sped', 'citta_sped', 'nazione_sped');
			$crud->field_type('id_cliente', 'hidden', $cliente_id);
			
			$crud->change_field_type('flag_predefinito_sped', 'true_false');
			
			if($crud->getState() == 'add'){
				//$crud->change_field_type('flag_predefinito_sped', 'hidden', 0);
				$crud->field_type('flag_predefinito_sped', 'hidden', '0');
			} 
			else if($crud->getState() == 'edit')
			{
				$crud->change_field_type('flag_predefinito_sped', 'true_false');
			}
			// callbacks
			$crud->callback_after_update(array($this, '_callback_update_customer'));
						
			$output = $crud->render();

			$data['curr_page'] = 'ADMIN-CUSTOMERS';
			$data['curr_page_title'] = 'Clienti';
			$data['collapseParentMenu'] = 'Clienti';
			$data['resourcetype'] = 'CRUD';
			$data['curr_function_title'] = 'Indirizzi spedizione per <b>' . $cliente->cognome . ' ' . $cliente->nome .'</b>';
			$output->data = $data;
			$this->load->view('admin/customers_shipping',(array)$output);
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
	
	// verifica flag_predefinito_sped dopo inserimento 
	function _callback_update_customer($post_array, $primary_key) {
		if($post_array['flag_predefinito_sped'] > 0) {
			$data = array(
			   'flag_predefinito_sped' => 0
			);
			$this->db->where('id_indirizzo_spedizione !=', $primary_key);
			$this->db->update('indirizzo_spedizione', $data);
			
		}
		return true;	
	}
	
	public function billing($cliente_id) {
		$this->checkUserPermissions();
		// CRUD indirizzo_spedizione
		try {
			// load cliente da ordine
			$this->db->from('clienti');
			$this->db->where('id_cliente', $cliente_id);
			$cliente = $this->db->get()->row();
		
			$crud = new grocery_CRUD();
			$this->crud = $crud;

			// tema
			$crud->set_theme('bootstrap');
			// tabella
			$crud->set_table('indirizzo_fatturazione');
			$crud->where('id_cliente', $cliente_id);
			// nome in tabella
			$crud->display_as('indirizzo_fatt', 'Indirizzo');
			$crud->display_as('civico_fatt', 'Civico');
			$crud->display_as('cap_fatt', 'Cap');
			$crud->display_as('citta_fatt', 'Città');
			$crud->display_as('nazione_fatt', 'Nazione');
			$crud->display_as('riferimento_fatt', 'Riferimento');
			$crud->display_as('note_fatt', 'Note');
			// realazioni join
			$crud->set_relation('nazione_fatt', 'countries', '{country_name} [{country_code}]');
			// colonne da mostrare
			$crud->columns('id_cliente', 'indirizzo_fatt', 'civico_fatt', 'cap_fatt', 'citta_fatt', 'nazione_fatt');
			$crud->add_fields('id_cliente', 'id_cliente', 'indirizzo_fatt', 'civico_fatt', 'cap_fatt', 'citta_fatt', 'nazione_fatt');
			// campi per edit
			$crud->edit_fields('id_cliente', 'indirizzo_fatt', 'civico_fatt', 'cap_fatt', 'citta_fatt', 'nazione_fatt');
			$crud->required_fields('indirizzo_fatt', 'civico_fatt', 'cap_fatt', 'citta_fatt', 'nazione_fatt');
			$crud->field_type('id_cliente', 'hidden', $cliente_id);
			
			if($crud->getState() == 'add'){
				
			} 
			else if($crud->getState() == 'edit')
			{
				
			}
			// callbacks
			$crud->callback_after_insert(array($this, '_update_fatturazione_cliente'));
			//$crud->callback_after_update(array($this, '_callback_update_billing'));
   			
			$crud->set_lang_string('insert_success_message',
			  'I dati sono stati correttamente inseriti.<br/>Attendi il caricamento della pagina.
			 <script type="text/javascript">
			  window.location = "'.site_url('admin/customers').'";
			 </script>
			 <div style="display:none">'
   			);
			
			$crud->set_lang_string('update_success_message',
				 'I dati sono stati correttamente inseriti.<br/>Attendi il caricamento della pagina.
				 <script type="text/javascript">
				  window.location = "'.site_url('admin/customers').'";
				 </script>
				 <div style="display:none">'
    		);
			
			$crud->unset_back_to_list();
			
			$output = $crud->render();

			$data['curr_page'] = 'ADMIN-CUSTOMERS';
			$data['curr_page_title'] = 'Clienti';
			$data['collapseParentMenu'] = 'Clienti';
			$data['resourcetype'] = 'CRUD';
			$data['curr_function_title'] = 'Indirizzo fatturazione per <b>' . $cliente->cognome . ' ' . $cliente->nome .'</b>';
			$output->data = $data;
			$this->load->view('admin/customers_shipping',(array)$output);
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
	
	// imposta nuovo indirizzo fatturazione per il cliente selezionato
	function _update_fatturazione_cliente($post_array, $primary_key)
	{		
		$data = array(
		   'id_indirizzo_fatturazione' => $primary_key
		);

		$this->db->where('id_cliente', $post_array['id_cliente']);
		$this->db->update('clienti', $data);

		return true;
	}
	
}
