<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Productcolors extends Admin_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
	}
	
	public function crud()
	{
		$this->checkUserPermissions();
		//CRUD 
		try{
			$crud = new grocery_CRUD();
			// tema
			$crud->set_theme('bootstrap');
			// tabella
			$crud->set_table('colori_prodotti');
			// nome in tabella
			$crud->display_as('nome_colore', 'Nome colore');
			$crud->display_as('codice_colore', 'Codice colore');
			// campi obbligatori
			$crud->required_fields('nome_colore', 'codice_colore');
			// colonne da mostrare
			$crud->columns('nome_colore', 'codice_colore');
			// unset delete action
			$crud->unset_delete();
	
			$output = $crud->render();

			$data['curr_page'] = 'ADMIN-PRODUCTCOLORS';
			$data['curr_page_title'] = 'Prodotti';
			$data['collapseParentMenu'] = 'prodotti';
			$data['resourcetype'] = 'CRUD';
			$output->data = $data;
			$this->load->view('admin/productcolors',(array)$output);
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
}
