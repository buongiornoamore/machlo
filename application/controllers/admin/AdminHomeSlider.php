<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class AdminHomeSlider extends Admin_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
	}
	
	public function crud()
	{
		$this->checkUserPermissions();
		//CRUD
		try{
			$crud = new grocery_CRUD();
			// tema
			$crud->set_theme('bootstrap');
			// tabella
			$crud->set_table('home_slider');
			$crud->order_by('hs_order', 'asc');
			$crud->order_by('id_lingua', 'asc');
			// nome in tabella
			$crud->display_as('hs_order', 'Ordine');
			$crud->display_as('hs_image', 'Immagine');
			$crud->display_as('hs_title', 'Titolo');
			$crud->display_as('id_lingua', 'Lingua');
			// file upload
			$crud->set_field_upload('hs_image', 'assets/assets-frontend/img/hero-slider');
			// realazioni join
			$crud->set_relation('id_lingua', 'lingue', 'nome_lingue');
	    	// campi obbligatori
			$crud->required_fields('hs_image', 'hs_order', 'id_lingua');
			// regole validazione campi
		//	$crud->set_rules('prezzo', 'Prezzo', 'required|numeric');
		//	$crud->set_rules('prezzo_scontato', 'Prezzo scontato', 'numeric');
			// campi per add
		//	$crud->add_fields('categorie', 'id_tipo_prodotto', 'codice', 'nome', 'prezzo', 'prezzo_scontato', 'stato', 'id_colori_prodotti', 'taglie', 'tags', 'ordine');
		//	$crud->edit_fields('categorie', 'id_tipo_prodotto', 'codice', 'nome', 'prezzo', 'prezzo_scontato', 'stato', 'id_colori_prodotti', 'taglie', 'tags', 'ordine', 'url_img_piccola', 'url_img_grande', 'url_img_grande_retro');
			//if($crud->getState() == 'add')
    	//	{
		//		$crud->field_type('stato', 'dropdown', 3); // stato sospeso
		//	} 
			// callbacks
		//	$crud->callback_before_insert(array($this,'formatProdottoCodice'));
		//	$crud->callback_before_update(array($this,'formatProdottoCodice'));
			// colonne da mostrare
			$crud->columns('hs_image', 'hs_title', 'id_lingua', 'hs_order');
			// unset delete action
			// $crud->unset_delete();
			// custom action
		//	$crud->add_action('Traduzioni immagine', '', '', 'fa-file-text', array($this, 'load_traduzioni'));
			
			$output = $crud->render();

			$data['curr_page'] = 'ADMIN-HOMESLIDER';
			$data['curr_page_title'] = 'Home slider';
			$data['collapseParentMenu'] = 'configurazioni';
			$data['resourcetype'] = 'CRUD';
			$output->data = $data;
			$this->load->view('admin/homeslider',(array)$output);
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}

	function load_traduzioni($primary_key , $row)
	{
		return site_url('admin/galleryimages/trad/'.$row->id_ig.'/'.$row->nome_ig);
	}

	public function traductions($ig_id, $ig_name)
	{
		// CRUD traduzioni
		try {
			$crud = new grocery_CRUD();
			// tema
			$crud->set_theme('bootstrap');
			// tabella
			$crud->set_table('immagini_gallery_traduzioni');
			$crud->where('id_ig', $ig_id);
			// nome in tabella
			$crud->display_as('titolo_ig', 'Titolo immagine');
			$crud->display_as('testo_ig', 'Testo immagine');
			$crud->display_as('id_lingua', 'Lingua');
			// realazioni join
			$crud->set_relation('id_lingua', 'lingue', 'nome_lingue');
			// campi obbligatori
			$crud->required_fields('titolo_ig', 'testo_ig', 'id_lingua');
			$crud->edit_fields('titolo_ig', 'testo_ig', 'id_lingua');
			$crud->add_fields('id_ig', 'titolo_ig', 'testo_ig', 'id_lingua');
			// colonne da mostrare
			$crud->columns('titolo_ig', 'testo_ig', 'id_lingua');
			// hidden fileds
			$crud->field_type('id_ig', 'hidden', $ig_id);
			$output = $crud->render();
			
			$data['curr_page'] = 'ADMIN-GALLERY-IMAGES';
			$data['curr_page_title'] = 'Gallery';
			$data['collapseParentMenu'] = 'gallery';
			$data['curr_function_title'] = 'Traduzioni per immagine <b>' . urldecode($ig_name) . '</b>';
			$data['resourcetype'] = 'CRUD';
			$output->data = $data;
			$this->load->view('admin/galleryimages_trad',(array)$output);
			
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
	
}
