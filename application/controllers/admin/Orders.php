<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/** PRINTFULL API ***/
use Printful\Exceptions\PrintfulApiException;
use Printful\Exceptions\PrintfulException;
use Printful\PrintfulApiClient;

require_once(APPPATH . 'libraries/Printful/vendor/autoload.php');
/*******************/

class Orders extends Admin_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
	}
	
	// Sincronizza lo stato e le info degli ordini presenti su printful
	public function syncPrintfulOrders()
	{
		$pf = new PrintfulApiClient(PRINTFUL_APIKEY);
		$print_debug = false; // stampa echo di debug
		$result = 'ERRORE';
		
		// verifica se il sync è libero altrimenti attendi
		$this->db->from('sync_test');
		$this->db->where('id_sync_test', 2);
		$syncTest = $this->db->get()->row();
		
		date_default_timezone_set("Europe/Rome"); //set you countary name from below timezone list
		$dateNow = date("Y-m-d H:i:s", time()); //now
				
		if($syncTest->sync_status != 'PROGRESS') {
			
			//update sync test PROGRESS			
			$data_synctest_var = array(
			   'sync_status' => 'PROGRESS',
			   'sync_type' => 'orders',
			   'time_sync_test' => $dateNow
			);
			$this->db->where('id_sync_test', 2);
			$this->db->update('sync_test', $data_synctest_var);
			
			if($print_debug) 
				echo '*** START SYNC ORDERS at ' . $dateNow . '<br>';
				
			try {
				
				// retrieve all orders locally
				$status_excluded = array(3, 4);

				$this->db->select('*');
				$this->db->from('ordini');
				$this->db->where_not_in('stato_ordine', $status_excluded);
				$query_ordine = $this->db->get();
				
				foreach ($query_ordine->result() as $ordineLocal) {
					//$order_id = 3861492;
					//$order = $pf->get('orders/'.$order_id);
					$order_id = $ordineLocal->id_ordine;
					$order = $pf->get('orders/@'.$order_id);
					if($print_debug) {
						echo 'ORDER STATUS: ' . $ordineLocal->stato_ordine.'<br>';
						echo 'ID: ' . $order['id'].'<br>';
						echo 'EXTERNAL ID: ' . $order['external_id'].'<br>';
						echo 'STATUS: ' . $order['status'].'<br>';
						echo 'SHIPPING: ' . $order['shipping'].'<br>';
					}
					// update orders state infos
					$data_order_update = array(
					   'printful_status' => $order['status'],
					   'printful_last_update' => $dateNow
					);
				
					$this->db->where('id_ordine', $order_id);
					$this->db->update('ordini', $data_order_update);
					
					// update and sync shipments table
					if(count($order['shipments']) > 0) {
						//echo '<pre>';
						//var_dump($order['shipments']);
						foreach ($order['shipments'] as $ship) {
							if($print_debug) {
								if($ship['ship_date'] != '') 
									echo 'SHIPMENT STATUS: ' . $ship['ship_date'].'<br>';
								if($ship['status'] != '') 
									echo 'SHIPMENT STATUS: ' . $ship['status'].'<br>';
								if($ship['tracking_number'] != '') 	
									echo 'TRACKING NUMBER: ' . $ship['tracking_number'].'<br>';
								if($ship['tracking_url'] != '') 	
									echo 'TRACKING URL: ' . $ship['tracking_url'].'<br>';
							}
							// update ship info
							$this->db->where('ship_printful_id', $ship['id']);
							$q = $this->db->get('shipments');
							
							if ( $q->num_rows() > 0 ) {
								// update
								$data_update = array(
								   'carrier' => $ship['carrier'],
								   'service' => $ship['service'],
								   'tracking_number' => $ship['tracking_number'],
								   'tracking_url' => $ship['tracking_url'],
								   'created' => $ship['created'],
								   'ship_date' => $ship['ship_date'],
								   'reshipment' => $ship['reshipment'],
								   'location' => $ship['location'],
								   'items' => count($ship['items']),
								   'last_update_time' => $dateNow
								);
							
								$this->db->where('ship_printful_id', $ship['id']);
								$this->db->update('shipments', $data_update);
							} else {
								// insert
								$data_insert = array(
								   'ship_id' => NULL,
								   'id_ordine' => $order_id,
								   'ship_printful_id' => $ship['id'],
								   'ship_type' => $order['shipping'],
								   'carrier' => $ship['carrier'],
								   'service' => $ship['service'],
								   'tracking_number' => $ship['tracking_number'],
								   'tracking_url' => $ship['tracking_url'],
								   'created' => $ship['created'],
								   'ship_date' => $ship['ship_date'],
								   'reshipment' => $ship['reshipment'],
								   'location' => $ship['location'],
								   'items' => count($ship['items']),
								   'last_update_time' => $dateNow
								 );
					
								 $this->db->insert('shipments', $data_insert);
							}
						}
					}
				}
				
				//update sync test PROGRESS			
				$data_synctest_var_completed = array(
				   'sync_status' => 'COMPLETED'
				);
				$this->db->where('id_sync_test', 2);
				$this->db->update('sync_test', $data_synctest_var_completed);
				
				$result = 'SUCCESS';
				
			} catch (PrintfulApiException $e) { //API response status code was not successful
				//echo 'Printful API Exception: ' . $e->getCode() . ' ' . $e->getMessage();
				$result = 'ERRORE Printful API Exception: ' . $e->getCode() . ' ' . $e->getMessage();
			} catch (PrintfulException $e) { //API call failed
				//echo 'Printful Exception: ' . $e->getMessage();
				//var_export($pf->getLastResponseRaw());
				$result = 'ERRORE Printful Exception: ' . $e->getMessage();
			}	
		
		} else {// end synctest if
			if($print_debug) 
				echo 'Sync not executed because test status is ' . $syncTest->sync_status;
			$result = 'BUSY';
		}
		
		if($print_debug) 
			echo '*** END SYNC ORDERS at ' . date("Y-m-d H:i:s", time()); //now
		
		echo $result;
	}
	
	// Invia l'ordine a printfull
	public function sendPrintfulOrder($id_ordine)
	{
		//$apiKey = 'naq7o8kj-6x61-iahm:qqv6-cad6lshspq29'; // machlo
		$pf = new PrintfulApiClient(PRINTFUL_APIKEY);
		$print_debug = false; // stampa echo di debug
		$result = 'error';
		try {
			
			date_default_timezone_set("Europe/Rome"); //set you countary name from below timezone list
	    	$dateNow = date("Y-m-d H:i:s", time()); //now
			
			// Recupera dati completi dell'ordine del sito 
			$this->db->select('*');
			$this->db->from('ordini');
			$this->db->join('clienti', 'clienti.id_cliente = ordini.id_cliente');
			$this->db->where('id_ordine', $id_ordine);
			
			$query_ordine = $this->db->get();
			$ordine = $query_ordine->row();
			
			// Recupera info indirizzo di spedizione
			$sped_customer_name = '';
			$sped_address = '';
			$sped_city = '';
			$sped_country = '';
			$sped_country_code = '';
			$sped_zip = '';
			$sped_phone = $ordine->telefono;
			$sped_email = $ordine->email;
			if($ordine->id_indirizzo_fatturazione_spedizione > 0) {
				$this->db->select('*');
				$this->db->from('indirizzo_fatturazione');
				$this->db->join('countries', 'countries.country_code = indirizzo_fatturazione.nazione_fatt');
				$this->db->where('indirizzo_fatturazione.id_indirizzo_fatturazione', $ordine->id_indirizzo_fatturazione_spedizione);
				
				$query_sped = $this->db->get();
				$sped = $query_sped->row();
				
				$sped_customer_name = ($sped->riferimento_fatt != '' ? $sped->riferimento_fatt : $ordine->cognome . ' ' . $ordine->nome);
				$sped_address = $sped->indirizzo_fatt.', '.$sped->civico_fatt;
				$sped_city = $sped->citta_fatt;
				$sped_country = $sped->country_name;
				$sped_country_code = $sped->nazione_fatt;
				$sped_zip = $sped->cap_fatt;
			} else {
				$this->db->select('*');
				$this->db->from('indirizzo_spedizione');
				$this->db->join('countries', 'countries.country_code = indirizzo_spedizione.nazione_sped');
				$this->db->where('.indirizzo_spedizioneid_indirizzo_spedizione', $ordine->id_indirizzo_spedizione);
				
				$query_sped = $this->db->get();
				$sped = $query_sped->row();
				
				$sped_customer_name = ($sped->riferimento_sped != '' ? $sped->riferimento_sped : $ordine->cognome . ' ' . $ordine->nome);
				$sped_address = $sped->indirizzo_sped.', '.$sped->civico_sped;
				$sped_city = $sped->citta_sped;
				$sped_country = $sped->country_name;
				$sped_country_code = $sped->nazione_sped;
				$sped_zip = $sped->cap_sped;
			}
			
			// Carica da storico carrello e recupera dal sync di printfull tutte le varianti complete
			$query_storico = $this->db->where('id_ordine', $id_ordine)->get('storico_carrello');
			$itemsArray = array();
			foreach ($query_storico->result() as $storico)
			{
				// recupera variate da printfull
				$data_var = $pf->get('sync/variant/@'.$storico->codice_variante); 
				
				$varItemArray = [
							'variant_id' => $data_var['sync_variant']['variant_id'],
							'external_id' => $storico->codice_variante, // Blu cat - Black / S
							'name' => $data_var['sync_variant']['name'], // Display name
					//		'retail_price' => '29.99', // Retail price for packing slip
							'quantity' => $storico->qty,
							'files' => $data_var['sync_variant']['files'],
						];
						
				array_push($itemsArray, $varItemArray);	
			}
			
			// Invia ordine API
			$order = $pf->post('orders',
				[
					'external_id' => $ordine->id_ordine, // id order number
					'recipient' => [
						'name' => $sped_customer_name,
						'address1' => $sped_address,
						'city' => $sped_city,
						'country_code' => $sped_country_code,
						'country_name' => $sped_country,
						'zip' => $sped_zip,
						'phone' => $sped_phone,
						'email' => $sped_email,
					],
					'retail_costs' => [
						'total' => $ordine->totale_ordine,
					],
					'items' => $itemsArray,
					'currency' => 'EUR'
				],
				[
				'update_existing' => 1, // sovrescrive se esiste external_id
				'confirm' => 0 // conferma ordine
				] 
			);

			// Update ordine e stato da printfull 
			$data_update = array(
			   'printful_status' => $order['status'],
			   'printful_id' => $order['id'],
			   'printful_last_update' => $dateNow
			);
			$this->db->where('id_ordine', $id_ordine);
			$this->db->update('ordini', $data_update);

			if($print_debug) {
				echo '<pre>';
				echo json_encode($order, JSON_PRETTY_PRINT);
				//var_export($order);
			}
			
			$result = 'SUCCESS';
		} catch (PrintfulApiException $e) { //API response status code was not successful
			//echo 'Printful API Exception: ' . $e->getCode() . ' ' . $e->getMessage();
			$result = 'ERRORE Printful API Exception: ' . $e->getCode() . ' ' . $e->getMessage();
		} catch (PrintfulException $e) { //API call failed
			//echo 'Printful Exception: ' . $e->getMessage();
			//var_export($pf->getLastResponseRaw());
			$result = 'ERRORE Printful Exception: ' . $e->getMessage();
		}	
		
		echo $result;
	}
	
	// Conferma ordine draft a printfull
	public function confirmorder($id_ordine)
	{
		//$apiKey = 'naq7o8kj-6x61-iahm:qqv6-cad6lshspq29'; // machlo
		$pf = new PrintfulApiClient(PRINTFUL_APIKEY);
		$print_debug = false; // stampa echo di debug
		$result = 'error';
		try {
			
			date_default_timezone_set("Europe/Rome"); //set you countary name from below timezone list
	    	$dateNow = date("Y-m-d H:i:s", time()); //now
			
			// Recupera dati completi dell'ordine del sito 
			$this->db->select('*');
			$this->db->from('ordini');
			$this->db->join('clienti', 'clienti.id_cliente = ordini.id_cliente');
			$this->db->where('id_ordine', $id_ordine);
			
			$query_ordine = $this->db->get();
			$ordine = $query_ordine->row();
			
			if($ordine->printful_status == 'draft') {
				
				$order = $pf->post('orders/'.$ordine->printful_id.'/confirm'); 
				
				// Update ordine e stato da printfull 
				$data_update = array(
				   'printful_status' => $order['status'],
				   'printful_last_update' => $dateNow
				);
				$this->db->where('id_ordine', $id_ordine);
				$this->db->update('ordini', $data_update);
					
				$result = 'SUCCESS';	
			
			} else {
			
				$result = 'ERRORE l\'ordine #'.$id_ordine.' non è in stato DRAFT e quindi non può essere confermato';
			
			}
		
		} catch (PrintfulApiException $e) { //API response status code was not successful
			//echo 'Printful API Exception: ' . $e->getCode() . ' ' . $e->getMessage();
			$result = 'ERRORE Printful API Exception: ' . $e->getCode() . ' ' . $e->getMessage();
		} catch (PrintfulException $e) { //API call failed
			//echo 'Printful Exception: ' . $e->getMessage();
			//var_export($pf->getLastResponseRaw());
			$result = 'ERRORE Printful Exception: ' . $e->getMessage();
		}	
		
		echo $result;
	}
	
	public function crud()
	{
		$this->checkUserPermissions();
		//CRUD
		try{
			$crud = new grocery_CRUD();
			// tema
			$crud->set_theme('bootstrap');
			// tabella
			$crud->set_table('ordini');
			// nome in tabella
			$crud->display_as('id_ordine', '# Ordine');
			$crud->display_as('id_cliente', 'Cliente');
			$crud->display_as('data_ordine', 'Data ordine');
			$crud->display_as('totale_ordine', 'Totale ordine');
			$crud->display_as('tipo_pagamento', 'Pagamento');
			$crud->display_as('stato_ordine', 'Stato ordine');
			$crud->display_as('printful_status', 'Stato Printful');
			$crud->display_as('printful_id', 'ID Printful');
			$crud->display_as('printful_last_update', 'Sincronizzazione Printful');
			$crud->display_as('id_indirizzo_spedizione', 'Indirizzo spedizione');
			$crud->display_as('id_indirizzo_fatturazione_spedizione', 'Spedisci a indirizzo di fatturazione');
			$crud->display_as('coupon_code', 'Codice coupon');
			// file upload
			// realazioni join
			$crud->set_relation('stato_ordine', 'stato_ordine', 'desc_stato_ordine');
			$crud->set_relation('stato_pagamento', 'stato_pagamento', 'desc_stato_pagamento');
			// campi obbligatori
			// campi per add
			$crud->add_fields('id_cliente', 'id_indirizzo_fatturazione_spedizione', 'id_indirizzo_spedizione', 'stato_ordine', 'data_ordine', 'totale_ordine', 'note_ordine', 'tipo_pagamento', 'stato_pagamento', 'punti');
		
			$crud->edit_fields('id_ordine', 'id_cliente', 'id_indirizzo_spedizione', 'id_indirizzo_fatturazione_spedizione', 'stato_ordine', 'data_ordine', 'totale_ordine', 'note_ordine', 'tipo_pagamento', 'stato_pagamento', 'token_pagamento', 'punti', 'coupon_code', 'printful_status', 'printful_id', 'printful_last_update');
			
			$crud->required_fields('id_cliente', 'data_ordine', 'tipo_pagamento', 'stato_pagamento', 'stato_ordine');
			
			if($crud->getState() == 'add')
    		{
				$crud->set_relation('id_cliente', 'clienti', '#{id_cliente} - {cognome} {nome}');
				$crud->set_relation('id_indirizzo_spedizione', 'indirizzo_spedizione', 'note_sped');
				$crud->set_relation('tipo_pagamento', 'tipo_pagamento', 'desc_tipo_pagamento');
				$crud->set_relation('stato_pagamento', 'stato_pagamento', 'desc_stato_pagamento');
				$crud->set_relation('stato_ordine', 'stato_ordine', 'desc_stato_ordine');		
				//$crud->field_type('id_indirizzo_fatturazione_spedizione', 'true_false', array( "NULL"  => "No", "1" => "Si"));
			} 
			else if($crud->getState() == 'edit')
			{
				// carica ordine dal db
				$this->db->join('clienti', 'clienti.id_cliente = ordini.id_cliente');
				$this->db->where('id_ordine', $crud->getStateInfo()->primary_key);
				$this->db->from('ordini');
				$query_if = $this->db->get();
				$ordine_if = $query_if->row();

				$crud->change_field_type('id_ordine', 'readonly');
				$crud->set_relation('id_indirizzo_spedizione', 'indirizzo_spedizione', '{riferimento_sped} | {indirizzo_sped}, {civico_sped} - {cap_sped} {citta_sped} [{nazione_sped}]');
				$crud->set_relation('id_indirizzo_fatturazione_spedizione', 'indirizzo_fatturazione', '{riferimento_fatt} | {indirizzo_fatt}, {civico_fatt} - {cap_fatt} {citta_fatt} [{nazione_fatt}]', array('id_indirizzo_fatturazione' => $ordine_if->id_indirizzo_fatturazione));
				$crud->set_relation('id_cliente', 'clienti', '#{id_cliente} - {cognome} {nome}');
				$crud->set_relation('tipo_pagamento', 'tipo_pagamento', 'desc_tipo_pagamento');
				$crud->change_field_type('token_pagamento', 'readonly');
				$crud->change_field_type('coupon_code', 'readonly');
				$crud->change_field_type('printful_status', 'readonly');
				$crud->change_field_type('printful_id', 'readonly');
				$crud->change_field_type('printful_last_update', 'readonly');
			}
			else if($crud->getState() == 'read')
			{
				$crud->display_as('token_pagamento', 'Carrello');
				$crud->set_relation('id_cliente', 'clienti', '#{id_cliente} - {cognome} {nome}');
				$crud->set_relation('tipo_pagamento', 'tipo_pagamento', 'desc_tipo_pagamento');
				$crud->set_relation('stato_pagamento', 'stato_pagamento', 'desc_stato_pagamento');
				$crud->set_relation('stato_ordine', 'stato_ordine', 'desc_stato_ordine');		
				$crud->set_relation_n_n('carrello', 'storico_carrello', 'prodotti', 'id_ordine', 'id_prodotto', '{prodotti.nome} x {qty}', 'qty');	
				$crud->change_field_type('coupon_code', 'readonly');
			//	$crud->fields('token_pagamento');
			//	$crud->callback_field('token_pagamento', array($this, '_callback_read_carrello'));	
			}
			// text editor
			$crud->unset_texteditor('note_ordine');
			// colonne da mostrare
			$crud->columns('id_ordine', 'id_cliente', 'data_ordine', 'totale_ordine', 'tipo_pagamento', 'stato_ordine', 'printful_status');
			// unset action
			$crud->unset_delete();
			$crud->unset_read();
			// custom action
			$crud->add_action('Invia a Printful', '', '', 'fa-share-square send-to-printful-btn', array($this, 'send_order_printful'));
			$crud->add_action('Conferma a Printful', '', '', 'fa-check-square confirm-to-printful-btn', array($this, 'confirm_order_printful'));
			$crud->add_action('Invia email al cliente', '', '', 'fa-envelope icon-color-orange', array($this, 'load_email_page'));
			$crud->add_action('Carrello', '', '', 'fa-shopping-cart icon-color-blue', array($this, 'load_carrello'));
			$crud->add_action('Dettaglio storico', '', '', 'fa-print icon-color-purple', array($this, 'dettaglio_ordine'));
			$crud->add_action('Spedizioni', '', '', 'fa-truck icon-color-green', array($this, 'load_shipments'));
			// callbacks
			$crud->callback_column('id_cliente', array($this, '_callback_cliente_info'));
			$crud->callback_column('totale_ordine', array($this, '_callback_value_to_euro'));
			$crud->callback_column('tipo_pagamento', array($this, '_callback_pagamento'));
			$crud->callback_after_delete(array($this,'_delete_order_cart'));
			$crud->callback_after_insert(array($this, '_insert_ordine_storico_clienti'));
			$crud->callback_after_update(array($this, '_update_ordine_storico_clienti'));
		//	$crud->callback_add_field('gender',array($this,'add_field_callback_1'));
		//	$crud->callback_before_update(array($this, '_callback_before_update'));
			$crud->callback_add_field('id_indirizzo_fatturazione_spedizione', array($this, '_fatturazione_spedizione_field'));
				
			$output = $crud->render();

			$data['curr_page'] = 'ADMIN-ORDERS';
			$data['curr_page_title'] = 'Ordini';
			$data['collapseParentMenu'] = 'ordini';
			$data['resourcetype'] = 'CRUD';
			$output->data = $data;
			$this->load->view('admin/orders',(array)$output);
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
	
	function _fatturazione_spedizione_field() {
 		return  ' <input type="checkbox" id="id_indirizzo_fatturazione_spedizione-check" name="id_indirizzo_fatturazione_spedizione" value="1" />&nbsp;&nbsp;&nbsp;(spedisci all\'indirizzo di fatturazione del cliente)';
    }
	
	function _insert_ordine_storico_clienti($post_array, $primary_key) {
		$record = $this->db->query('SELECT LAST_INSERT_ID()')->row_array();
		$insert_id = $record['LAST_INSERT_ID()'];
		// salva sul db i dati nuovi dell'ordine
		$this->db->select('ordini.*, clienti.id_indirizzo_fatturazione AS id_indirizzo_fatturazione_selected, clienti.cognome AS cognome, clienti.nome AS nome, clienti.email AS email, clienti.telefono AS telefono, clienti.partita_iva AS partita_iva, clienti.codice_fiscale AS codice_fiscale, clienti.id_lingua AS id_lingua');
		$this->db->from('ordini');
		$this->db->join('clienti', 'clienti.id_cliente = ordini.id_cliente');
		$this->db->where('id_ordine', $insert_id);

		$query = $this->db->get();
		$ordine = $query->row();
		
		// verifica e imposta indirizzo di fatturazione come indirizzo di spedizione
		$id_indirizzo_fatturazione_selected = 0;
		if(isset($ordine->id_indirizzo_fatturazione_spedizione) && $ordine->id_indirizzo_fatturazione_spedizione != NULL) {
			// recuperalo dal cliente 
			$id_indirizzo_fatturazione_selected = $ordine->id_indirizzo_fatturazione_selected;
			// UPDATE ORDINE
			$this->db->where('id_ordine', $insert_id);
			$this->db->update('ordini', array('id_indirizzo_fatturazione_spedizione' =>$id_indirizzo_fatturazione_selected));
		}
		
		// indirizzo di fatturazione
		$this->db->select('*');
		$this->db->from('indirizzo_fatturazione');
		$this->db->where('id_indirizzo_fatturazione', $id_indirizzo_fatturazione_selected);

		$query_fatt = $this->db->get();
		$ind_fatt = $query_fatt->row();
		
		$indirizzo_fatt_storico = $ind_fatt->riferimento_fatt . ' | ' . $ind_fatt->indirizzo_fatt . ', ' . $ind_fatt->civico_fatt . ' - ' . $ind_fatt->cap_fatt . ' ' . $ind_fatt->citta_fatt. ' (' . $ind_fatt->nazione_fatt . ')';
		
		$indirizzo_sped_storico = '';
		$indirizzo_sped_storico_note = '';
		if(isset($ordine->id_indirizzo_fatturazione_spedizione) && $ordine->id_indirizzo_fatturazione_spedizione != NULL) {
			// uguale a fatturazione
			$indirizzo_sped_storico = $indirizzo_fatt_storico;
			$indirizzo_sped_storico_note = $ind_fatt->note_fatt;
		} else {
			// carica indirizzo di spedizione
			// indirizzo di fatturazione
			$this->db->select('*');
			$this->db->from('indirizzo_spedizione');
			$this->db->where('id_indirizzo_spedizione', $ordine->id_indirizzo_spedizione);
	
			$query_sped = $this->db->get();
			$ind_sped = $query_sped->row();
			
			$indirizzo_sped_storico = $ind_sped->riferimento_sped . ' | ' . $ind_sped->indirizzo_sped . ', ' . $ind_sped->civico_sped . ' - ' . $ind_sped->cap_sped . ' ' . $ind_sped->citta_sped. ' (' . $ind_sped->nazione_sped . ')';
			$indirizzo_sped_storico_note = $ind_sped->note_sped;
		} 
					
		$data = array(
		   'id_storico_clienti' => NULL,
		   'id_ordine' => $insert_id,
		   'cognome' => $ordine->cognome,
		   'nome' => $ordine->nome,
		   'email' => $ordine->email,
		   'telefono' => $ordine->telefono,
		   'indirizzo_fatturazione' => $indirizzo_fatt_storico,
		   'indirizzo_spedizione' => $indirizzo_sped_storico,
		   'note_indirizzo_fatturazione' => $ind_fatt->note_fatt,
		   'note_indirizzo_spedizione' => $indirizzo_sped_storico_note,
		   'partita_iva' => $ordine->partita_iva,
		   'codice_fiscale' => $ordine->codice_fiscale,
		   'id_lingua' => $ordine->id_lingua
		);
	
		$this->db->insert('storico_clienti', $data);
	}
	
	function _update_ordine_storico_clienti($post_array, $primary_key) {
		// salva sul db i dati nuovi dell'ordine
		$this->db->select('*');
		$this->db->from('ordini');
		$this->db->join('clienti', 'clienti.id_cliente = ordini.id_cliente');
		$this->db->where('id_ordine', $primary_key);

		$query = $this->db->get();
		$ordine = $query->row();
		
		// indirizzo di fatturazione
		$this->db->select('*');
		$this->db->from('indirizzo_fatturazione');
		$this->db->where('id_indirizzo_fatturazione', $ordine->id_indirizzo_fatturazione);

		$query_fatt = $this->db->get();
		$ind_fatt = $query_fatt->row();
		
		$indirizzo_fatt_storico = $ind_fatt->riferimento_fatt . ' | ' . $ind_fatt->indirizzo_fatt . ', ' . $ind_fatt->civico_fatt . ' - ' . $ind_fatt->cap_fatt . ' ' . $ind_fatt->citta_fatt. ' (' . $ind_fatt->nazione_fatt . ')';
		
		$indirizzo_sped_storico = '';
		$indirizzo_sped_storico_note = '';
		if(isset($ordine->id_indirizzo_fatturazione_spedizione) && $ordine->id_indirizzo_fatturazione_spedizione != NULL) {
			// uguale a fatturazione
			$indirizzo_sped_storico = $indirizzo_fatt_storico;
			$indirizzo_sped_storico_note = $ind_fatt->note_fatt;
		} else {
			// carica indirizzo di spedizione
			// indirizzo di fatturazione
			$this->db->select('*');
			$this->db->from('indirizzo_spedizione');
			$this->db->where('id_indirizzo_spedizione', $ordine->id_indirizzo_spedizione);
	
			$query_sped = $this->db->get();
			$ind_sped = $query_sped->row();
			
			$indirizzo_sped_storico = $ind_sped->riferimento_sped . ' | ' . $ind_sped->indirizzo_sped . ', ' . $ind_sped->civico_sped . ' - ' . $ind_sped->cap_sped . ' ' . $ind_sped->citta_sped. ' (' . $ind_sped->nazione_sped . ')';
			$indirizzo_sped_storico_note = $ind_sped->note_sped;
		} 
					
		$data = array(
		   'cognome' => $ordine->cognome,
		   'nome' => $ordine->nome,
		   'email' => $ordine->email,
		   'telefono' => $ordine->telefono,
		   'indirizzo_fatturazione' => $indirizzo_fatt_storico,
		   'indirizzo_spedizione' => $indirizzo_sped_storico,
		   'note_indirizzo_fatturazione' => $ind_fatt->note_fatt,
		   'note_indirizzo_spedizione' => $indirizzo_sped_storico_note,
		   'partita_iva' => $ordine->partita_iva,
		   'codice_fiscale' => $ordine->codice_fiscale,
		   'id_lingua' => $ordine->id_lingua
		);
	
		$this->db->where('id_ordine', $primary_key);
		$this->db->update('storico_clienti', $data);
	}
	
	public function email($order_id) {
		$this->checkUserPermissions();
		//CRUD contatti email
		try {
			// load cliente da ordine
		//	$this->db->from('contatti_moduli');
		//	$this->db->join('lingue', 'lingue.id_lingue = contatti_moduli.id_lingua');
		//	$this->db->where('id_contatto', $cont_id);
		//	$query = $this->db->get();
		//	$cont = $query->row();
			
			// recupera la mail del cliente con una query e poi passala come href
		    // verificare se user_id allora prendi email da users altrimenti prendi da clienti
			$this->db->select('ordini.*, lingue.*, clienti.email AS email_cliente, users.email AS email_user, clienti.nome AS nome');
			$this->db->from('ordini');
			$this->db->join('clienti', 'clienti.id_cliente = ordini.id_cliente');
			$this->db->join('lingue', 'lingue.id_lingue = clienti.id_lingua');
			$this->db->join('users', 'users.id = clienti.user_id', 'LEFT');
			$this->db->where('ordini.id_ordine', $order_id);
			$query_cliente = $this->db->get();
			$cliente = $query_cliente->row();
			$email_to = ($cliente->email_cliente != '' ? $cliente->email_cliente : $cliente->email_user);
			
		//	$this->cont_id = $cont->id_contatto;
		//    $this->cont_email = $cont->email;
		//	$this->cont_nome = $cont->nome;
		//	$this->cont_lingua = $cont->id_lingua;
			
			$crud = new grocery_CRUD();

			// tema
			$crud->set_theme('bootstrap');
			// tabella
			$crud->set_table('email_templates');
			$crud->where('lingua_traduzione_id', $cliente->id_lingue);
			// nome in tabella
			$crud->display_as('lingua_traduzione_id', 'Lingua');
			// realazioni join
			$crud->set_relation('lingua_traduzione_id', 'lingue', 'nome_lingue');
			// colonne da mostrare
			$crud->columns('nome_template', 'lingua_traduzione_id');
			// unset delete action
			$crud->unset_delete();
			$crud->unset_edit();
			$crud->unset_read();
			$crud->add_action('Invia email', '', '', 'fa-envelope', array($this, 'send_email_templates'));
			$crud->add_action('Preview email', '', '', 'fa-html5', array($this, 'preview_email_templates'));
		//	$crud->field_type('id_contatto', 'hidden', $cont_id);
			$output = $crud->render();

			$data['curr_page'] = 'ADMIN-ORDERS';
			$data['curr_page_title'] = 'Ordini';
			$data['collapseParentMenu'] = 'ordini';
			$data['curr_function_title'] = 'Email per <b>' . $email_to . '</b> - ' . $cliente->nome_lingue;
			$data['curr_customer_email'] = $email_to;
			$data['curr_customer_id'] = 0;
			$data['curr_customer_name'] = $cliente->nome;
			$data['curr_customer_lang'] = $cliente->id_lingue;
			$data['resourcetype'] = 'CRUD';
			$output->data = $data;
			$this->load->view('admin/orders_email',(array)$output);
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
	
	function send_email_templates($primary_key, $row)
	{
		return site_url('admin/send_email_template/'.$row->id_template);
	}
	
	function preview_email_templates($primary_key, $row)
	{
		return site_url('email_template/'.$row->id_template.'/testmail');
	}
	
	function load_email_page($primary_key, $row)
	{	
		return site_url('admin/orders/email/'.$row->id_ordine);
	}
	
	function send_order_printful($primary_key, $row)
	{	
		return site_url('admin/orders/sendorder/'.$row->id_ordine);
	}
	
	function confirm_order_printful($primary_key, $row)
	{	
		return site_url('admin/orders/confirmorder/'.$row->id_ordine);
	}
	
	function load_carrello($primary_key , $row)
	{
		return site_url('admin/orders/cart/'.$row->id_ordine);
	}
	
	function load_shipments($primary_key , $row)
	{
		return site_url('admin/orders/shipments/'.$row->id_ordine);
	}
	
	function dettaglio_ordine($primary_key , $row)
	{
		return site_url('admin/orders/detail/'.$row->id_ordine);
	}
	
	function _callback_value_to_euro($value, $row)
	{
		return stampaValutaHtml($value, true, true);
	}
	
	function _callback_pagamento($value, $row)
	{
		// carica pagamento dal db
		$this->db->where('id_tipo_pagamento', $value);
		$this->db->from('tipo_pagamento');
		$query = $this->db->get();
		$pagamento = $query->row();
		return strtoupper($pagamento->desc_tipo_pagamento).' <i class="fa '.$pagamento->icon_tipo_pagamento.'"></i>';
	}
	
	function _callback_cliente_info($value, $row)
	{
		// carica cliente dal db
		$this->db->where('id_cliente', $value);
		$this->db->from('clienti');
		$query = $this->db->get();
		$cliente = $query->row();
		// mostra link per vedere i dati del cliente corrente se esistente
		if(isset($cliente))
			return "<a href='".site_url('admin/customers/edit/'.$cliente->id_cliente)."' title=\"Vedi scheda cliente\">".$cliente->cognome. " " . $cliente->nome ."</a>";
		else 
			return "Consultare storico";
	}
	
	function _delete_order_cart($primary_key) {
		// cancella strico_carrello collegato e immagini dello storico
		$query_storico = $this->db->where('id_ordine', $primary_key)->get('storico_carrello');
		foreach ($query_storico->result() as $storico)
		{
		unlink('assets/assets-frontend/img/shop/storico/'.$storico->url_immagine); 
		$this->db->delete('storico_carrello', array('id_storico_carrello' => $storico->id_storico_carrello)); 
		}
		return true;	
	}
	
	public function loadIndirizziSpedizioneAjax($cliente_id) { 
       $result = $this->db->where("id_cliente", $cliente_id)->get("indirizzo_spedizione")->result();
       echo json_encode($result);
    }
	
	public function cart($order_id)
	{
		$this->checkUserPermissions();
		//CRUD cart
		try {
			$this->order_id_cart = $order_id;
			$crud = new grocery_CRUD();
			// tema
			$crud->set_theme('bootstrap');
			// tabella
			$crud->set_table('storico_carrello');
			$crud->where('id_ordine', $order_id);
			// nome in tabella
			$crud->display_as('url_immagine', 'Immagine');
			$crud->display_as('prezzo_scontato', 'Scontato');
			$crud->display_as('qty', 'Q.tà');
			$crud->display_as('colore_prodotto', 'Colore');
			$crud->display_as('taglia', 'Taglia');
			// upload
			$crud->set_field_upload('url_immagine', 'assets/assets-frontend/img/shop/storico');
			// realazioni join
			// campi
			$crud->add_fields('id_ordine', 'id_prodotto', 'id_variante', 'prezzo', 'prezzo_scontato', 'qty', 'colore_prodotto', 'taglia', 'url_immagine');
			$crud->edit_fields('id_prodotto', 'id_variante', 'prezzo', 'prezzo_scontato', 'qty', 'colore_prodotto', 'taglia', 'url_immagine');
			$crud->required_fields('id_prodotto', 'id_variante', 'qty');
			// colonne da mostrare
			$crud->columns('url_immagine', 'nome', 'codice', 'prezzo', 'prezzo_scontato', 'qty', 'colore_prodotto', 'taglia');
			// unset actions
			//$crud->unset_edit();
			$crud->unset_read();
			// callbacks
			$crud->callback_after_insert(array($this, '_update_storico_ordine'));
			$crud->callback_before_update(array($this,'_delete_img_file'));
			$crud->callback_after_update(array($this, '_update_storico_ordine'));
			$crud->callback_before_delete(array($this,'_delete_img_file_by_id'));
			$crud->callback_column('prezzo', array($this, '_callback_value_to_euro'));
			$crud->callback_column('prezzo_scontato', array($this, '_callback_value_to_euro'));
			
			if($crud->getState() == 'add')
    		{
				$crud->field_type('id_ordine', 'hidden', $order_id);
				$crud->set_relation('id_prodotto', 'prodotti', '{codice} - {nome}');
				$crud->set_relation('id_variante', 'varianti_prodotti', '{codice} - {nome}');
				$crud->change_field_type('nome', 'readonly');
				$crud->change_field_type('prezzo', 'readonly');
				$crud->change_field_type('colore_prodotto', 'readonly');
				$crud->change_field_type('taglia', 'readonly');
				$crud->change_field_type('url_immagine', 'readonly');
			}
			else if ($crud->getState() == 'edit')
			{
				$crud->set_relation('id_prodotto', 'prodotti', '{codice} - {nome}');
				$crud->set_relation('id_variante', 'varianti_prodotti', '{codice} - {nome}');
				$crud->change_field_type('nome', 'readonly');
				$crud->change_field_type('prezzo', 'readonly');
				$crud->change_field_type('colore_prodotto', 'readonly');
				$crud->change_field_type('taglia', 'readonly');
				$crud->change_field_type('url_immagine', 'readonly');
			//	$crud->change_field_type('colore_prodotto_id', 'readonly');
				//$crud->change_field_type('prezzo', 'readonly');
			//	$crud->change_field_type('url_immagine', 'readonly', 'active');
			}
				
			$output = $crud->render();
			
			$data['curr_state'] = $crud->getState();
			$data['curr_page'] = 'ADMIN-ORDERS';
			$data['curr_page_title'] = 'Carrello';
			$data['collapseParentMenu'] = 'ordini';
			$data['curr_function_title'] = 'Carrello ordine <b>#' . $order_id . '</b>';
			$data['resourcetype'] = 'CRUD';
			$output->data = $data;
			$this->load->view('admin/orders_cart',(array)$output);
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
	
	function _delete_img_file($post_array, $primary_key) {
		// cancella immagine corrente prima della sovrascrittura o della cancellazione
		unlink('assets/assets-frontend/img/shop/storico/'.$post_array['url_immagine']); 
		return $post_array;	
	}
	
	function _delete_img_file_by_id($primary_key) {
		// cancella immagine corrente prima della cancellazione
		$query_storico = $this->db->where('id_storico_carrello', $primary_key)->get('storico_carrello');
		$storico = $query_storico->row();
		unlink('assets/assets-frontend/img/shop/storico/'.$storico->url_immagine); 
		// modifica il totale ordine
		$this->_update_storico_ordine(array(), $primary_key);
		return true;	
	}
	
	function _update_storico_ordine($post_array, $primary_key) {
		// carica i dati dal db e popola tutti i campi
		$url_image_to_copy = '';

		$this->db->select('varianti_prodotti.*, prodotti.nome AS nome, tipo_prodotto.descrizione_tipo_prodotto AS descrizione_tipo_prodotto');
		$this->db->from('varianti_prodotti');
		$this->db->join('prodotti', 'prodotti.codice = varianti_prodotti.codice_prodotto');
		$this->db->join('tipo_prodotto', 'prodotti.id_tipo_prodotto = tipo_prodotto.id_tipo_prodotto');	
		$this->db->where('varianti_prodotti.id_variante', $post_array['id_variante']);

		$query = $this->db->get();
		$variante = $query->row();
		
		$prezzo_scontato_input = (isset($post_array['prezzo_scontato']) && $post_array['prezzo_scontato'] != NULL && $post_array['prezzo_scontato'] > 0 ? $post_array['prezzo_scontato'] : $variante->prezzo_scontato);
					
		$data = array(
		   'data_storicizzazione' => date('Y-m-d H:i:s'),
		   'tipo_prodotto' => $variante->descrizione_tipo_prodotto,
		   'codice_variante' => $variante->codice,
		   'nome' => $variante->nome,
		   'prezzo' => $variante->prezzo,
		   'prezzo_scontato' => $prezzo_scontato_input,
		   'url_immagine' => $variante->url_img_grande,
		   'colore_prodotto' => $variante->colore,
		   'colore_prodotto_codice' => $variante->colore_codice
		);
		
		$url_image_to_copy = $variante->url_img_grande;
	
		$this->db->where('id_storico_carrello', $primary_key);
		$this->db->update('storico_carrello', $data);
		
		// copia immagine del prodotto in folder storico ASSETS_ROOT_FOLDER_FRONTEND_IMG
		copy('assets/assets-frontend/img/shop/'.$url_image_to_copy, 'assets/assets-frontend/img/shop/storico/'.$url_image_to_copy);
			
		// modifica il totale ordine
		$this->_update_totale_ordine($post_array, $primary_key);
	}
	
	function _update_totale_ordine($post_array, $primary_key)
	{
		// ricalcola il totale ordine in base allo storico_carrello modificato e assegna i punti
		$totale_ordine = 0;
		
		$this->db->select('*');
		$this->db->from('storico_carrello');
		$this->db->where('id_ordine', $this->order_id_cart);
		$query = $this->db->get();
		$carrello_list = $query->result();
		
		foreach($carrello_list as $item) {
			if($item->prezzo_scontato != NULL && $item->prezzo_scontato > 0)
				$totale_ordine += ($item->prezzo_scontato * $item->qty);
			else 
				$totale_ordine += ($item->prezzo * $item->qty);
		}
		
		// calcola punti per il cliente e metti anche in ordine
		$points = round($totale_ordine / PERC_PUNTI_RETURN);
		
		$data = array(
		   'totale_ordine' => $totale_ordine,
		   'punti' => $points
		);

		$this->db->where('id_ordine', $this->order_id_cart);
		$this->db->update('ordini', $data);

		return true;
	}
	
	public function loadVariantiAjax($prod_code) { 
       $result = $this->db->where("codice_prodotto", $prod_code)->get("varianti_prodotti")->result();
       echo json_encode($result);
    }
   
    public function detail($order_id)
	{
		$this->checkUserPermissions();
		// NO-CRUD dettaglio ordine
		try {
			$output = array();

			$this->db->select('*');
			$this->db->from('ordini');
			$this->db->join('storico_clienti', 'storico_clienti.id_ordine = ordini.id_ordine');
			$this->db->join('stato_pagamento', 'ordini.stato_pagamento = stato_pagamento.id_stato_pagamento');
			$this->db->join('stato_ordine', 'ordini.stato_ordine = stato_ordine.id_stato_ordine');
			$this->db->join('tipo_pagamento', 'ordini.tipo_pagamento = tipo_pagamento.id_tipo_pagamento');
			$this->db->where('ordini.id_ordine', $order_id);
			$query = $this->db->get();
			$ordine = $query->row();
			$data['ordine'] = $ordine;
			
			// caricare varianti prodotti
			$this->db->select('storico_carrello.*, varianti_prodotti.codice AS codice');
			$this->db->from('storico_carrello');
			$this->db->join('varianti_prodotti', 'varianti_prodotti.id_variante = storico_carrello.id_variante');
			//$this->db->join('prodotti', 'prodotti.id_prodotti = storico_carrello.id_prodotto');
			$this->db->where('id_ordine', $order_id);
			$query_prod = $this->db->get();
			$data['carrello_prodotti'] = $query_prod->result();
			
			$data['curr_page'] = 'ADMIN-ORDERS';
			$data['curr_page_title'] = 'Dettaglio';
			$data['collapseParentMenu'] = 'ordini';
			$data['curr_function_title'] = 'Dettaglio ordine <b>#' . $order_id . '</b>';
			$data['resourcetype'] = 'NO-CRUD';
			$output['data'] = $data;
			$this->load->view('admin/orders_detail',(array)$output);
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}

	public function shipments($order_id)
	{
		//CRUD shipments
		try {
			$crud = new grocery_CRUD();
			// tema
			$crud->set_theme('bootstrap');
			// tabella
			$crud->set_table('shipments');
			$crud->where('id_ordine', $order_id);
			// nome in tabella
			$crud->display_as('last_update_time', 'Ultima sincronizzazione');
			// colonne da mostrare
			$crud->columns('ship_printful_id', 'service', 'items', 'tracking_number', 'ship_date');
			// unset delete action
			$crud->unset_delete();
			$crud->unset_add();
			$crud->unset_edit();
			
			$output = $crud->render();

			$data['curr_page'] = 'ADMIN-ORDERS';
			$data['curr_page_title'] = 'Ordini';
			$data['collapseParentMenu'] = 'ordini';
			$data['curr_function_title'] = 'Spedizioni ordine <b>#' . $order_id . '</b>';
			$data['resourcetype'] = 'CRUD';
			
			$output->data = $data;
			$this->load->view('admin/orders_shipments',(array)$output);
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
		
}
