<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/** PRINTFULL API ***/
use Printful\Exceptions\PrintfulApiException;
use Printful\Exceptions\PrintfulException;
use Printful\PrintfulApiClient;

require_once(APPPATH . 'libraries/Printful/vendor/autoload.php');
/*******************/

class Products extends Admin_Controller {

	public function __construct()
	{
		parent::__construct();
	}

	public function index()
	{
	}
	
	/* Esegue il sync dei prodotti e delle varianti su printfull 
	*  $type - update | force
	*/	
	public function sync($type)
	{
		ini_set('max_execution_time', 0);
		set_time_limit(0);

		$prodotti_sync_table = 'prodotti';
		$varianti_sync_table = 'varianti_prodotti';
		$prodotti_sync_img_cart_folder = './assets/assets-frontend/img/cart/';
		$prodotti_sync_img_shop_folder = './assets/assets-frontend/img/shop/';
		
		//$apiKey = 'naq7o8kj-6x61-iahm:qqv6-cad6lshspq29'; // machlo
		$pf = new PrintfulApiClient(PRINTFUL_APIKEY);
		$print_debug = true; // stampa echo di debug
		$variant_force_sync = false; // effettua update delle varianti completo altrimenti solo in_stock e price
		$product_force_sync = false; // effettua update del prodotto completo altrimenti solo nome, prezzo, varianti e stato
		
		if($type == 'force') {
			$variant_force_sync = true;
			$product_force_sync = true;
		} 
			
		$syncMap = array();
		
		$limit = 100;
		$total = 0;
		$offset = 0;
		
		date_default_timezone_set("Europe/Rome"); //set you countary name from below timezone list
	    $dateNow = date("Y-m-d H:i:s", time()); //now
		
		// verifica se il sync è libero altrimenti attendi
		$this->db->from('sync_test');
		$this->db->where('id_sync_test', 1);
		$syncTest = $this->db->get()->row();
		
		if($syncTest->sync_status != 'PROGRESS') {

			//update sync test PROGRESS			
			$data_synctest_var = array(
			   'sync_status' => 'PROGRESS',
			   'sync_type' => $type,
			   'time_sync_test' => $dateNow
			);
			$this->db->where('id_sync_test', 1);
			$this->db->update('sync_test', $data_synctest_var);
			
			if($print_debug) {
				echo '*** START SYNC ['.$type.'] at ' . $dateNow . '<br>';
				echo 'max_execution_time = ' . ini_get('max_execution_time') . '<br>';
			}
			
			try {
				// SETTO TUTTI I PRODOTTI CON SYNC STATUS A 0 tutti quelli che resteranno così saranno rimossi
				// 0 REMOVED | 1 NEW | 2 UPDATED | 3 FORCE UPDATED
				$data_update_prod_sync_status = array(
				   'sync_status' => 0 // removed
				);
				$this->db->update($prodotti_sync_table, $data_update_prod_sync_status);
				$this->db->update($varianti_sync_table, $data_update_prod_sync_status);
				 
				// RECUPERO LA LISTA DEI PRODOTTI SU PRINTFUL SYNC CON LA PAGINAZIONE
				$data = $pf->get('sync/products?limit='.$limit);
				$syncMap = array_merge($syncMap, $data);
				
				$resp = $pf->getLastResponse();
				
				$total = $resp['paging']['total'];
				
				while($offset < $total) {
					$offset += $limit;
					$data = $pf->get('sync/products?offset='.$offset.'limit='.$limit);
					$syncMap = array_merge($syncMap, $data);
				}
				
				if($print_debug) 
					echo '** FOUND ['.count($syncMap).'] products <br><br>';
				
				foreach($syncMap as $item) {
					
					 if($print_debug) {
						 echo 'ID: ' . $item['id'] . '<br>';
						 echo 'EXTERNAL ID: ' . $item['external_id'] . '<br>';
						 echo 'NAME: ' . $item['name'] . '<br>';
						 echo 'VARIANTS: ' . $item['variants'] . '<br>';
						 echo 'SYNCED: ' . $item['synced'] . '<br><br>';
					 }
					 
					 $prod_codice = $item['external_id'];
					 $prod_nome = $item['name'];
					 $prod_img_thumb = '';
					 $prod_img = '';
					 $prod_colore = '';
					 $prod_colore_codice = '';
					 $prod_in_stock_variants = 0; // contatore delle varianti presenti in_stock
					 
					 // RECUPERA LE INFO DEL SINGOLO PRODOTTO E DELLE SUE VARIANTI
					 $data_prod = $pf->get('sync/products/@'.$item['external_id']);
					 $var_counter = 0;
					 $var_min_price = 0;			 
		
					 foreach($data_prod['sync_variants'] as $variant) {
						 
						 $var_url_img_piccola = '';
						 $var_url_img_grande = '';
						 $var_url_img_grande_retro = '';
								
						 // EXTRACT PRINTFUL VARIANTS PER OGNI PRODOTTO
						 $data_var = $pf->get('products/variant/'.$variant['variant_id']); 
	
						 if($var_counter == 0) {
							if(floatval($data_var['variant']['price']) < $var_min_price || $var_min_price == 0) 
								$var_min_price = $data_var['variant']['price']; // prezzo più basso a partire da
						// 	$prod_colore = $data_var['variant']['color'];
						// 	$prod_colore_codice = trim(ltrim($data_var['variant']['color_code'], '#'));
						// 	$prod_taglia = $data_var['variant']['size'];
						 }
						
						// CHECK IN STOCK PER PRODOTTO
						if($data_var['variant']['in_stock']) {
							$prod_in_stock_variants++;
						}
						
						// FILES type preview
						foreach($variant['files'] as $img_file) {
						 if($img_file['type'] == 'preview') {
							$var_url_img_piccola = $img_file['thumbnail_url'];
							$var_url_img_grande = $img_file['preview_url'];
							//@TODO recuperare l'immagine dal generatore di mockups
							//$var_url_img_grande_retro = $img_file['preview_url'];
							
							if($var_counter == 0) {
								$prod_img_thumb = $img_file['thumbnail_url'];
								$prod_img = $img_file['preview_url'];
							}
						 }
						}
						// OPTIONS
						foreach($variant['options'] as $opt) {
						 if($opt['id'] == 'stitch_color') {
						//	echo 'VARIANT STITCH COLOR: ' . $opt['value'] . '<br>';
						//	if($var_counter == 0)
						//		$prod_colore = $opt['value'];
						 }
						}
						
						if($print_debug) { 
							echo 'VARIANT EXTERNAL ID: ' . $variant['external_id'] . '<br>';
							echo 'VARIANT NAME: ' . $variant['name'] . '<br>';
							echo 'VARIANT PRINTFULL ID: ' . $variant['variant_id'] . '<br>';
							echo 'VARIANT PRICE: ' . $data_var['variant']['price'] . '<br>';
							echo 'VARIANT SIZE: ' . $data_var['variant']['size'] . '<br>';
							echo 'VARIANT COLOR: ' . $data_var['variant']['color'] . ' | ' . $data_var['variant']['color_code'] . '<br>';
							echo 'VARIANT IN STOCK: ' . $data_var['variant']['in_stock'] . '<br>';
							echo '<br>';
						}
						
						/** UPDATE O INSERT VARIANTE **/
						$var_prodotto_codice = $prod_codice;                                       // codice_prodotto 
						$var_nome = $variant['name'];                                              // nome
						$var_colore = $data_var['variant']['color'];                               // colore
						$var_colore_codice = trim(ltrim($data_var['variant']['color_code'], '#')); // colore_codice
						$var_taglia = $data_var['variant']['size'];                                // taglia
						$var_codice = $variant['external_id'];                                     // codice
						$var_prezzo = $data_var['variant']['price'];                               // prezzo
						$var_prezzo_scontato = 0;                                                  // prezzo_scontato
						//$var_url_img_piccola = '';                                               // url_img_piccola   
						//$var_url_img_grande = '';                                                // url_img_grande
						//$var_url_img_grande_retro = '';                                          // url_img_grande_retro
						$var_stato = ($data_var['variant']['in_stock'] ? 1 : 3);                   // stato
						$var_in_stock = $data_var['variant']['in_stock'];                          // in_stock
						
						// INSERT OR UPDATE VARIANT
						$this->db->select('*');
						$this->db->from($varianti_sync_table);
						$this->db->where('codice', $var_codice);
						$query_var= $this->db->get();
						$count_var = $query_var->num_rows();
						
						if ($count_var === 0) {
							$data_var = array(
							   'id_variante' => NULL,
							   'codice_prodotto' => $var_prodotto_codice,
							   'nome' => $var_nome,
							   'colore' => $var_colore,
							   'colore_codice' => $var_colore_codice,
							   'taglia' => $var_taglia,
							   'codice' => $var_codice,
							   'prezzo' => $var_prezzo,
							   'prezzo_scontato' => $var_prezzo_scontato,
							   'url_img_piccola' => ($var_url_img_piccola != '' ? $var_codice.".png" : ''),
							   'url_img_grande' => ($var_url_img_grande != '' ? $var_codice."_f.png" : ''),
							   'url_img_grande_retro' => ($var_url_img_grande_retro != '' ? $var_codice."_b.png" : ''),
							   'stato' => $var_stato,
							   'in_stock' => $var_in_stock,
							   'sync_status' => 1, // NEW
							   'sync_time' => $dateNow
							 );
				
							 $this->db->insert($varianti_sync_table, $data_var);
						
							 // COPIO LE IMMAGINI DA URL IN LOCALE O SOVRASCRIVO
							 if($var_url_img_piccola != '') {
								$this->check_remove_image_file($prodotti_sync_img_cart_folder.$var_codice.".png");
								copy($var_url_img_piccola, $prodotti_sync_img_cart_folder.$var_codice.".png");
							 }
							 if($var_url_img_grande != '') {
								$this->check_remove_image_file($prodotti_sync_img_shop_folder.$var_codice."_f.png");
								copy($var_url_img_grande, $prodotti_sync_img_shop_folder.$var_codice."_f.png");
							 }
							 if($var_url_img_grande_retro != '') {
								$this->check_remove_image_file($prodotti_sync_img_shop_folder.$var_codice."_b.png");
								copy($var_url_img_grande_retro, $prodotti_sync_img_shop_folder.$var_codice."_b.png");
							 }
						} else {
							// UPDATE				
							if($variant_force_sync) { // FORCE 
								$data_update_var = array(
								   'nome' => $var_nome,
								   'colore' => $var_colore,
								   'colore_codice' => $var_colore_codice,
								   'taglia' => $var_taglia,
								   'prezzo' => $var_prezzo,
								   'prezzo_scontato' => $var_prezzo_scontato,
								   'url_img_piccola' => ($var_url_img_piccola != '' ? $var_codice.".png" : ''),
								   'url_img_grande' => ($var_url_img_grande != '' ? $var_codice."_f.png" : ''),
								   'url_img_grande_retro' => ($var_url_img_grande_retro != '' ? $var_codice."_b.png" : ''),
								   'stato' => $var_stato,
								   'in_stock' => $var_in_stock,
								   'sync_status' => 3, // force updated
								   'sync_time' => $dateNow
								);
								 // COPIO LE IMMAGINI DA URL IN LOCALE O SOVRASCRIVO
								 if($var_url_img_piccola != '') {
									$this->check_remove_image_file($prodotti_sync_img_cart_folder.$var_codice.".png");
									copy($var_url_img_piccola, $prodotti_sync_img_cart_folder.$var_codice.".png");
								 }
								 if($var_url_img_grande != '') {
									$this->check_remove_image_file($prodotti_sync_img_shop_folder.$var_codice."_f.png");
									copy($var_url_img_grande, $prodotti_sync_img_shop_folder.$var_codice."_f.png");
								 }
								 if($var_url_img_grande_retro != '') {
									$this->check_remove_image_file($prodotti_sync_img_shop_folder.$var_codice."_b.png");
									copy($var_url_img_grande_retro, $prodotti_sync_img_shop_folder.$var_codice."_b.png");
								 }
							} else {
								// UPDATE
								$data_update_var = array(
								   'stato' => $var_stato,
								   'in_stock' => $var_in_stock,
								   'sync_status' => 2, // updated
								   'sync_time' => $dateNow
								);
							}
							$this->db->where('codice', $var_codice);
							$this->db->update($varianti_sync_table, $data_update_var);
						}
						
						$var_counter++;
					}
	
					// INSERISCO NUOVO PRODOTTO DAL SYNC SE NON ESISTENTE ALTRIMENTI UPDATE
					$this->db->select('*');
					$this->db->from($prodotti_sync_table);
					$this->db->where('codice', $prod_codice);
					$query_prod = $this->db->get();
					$count_prod = $query_prod->num_rows();
					
					if ($count_prod === 0) {
						$data = array(
						   'id_prodotti' => NULL,
						   'id_tipo_prodotto' => 5,
						   'codice' => $prod_codice,
						   'nome' => $prod_nome,
						   'prezzo' => $var_min_price,
						   'url_img_piccola' => ($prod_img_thumb != '' ? $prod_codice.".png" : ''),
						   'url_img_grande' => ($prod_img != '' ? $prod_codice.".png" : ''),
						   'stato' => 3,
						   'varianti' => $item['variants'],
						   'ordine' => 1,
						   'sync_status' => 1, // NEW
						   'sync_time' => $dateNow
						 );
			
						 $this->db->insert($prodotti_sync_table, $data);
						 // COPIO LE IMMAGINI DA URL IN LOCALE O SOVRASCRIVO
						 if($prod_img_thumb != '') {
							$this->check_remove_image_file($prodotti_sync_img_cart_folder.$prod_codice.".png"); 
							copy($prod_img_thumb, $prodotti_sync_img_cart_folder.$prod_codice.".png");
						 }
						 if($prod_img != '') {
							$this->check_remove_image_file($prodotti_sync_img_shop_folder.$prod_codice.".png"); 
							copy($prod_img, $prodotti_sync_img_shop_folder.$prod_codice.".png");
						 }
					} else {
						// UPDATE				
						if($product_force_sync) { // FORCE
							$data_update_prod = array(
							   'nome' => $prod_nome,
							   'prezzo' => $var_min_price,
							   'url_img_piccola' => ($prod_img_thumb != '' ? $prod_codice.".png" : ''),
							   'url_img_grande' => ($prod_img != '' ? $prod_codice.".png" : ''),
							   'varianti' => $item['variants'],
							   'stato' => ($prod_in_stock_variants > 0 ? 1 : 3),
							   'sync_status' => 3, // force updated
							   'sync_time' => $dateNow
							);
							// COPIO LE IMMAGINI DA URL IN LOCALE O SOVRASCRIVO
							 if($prod_img_thumb != '') {
								$this->check_remove_image_file($prodotti_sync_img_cart_folder.$prod_codice.".png"); 
								copy($prod_img_thumb, $prodotti_sync_img_cart_folder.$prod_codice.".png");
							 }
							 if($prod_img != '') {
								$this->check_remove_image_file($prodotti_sync_img_shop_folder.$prod_codice.".png"); 
								copy($prod_img, $prodotti_sync_img_shop_folder.$prod_codice.".png");
							 }
						} else {
							// UPDATE SYNC
							$data_update_prod = array(
							   'sync_status' => 2, // updated
							   'varianti' => $item['variants'],
							   'stato' => ($prod_in_stock_variants > 0 ? 1 : 3),
							   'sync_time' => $dateNow
							);
						}
						$this->db->where('codice', $prod_codice);
						$this->db->update($prodotti_sync_table, $data_update_prod);
					}
	
					if($print_debug) { 
						// PRODUCT
						echo 'PRODUCT CODE: ' . $prod_codice . '<br>';
						echo 'PRODUCT NAME: ' . $prod_nome . '<br>';
						echo 'PRODUCT PRICE: ' . $var_min_price . '<br>';
						echo 'PRODUCT IMG THUMB: ' . $prod_img_thumb . '<br>';
						echo 'PRODUCT IMG: ' . $prod_img . '<br>';
						echo '<br>************************************************************************<br><br>';
					}
		
				 }	
				 
				// Elimina varianti non presenti in whishist (SPOSTATO IN ACCOUNT FONTEND)
				/*$this->db->select('wishlist.*, varianti_prodotti.id_variante AS id_variante');
				$this->db->join('varianti_prodotti', 'varianti_prodotti.codice = wishlist.codice_variante');
				$this->db->from('wishlist');
				$query_wish= $this->db->get();
				foreach ($query_wish->result() as $wishToDelete)
				{
					if($wishToDelete->id_variante == NULL) {
						$this->db->delete('wishlist', array('id_wishlist' => $wishToDelete->id_wishlist)); 
						if($print_debug) 
							echo 'DELETE from WISHLIST ' . $wishToDelete->id_wishlist . ' | ' . $wishToDelete->codice_variante;
					}
				}*/
				
				// Elimina varianti o prodotti cancellati da carrello
				
				
				// Elimina tutti i prodotti con sync a 0 (CANCELLATI)
				$query_delete = $this->db->where('sync_status', 0)->get($prodotti_sync_table);
				foreach ($query_delete->result() as $prodToDelete)
				{
					$this->remove_product($prodToDelete, $print_debug, $prodotti_sync_img_cart_folder, $prodotti_sync_img_shop_folder, $varianti_sync_table, $prodotti_sync_table);
				}
				
				// Elimina e verifica eventuali varianti da cancellare
				$query_delete_var = $this->db->where('sync_status', 0)->get($varianti_sync_table);
				foreach ($query_delete_var->result() as $varToDelete)
				{
					$this->remove_variant($varToDelete, $print_debug, $prodotti_sync_img_cart_folder, $prodotti_sync_img_shop_folder, $varianti_sync_table);
				}
				
				//update sync test PROGRESS			
				$data_synctest_var_completed = array(
				   'sync_status' => 'COMPLETED'
				);
				$this->db->where('id_sync_test', 1);
				$this->db->update('sync_test', $data_synctest_var_completed);
				
			} catch (PrintfulApiException $e) { //API response status code was not successful
				echo 'Printful API Exception: ' . $e->getCode() . ' ' . $e->getMessage();
				//update sync test PROGRESS			
				$data_synctest_var_completed = array(
				   'sync_status' => 'FAILED',
				   'sync_type' => $type,
				   'time_sync_test' => $dateNow
				);
			
				$this->db->where('id_sync_test', 1);
				$this->db->update('sync_test', $data_synctest_var_completed);
			} catch (PrintfulException $e) { //API call failed
				//update sync test PROGRESS			
				$data_synctest_var_completed = array(
				   'sync_status' => 'FAILED',
				   'sync_type' => $type,
				   'time_sync_test' => $dateNow
				);
				$this->db->where('id_sync_test', 1);
				$this->db->update('sync_test', $data_synctest_var_completed);
				echo 'Printful Exception: ' . $e->getMessage();
				var_export($pf->getLastResponseRaw());
			} catch (Exception $e) {
				echo 'Exception: ' . $e->getCode() . ' ' . $e->getMessage();
    			$data_synctest_var_completed = array(
				   'sync_status' => 'FAILED',
				   'sync_type' => $type,
				   'time_sync_test' => $dateNow
				);
				$this->db->where('id_sync_test', 1);
				$this->db->update('sync_test', $data_synctest_var_completed);
			} 	
	
			if($print_debug) 
				echo '*** END SYNC at ' . date("Y-m-d H:i:s", time()); //now
		} else {// end synctest if
			//if($print_debug) 
				//echo 'Sync not executed because test status is ' . $syncTest->sync_status;
			echo 'BUSY';
		}
	}
	
	/* Effettua sync update delle varianti in_stock se non le trova le disabilita 
		in_stock 0 | stato 3 (sospeso)
	*/
	public function sync_variants()
	{
		try {
				
			$print_debug = true; // stampa echo di degub
			$debug_newline = PHP_EOL;
			
			$this->db->select('*');
			$this->db->from('varianti_prodotti');
			$query_var= $this->db->get();
			
			date_default_timezone_set("Europe/Rome"); //set you countary name from below timezone list
			$dateNow = date("Y-m-d H:i:s", time()); //now
			
			// verifica se il sync è libero altrimenti attendi
			$this->db->from('sync_test');
			$this->db->where('id_sync_test', 1);
			$syncTest = $this->db->get()->row();
			
			if($syncTest->sync_status != 'PROGRESS') {
	
				//update sync test PROGRESS			
				$data_synctest_var = array(
				   'sync_status' => 'PROGRESS',
				   'sync_type' => 'sync_variants',
				   'time_sync_test' => $dateNow
				);
				$this->db->where('id_sync_test', 1);
				$this->db->update('sync_test', $data_synctest_var);
			
				if(isset($_SERVER['REMOTE_ADDR']))
					$debug_newline = '<br>';
							
				if($print_debug) 
					echo '*** START SYNC VARIANTS IN STOCK at ' . $dateNow . $debug_newline;
			
				$pf = new PrintfulApiClient(PRINTFUL_APIKEY);
	
				foreach ($query_var->result() as $variant){
					try {
						//$variantcode = '5a9e7a457b0983'; // per testare
						$variantcode = $variant->codice;
						$data_var = $pf->get('products/variant/@'.$variantcode); 
						echo 'Variante codice trovato in sync ' . $variantcode . ' in_stock ' . $data_var['variant']['in_stock'] . $debug_newline;
						$data_update_var = array(
						   'in_stock' => $data_var['variant']['in_stock'],
						   'sync_status' => 2, // updated
						   'sync_time' => $dateNow
						);	
						// update
						$this->db->where('codice', $variantcode);
						$this->db->update('varianti_prodotti', $data_update_var);
					} catch (PrintfulApiException $e) { //API response status code was not successful
						if($e->getCode() == 404) {
							if($print_debug) 
								echo 'Variante codice non trovato: ' . $e->getCode() . ' ' . $e->getMessage() . $debug_newline;
							$data_update_var = array(
							   'stato' => 3, // sospeso
							   'in_stock' => 0,
							   'sync_status' => 2, // updated
							   'sync_time' => $dateNow
							);		
							// update
							$this->db->where('codice', $variantcode);
							$this->db->update('varianti_prodotti', $data_update_var);
						} else {
							if($print_debug) 
								echo 'PrintfulApi Exception: ' . $e->getCode() . ' ' . $e->getMessage();
						}
					}
				}
				//update sync test COMPLETED			
				$sync_end_status = 'COMPLETED';
			} else {
				if($print_debug) 
					echo 'Sync not executed because test status is ' . $syncTest->sync_status;
				echo 'BUSY';
			}
		} catch (PrintfulException $e) { //API call failed		
			$sync_end_status = 'FAILED';
			echo 'Printful Exception: ' . $e->getMessage();
			//var_export($pf->getLastResponseRaw());
		} catch (Exception $e) {
			$sync_end_status = 'FAILED';
			echo 'Exception: ' . $e->getMessage();
		} finally {
			// set sync status
			$data_synctest_var_completed = array(
			    'sync_status' => $sync_end_status,
			    'sync_type' => 'sync_variants',
			    'time_sync_test' => $dateNow
			);
			$this->db->where('id_sync_test', 1);
			$this->db->update('sync_test', $data_synctest_var_completed);
    		if($print_debug) 
				echo '*** END SYNC at' . date("Y-m-d H:i:s", time()); //now
  		}	
	}
	
	/* Effettua cancellazione del prodotto e di tutte le varianti con immagini dal local */
	private function remove_product($product, $print_debug, $prodotti_sync_img_cart_folder, $prodotti_sync_img_shop_folder, $varianti_sync_table, $prodotti_sync_table)
	{
		// Recupera le varianti del prodotto
		$query_delete = $this->db->where('codice_prodotto', $product->codice)->get($varianti_sync_table);
		foreach ($query_delete->result() as $varToDelete){
			// Elimina varianti / immagini
			$this->remove_variant($varToDelete, $print_debug, $prodotti_sync_img_cart_folder, $prodotti_sync_img_shop_folder, $varianti_sync_table);
			if($print_debug)
				echo 'REMOVE VARIANT FROM PRODUCT: ' . $varToDelete->codice . '<br>'; 	
		}

		// Elimina prodotto / immagini / traduzioni
		$this->check_remove_image_file($prodotti_sync_img_cart_folder.$product->codice.".png"); 
		$this->check_remove_image_file($prodotti_sync_img_shop_folder.$product->codice.".png");
		$this->db->delete('prodotti_traduzioni', array('id_prodotti' => $product->id_prodotti)); 
		$this->db->delete($prodotti_sync_table, array('codice' => $product->codice));
		if($print_debug)
			echo 'REMOVE PRODUCT: ' . $product->codice . '<br>'; 	
	}
	
	/* Effettua cancellazione della variante con immagini dal local */
	private function remove_variant($variant, $print_debug, $prodotti_sync_img_cart_folder, $prodotti_sync_img_shop_folder, $varianti_sync_table)
	{
		// Elimina varianti / immagini
		$this->check_remove_image_file($prodotti_sync_img_cart_folder.$variant->codice.".png"); 
		$this->check_remove_image_file($prodotti_sync_img_shop_folder.$variant->codice."_f.png");
		$this->check_remove_image_file($prodotti_sync_img_shop_folder.$variant->codice."_b.png");
		$this->db->delete($varianti_sync_table, array('codice' => $variant->codice));
		if($print_debug)
			echo 'REMOVE VARIANT: ' . $variant->codice . '<br>'; 			
	}
	 
	/* Controlla se esiste e rimuove file */
	private function check_remove_image_file($file_path) {
		# delete file if exists
		if (file_exists($file_path)) { 
			unlink ($file_path);
		}
	}
	
	public function crud()
	{
		$this->checkUserPermissions();
		//CRUD
		try{
			$crud = new grocery_CRUD();
			// tema
			$crud->set_theme('bootstrap');
			// tabella
			$crud->set_table('prodotti'); // tornare a prodotti
			$crud->order_by('ordine', 'desc');
			// nome in tabella
			$crud->display_as('id_tipo_prodotto', 'Tipo prodotto');
			$crud->display_as('stato', 'Stato');
			$crud->display_as('ordine', 'Posizione');
			$crud->display_as('varianti', 'Varianti');
			$crud->display_as('url_img_piccola', 'Imm. carrello');
			$crud->display_as('url_img_grande', 'Imm. fronte');
			$crud->display_as('tags', 'Tags');
			$crud->display_as('sync_status', 'Sync');
			// file upload
			$crud->set_field_upload('url_img_piccola', 'assets/assets-frontend/img/cart');
			$crud->set_field_upload('url_img_grande', 'assets/assets-frontend/img/shop');
			// realazioni join
			$crud->set_relation('id_tipo_prodotto', 'tipo_prodotto', 'descrizione_tipo_prodotto');
			$crud->set_relation('stato', 'stato_prodotti', 'stato_prodotti_desc');
			$crud->set_relation('tipo_stampa', 'tipo_stampa', 'desc_tipo_stampa');
			$crud->set_relation_n_n('tags', 'tags_prodotti', 'tags', 'id_prodotto', 'id_tag', 'nome_tag');
			$crud->set_relation_n_n('categorie', 'prodotti_categorie', 'categorie', 'id_prodotto', 'id_categoria', 'nome');
			$crud->set_relation('sync_status', 'sync_status', 'desc_sync_status');
			// campi obbligatori
			$crud->required_fields('prezzo', 'id_tipo_prodotto', 'stato');
			// regole validazione campi
			$crud->set_rules('prezzo', 'Prezzo', 'required|numeric');
			// campi per edit
			$crud->edit_fields('categorie', 'id_tipo_prodotto', 'codice', 'nome', 'prezzo', 'stato', 'tags', 'ordine', 'varianti','sync_status', 'sync_time', 'tipo_stampa', 'url_img_piccola');

			if($crud->getState() == 'edit')
    		{
				$crud->change_field_type('codice', 'readonly');
				$crud->change_field_type('varianti', 'readonly');
				$crud->change_field_type('nome', 'readonly');
				$crud->change_field_type('sync_status', 'readonly');
				$crud->change_field_type('sync_time', 'readonly');
				$crud->change_field_type('url_img_piccola', 'readonly');
			} 
			// colonne da mostrare
			$crud->columns('url_img_grande', 'codice', 'categorie', 'nome', 'prezzo', 'varianti', 'ordine', 'stato', 'varianti','sync_status');
			// unset delete action
			$crud->unset_delete();
			//$crud->unset_add(); nascondi nella pagina
			// custom action
			$crud->add_action('Traduzioni prodotto', '', '', 'fa-file-text', array($this, 'load_traduzioni'));
			$crud->add_action('Varianti prodotto', '', '', 'fa-shopping-bag', array($this, 'load_varianti'));
			
			$output = $crud->render();

			$data['curr_page'] = 'ADMIN-PRODUCTS';
			$data['curr_page_title'] = 'Prodotti';
			$data['collapseParentMenu'] = 'prodotti';
			$data['resourcetype'] = 'CRUD';
			$output->data = $data;
			$this->load->view('admin/products',(array)$output);
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}

	function load_varianti($primary_key , $row)
	{
		return site_url('admin/products/variants/'.$row->id_prodotti.'/'.$row->codice);
	}

	function load_traduzioni($primary_key , $row)
	{
		return site_url('admin/products/trad/'.$row->id_prodotti.'/'.$row->codice);
	}

	public function variants($prod_id, $prod_code)
	{
		//CRUD varianti
		try {
			$crud = new grocery_CRUD();
			// tema
			$crud->set_theme('bootstrap');
			// tabella
			$crud->set_table('varianti_prodotti');
			$crud->where('codice_prodotto', $prod_code);
			// nome in tabella
			$crud->display_as('stato', 'Stato');
			$crud->display_as('prezzo_scontato', 'Scontato');
			$crud->display_as('url_img_piccola', 'Imm. carrello');
			$crud->display_as('url_img_grande', 'Imm. fronte');
			$crud->display_as('url_img_grande_retro', 'Imm. retro');
			$crud->display_as('sync_status', 'Sync');
			// file upload
			$crud->set_field_upload('url_img_piccola', 'assets/assets-frontend/img/cart');
			$crud->set_field_upload('url_img_grande', 'assets/assets-frontend/img/shop');
			$crud->set_field_upload('url_img_grande_retro', 'assets/assets-frontend/img/shop');
			// realazioni join
			$crud->set_relation('stato', 'stato_prodotti', 'stato_prodotti_desc');
			$crud->set_relation('sync_status', 'sync_status', 'desc_sync_status');
			// campi obbligatori
			$crud->required_fields('prezzo', 'stato');
			$crud->edit_fields('codice', 'nome', 'stato', 'colore', 'taglia', 'prezzo', 'prezzo_scontato', 'in_stock', 'sync_status', 'sync_time', 'url_img_piccola', 'url_img_grande', 'url_img_grande_retro');
			// regole validazione campi
			$crud->set_rules('prezzo', 'Prezzo', 'required|numeric');
			$crud->set_rules('prezzo_scontato', 'Prezzo scontato', 'numeric');
			// colonne da mostrare
			$crud->callback_edit_field('in_stock', array($this, '_callback_in_stock_value'));
			$crud->columns('url_img_grande', 'codice', 'prezzo', 'prezzo_scontato', 'colore', 'taglia', 'stato', 'sync_status');
			// unset delete action
			$crud->unset_delete();
			$crud->unset_add();
			if($crud->getState() == 'edit')
    		{
				$crud->change_field_type('taglia', 'readonly');
				$crud->change_field_type('colore', 'readonly');
				$crud->change_field_type('codice', 'readonly');
				$crud->change_field_type('nome', 'readonly');
				$crud->change_field_type('in_stock', 'readonly');
				$crud->change_field_type('sync_status', 'readonly');
				$crud->change_field_type('sync_time', 'readonly');
				$crud->change_field_type('url_img_piccola', 'readonly');
				$crud->change_field_type('url_img_grande', 'readonly');
			} 
			$crud->field_type('id_prodotto', 'hidden', $prod_id);
			$output = $crud->render();

			$data['curr_page'] = 'ADMIN-PRODUCTS';
			$data['curr_page_title'] = 'Prodotti';
			$data['collapseParentMenu'] = 'prodotti';
			$data['curr_function_title'] = 'Varianti per <b>' . $prod_code . '</b>';
			$data['resourcetype'] = 'CRUD';
			$output->data = $data;
			$this->load->view('admin/variants',(array)$output);
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
	
	function _callback_in_stock_value($value, $row)
	{
		return ($value == 0 ? 'NON DISPONIBILE' : 'DISPONIBILE');
	}
	
	public function traductions($prod_id, $prod_code)
	{
		//CRUD varianti
		try {
			$crud = new grocery_CRUD();
			// tema
			$crud->set_theme('bootstrap');
			// tabella
			$crud->set_table('prodotti_traduzioni');
			$crud->where('id_prodotti', $prod_id);
			// nome in tabella
			$crud->display_as('descrizione', 'Descrizione');
			$crud->display_as('descrizione_breve', 'Descrizione breve');
			$crud->display_as('lingua_traduzione_id', 'Lingua');
			// realazioni join
			$crud->set_relation('lingua_traduzione_id', 'lingue', 'nome_lingue');
			// campi obbligatori
			$crud->required_fields('descrizione', 'descrizione_breve', 'lingua_traduzione_id');
			$crud->edit_fields('descrizione','descrizione_breve', 'lingua_traduzione_id');
			$crud->add_fields('id_prodotti', 'descrizione', 'descrizione_breve', 'lingua_traduzione_id');
			// colonne da mostrare
			$crud->columns('descrizione', 'descrizione_breve', 'lingua_traduzione_id');
			// unset delete action
			//$crud->unset_delete();
			$crud->unset_texteditor('descrizione');
			$crud->field_type('id_prodotti', 'hidden', $prod_id);
			$output = $crud->render();

			$data['curr_page'] = 'ADMIN-PRODUCTS';
			$data['curr_page_title'] = 'Prodotti';
			$data['collapseParentMenu'] = 'prodotti';
			$data['curr_function_title'] = 'Traduzioni per <b>' . $prod_code . '</b>';
			$data['resourcetype'] = 'CRUD';
			$output->data = $data;
			$this->load->view('admin/products_trad',(array)$output);
		}catch(Exception $e){
			show_error($e->getMessage().' --- '.$e->getTraceAsString());
		}
	}
	
}
