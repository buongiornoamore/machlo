<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
* Name:  Language file pages - ENGLISH
*/
/* MENU */
$lang['MENU_HOME'] = "Home";
$lang['MENU_SHOP'] = "Shop";
$lang['MENU_ABOUT'] = "About us";
$lang['MENU_SHIPPING'] = "Shipping";
$lang['MENU_RULES'] = "Rules";
$lang['MENU_GALLERY'] = "Gallery";
$lang['MENU_PRIVACY'] = "Privacy";
$lang['MENU_CONTACTS'] = "Contacts";
/* HOME */
$lang['PAGE_HOME_CODE'] = "HOME";
$lang['PAGE_HOME_TITLE'] = "Artistic apparel";
$lang['PAGE_HOME_META_DESCRIPTION'] = "Welcome to Ma Chlò\' s website, selling quality garments based on artwork with worldwide shipping.";
$lang['PAGE_HOME_DESCRIPTION'] = "";
$lang['PAGE_HOME_IMAGE'] = "";
/* SHOP */
$lang['PAGE_SHOP_CODE'] = "SHOP";
$lang['PAGE_SHOP_TITLE'] = "Our products";
$lang['PAGE_SHOP_META_DESCRIPTION'] = "Here you can find all of our products, sizes and colors available to bring our works to your home.";
$lang['PAGE_SHOP_DESCRIPTION'] = "";
$lang['PAGE_SHOP_IMAGE'] = "";
/* ABOUT */
$lang['PAGE_ABOUT_CODE'] = "ABOUT";
$lang['PAGE_ABOUT_TITLE'] = "About us";
$lang['PAGE_ABOUT_META_DESCRIPTION'] = "Ma Chlò was born in Sardinia in 2017 by the idea and collaboration of Stefania, Simona and Francesca who wanted to reproduce on dresses the paintings of one of the founders who have always been fond of drawing and art.";
$lang['PAGE_ABOUT_DESCRIPTION'] = "Ma Chlò was founded in Sardinia in 2017 by the collaboration of Stefania, Simona and Francesca. One of our founders has always been interested in art and we wanted to display her paintings on our clothing. Ma Chlò doesn’t use prints but original and real works of art which represent nature in all its shapes and colors and include different subjects and their imaginations. Our unique offering also takes inspiration from the scents, magic and landscape of Sardinia. Each picture is inspired by a trip, an emotion or an experience that the artist wants to express in her art and which every woman who wears our clothes can feel.<br><br>Apart from nature in the form of animals, trees and flowers, Ma Chlò offers our customers the clothing line: \"The Masks\" that represents the evolution of a love story. \"The idea of the masks,\" the artist tells us, “arose during a walk along the last kilometre and a half of the remaining section of the Berlin Wall. I was fascinated by a painting on this famous wall that represented two faces which had been divided from each other. After playing with the colours I understood after the first sketch that this was a story, a love story, that no barrier could ever stop.\" Different phases of the story are represented and include “the meeting”, ”the look”, “the kiss”, “the separation”, “the silence”, “the breakup”, “eternity” and “a new beginning” that Ma Chlò wants every woman to wear as an expression of her experiences. Ma Chlò currently offers t-shirts and dresses and plans to expand its range of products as we continue to grow.<h3>Mission</h3>We would like to create a meeting point between fashion and art.  We believe that our originality and the meaning of our paintings distinguishes us from other clothing companies.";
$lang['PAGE_ABOUT_IMAGE'] = "about.jpg";
/* SHIPPING */
$lang['PAGE_SHIPPING_CODE'] = "SHIPPING";
$lang['PAGE_SHIPPING_TITLE'] = "Shipping and delivery";
$lang['PAGE_SHIPPING_META_DESCRIPTION'] = "Welcome to Ma Chlò\\\'s website, selling quality garments based on artwork with worldwide shipping.";
$lang['PAGE_SHIPPING_DESCRIPTION'] = "<h3>Shipping rules and price</h3>
Ma Chlò shipping is free, the delivery time is within 7/10 business days once the order will be fulfilled, or you can choose an express shipping for 20€";
$lang['PAGE_SHIPPING_IMAGE'] = "99fc4-blog.jpg";
/* RULES */
$lang['PAGE_RULES_CODE'] = "RULES";
$lang['PAGE_RULES_TITLE'] = "Rules and conditions";
$lang['PAGE_RULES_META_DESCRIPTION'] = "Ma Chlò is a registered trademark, find out how it works and what the service policy is.";
$lang['PAGE_RULES_DESCRIPTION'] = "Ma Chlò is a registered trade mark, all of its contents, like images, photos, works, sketches, figures, logos and every other material, in any format, published on machlo.com, included menus, web pages, graphics, colors, schemes, tools, characters and the design of the web site, layouts, methods, trials, functions and the software that make part of Ma Chlò, are protected by copyright and by all other intellectual rights of the Owner and the other holders of the rights. All reproduction, in whole or in part, in any form, of Ma Chlò, without written permission of the Owner, is forbidden.<br/><br/> Ma Chlò tightly guarantees an use of the personal data tied up to its own services, to the management of the site and the escape of the orders and they won\'t come in some way sold to third.<br/><br/> Ma Chlò cannot guarantee to its own consumers that the measures adopted for the safety of the site and the transmission of the data and the information on the site is able to limit or to exclude any risk of access not allowed or of dispersion of the data from devices of pertinence of the consumer. For such motive, we suggest to the consumers of the site to make sure that his/her own computer both endowed with suitable software for the protection of the transmission online of data (for instance adjourned antivirus) and that the proper Internet provider has adopted online fit measures for the safety of the data transmission.<br>
<h3>Payments</h3>
The consumer is able to choose the method of suitable payment during the purchase in the form of order. All of our products are made in Usa, the prices and the transactions are in Euro, possible changes currency, customs, errands on payments with credit card and Paypal are to load of the consumer. The financial information (Encircled Visa / Mastercard PayPal) will be cryptographically forwarded , without third parties being able to access said information in any way . Information will never be visualized or you memorize from Ma Chlò.<br/><br/>
<h3>Refund</h3>
The consumer has the right to withdraw from the contract concluded with the Vendor, within 14 working days from the day of receiving the products purchased on “Ma Chlò”.<br/> Return will be a customer\'s charge.<br/> An item cannot be exchanged for another one.<br/><br/> To ask for the authorization to theproduct, you can contact us by our web site on the section “contact” writing an e-mail message to the address info@machlo.com with indications of the product.<br/> The Right of Return is considered correctly followed when the following conditions are also completely met:<br/><br/> •	The products must not have been damaged, worn, washed and must not show any sign of use.<br/> •	the products must be returned in their original packaging to Ma Chlò within 14 days from the date of communication, from the consumer, of the right of recess.<br/><br/> The refund will be performed with the same metod used for the payment after having received the product.<br/><br/> Ma Chlò reserves the right to refuse non authorized refunds or however you doesn\'t conform to all the anticipated conditions.";
$lang['PAGE_RULES_IMAGE'] = "";
/* PRIVACY */
$lang['PAGE_PRIVACY_CODE'] = "PRIVACY";
$lang['PAGE_PRIVACY_TITLE'] = "Privacy";
$lang['PAGE_PRIVACY_META_DESCRIPTION'] = "Find out how we treat your sensitive data, use cookies, and sessions data.";
$lang['PAGE_PRIVACY_DESCRIPTION'] = "<h3>Cookie policy</h3>
This site uses some technical cookies that are used to navigate and provide a service already required by the user as the shopping cart. They are not used for further purposes and are normally installed on most websites. A cookie is a small piece of information that is saved on the user\\\'s device that visits a website. The cookie does not contain personal data and can not be used to identify the user inside other websites, including the analyst\\\'s website. Cookies can also be used to store your favorite settings, such as your language and country, so that you can make them available immediately to your next visit. We do not use IP addresses or cookies to personally identify users. We use the web analytics system to increase the efficiency of our portal. <br> <br> For more information on cookies, please visit www.allaboutcookies.org to provide you with directions on how to handle your cookies Your preferences, and possibly delete cookies as a browser you are using. <br> On this website we use the Google Analytics analysis system to measure and analyze visits to our site. We use IP addresses to collect data on Internet traffic, browsers, and user computers. This information is only examined for statistical purposes. The user\\\'s anonymity is respected. About the operation of the open source Google Analytics web analytics software. <br> <br> We reiterate that technical cookies (such as those listed above) are required to navigate and essential, such as authentication, validation, management of a session of Navigation and fraud prevention and allow, for example: to identify whether the user has regularly accessed areas of the site that require prior authentication or user validation and session management for various services and applications or the retention of Data for secure access or the control and prevention of fraud. <br> It is not compulsory to acquire the consent of operating only technical or third-party cookies or analytics similar to technical cookies. Their deactivation and / or denial of their operation will result in the inability to properly navigate the Site and / or the inability to access the services, pages, features or content available there. The data entered by our customers inside forms, shopping carts, orders and payment procedures will only be used for orders and deliveries. For this reason, customers are required to provide the data required to complete the required forms, otherwise we will not be able to use our service. In particular, the data used for online payments will not be treated and / or stored on Our systems as they have passed directly to the Stripe and / or Paypal payment services via Secure SSL Transaction (HTTPS). <br> At the time of ordering the quote or information, the user\\\'s email address will come Inserted in our newsletter system and customer information from which it will always be possible to easily disagree with the link in any communication sent. <br> In any case, we confirm that the data used and stored for the purpose of service will never and No case has been transferred to third parties for any purpose or use.";
$lang['PAGE_PRIVACY_IMAGE'] = "";
/* CONTACTS */
$lang['PAGE_CONTACTS_CODE'] = "CONTACTS";
$lang['PAGE_CONTACTS_TITLE'] = "Contacts and infos";
$lang['PAGE_CONTACTS_META_DESCRIPTION'] = "You can contact Ma Chlò in many different ways: email, phone and socials.";
$lang['PAGE_CONTACTS_DESCRIPTION'] = "";
$lang['PAGE_CONTACTS_IMAGE'] = "";
/* GALLERY */
$lang['PAGE_GALLERY_CODE'] = "GALLERY";
$lang['PAGE_GALLERY_TITLE'] = "Images gallery";
$lang['PAGE_GALLERY_META_DESCRIPTION'] = "This is our best images gallery.";
$lang['PAGE_GALLERY_DESCRIPTION'] = "";
$lang['PAGE_GALLERY_IMAGE'] = "";
