-- phpMyAdmin SQL Dump
-- version 4.3.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Mar 27, 2018 alle 16:26
-- Versione del server: 5.6.24
-- PHP Version: 5.6.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `machlo28_framework`
--

-- --------------------------------------------------------

--
-- Struttura della tabella `carrello`
--

CREATE TABLE IF NOT EXISTS `carrello` (
  `id_carrello` int(11) NOT NULL,
  `id_variante` int(11) NOT NULL,
  `id_prodotto` int(11) NOT NULL,
  `id_sessione_utente` varchar(250) DEFAULT NULL,
  `id_cliente` int(11) DEFAULT NULL,
  `data_creazione` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `qty` int(11) NOT NULL,
  `taglia` varchar(10) NOT NULL,
  `taglia_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `carrello`
--

INSERT INTO `carrello` (`id_carrello`, `id_variante`, `id_prodotto`, `id_sessione_utente`, `id_cliente`, `data_creazione`, `qty`, `taglia`, `taglia_id`) VALUES
(1, 188, 38, 'bba3324edc2abef363ecb7902a944c08', 0, '2018-03-26 13:57:23', 1, 'XS', 0);

-- --------------------------------------------------------

--
-- Struttura della tabella `categorie`
--

CREATE TABLE IF NOT EXISTS `categorie` (
  `id_categorie` int(11) NOT NULL,
  `url_categorie` varchar(25) NOT NULL,
  `nome` varchar(100) DEFAULT NULL,
  `descrizione` varchar(100) DEFAULT NULL,
  `immagine` varchar(150) NOT NULL,
  `label_color_class` int(11) NOT NULL,
  `stato` int(11) NOT NULL,
  `ordine` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `categorie`
--

INSERT INTO `categorie` (`id_categorie`, `url_categorie`, `nome`, `descrizione`, `immagine`, `label_color_class`, `stato`, `ordine`) VALUES
(1, 'tank-top', 'Tank Top', 'Canotta giromanica', 'tt.jpg', 1, 1, 2),
(2, 'crop-top', 'Crop Top', 'Top corto giromanica', 'ct.jpg', 2, 3, 5),
(3, 'cap-sleeve-top', 'Cap Sleeve Top', 'Cap Sleeve Top', 'cst.jpg', 3, 3, 8),
(4, 'fashion-fit-t-shirt', 'Fashion Fit T-Shirt', 'T-Shirt fashion attillate', 'fft.jpg', 1, 1, 7),
(5, 'love-story-masks', 'Love Story Masks', 'T-Shirt con maschere', 'lsm.jpg', 2, 1, 0),
(6, 'dress', 'Dress', 'Abiti', 'md.jpg', 3, 1, 4),
(7, 'racerback-tank-top', 'Racerback Tank Top', 'Canotte stile racer', 'rtt.jpg', 1, 3, 3),
(8, 'short-sleeve-t-shirt', 'Short Sleeve T-Shirt', 'T-Shirt manica accorciata', 'ssl.jpg', 2, 1, 10),
(9, 'long-sleeve-t-shirt', 'Long sleeve T-Shirt', 'Maglia semplice a maniche lunghe', '7a343-lsl_blucat_black_f.jpg', 1, 1, 9),
(10, 'woman', 'Woman', 'Prodotti da donna', '32d41-women.jpg', 3, 1, 10),
(12, 'new-collection', 'New collection', 'T-shirt donna', '435d2-ssl_colored_cats_black_f.jpg', 1, 1, 9),
(13, 'yoga-leggings', 'Yoga leggings', 'Leggings', '5bb59-lgg_masks_black_f.jpg', 1, 1, 7),
(14, 'black-friday', 'Black Friday', 'Promozione prodotti in Black Friday', '5761c-bf.jpg', 1, 1, 1),
(15, 'mugs_tazze', 'mugs', 'tazze in ceramica', '34e8b-cm_color_tree_red_f.jpg', 1, 1, 7),
(16, 'sale', 'Sale', 'articoli in saldo', '0274a-fft_yellowcat_white_f.jpg', 1, 1, 15);

-- --------------------------------------------------------

--
-- Struttura della tabella `categorie_gallery`
--

CREATE TABLE IF NOT EXISTS `categorie_gallery` (
  `id_categoria_gallery` int(11) NOT NULL,
  `nome_categoria_gallery` varchar(250) NOT NULL,
  `stato_categoria_gallery` tinyint(4) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `categorie_gallery`
--

INSERT INTO `categorie_gallery` (`id_categoria_gallery`, `nome_categoria_gallery`, `stato_categoria_gallery`) VALUES
(1, 'Prodotti', 1),
(2, 'Su di noi', 1),
(3, 'Coming soon', 2),
(4, 'Ma Chlò world', 1),
(5, 'Dipinti', 1);

-- --------------------------------------------------------

--
-- Struttura della tabella `categorie_gallery_traduzioni`
--

CREATE TABLE IF NOT EXISTS `categorie_gallery_traduzioni` (
  `id_categorie_gallery_traduzioni` int(11) NOT NULL,
  `id_categoria_gallery` int(11) NOT NULL,
  `descrizione_categoria_gallery` varchar(250) NOT NULL,
  `id_lingua` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `categorie_gallery_traduzioni`
--

INSERT INTO `categorie_gallery_traduzioni` (`id_categorie_gallery_traduzioni`, `id_categoria_gallery`, `descrizione_categoria_gallery`, `id_lingua`) VALUES
(1, 1, 'Prodotti', 1),
(2, 1, 'Products', 2),
(3, 2, 'Su di noi', 1),
(4, 2, 'About us', 2),
(5, 3, 'Novità', 1),
(6, 3, 'Coming soon', 2),
(9, 4, 'Ma Chlò world', 2),
(10, 4, 'Ma Chlò world', 1),
(11, 5, 'Dipinti', 1),
(12, 5, 'Artworks', 2);

-- --------------------------------------------------------

--
-- Struttura della tabella `categorie_traduzioni`
--

CREATE TABLE IF NOT EXISTS `categorie_traduzioni` (
  `id_categoria_traduzioni` int(11) NOT NULL,
  `id_categoria` int(11) NOT NULL,
  `nome_categoria_trad` varchar(250) NOT NULL,
  `id_lingua` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `categorie_traduzioni`
--

INSERT INTO `categorie_traduzioni` (`id_categoria_traduzioni`, `id_categoria`, `nome_categoria_trad`, `id_lingua`) VALUES
(1, 6, 'Abiti', 1),
(2, 6, 'Dress', 2),
(3, 8, 'T-shirt manica accorciata', 1),
(4, 8, 'Short sleeve t-shirt', 2);

-- --------------------------------------------------------

--
-- Struttura della tabella `clienti`
--

CREATE TABLE IF NOT EXISTS `clienti` (
  `id_cliente` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `nome` varchar(250) DEFAULT NULL,
  `cognome` varchar(250) DEFAULT NULL,
  `email` varchar(250) NOT NULL,
  `telefono` varchar(15) DEFAULT NULL,
  `partita_iva` varchar(11) CHARACTER SET latin1 DEFAULT NULL,
  `codice_fiscale` varchar(16) CHARACTER SET latin1 DEFAULT NULL,
  `newsletter` tinyint(4) NOT NULL DEFAULT '0',
  `punti` bigint(20) NOT NULL DEFAULT '0',
  `id_lingua` int(11) NOT NULL DEFAULT '1',
  `id_indirizzo_fatturazione` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `clienti`
--

INSERT INTO `clienti` (`id_cliente`, `user_id`, `nome`, `cognome`, `email`, `telefono`, `partita_iva`, `codice_fiscale`, `newsletter`, `punti`, `id_lingua`, `id_indirizzo_fatturazione`) VALUES
(1, 6, 'Stefania', 'Laconi', 'stef.laconi@gmail.com', '3386819197', '', '', 0, 0, 1, 2),
(2, 7, 'Francesca', 'Meloni', 'francescamariameloni@gmail.com', '3465617042', NULL, NULL, 0, 0, 1, 3),
(3, 8, 'Valentina', 'Deriu', 'valentina.deriu92@gmail.com', '3891966592', '', 'DREVNT92C59I452S', 0, 0, 1, 4),
(5, 0, 'Dario', 'Bonelli', 'dott.bon@hotmail.it', '3408064869', NULL, NULL, 1, 0, 1, 10),
(6, 10, 'Michela', 'Cossu', '', NULL, NULL, NULL, 0, 0, 1, 19),
(13, 0, 'Pippo', 'Baudo', 'pippobaudo@hotmcash.com', '2342342343', '', 'PPPBBBDAHJASD', 1, 0, 1, 20);

-- --------------------------------------------------------

--
-- Struttura della tabella `colori_classi`
--

CREATE TABLE IF NOT EXISTS `colori_classi` (
  `id_colore_classe` int(11) NOT NULL,
  `colore_classe` varchar(100) NOT NULL,
  `colore_classe_nome` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `colori_classi`
--

INSERT INTO `colori_classi` (`id_colore_classe`, `colore_classe`, `colore_classe_nome`) VALUES
(1, 'text-danger', 'Rosso'),
(2, 'text-success', 'Verde'),
(3, 'text-warning', 'Arancio');

-- --------------------------------------------------------

--
-- Struttura della tabella `colori_prodotti`
--

CREATE TABLE IF NOT EXISTS `colori_prodotti` (
  `id_colori_prodotti` int(11) NOT NULL,
  `nome_colore` varchar(50) NOT NULL,
  `codice_colore` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `colori_prodotti`
--

INSERT INTO `colori_prodotti` (`id_colori_prodotti`, `nome_colore`, `codice_colore`) VALUES
(1, 'Black', '000000'),
(2, 'White', 'ffffff'),
(3, 'Aqua', '43a9d1'),
(4, 'Berry', 'd2528f'),
(5, 'Blue', '6680b3'),
(6, 'Coral', 'ce474d'),
(7, 'Creamy', 'e5ded8'),
(8, 'Navy', '26273b'),
(9, 'Purple', '523756'),
(10, 'Pink', 'eba3b9'),
(11, 'Raspberry', 'a91671'),
(12, 'Jade', '9cc3c0'),
(13, 'Red', 'ae2f38'),
(14, 'Yellow', 'f3dc5d'),
(15, 'Green', '357249'),
(16, 'Darkgrey', '45413e'),
(17, 'Hotpink', 'db2c77'),
(18, 'Royal', '3e609d'),
(19, 'Strawberry', 'a11927'),
(20, 'Silver', 'c6c6c6'),
(21, 'Darkgreen', '35734a'),
(22, 'Grey', '474340'),
(23, 'White', 'e5e6e1'),
(24, 'White', '');

-- --------------------------------------------------------

--
-- Struttura della tabella `constants_framework`
--

CREATE TABLE IF NOT EXISTS `constants_framework` (
  `id_cf` int(11) NOT NULL,
  `cf_name` varchar(100) NOT NULL,
  `cf_value` varchar(250) NOT NULL,
  `cf_description` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `constants_framework`
--

INSERT INTO `constants_framework` (`id_cf`, `cf_name`, `cf_value`, `cf_description`) VALUES
(1, 'SITE_URL_PATH', 'https://www.machlo.com', 'Indirizzo online del sito'),
(2, 'SITE_TITLE_NAME', 'Ma Chlò', 'Titolo del sito online'),
(3, 'SMTP_HOST_CUSTOM', 'secureit14.sgcpanel.com', 'Indirizzo del server SMTP'),
(4, 'SMTP_USER_CUSTOM', 'info@machlo.com', 'Utente del server SMTP'),
(5, 'SMTP_PASS_CUSTOM', 'mach17@Info', 'Password del server SMTP'),
(6, 'DB_HOSTNAME', '77.104.188.114', 'Indirizzo del DB'),
(7, 'DB_USERNAME', 'machlo28', 'Utente del DB'),
(8, 'DB_PASSWORD', 'mach17@Cpanel', 'Password del DB'),
(9, 'DB_DATABASE', 'machlo28_framework', 'Nome del DB'),
(10, 'COMPANY_NAME', 'Ma Chlò', 'Nome della società/ditta'),
(11, 'COMPANY_EMAIL', 'info@machlo.com', 'Email della società/ditta'),
(12, 'COMPANY_COPYRIGHT', 'Copyright © 2017 Ma Chlò', 'Testo footer copyright'),
(13, 'COMPANY_PHONE', '+39  3384095440', 'Telefono società/ditta'),
(14, 'COMPANY_ADDRESS', 'Sardegna', 'Indirizzo società/ditta'),
(15, 'GPLUS_LINK', '', 'Link social G+'),
(16, 'YOUTUBE_LINK', '', 'Link social YOUTUBE'),
(17, 'PINTEREST_LINK', '', 'Link social PINTEREST'),
(18, 'TWITTER_LINK', '', 'Link social TWITTER'),
(19, 'FACEBOOK_LINK', 'https://www.facebook.com/machloofficial/', 'Link social FACEBOOK'),
(20, 'INSTAGRAM_LINK', 'https://www.instagram.com/machloofficial/', 'Link social INSTAGRAM'),
(21, 'GOOGLE_ANALITYCS_ID', 'UA-101846078-1', 'UID Google Analitycs'),
(22, 'PERC_PUNTI_RETURN', '10', 'Percentuale di ritorno punti cliente'),
(23, 'COMNINGSOON_LOGO', 'logo.png', 'File logo del COMING SOON'),
(24, 'COMNINGSOON_BACK_IMAGE', 'default.jpg', 'File immagine di sfondo Coming soon'),
(25, 'COMNINGSOON_BTN_COLOR', 'EE2D20', 'Colore del pulsante Coming soon'),
(26, 'PAYPAL_EMAIL', 'francescamariameloni@gmail.com', 'Email di Paypal per i pagamenti'),
(27, 'PAYPAL_ENV', 'production', 'Environment di paypal [sandbox]'),
(28, 'PAYPAL_SANDBOX_CLIENT_ID', 'AePtA0e7pTraKV9LHzlmvJwPhCkH8rMdQVn-SmI26u3UwJHUQdiElVfBJbXG1ki56YmkdvjD_TPruv0u', 'Cliend ID sandbox di Paypal'),
(29, 'PAYPAL_LIVE_CLIENT_ID', 'AeOj9VKIkjAZ55ehDSwWLi5SX-shtu4zI01YHb39ZgAqXXaxTikI8gfzLeSwtus01zBtd-iz-9rZXGoA', 'Cliend ID sandbox di Paypal LIVE'),
(30, 'STRIPE_PK', 'pk_live_rDVufl5jZSMlu4gUYqLGnAwI', 'Stipe Public Key'),
(31, 'STRIPE_SK', 'sk_live_bOElpqdJnCci1yfVKvrNsoE4', 'Stripe Secret Key'),
(32, 'PRINTFUL_APIKEY', 'naq7o8kj-6x61-iahm:qqv6-cad6lshspq29', 'Api key di Printful'),
(33, 'POINTS_ENABLED', '0', 'Gestione punti (0 disabilitati | 1 abilitati)');

-- --------------------------------------------------------

--
-- Struttura della tabella `contatti_moduli`
--

CREATE TABLE IF NOT EXISTS `contatti_moduli` (
  `id_contatto` int(11) NOT NULL,
  `nome` varchar(250) NOT NULL,
  `email` varchar(250) NOT NULL,
  `telefono` varchar(250) NOT NULL,
  `messaggio` text NOT NULL,
  `data` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `stato_contatto` tinyint(4) NOT NULL,
  `data_unsubscribe` datetime DEFAULT NULL,
  `id_lingua` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `contatti_moduli`
--

INSERT INTO `contatti_moduli` (`id_contatto`, `nome`, `email`, `telefono`, `messaggio`, `data`, `stato_contatto`, `data_unsubscribe`, `id_lingua`) VALUES
(1, 'Roberto', 'roberto.rossi77@gmail.com', '0', 'Ciao ecco il messaggio', '2018-01-09 06:10:02', 1, NULL, 1),
(2, 'Stefania', 'stef.laconi@gmail.com', '0', 'Info', '2018-01-24 07:22:14', 1, NULL, 1),
(3, 'Stefania Laconi', 'stef.laconi@gmail.com', '0', 'Salve,\nvorrei avere maggiori info riguardanti i vostri prodotti.', '2018-01-24 07:24:40', 1, NULL, 1),
(4, '0', '0', '0', '0', '2018-01-24 10:35:15', 1, NULL, 1),
(5, 'Stefania', 'stef.laconi@gmail.com', '0', 'Richiedo cortesemente maggiori info ', '2018-01-29 04:34:47', 1, NULL, 1),
(6, 'Valentina', 'valentina.deriu92@gmail.com', '', '0', '2018-01-07 23:00:00', 1, NULL, 1);

-- --------------------------------------------------------

--
-- Struttura della tabella `contatti_newsletter`
--

CREATE TABLE IF NOT EXISTS `contatti_newsletter` (
  `id_contatto_newsletter` int(11) NOT NULL,
  `email_contatto` varchar(250) NOT NULL,
  `data_contatto` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `stato_contatto` tinyint(4) NOT NULL,
  `data_unsubscribe` datetime DEFAULT NULL,
  `lingua_traduzione_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `contatti_newsletter`
--

INSERT INTO `contatti_newsletter` (`id_contatto_newsletter`, `email_contatto`, `data_contatto`, `stato_contatto`, `data_unsubscribe`, `lingua_traduzione_id`) VALUES
(1, 'roberto.rossi77@gmail.com', '2018-01-09 13:18:15', 1, NULL, 1),
(2, 'stef.laconi@gmail.com', '2018-01-12 13:47:31', 1, NULL, 1),
(3, 'stefania.laconi@tiscali.it', '2018-01-24 07:22:44', 1, NULL, 1),
(4, 'francescamariameloni@gmail.com', '2018-01-30 06:18:33', 1, NULL, 1),
(5, 'veronikka@live.it', '2018-02-02 08:53:10', 1, NULL, 1),
(6, 'tattiornella@gmail.com', '2018-02-02 11:00:47', 1, NULL, 1),
(7, 'valentina.deriu92@gmail.com', '2018-01-07 23:00:00', 1, NULL, 1),
(8, 'mikelacossu80@gmail.com', '2018-02-07 06:42:23', 1, NULL, 1);

-- --------------------------------------------------------

--
-- Struttura della tabella `coupon`
--

CREATE TABLE IF NOT EXISTS `coupon` (
  `id_coupon` int(11) NOT NULL,
  `data_scadenza_coupon` datetime DEFAULT NULL,
  `importo_coupon` double NOT NULL,
  `percentuale_coupon` int(5) NOT NULL,
  `codice_coupon` varchar(250) NOT NULL,
  `stato_coupon` int(11) NOT NULL,
  `tipo_coupon` int(11) NOT NULL,
  `utilizzatore_coupon` varchar(250) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `coupon`
--

INSERT INTO `coupon` (`id_coupon`, `data_scadenza_coupon`, `importo_coupon`, `percentuale_coupon`, `codice_coupon`, `stato_coupon`, `tipo_coupon`, `utilizzatore_coupon`) VALUES
(1, '2018-02-28 00:00:00', 0, 10, 'FHEMWUNJ', 1, 4, ''),
(2, '2018-03-31 00:00:00', 0, 15, 'FYTBHFNA', 1, 2, ''),
(3, '2018-12-31 00:00:00', 0, 15, 'HHILZWYA', 1, 4, ''),
(4, '2018-06-21 00:00:00', 0, 15, 'RORHDVGJ', 1, 4, '');

-- --------------------------------------------------------

--
-- Struttura della tabella `email_templates`
--

CREATE TABLE IF NOT EXISTS `email_templates` (
  `id_template` int(11) NOT NULL,
  `nome_template` varchar(250) NOT NULL,
  `subject_template` varchar(250) NOT NULL,
  `titolo_template` varchar(250) NOT NULL,
  `testo_template` text NOT NULL,
  `id_tipo_template` int(11) NOT NULL,
  `lingua_traduzione_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `email_templates`
--

INSERT INTO `email_templates` (`id_template`, `nome_template`, `subject_template`, `titolo_template`, `testo_template`, `id_tipo_template`, `lingua_traduzione_id`) VALUES
(1, 'Prova ', 'Newsletter da Ma Chlò', 'La mia prima newsletter', 'Questo è il body della mia priam newsletter <br><br>\r\nGrazie\r\n<a href="buongiornoamore.it">ba</a>', 2, 1);

-- --------------------------------------------------------

--
-- Struttura della tabella `groups`
--

CREATE TABLE IF NOT EXISTS `groups` (
  `id` mediumint(8) unsigned NOT NULL,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `groups`
--

INSERT INTO `groups` (`id`, `name`, `description`) VALUES
(1, 'admin', 'Administrator'),
(2, 'members', 'General User'),
(3, 'customers', 'Customer of ecommerce shop');

-- --------------------------------------------------------

--
-- Struttura della tabella `home_slider`
--

CREATE TABLE IF NOT EXISTS `home_slider` (
  `hs_id` int(11) NOT NULL,
  `hs_image` varchar(250) NOT NULL,
  `hs_order` int(11) NOT NULL,
  `hs_title` varchar(250) NOT NULL,
  `id_lingua` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `home_slider`
--

INSERT INTO `home_slider` (`hs_id`, `hs_image`, `hs_order`, `hs_title`, `id_lingua`) VALUES
(1, '02.jpg', 1, 'Colora le emozioni con<br>Ma Chlò', 1),
(2, '03.jpg', 2, '', 1),
(3, '02_en.jpg', 1, 'Paint emotions with<br>Ma Chlò', 2),
(4, '01.jpg', 3, '', 1),
(5, '03_en.jpg', 2, '', 2),
(6, '01_en.jpg', 3, '', 2);

-- --------------------------------------------------------

--
-- Struttura della tabella `immagini_gallery`
--

CREATE TABLE IF NOT EXISTS `immagini_gallery` (
  `id_ig` int(11) NOT NULL,
  `id_categoria_ig` int(11) NOT NULL,
  `nome_ig` varchar(250) NOT NULL,
  `stato_ig` tinyint(4) NOT NULL,
  `immagine_thumb_ig` varchar(250) NOT NULL,
  `immagine_ig` varchar(250) NOT NULL,
  `ordine_ig` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `immagini_gallery`
--

INSERT INTO `immagini_gallery` (`id_ig`, `id_categoria_ig`, `nome_ig`, `stato_ig`, `immagine_thumb_ig`, `immagine_ig`, `ordine_ig`) VALUES
(1, 1, 'Uno', 2, '', '', 0),
(2, 2, 'Due', 2, '', '', 0),
(3, 3, 'Tre', 2, '', '', 2),
(4, 1, 'Quattro', 2, '', '', 4),
(5, 4, 'Masks t-shirt', 1, 'e2b4b-masks_thumbs.jpg', '4821d-masks.jpg', 5),
(6, 5, 'Il bacio', 1, 'e39d0-il_bacio_thumbs.jpg', '91ab5-il_bacio.jpg', 67),
(7, 5, 'Il pensiero', 1, '4e001-il_pensiero_thumbs.jpg', 'd8a1f-il_pensiero.jpg', 68),
(8, 5, 'Il silenzio', 1, 'cfea9-il_silenzio_thumbs.jpg', '4a5d6-il_silenzio.jpg', 64),
(9, 5, 'Incontro', 1, '423c4-incontro_thumbs.jpg', '0de31-incontro.jpg', 70),
(10, 5, 'Infinito', 1, 'eb615-infinito_thumbs.jpg', '8772b-infinito.jpg', 65),
(11, 5, 'La separazione', 1, '57b01-la_separazione_thumbs.jpg', '872d0-la_separazione.jpg', 63),
(12, 5, 'Lo sguardo', 1, 'bfc64-lo_sguardo_thumbs.jpg', '3f39f-lo_sguardo.jpg', 69),
(13, 5, 'Un nuovo inizio', 1, 'bcd37-un_nuovo_inizio_thumbs.jpg', '20947-un_nuovo_inizio.jpg', 66),
(14, 2, 'Stefania e Francesca', 1, 'dcf66-stefania_e_francesca_thumbs.jpg', '9011d-stefania_e_francesca.jpg', 5),
(15, 4, 'Girls', 1, '33725-girls3_thumbs.jpg', '9f1d8-girls3.jpg', 4),
(16, 5, 'Colored tree', 1, '74c28-colored_tree_thumbs.jpg', '75e15-colored_tree.jpg', 19),
(17, 5, 'Dreaming on a colored tree', 1, 'a3af3-dreaming_on_a_colored_tree_thumbs.jpg', 'e8d08-dreaming_on_a_colored_tree.jpg', 18),
(18, 5, 'Life o a tree', 1, '70a75-life_of_a_tree_thumbs.jpg', 'bf81e-life_of_a_tree.jpg', 17),
(19, 5, 'Flowers', 1, '24fe6-flowers_thumbs.jpg', '5c9c8-flowers.jpg', 23),
(20, 5, 'Monkeys into the jungle', 1, '9130f-monkeys_into_the_jungle_thumbs.jpg', 'b9e80-monkeys_into_the_jungle.jpg', 21),
(21, 5, 'Reaching for the moon', 1, 'dda19-reaching_for_the_moon_thumbs.jpg', 'c8c87-reaching_for_the_moon.jpg', 16),
(22, 1, 'Mug', 1, '9a7c2-a_monkey_playing_with_a_snake_thumbs.jpg', '50a7c-a_monkey_playing_with_a_snake.jpg', 0),
(23, 4, 'Jungle flowers dress', 1, '6ecd8-jungle_flowers_dress_thumbs.jpg', '13d3d-jungle_flowers_dress.jpg', 3),
(24, 4, 'Ma Chlò', 1, '4ed09-machlo_thumbs.jpg', 'b23ae-machlo.jpg', 5),
(25, 4, 'Lips t-shirt', 1, '7d661-lips_tshirt_thumbs.jpg', 'b51f7-lips_tshirt.jpg', 4),
(26, 5, 'a woman with a monkey', 1, '8cdda-a_woman_with_a_monkey_thumbs.jpg', '36eff-a_woman_with_a_monkey.jpg', 20),
(27, 5, 'A monkey playing with a snake', 1, '3cbcb-a_monkey_playing_a_snake_thumbs.jpg', '0d177-a_monkey_playing_with_a_snake1.jpg', 22),
(28, 5, 'Gallinella sarda', 1, 'd7dc8-gallinella_sarda_thumbs.jpg', '2a1c2-gallinella_sarda.jpg', 42),
(29, 5, 'Snakes', 1, 'e5773-snakes_thumbs.jpg', '1a470-snakes.jpg', 40),
(30, 5, 'Colorful town', 1, 'd8a8d-colorful_town_thumbs.jpg', '56766-colorful_town.jpg', 30),
(31, 5, 'Wild girl', 1, '79bcc-wild_girl_thumbs.jpg', '292c9-wild_girl.jpg', 29),
(32, 5, 'Talking', 1, '710aa-talking_thumbs.jpg', '7ebb2-talking.jpg', 18),
(33, 5, 'Sleeping mask', 1, '5f62b-sleeping_mask_thumbs.jpg', '881da-sleeping_mask.jpg', 51),
(34, 5, 'geometric', 1, '762ff-geometric_thumbs.jpg', '2b416-geometric.jpg', 22),
(35, 5, 'Eclipse', 1, 'c4580-eclipse_thumbs.jpg', '11f66-eclipse.jpg', 62),
(36, 5, 'Jellyfish', 1, '38a22-jellyfish_thumbs.jpg', 'ab602-jellyfish.jpg', 61),
(37, 5, 'Japan', 1, 'b0921-japan_thumbs.jpg', 'b3042-japan.jpg', 60),
(38, 5, 'Explosion', 1, 'eedac-explosion_thumbs.jpg', 'a5716-explosion.jpg', 59),
(39, 5, 'Kaos', 1, '8f7cc-kaos_thumbs.jpg', 'ce2c1-kaos.jpg', 58),
(40, 5, 'Rain', 1, '208d8-rain_thumbs.jpg', 'bcdd1-rain.jpg', 57),
(41, 5, 'Stand by', 1, '8c5ca-standby_thumbs.jpg', '9c3e8-standby.jpg', 56),
(42, 5, 'Storm', 1, 'a9516-storm_thumbs.jpg', '9b09f-storm.jpg', 55),
(43, 5, 'Yang', 1, '6c2da-yang_thumbs.jpg', '94d1e-yang.jpg', 54),
(44, 5, 'Universe', 1, '73e62-universe_thumbs.jpg', '7fe16-universe.jpg', 53),
(45, 1, 'Lips dress', 1, 'ed97d-lips_dress_thumbs-2-.jpg', 'e8a69-lips_dress.jpg', 0),
(46, 1, 'Masks', 1, 'd5915-tshirt_masks_thumbs.jpg', '1dcc2-tshirt_masks.jpg', 2),
(47, 1, 'Yoga leggings', 1, '056de-yoga_leggings_thumbs.jpg', 'f230e-yoga_leggings.jpg', 5),
(48, 4, 'Jumping', 1, 'eb42e-jumping_thumbs.jpg', '36987-jumping.jpg', 8),
(49, 1, 'Love story masks', 1, '18daa-un_nuovo_inizio_tshirt_thumbs-2-.jpg', '119d0-un_nuovo_inizio_tshirt.jpg', 10);

-- --------------------------------------------------------

--
-- Struttura della tabella `immagini_gallery_traduzioni`
--

CREATE TABLE IF NOT EXISTS `immagini_gallery_traduzioni` (
  `id_immagini_gallery_traduzioni` int(11) NOT NULL,
  `titolo_ig` varchar(250) NOT NULL,
  `testo_ig` varchar(250) NOT NULL,
  `id_lingua` int(11) NOT NULL,
  `id_ig` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `immagini_gallery_traduzioni`
--

INSERT INTO `immagini_gallery_traduzioni` (`id_immagini_gallery_traduzioni`, `titolo_ig`, `testo_ig`, `id_lingua`, `id_ig`) VALUES
(1, 'Titolo 1', 'TEsto 1', 1, 1),
(2, 'Titolo 1', 'Text 1', 2, 1),
(3, 'Titolo 2', 'Testo 2', 1, 2),
(4, 'Title 2', 'Text 3', 2, 2),
(5, 'Titolo 3', 'Testo 3', 1, 3),
(6, 'Title 3', 'Text 3', 2, 3),
(7, 'Titolo 4', 'Testo 4', 1, 4),
(8, 'Title 4', 'Text 4', 2, 4),
(10, 'Masks ', 'T-shirt con maschere', 1, 5),
(11, 'Masks', 'Masks t-shirt', 2, 5),
(12, 'Il bacio', 'Dipinto il bacio', 1, 6),
(13, 'Il bacio', 'Il bacio painting', 2, 6),
(14, 'Il pensiero', 'Dipinto il pensiero', 1, 7),
(15, 'Il pensiero', 'Il pensiero painting', 2, 7),
(16, 'Il silenzio', 'Dipinto il silenzio', 1, 8),
(17, 'Il silenzio', 'Il silenzio painting', 2, 8),
(18, 'Incontro', 'Dipinto incontro', 1, 9),
(19, 'Incontro', 'Incontro painting', 2, 9),
(20, 'Infinito', 'Dipinto infinito', 1, 10),
(21, 'Infinito', 'Infinito painting', 2, 10),
(22, 'La separazione', 'Dipinto la separazione', 1, 11),
(23, 'La separazione', 'La separazione painting', 2, 11),
(24, 'Lo sguardo', 'Dipinto lo sguardo', 1, 12),
(25, 'Lo sguardo', 'Lo sguardo painting', 2, 12),
(26, 'Un nuovo inizio', 'Dipinto un nuovo inizio', 1, 13),
(27, 'Un nuovo inizio', 'Un nuovo inizio painting', 2, 13),
(28, 'Stefania e Francesca', 'Ritratto di Stefania e Francesca', 1, 14),
(29, 'Stefania e Francesca', 'Stefania and Francesca portrait', 2, 14),
(30, 'Girls', 'Ritratto Ma Chlò girls', 1, 15),
(31, 'Girls', 'Ma Chlò girls', 2, 15),
(32, 'Colored tree', 'Dipinto colored tree', 1, 16),
(33, 'Colored tree', 'Colored tree painting', 2, 16),
(34, 'Dreaming on a colored tree', 'Dipinto dreaming on a colored tree', 1, 17),
(35, 'Dreaming on a colored tree', 'Dreaming on a colored tree painting', 2, 17),
(36, 'Life of a tree', 'Dipinto life of a tree', 1, 18),
(37, 'Life of a tree', 'Life of a tree painting', 2, 18),
(38, 'Flowers', 'Dipinto flowers', 1, 19),
(39, 'Flowers', 'Flowers painting', 2, 19),
(40, 'Monkeys into the jungle', 'Dipinto monkeys into the jungle', 1, 20),
(41, 'Monkeys into the jungle', 'Monkeys into the jungle painting', 2, 20),
(42, 'Reaching for the moon', 'Dipinto reaching for the moon', 1, 21),
(43, 'Reaching for the moon', 'Reaching for the moon painting', 2, 21),
(44, 'Mug', 'tazza in ceramica', 1, 22),
(45, 'Mug', 'Ceramic mug', 2, 22),
(46, 'Jungle flowers', 'Jungle flowers dress', 1, 23),
(47, 'Jungle flowers', 'Jungle flowers dress', 2, 23),
(48, 'Ma Chlò', 'Foto Ma Chlò', 1, 24),
(49, 'Ma Chlò', 'Ma Chlò portrait', 2, 24),
(50, 'Lips ', 'Lips T-shirt', 1, 25),
(51, 'Lips', 'Lips T-shirt', 2, 25),
(52, 'a woman with a monkey', 'Dipinto a woman with a monkey', 1, 26),
(53, 'a woman with a monkey', 'A woman with a monkey painting', 2, 26),
(54, 'A monkey playing with a snake', 'Dipinto a monkey playing with a snake', 1, 27),
(55, 'A monkey playing with a snake', 'A monkey playing with a snake painting', 2, 27),
(56, 'Gallinella sarda', 'Dipinto gallinella sarda', 1, 28),
(57, 'Gallinella sarda', 'Gallinella sarda painting', 2, 28),
(58, 'Snakes', 'Dipinto snakes', 1, 29),
(59, 'Snakes', 'Snakes painting', 2, 29),
(60, 'Colorful town', 'Dipinto colorful town', 1, 30),
(61, 'Colorful town', 'Colorful town painting', 2, 30),
(62, 'Wild girl', 'Dipinto wild girl', 1, 31),
(63, 'Wild girl', 'Wild girl painting', 2, 31),
(64, 'Talking', 'Dipinto talking', 1, 32),
(65, 'Talking', 'Talking painting', 2, 32),
(66, 'Sleeping mask', 'Dipinto sleeping mask', 1, 33),
(67, 'Sleeping mask', 'Sleeping mask painting', 2, 33),
(68, 'Geometric', 'Dipinto geometric', 1, 34),
(69, 'Geometric', 'Geometric painting', 2, 34),
(70, 'Eclipse', 'Dipinto eclipse', 1, 35),
(71, 'Eclipse', 'Eclipse painting', 2, 35),
(72, 'Jellyfish', 'Dipinto jellyfish', 1, 36),
(73, 'Jellyfish', 'Jellyfish painting', 2, 36),
(74, 'Japan', 'Dipinto japan', 1, 37),
(75, 'Japan', 'Japan painting', 2, 37),
(76, 'Explosion', 'Dipinto explosion', 1, 38),
(77, 'Explosion', 'Explosion painting', 2, 38),
(78, 'Kaos', 'Dipinto kaos', 1, 39),
(79, 'Kaos', 'Kaos painting', 2, 39),
(80, 'Rain', 'Dipinto rain', 1, 40),
(81, 'Rain', 'Rain painting', 2, 40),
(82, 'Stand by', 'Dipinto stand by', 1, 41),
(83, 'Stand by', 'Stand by painting', 2, 41),
(84, 'Storm', 'Dipinto storm', 1, 42),
(85, 'Storm', 'Storm painting', 2, 42),
(86, 'Yang', 'Dipinto yang', 1, 43),
(87, 'Yang', 'Yang painting', 2, 43),
(88, 'Universe', 'Dipinto universe', 1, 44),
(89, 'Universe', 'Universe painting', 2, 44),
(91, 'Lips', 'Abito lips', 1, 45),
(92, 'Lips', 'Lips dress', 2, 45),
(93, 'Masks', 'T-shirt con maschere', 1, 46),
(94, 'Masks', 'Masks t-shirt', 2, 46),
(95, 'Yoga leggings', 'Yoga leggings eclipse', 1, 47),
(96, 'Yoga leggings', 'Eclipse yoga leggings', 2, 47),
(97, 'Jumping', 'Lips t-shirt', 1, 48),
(98, 'Jumping', 'Lips t-shirt', 2, 48),
(99, 'Love story masks', 'T-shirt love story masks', 1, 49),
(100, 'Love story masks', 'Love story masks t-shirt', 2, 49);

-- --------------------------------------------------------

--
-- Struttura della tabella `indirizzo_fatturazione`
--

CREATE TABLE IF NOT EXISTS `indirizzo_fatturazione` (
  `id_indirizzo_fatturazione` bigint(20) NOT NULL,
  `id_cliente` int(11) NOT NULL,
  `indirizzo_fatt` varchar(255) DEFAULT NULL,
  `civico_fatt` varchar(15) DEFAULT NULL,
  `cap_fatt` varchar(10) DEFAULT NULL,
  `nazione_fatt` varchar(50) DEFAULT NULL,
  `citta_fatt` varchar(50) DEFAULT NULL,
  `riferimento_fatt` varchar(250) DEFAULT NULL,
  `note_fatt` varchar(100) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `indirizzo_fatturazione`
--

INSERT INTO `indirizzo_fatturazione` (`id_indirizzo_fatturazione`, `id_cliente`, `indirizzo_fatt`, `civico_fatt`, `cap_fatt`, `nazione_fatt`, `citta_fatt`, `riferimento_fatt`, `note_fatt`) VALUES
(1, 0, 'via Agrigento ', '38/b', '07026', 'Italia', 'Olbia', '', ''),
(2, 1, 'via Agrigento', '07026', '07026', 'Italia', 'Olbia', 'Olbia', ''),
(3, 0, 'Viale Aldo Moro', '07026', '07026', 'Italia', 'Olbia', 'Olbia', ''),
(4, 0, 'Mini Market Masala Via Tinnura, 27', '27', '08010', 'Italia', 'Suni', 'Nonna Isa', 'Mini Market Masala Via Tinnura'),
(9, 0, 'Via Civitavecchia ', '34', '07026', 'Italia ', 'Olbia', 'Dario Bonelli', ''),
(10, 0, 'Via Civitavecchia ', '07026', '07026', 'Italia ', 'Olbia', 'Olbia', ''),
(19, 6, 'Via Bollater', '45', '00178', 'italia', 'Roma', NULL, NULL),
(20, 13, 'Viale Prova', '12', '00478', 'Italia', 'Roma', NULL, NULL);

-- --------------------------------------------------------

--
-- Struttura della tabella `indirizzo_spedizione`
--

CREATE TABLE IF NOT EXISTS `indirizzo_spedizione` (
  `id_indirizzo_spedizione` bigint(20) NOT NULL,
  `id_cliente` int(11) NOT NULL,
  `indirizzo_sped` varchar(255) DEFAULT NULL,
  `civico_sped` varchar(15) DEFAULT NULL,
  `cap_sped` varchar(10) DEFAULT NULL,
  `nazione_sped` varchar(50) DEFAULT NULL,
  `citta_sped` varchar(50) DEFAULT NULL,
  `riferimento_sped` varchar(250) DEFAULT NULL,
  `note_sped` varchar(100) DEFAULT NULL,
  `flag_predefinito_sped` tinyint(4) NOT NULL DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `indirizzo_spedizione`
--

INSERT INTO `indirizzo_spedizione` (`id_indirizzo_spedizione`, `id_cliente`, `indirizzo_sped`, `civico_sped`, `cap_sped`, `nazione_sped`, `citta_sped`, `riferimento_sped`, `note_sped`, `flag_predefinito_sped`) VALUES
(1, 13, 'Via Nazionale', '14', '00157', 'Italia', 'Novara', 'Pippo2', 'note 2', 1),
(2, 13, 'Viale Mani dal Naso', '15', '00145', 'Italia', 'Roma', 'Pippotto', 'Piano interrato', 0),
(3, 13, 'Via dei mille', '12/B', '01545', 'Italia', 'Napoli', NULL, NULL, 0),
(4, 13, 'Via Roma', '14', '80125', 'Italia', 'Napoli', NULL, NULL, 0),
(5, 13, 'Via Marina', '4', '80100', 'Italia', 'Napoli', 'Io', 'casa', 0),
(6, 13, 'Piazza Scotti', '145', '00178', 'Italia', 'Roma', NULL, NULL, 0),
(7, 13, 'Piazza Tuscolo', '5', '00145', 'Italia', 'Roma', NULL, NULL, 0);

-- --------------------------------------------------------

--
-- Struttura della tabella `lingue`
--

CREATE TABLE IF NOT EXISTS `lingue` (
  `id_lingue` int(11) NOT NULL,
  `nome_lingue` varchar(50) NOT NULL,
  `abbr_lingue` varchar(10) NOT NULL,
  `locale_paypal_lingue` varchar(10) NOT NULL,
  `codice_ci` varchar(150) NOT NULL,
  `stato_lingua` tinyint(4) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `lingue`
--

INSERT INTO `lingue` (`id_lingue`, `nome_lingue`, `abbr_lingue`, `locale_paypal_lingue`, `codice_ci`, `stato_lingua`) VALUES
(1, 'ITALIANO', 'IT', 'it_IT', 'italian', 1),
(2, 'ENGLISH', 'EN', 'en_GB', 'english', 1);

-- --------------------------------------------------------

--
-- Struttura della tabella `lingue_labels_lang`
--

CREATE TABLE IF NOT EXISTS `lingue_labels_lang` (
  `id_lingue_labels_lang` int(11) NOT NULL,
  `lingue_labels_lang_label` varchar(250) NOT NULL,
  `lingue_labels_lang_value` text NOT NULL,
  `lingue_labels_lang_desc` varchar(250) NOT NULL,
  `lingue_labels_lang_type` varchar(250) NOT NULL,
  `id_lingua` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=375 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `lingue_labels_lang`
--

INSERT INTO `lingue_labels_lang` (`id_lingue_labels_lang`, `lingue_labels_lang_label`, `lingue_labels_lang_value`, `lingue_labels_lang_desc`, `lingue_labels_lang_type`, `id_lingua`) VALUES
(1, 'HOME_CATEGORY_TITLE', 'Scopri i nostri prodotti', 'Intestazione sezione categorie home page', 'frontend', 1),
(2, 'HOME_SHOW_ALL_PRODUCTS', 'Vedi tutti i prodotti', 'Testo pulsante collegamento ai prodotti Home Page', 'frontend', 1),
(3, 'HOME_FEATURE_SHIPPING_TITLE', 'Spedizione in tutto il mondo', 'Testo icona mappa Home Page', 'frontend', 1),
(4, 'HOME_FEATURE_SHIPPING_DESC', 'Spedizione gratuita in 7/10 giorni', 'Descrizione icona mappa Home Page', 'frontend', 1),
(5, 'HOME_FEATURE_QUALITY_TITLE', 'Qualità garantita', 'Testo icona frecce Home Page', 'frontend', 1),
(6, 'HOME_FEATURE_QUALITY_DESC', 'La garanzia dei nostri prodotti', 'Descrizione icona frecce Home Page', 'frontend', 1),
(7, 'HOME_FEATURE_SUPPORT_TITLE', 'Supporto Online', 'Testo icona supporto Home Page', 'frontend', 1),
(8, 'HOME_FEATURE_SUPPORT_DESC', 'Supporto clienti Online', 'Descrizione icona supporto Home Page', 'frontend', 1),
(9, 'HOME_FEATURE_PAYMENTS_TITLE', 'Pagamenti sicuri', 'Testo icona pagamento Home Page', 'frontend', 1),
(10, 'HOME_FEATURE_PAYMENTS_DESC', 'Pagamenti sicuri con protocollo SSL', 'Descrizione icona pagamento Home Page', 'frontend', 1),
(11, 'FOOTER_HELP', 'Hai bisogno di aiuto? Contattaci', 'Testo footer help sinistra', 'frontend', 1),
(12, 'FOOTER_NEWSLTTER_TITLE', 'NEWSLETTER', 'NEWSLETTER', 'frontend', 1),
(13, 'FOOTER_NEWSLTTER_DESC', 'Ricevi le offerte e gli sconti riservati ai clienti.', 'Testo footer newsletter', 'frontend', 1),
(14, 'FOOTER_NEWSLTTER_INPUT', 'Inserisci la tua e-mail', 'Etichetta campo newsletter Footer', 'frontend', 1),
(15, 'FOOTER_PAYMENTS_TITLE', 'METODI DI PAGAMENTO', 'Testo pagamenti footer destra', 'frontend', 1),
(16, 'FOOTER_PAYMENTS_DESC', 'Con noi puoi pagare con i principali metodi di pagamento.', 'Descrizione pagamenti footer destra', 'frontend', 1),
(17, 'LABEL_FILTER', 'Filtra', 'Filtra', 'frontend', 1),
(18, 'LABEL_SEARCH', 'Cerca', 'Cerca', 'frontend', 1),
(19, 'LABEL_ORDER', 'Ordina per', 'Ordina per', 'frontend', 1),
(20, 'LABEL_DEFAULT', 'Default', 'Default', 'frontend', 1),
(21, 'LABEL_SALE', 'Offerta', 'Offerta', 'frontend', 1),
(22, 'LABEL_NEW', 'Novità', 'Novità', 'frontend', 1),
(23, 'LABEL_FEEDBACK', 'Voto medio', 'Voto medio', 'frontend', 1),
(24, 'LABEL_BEST', 'Best seller', 'Best seller', 'frontend', 1),
(25, 'LABEL_ALPHA', 'Alfabetico', 'Alfabetico', 'frontend', 1),
(26, 'LABEL_PRICE', 'Prezzo', 'Prezzo', 'frontend', 1),
(27, 'LABEL_TAGS', 'Tags', 'Tags', 'frontend', 1),
(28, 'LABEL_SEE_MORE', 'Vedi altri', 'Vedi altri', 'frontend', 1),
(29, 'LABEL_ADD_TO_CART', 'Aggiungi', 'Aggiungi', 'frontend', 1),
(30, 'LABEL_ADD_TO_WHISH', 'Aggiungi alla Whishlist', 'Aggiungi alla Whishlist', 'frontend', 1),
(31, 'LABEL_ALL', 'Tutti', 'Tutti', 'frontend', 1),
(32, 'LABEL_MY_ACCOUNT', 'Il mio profilo', 'Il mio profilo', 'frontend', 1),
(33, 'LABEL_USER_ACCOUNT', 'Account utente', 'Account utente', 'frontend', 1),
(34, 'LABEL_USER_REGISTER', 'Registrazione', 'Registrazione', 'frontend', 1),
(35, 'LABEL_USER_SIGN_UP', 'Iscriviti', 'Iscriviti', 'frontend', 1),
(36, 'LABEL_USER_LOGIN', 'Effettua il Login', 'Effettua il Login', 'frontend', 1),
(37, 'LABEL_UPDATE', 'Salva', 'Salva', 'frontend', 1),
(38, 'LABEL_GOT', 'Hai', 'Hai', 'frontend', 1),
(39, 'LABEL_POINTS', 'punti', 'punti', 'frontend', 1),
(40, 'LABEL_POINTS_DESC', 'Puoi utilizzare i tuoi punti per acquistare prodotti dal nostro shop o accedere alle promozioni speciali.', 'Puoi utilizzare i tuoi punti per acquistare prodotti dal nostro shop o accedere alle promozioni speciali.', 'frontend', 1),
(41, 'LABEL_MANAGE_POINTS', 'Gestisci i tuoi punti', 'Gestisci i tuoi punti', 'frontend', 1),
(42, 'LABEL_BACK_SHOP', 'Torna allo Shop', 'Torna allo Shop', 'frontend', 1),
(43, 'LABEL_LOGOUT', 'Logout', 'Logout', 'frontend', 1),
(44, 'LABEL_PROFILE', 'Profilo', 'Profilo', 'frontend', 1),
(45, 'LABEL_ORDERS', 'Ordini', 'Ordini', 'frontend', 1),
(46, 'LABEL_ADDRESSES', 'Indirizzi di spedizione', 'Indirizzi di spedizione', 'frontend', 1),
(47, 'LABEL_WHISHLIST', 'Whishlist', 'Whishlist', 'frontend', 1),
(48, 'LABEL_NAME', 'Nome', 'Nome', 'frontend', 1),
(49, 'LABEL_SURNAME', 'Cognome', 'Cognome', 'frontend', 1),
(50, 'LABEL_EMAIL', 'Email', 'Email', 'frontend', 1),
(51, 'LABEL_PHONE', 'Telefono', 'Telefono', 'frontend', 1),
(52, 'LABEL_COUNTRY', 'Stato/Paese', 'Stato/Paese', 'frontend', 1),
(53, 'LABEL_CITY', 'Città', 'Città', 'frontend', 1),
(54, 'LABEL_ADDRESS', 'Indirizzo', 'Indirizzo', 'frontend', 1),
(55, 'LABEL_ADDRESS_REF', 'Riferimento spedizione c/o (es Mario Rossi)', 'Riferimento spedizione c/o (es Mario Rossi)', 'frontend', 1),
(56, 'LABEL_ADDRESS_REF_FATT', 'Riferimento fatturazione (persona o azienda)', 'Riferimento fatturazione (persona o azienda)', 'frontend', 1),
(57, 'LABEL_CIVICO', 'Civico', 'Civico', 'frontend', 1),
(58, 'LABEL_POSTAL_CODE', 'CAP', 'CAP', 'frontend', 1),
(59, 'LABEL_ADDRESS_NOTES', 'Note indirizzo', 'Note indirizzo', 'frontend', 1),
(60, 'LABEL_ORDER_NOTES', 'Note ordine', 'Note ordine', 'frontend', 1),
(61, 'LABEL_TOTAL_ORDER', 'Totale ordine', 'Totale ordine', 'frontend', 1),
(62, 'LABEL_TOTAL', 'Totale', 'Totale', 'frontend', 1),
(63, 'LABEL_QTY', 'Quantità', 'Quantità', 'frontend', 1),
(64, 'LABEL_SUBTOTAL_ORDER', 'Subtotale', 'Subtotale', 'frontend', 1),
(65, 'LABEL_TOTAL_CART', 'Totale carrello', 'Totale carrello', 'frontend', 1),
(66, 'LABEL_TOTAL_ORDER_NOTES', '* Note: il totale include i costi di spedizione', '* Note: il totale include i costi di spedizione', 'frontend', 1),
(67, 'LABEL_TOTAL_CART_NOTES', '* Note: il totale non include eventuali spese di spedizione. I costi di spedizione o consegna verranno calcolati nel checkout successivo', '* Note: il totale non include eventuali spese di spedizione. I costi di spedizione o consegna verranno calcolati nel checkout successivo', 'frontend', 1),
(68, 'LABEL_CONFIRM', 'CONFERMA', 'CONFERMA', 'frontend', 1),
(69, 'LABEL_REMOVE', 'ELIMINA', 'ELIMINA', 'frontend', 1),
(70, 'LABEL_BACK_TO_CART', 'RITORNA AL CARRELLO', 'RITORNA AL CARRELLO', 'frontend', 1),
(71, 'LABEL_UPDATE_CART', 'AGGIORNA IL CARRELLO', 'AGGIORNA IL CARRELLO', 'frontend', 1),
(72, 'LABEL_BACK_TO_SHOP', 'TORNA ALLO SHOP', 'TORNA ALLO SHOP', 'frontend', 1),
(73, 'LABEL_CART', 'Carrello', 'Carrello', 'frontend', 1),
(74, 'LABEL_CART_EMPTY', 'Il tuo carrello è vuoto !', 'Il tuo carrello è vuoto !', 'frontend', 1),
(75, 'LABEL_CHECKOUT', 'Checkout', 'Checkout', 'frontend', 1),
(76, 'LABEL_PAYMENT_METHOD', 'Modalità di pagamento', 'Modalità di pagamento', 'frontend', 1),
(77, 'LABEL_PAYMENT_METHOD_PAYPAL', 'Paypal', 'Paypal', 'frontend', 1),
(78, 'LABEL_PAYMENT_METHOD_CC', 'Carta di credito / Prepagata', 'Carta di credito / Prepagata', 'frontend', 1),
(79, 'LABEL_MESSAGE', 'Messaggio', 'Messaggio', 'frontend', 1),
(80, 'LABEL_SEND', 'INVIA', 'INVIA', 'frontend', 1),
(81, 'LABEL_PRODUCTS', 'prodotti', 'prodotti', 'frontend', 1),
(82, 'LABEL_CART_INFO_ACTUALLY_1', 'Attualmente ci sono', 'Attualmente ci sono', 'frontend', 1),
(83, 'LABEL_CART_INFO_ACTUALLY_2', 'nel carrello', 'nel carrello', 'frontend', 1),
(84, 'LABEL_SIZE', 'Tg', 'Tg', 'frontend', 1),
(85, 'LABEL_COLOR', 'Colore', 'Colore', 'frontend', 1),
(86, 'LABEL_CATEGORY', 'Categoria', 'Categoria', 'frontend', 1),
(87, 'LABEL_DESCRIPTION', 'Descrizione', 'Descrizione', 'frontend', 1),
(88, 'LABEL_REVIEWS', 'Commenti', 'Commenti', 'frontend', 1),
(89, 'LABEL_ALSO_LIKE', 'Ti potrebbero piacere', 'Ti potrebbero piacere', 'frontend', 1),
(90, 'LABEL_REFCODE', 'Rif', 'Rif', 'frontend', 1),
(91, 'LABEL_DETAIL', 'Dettaglio', 'Dettaglio', 'frontend', 1),
(92, 'LABEL_SHOPPING_CART', 'Carrello', 'Carrello', 'frontend', 1),
(93, 'LABEL_FRONT', 'FRONTE', 'FRONTE', 'frontend', 1),
(94, 'LABEL_BACK', 'RETRO', 'RETRO', 'frontend', 1),
(95, 'LABEL_AVAILABILITY', 'Disponibilità', 'Disponibilità', 'frontend', 1),
(96, 'LABEL_AVAILABILITY_HIGH', 'Alta', 'Alta', 'frontend', 1),
(97, 'LABEL_AVAILABILITY_LOW', 'Bassa', 'Bassa', 'frontend', 1),
(98, 'LABEL_404_MESSAGE', 'Oops.... la pagina richiesta non esiste !', 'Oops.... la pagina richiesta non esiste !', 'frontend', 1),
(99, 'LABEL_404_BTN', 'TORNA ALLA HOME', 'TORNA ALLA HOME', 'frontend', 1),
(100, 'LABEL_ORDER', 'Ordine', 'Ordine', 'frontend', 1),
(101, 'LABEL_YOUR_ACCOUNT', 'Il tuo account', 'Il tuo account', 'frontend', 1),
(102, 'LABEL_USER_ALREADY_REGISTERED', 'Sei già registrato ?', 'Sei già registrato ?', 'frontend', 1),
(103, 'LABEL_USER_NOTREGISTERED', 'Altrimenti puoi ordinare compilando i dati', 'Altrimenti puoi ordinare compilando i dati', 'frontend', 1),
(104, 'LABEL_USER_NOTREGISTERED_POINTS', 'non accumulerai punti e bonus riservati ai clienti registrati', 'non accumulerai punti e bonus riservati ai clienti registrati', 'frontend', 1),
(105, 'LABEL_COUPON', 'COUPON', 'COUPON', 'frontend', 1),
(106, 'LABEL_COUPON_HAVE', 'Hai un codice sconto?', 'Hai un codice sconto?', 'frontend', 1),
(107, 'LABEL_COUPON_INSERT', 'Inserisci il codice coupon', 'Inserisci il codice coupon', 'frontend', 1),
(108, 'LABEL_COUPON_APPLY', 'Applica coupon', 'Applica coupon', 'frontend', 1),
(109, 'LABEL_DISCOUNT', 'Sconto', 'Sconto', 'frontend', 1),
(110, 'LABEL_SHIPPING_THIS', 'Spedisci a questo indirizzo', 'Spedisci a questo indirizzo', 'frontend', 1),
(111, 'LABEL_SHIPPING_OTHER', 'Spedisci ad un altro indirizzo', 'Spedisci ad un altro indirizzo', 'frontend', 1),
(112, 'LABEL_NEW_ADDRESS', 'Nuovo indirizzo', 'Nuovo indirizzo', 'frontend', 1),
(113, 'LABEL_BILLING_ADDRESS', 'Indirizzo di fatturazione', 'Indirizzo di fatturazione', 'frontend', 1),
(114, 'LABEL_SHIPPING_ADDRESS', 'Indirizzo di spedizione', 'Indirizzo di spedizione', 'frontend', 1),
(115, 'LABEL_SHIPPING', 'Spedizione', 'Spedizione', 'frontend', 1),
(116, 'LABEL_CHANGE_PASSWORD', 'Cambio Password', 'Cambio Password', 'frontend', 1),
(117, 'LABEL_ORDER_DATE', 'Data ordine', 'Data ordine', 'frontend', 1),
(118, 'LABEL_STRIPE_DESC', 'Paga in tutta sicurezza con Stripe', 'Paga in tutta sicurezza con Stripe', 'frontend', 1),
(119, 'MSG_SEARCH_INSERT', 'Inserisci il testo da ricercare', 'Inserisci il testo da ricercare', 'frontend', 1),
(120, 'MSG_SAVE_NEWSLETTER', 'Iscrivimi alla newsletter', 'Iscrivimi alla newsletter', 'frontend', 1),
(121, 'MSG_SEND_CONTACT_US', 'Richiesta di contatto inviata con successo!<br/>Grazie.', 'Richiesta di contatto inviata con successo!<br/>Grazie.', 'frontend', 1),
(122, 'MSG_PAYPAL_NOTETOPAYER', 'L''indirizzo di spedizione resterà quello inserito nel modulo di checkout e non quello indicato nel pagamento PayPal !', 'L''indirizzo di spedizione resterà quello inserito nel modulo di checkout e non quello indicato nel pagamento PayPal !', 'frontend', 1),
(123, 'MSG_PAYPAL_CANCEL', 'Riprova ad effettuare il pagamemto !', 'Riprova ad effettuare il pagamemto !', 'frontend', 1),
(124, 'MSG_PAYPAL_ERROR', 'Errore durante il pagamento:', 'Errore durante il pagamento:', 'frontend', 1),
(125, 'MSG_NO_RESULT', 'Nessun risultato per ', 'Nessun risultato per ', 'frontend', 1),
(126, 'MSG_NO_RESULT_FILTER', 'Nessun risultato presente per i filtri selezionati.', 'Nessun risultato presente per i filtri selezionati.', 'frontend', 1),
(127, 'MSG_NO_SIZE_FOR_PRODUCTS_COLOR', 'Al momento non ci sono taglie disponibili per questo prodotto/colore', 'Al momento non ci sono taglie disponibili per questo prodotto/colore', 'frontend', 1),
(128, 'MSG_ORDER_SUCCESS', 'inserito con successo ! Grazie.', 'inserito con successo ! Grazie.', 'frontend', 1),
(129, 'MSG_ORDER_PAYMENT_ERROR', 'Errore durante il pagamento ordine. Per favore verifica il tuo ordine e riprova.', 'Errore durante il pagamento ordine. Per favore verifica il tuo ordine e riprova.', 'frontend', 1),
(130, 'MSG_SUCCESS_CONTACT', 'Il tuo messaggio è stato inviato correttamente. Grazie !', 'Il tuo messaggio è stato inviato correttamente. Grazie !', 'frontend', 1),
(131, 'MSG_FAILURE_CONTACT', 'Abbiamo riscontrato un problema nell''invio del mesaggio. Riprova!', 'Abbiamo riscontrato un problema nell''invio del mesaggio. Riprova!', 'frontend', 1),
(132, 'MSG_SUCCESS_NEWSLETTER', 'Adesso sei iscritto alla newsletter. Grazie !', 'Adesso sei iscritto alla newsletter. Grazie !', 'frontend', 1),
(133, 'MSG_UNIQUE_NEWSLETTER', 'L''indirizzo email è già iscritto alla newsletter', 'L''indirizzo email è già iscritto alla newsletter', 'frontend', 1),
(134, 'MSG_UNSUBSCRIBE_DONE', 'La tua email/iscrizione è stata rimossa. Grazie!', 'La tua email/iscrizione è stata rimossa. Grazie!', 'frontend', 1),
(135, 'MSG_UNSUBSCRIBE_NOTFOUND', 'Il contatto richiesto non è attualmente registrato', 'Il contatto richiesto non è attualmente registrato', 'frontend', 1),
(136, 'MSG_CART_REMOVED', 'Prodotto rimosso dal carrello', 'Prodotto rimosso dal carrello', 'frontend', 1),
(137, 'MSG_CART_ADDED', 'Prodotto inserito nel carrello', 'Prodotto inserito nel carrello', 'frontend', 1),
(138, 'MSG_CART_UPDATED', 'Prodotto aggiornato nel carrello', 'Prodotto aggiornato nel carrello', 'frontend', 1),
(139, 'MSG_SERVICE_FAILURE', 'Si è verificato un errore. Riprova!', 'Si è verificato un errore. Riprova!', 'frontend', 1),
(140, 'MSG_BILLING_ADDRESS_NECESSARY', 'L''indirizzo di fatturazione è obbligatorio ai fini del processo di acquisto! <br/>Sei sicuro di voler uscire?<br/>Ti sarà comunque chiesto nella fase di acquisto!', 'L''indirizzo di fatturazione è obbligatorio ai fini del processo di acquisto! <br/>Sei sicuro di voler uscire?<br/>Ti sarà comunque chiesto nella fase di acquisto!', 'frontend', 1),
(141, 'MSG_ALTERNATE_ADDRESS_LOGGED', 'inserisci un nuovo indirizzo o selezionane uno già presente nella lista', 'inserisci un nuovo indirizzo o selezionane uno già presente nella lista', 'frontend', 1),
(142, 'MSG_ALTERNATE_ADDRESS_NOTLOGGED', 'inserisci un nuovo indirizzo per la spedizione', 'inserisci un nuovo indirizzo per la spedizione', 'frontend', 1),
(143, 'MSG_COUPON_INVALID', 'Coupon non valido o scaduto', 'Coupon non valido o scaduto', 'frontend', 1),
(144, 'MSG_COUPON_INVALID_OVER', 'Il valore del Coupon inserito è maggiore del carrello!', 'Il valore del Coupon inserito è maggiore del carrello!', 'frontend', 1),
(145, 'LABEL_TP_SALE', 'Offerta', 'Offerta', 'frontend', 1),
(146, 'LABEL_TP_BESTSELLER', 'Più venduti', 'Più venduti', 'frontend', 1),
(147, 'LABEL_TP_TOPRATED', 'Più votati', 'Più votati', 'frontend', 1),
(148, 'LABEL_TP_STANDARD', 'Standard', 'Standard', 'frontend', 1),
(149, 'Standard', 'Nuovo', 'Nuovo', 'frontend', 1),
(150, 'SEND_AREYOUSURE_BTN', 'SEI SICURO ?', 'SEI SICURO ?', 'frontend', 1),
(151, 'LABEL_UNSUBSCRIBE', 'Cancellati', 'Cancellati', 'frontend', 1),
(152, 'LABEL_MY_ACCOUNT_EMAIL', 'Il mio profilo', 'Il mio profilo', 'email', 1),
(153, 'LABEL_SEE_EMAIL_ONLINE', 'Vedi email online', 'Vedi email online', 'email', 1),
(154, 'LABEL_EMAIL_SALES_TITLE', 'Controlla le nostre ultime offerte!', 'Ultime offerte', 'email', 1),
(155, 'LABEL_DETAIL_EMAIL', 'Vedi i dettagli', 'Vedi i dettagli', 'email', 1),
(156, 'TEXT_EMAIL_FOOTER_USUBSCRIBE', 'cancellati', 'Cancellati footer', 'email', 1),
(157, 'TEXT_EMAIL_FOOTER_RESERVED', 'Tutti i diritti riservati', 'Tutti i diritti riservati', 'email', 1),
(158, 'TEXT_EMAIL_FOOTER_COPYRIGHT', 'Se non vuoi più ricevere queste email per favore', 'Non ricevere più email', 'email', 1),
(159, 'LABEL_EMAIL_SUBJECT_CONTACT', 'Contatto dal sito', 'Contatto dal sito', 'email', 1),
(160, 'LABEL_EMAIL_CONTACT_TITLE', 'Ti ringraziamo per averci contattato!', 'Ti ringraziamo per il contatto', 'email', 1),
(161, 'LABEL_EMAIL_CONTACT_TEXT', 'Abbiamo ricevuto la tua comunicazione e ti invieremo una risposta nel minor tempo possibile. Grazie!', 'Comunicazione ricevuta', 'email', 1),
(162, 'LABEL_EMAIL_SUBJECT_NEWSLETTER', 'Iscrizione alla Newsletter', 'Iscrizione alla Newsletter', 'email', 1),
(163, 'LABEL_EMAIL_NEWSLETTER_TITLE', 'Grazie per esserti iscritto alla nostra newsletter!', 'Grazie newsletter', 'email', 1),
(164, 'LABEL_EMAIL_NEWSLETTER_TEXT', 'Riceverai gli aggiornamenti, le news e le nostre offerte esclusive per restare in contatto con il nostro mondo.', 'Riceverai gli aggiornamenti', 'email', 1),
(165, 'LABEL_EMAIL_SUBJECT_WELCOME', 'Benvenuto su Ma Chlò', 'Benvenuto su', 'email', 1),
(166, 'LABEL_EMAIL_WELCOME_TITLE', 'Benvenuto su Ma Chlò!', 'Benvenuto su TITOLO', 'email', 1),
(167, 'LABEL_EMAIL_WELCOME_TEXT', 'Grazie per esserti registrato! Speriamo che ti piaccia il nostro lavoro. Consulta alcune delle nostre ultime offerte qui sotto o clicca sul pulsante per visualizzare il tuo nuovo account.', 'Grazie per esserti registrato', 'email', 1),
(168, 'LABEL_EMAIL_SUBJECT_NEW_ORDER', 'Nuovo ordine', 'Nuovo ordine', 'email', 1),
(169, 'LABEL_TITLE_INVOICE', 'Riepilogo ordine', 'Riepilogo ordine', 'email', 1),
(170, 'LABEL_INVOICE_THANKS', 'Ti ringraziamo per il tuo ordine!', 'Ti ringraziamo per il tuo ordine!', 'email', 1),
(171, 'LABEL_INVOICE_THANKS_TEXT', 'Ti faremo sapere non appena i tuoi articoli saranno spediti.<br>Per cambiare o visualizzare il tuo ordine, ti preghiamo di visualizzare il tuo account facendo clic sul pulsante qui sotto.', 'Ti faremo sapere USER', 'email', 1),
(172, 'LABEL_INVOICE_THANKS_TEXT_NOUSER', 'Ti faremo sapere non appena i tuoi articoli saranno spediti.<br>Per cambiare o visualizzare il tuo ordine, ti preghiamo di creare il tuo account facendo clic sul pulsante qui sotto.', 'Ti faremo sapere NO USER', 'email', 1),
(173, 'LABEL_SHIPPING_ADDRESS_EMAIL', 'Indirizzo di spedizione', 'Indirizzo di spedizione', 'email', 1),
(174, 'LABEL_ORDER_EMAIL', 'Ordine', 'Ordine', 'email', 1),
(175, 'LABEL_ORDER_DATE_EMAIL', 'Data ordine', 'Data ordine', 'email', 1),
(176, 'LABEL_ADDRESS_NOTES_EMAIL', 'Note indirizzo', 'Note indirizzo', 'email', 1),
(177, 'LABEL_ORDER_NOTES_EMAIL', 'Note ordine', 'Note ordine', 'email', 1),
(178, 'LABEL_DESCRIPTION_EMAIL', 'Descrizione', 'Descrizione', 'email', 1),
(179, 'LABEL_QTY_EMAIL', 'Quantità', 'Quantità', 'email', 1),
(180, 'LABEL_SIZE_EMAIL', 'Taglia', 'Taglia', 'email', 1),
(181, 'LABEL_COLOR_EMAIL', 'Colore', 'Colore', 'email', 1),
(182, 'LABEL_SUBTOTAL_ORDER_EMAIL', 'Subtotale', 'Subtotale', 'email', 1),
(183, 'LABEL_COUPON_APPLY_EMAIL', 'Applica coupon', 'Applica coupon', 'email', 1),
(184, 'LABEL_SHIPPING_EMAIL', 'Spedizione', 'Spedizione', 'email', 1),
(185, 'LABEL_TOTAL_EMAIL', 'Totale', 'Totale', 'email', 1),
(186, 'HOME_CATEGORY_TITLE', 'Discover our products', 'Intestazione sezione categorie home page', 'frontend', 2),
(187, 'HOME_SHOW_ALL_PRODUCTS', 'Show all products', 'Testo pulsante collegamento ai prodotti Home Page', 'frontend', 2),
(188, 'HOME_FEATURE_SHIPPING_TITLE', 'Show all products', 'Testo icona mappa Home Page', 'frontend', 2),
(189, 'HOME_FEATURE_SHIPPING_DESC', 'Free shipping on all orders 7/10 days', 'Descrizione icona mappa Home Page', 'frontend', 2),
(190, 'HOME_FEATURE_QUALITY_TITLE', 'Quality Guarantee', 'Testo icona frecce Home Page', 'frontend', 2),
(191, 'HOME_FEATURE_QUALITY_DESC', 'Guarantee on our products', 'Descrizione icona frecce Home Page', 'frontend', 2),
(192, 'HOME_FEATURE_SUPPORT_TITLE', 'Online support', 'Testo icona supporto Home Page', 'frontend', 2),
(193, 'HOME_FEATURE_SUPPORT_DESC', 'Friendly customer support', 'Descrizione icona supporto Home Page', 'frontend', 2),
(194, 'HOME_FEATURE_PAYMENTS_TITLE', 'Secure Online Payments', 'Testo icona pagamento Home Page', 'frontend', 2),
(195, 'HOME_FEATURE_PAYMENTS_DESC', 'We posess SSL / Secure Certificate', 'Descrizione icona pagamento Home Page', 'frontend', 2),
(196, 'FOOTER_HELP', 'Do you need help? Contact us', 'Testo footer help sinistra', 'frontend', 2),
(197, 'FOOTER_NEWSLTTER_TITLE', 'NEWSLETTER', 'NEWSLETTER', 'frontend', 2),
(198, 'FOOTER_NEWSLTTER_DESC', 'To receive latest offers and discounts from the shop.', 'Testo footer newsletter', 'frontend', 2),
(199, 'FOOTER_NEWSLTTER_INPUT', 'Your e-mail', 'Etichetta campo newsletter Footer', 'frontend', 2),
(200, 'FOOTER_PAYMENTS_TITLE', 'PAYMENT METHODS', 'Testo pagamenti footer destra', 'frontend', 2),
(201, 'FOOTER_PAYMENTS_DESC', 'We support one of the following payment methods.', 'Descrizione pagamenti footer destra', 'frontend', 2),
(202, 'LABEL_FILTER', 'Filter', 'Filtra', 'frontend', 2),
(203, 'LABEL_SEARCH', 'Search', 'Cerca', 'frontend', 2),
(204, 'LABEL_ORDER', 'Order by', 'Ordina per', 'frontend', 2),
(205, 'LABEL_DEFAULT', 'Default', 'Default', 'frontend', 2),
(206, 'LABEL_SALE', 'Sale', 'Offerta', 'frontend', 2),
(207, 'LABEL_NEW', 'New', 'Novità', 'frontend', 2),
(208, 'LABEL_FEEDBACK', 'Feedback', 'Voto medio', 'frontend', 2),
(209, 'LABEL_BEST', 'Best seller', 'Best seller', 'frontend', 2),
(210, 'LABEL_ALPHA', 'Alphabetical', 'Alfabetico', 'frontend', 2),
(211, 'LABEL_PRICE', 'Price', 'Prezzo', 'frontend', 2),
(212, 'LABEL_TAGS', 'Tags', 'Tags', 'frontend', 2),
(213, 'LABEL_SEE_MORE', 'See more', 'Vedi altri', 'frontend', 2),
(214, 'LABEL_ADD_TO_CART', 'Add to cart', 'Aggiungi', 'frontend', 2),
(215, 'LABEL_ADD_TO_WHISH', 'Add to Whishlist', 'Aggiungi alla Whishlist', 'frontend', 2),
(216, 'LABEL_ALL', 'All', 'Tutti', 'frontend', 2),
(217, 'LABEL_MY_ACCOUNT', 'My account', 'Il mio profilo', 'frontend', 2),
(218, 'LABEL_USER_ACCOUNT', 'User account', 'Account utente', 'frontend', 2),
(219, 'LABEL_USER_REGISTER', 'Register', 'Registrazione', 'frontend', 2),
(220, 'LABEL_USER_SIGN_UP', 'Sign up', 'Iscriviti', 'frontend', 2),
(221, 'LABEL_USER_LOGIN', 'Please Login', 'Effettua il Login', 'frontend', 2),
(222, 'LABEL_UPDATE', 'Update', 'Salva', 'frontend', 2),
(223, 'LABEL_GOT', 'You have', 'Hai', 'frontend', 2),
(224, 'LABEL_POINTS', 'points', 'punti', 'frontend', 2),
(225, 'LABEL_POINTS_DESC', 'You can use your points to receive special offers and have dedicated discounts.', 'Puoi utilizzare i tuoi punti per acquistare prodotti dal nostro shop o accedere alle promozioni speciali.', 'frontend', 2),
(226, 'LABEL_MANAGE_POINTS', 'Manage your points', 'Gestisci i tuoi punti', 'frontend', 2),
(227, 'LABEL_BACK_SHOP', 'Back to Shop', 'Torna allo Shop', 'frontend', 2),
(228, 'LABEL_LOGOUT', 'Logout', 'Logout', 'frontend', 2),
(229, 'LABEL_PROFILE', 'Profile', 'Profilo', 'frontend', 2),
(230, 'LABEL_ORDERS', 'Orders', 'Ordini', 'frontend', 2),
(231, 'LABEL_ADDRESSES', 'Shipping addresses', 'Indirizzi di spedizione', 'frontend', 2),
(232, 'LABEL_WHISHLIST', 'Whishlist', 'Whishlist', 'frontend', 2),
(233, 'LABEL_NAME', 'Name', 'Nome', 'frontend', 2),
(234, 'LABEL_SURNAME', 'Surname', 'Cognome', 'frontend', 2),
(235, 'LABEL_EMAIL', 'Email', 'Email', 'frontend', 2),
(236, 'LABEL_PHONE', 'Phone', 'Telefono', 'frontend', 2),
(237, 'LABEL_COUNTRY', 'Country/State', 'Stato/Paese', 'frontend', 2),
(238, 'LABEL_CITY', 'City', 'Città', 'frontend', 2),
(239, 'LABEL_ADDRESS', 'Address', 'Indirizzo', 'frontend', 2),
(240, 'LABEL_ADDRESS_REF', 'Reference shipping c/o (ex Tom Brad)', 'Riferimento spedizione c/o (es Mario Rossi)', 'frontend', 2),
(241, 'LABEL_ADDRESS_REF_FATT', 'Reference invoice (person or company)', 'Riferimento fatturazione (persona o azienda)', 'frontend', 2),
(242, 'LABEL_CIVICO', 'Number', 'Civico', 'frontend', 2),
(243, 'LABEL_POSTAL_CODE', 'Postal code', 'CAP', 'frontend', 2),
(244, 'LABEL_ADDRESS_NOTES', 'Address notes', 'Note indirizzo', 'frontend', 2),
(245, 'LABEL_ORDER_NOTES', 'Order notes', 'Note ordine', 'frontend', 2),
(246, 'LABEL_TOTAL_ORDER', 'Total order', 'Totale ordine', 'frontend', 2),
(247, 'LABEL_TOTAL', 'Total', 'Totale', 'frontend', 2),
(248, 'LABEL_QTY', 'Qty', 'Quantità', 'frontend', 2),
(249, 'LABEL_SUBTOTAL_ORDER', 'Subtotal', 'Subtotale', 'frontend', 2),
(250, 'LABEL_TOTAL_CART', 'Total cart', 'Totale carrello', 'frontend', 2),
(251, 'LABEL_TOTAL_ORDER_NOTES', '* Notes: this total include the shipping cost', '* Note: il totale include i costi di spedizione', 'frontend', 2),
(252, 'LABEL_TOTAL_CART_NOTES', '* Notes: the total does not include any shipping charges. Shipping or delivery costs will be calculated in the next checkout', '* Note: il totale non include eventuali spese di spedizione. I costi di spedizione o consegna verranno calcolati nel checkout successivo', 'frontend', 2),
(253, 'LABEL_CONFIRM', 'CONFIRM', 'CONFERMA', 'frontend', 2),
(254, 'LABEL_REMOVE', 'REMOVE', 'ELIMINA', 'frontend', 2),
(255, 'LABEL_BACK_TO_CART', 'BACK TO CART', 'RITORNA AL CARRELLO', 'frontend', 2),
(256, 'LABEL_UPDATE_CART', 'UPDATE CART', 'AGGIORNA IL CARRELLO', 'frontend', 2),
(257, 'LABEL_BACK_TO_SHOP', 'BACK TO SHOP', 'TORNA ALLO SHOP', 'frontend', 2),
(258, 'LABEL_CART', 'Cart', 'Carrello', 'frontend', 2),
(259, 'LABEL_CART_EMPTY', 'Your cart is empty !', 'Il tuo carrello è vuoto !', 'frontend', 2),
(260, 'LABEL_CHECKOUT', 'Checkout', 'Checkout', 'frontend', 2),
(261, 'LABEL_PAYMENT_METHOD', 'Payment method', 'Modalità di pagamento', 'frontend', 2),
(262, 'LABEL_PAYMENT_METHOD_PAYPAL', 'Paypal', 'Paypal', 'frontend', 2),
(263, 'LABEL_PAYMENT_METHOD_CC', 'Credit card / Prepaid card', 'Carta di credito / Prepagata', 'frontend', 2),
(264, 'LABEL_MESSAGE', 'Message', 'Messaggio', 'frontend', 2),
(265, 'LABEL_SEND', 'SEND', 'INVIA', 'frontend', 2),
(266, 'LABEL_PRODUCTS', 'products', 'prodotti', 'frontend', 2),
(267, 'LABEL_CART_INFO_ACTUALLY_1', 'There are currently', 'Attualmente ci sono', 'frontend', 2),
(268, 'LABEL_CART_INFO_ACTUALLY_2', 'in your shopping cart', 'nel carrello', 'frontend', 2),
(269, 'LABEL_SIZE', 'Size', 'Tg', 'frontend', 2),
(270, 'LABEL_COLOR', 'Color', 'Colore', 'frontend', 2),
(271, 'LABEL_CATEGORY', 'Category', 'Categoria', 'frontend', 2),
(272, 'LABEL_DESCRIPTION', 'Description', 'Descrizione', 'frontend', 2),
(273, 'LABEL_REVIEWS', 'Reviews', 'Commenti', 'frontend', 2),
(274, 'LABEL_ALSO_LIKE', 'You May Also Like', 'Ti potrebbero piacere', 'frontend', 2),
(275, 'LABEL_REFCODE', 'Ref', 'Rif', 'frontend', 2),
(276, 'LABEL_DETAIL', 'Detail', 'Dettaglio', 'frontend', 2),
(277, 'LABEL_SHOPPING_CART', 'Shopping cart', 'Carrello', 'frontend', 2),
(278, 'LABEL_FRONT', 'FRONT', 'FRONTE', 'frontend', 2),
(279, 'LABEL_BACK', 'BACK', 'RETRO', 'frontend', 2),
(280, 'LABEL_AVAILABILITY', 'Availability', 'Disponibilità', 'frontend', 2),
(281, 'LABEL_AVAILABILITY_HIGH', 'High', 'Alta', 'frontend', 2),
(282, 'LABEL_AVAILABILITY_LOW', 'Low', 'Bassa', 'frontend', 2),
(283, 'LABEL_404_MESSAGE', 'Oops.... the page requested not exist !', 'Oops.... la pagina richiesta non esiste !', 'frontend', 2),
(284, 'LABEL_404_BTN', 'BACK TO HOME', 'TORNA ALLA HOME', 'frontend', 2),
(285, 'LABEL_ORDER', 'Order', 'Ordine', 'frontend', 2),
(286, 'LABEL_YOUR_ACCOUNT', 'Your account', 'Il tuo account', 'frontend', 2),
(287, 'LABEL_USER_ALREADY_REGISTERED', 'Are you already registered?', 'Sei già registrato ?', 'frontend', 2),
(288, 'LABEL_USER_NOTREGISTERED', 'Otherwise you can order filling in the data', 'Altrimenti puoi ordinare compilando i dati', 'frontend', 2),
(289, 'LABEL_USER_NOTREGISTERED_POINTS', 'you will not accumulate points and bonuses reserved for registered customers', 'non accumulerai punti e bonus riservati ai clienti registrati', 'frontend', 2),
(290, 'LABEL_COUPON', 'COUPON', 'COUPON', 'frontend', 2),
(291, 'LABEL_COUPON_HAVE', 'Do you have a discount code?', 'Hai un codice sconto?', 'frontend', 2),
(292, 'LABEL_COUPON_INSERT', 'Enter the coupon code', 'Inserisci il codice coupon', 'frontend', 2),
(293, 'LABEL_COUPON_APPLY', 'Apply coupons', 'Applica coupon', 'frontend', 2),
(294, 'LABEL_DISCOUNT', 'Discount', 'Sconto', 'frontend', 2),
(295, 'LABEL_SHIPPING_THIS', 'Send to this address', 'Spedisci a questo indirizzo', 'frontend', 2),
(296, 'LABEL_SHIPPING_OTHER', 'Send to another address', 'Spedisci ad un altro indirizzo', 'frontend', 2),
(297, 'LABEL_NEW_ADDRESS', 'New address', 'Nuovo indirizzo', 'frontend', 2),
(298, 'LABEL_BILLING_ADDRESS', 'Billing address', 'Indirizzo di fatturazione', 'frontend', 2),
(299, 'LABEL_SHIPPING_ADDRESS', 'Shipping address', 'Indirizzo di spedizione', 'frontend', 2),
(300, 'LABEL_SHIPPING', 'Shipping', 'Spedizione', 'frontend', 2),
(301, 'LABEL_CHANGE_PASSWORD', 'Change Password', 'Cambio Password', 'frontend', 2),
(302, 'LABEL_ORDER_DATE', 'Order date', 'Data ordine', 'frontend', 2),
(303, 'LABEL_STRIPE_DESC', 'Make secure payments with Stripe', 'Paga in tutta sicurezza con Stripe', 'frontend', 2),
(304, 'MSG_SEARCH_INSERT', 'Enter text to search', 'Inserisci il testo da ricercare', 'frontend', 2),
(305, 'MSG_SAVE_NEWSLETTER', 'Sign up to newsletter', 'Iscrivimi alla newsletter', 'frontend', 2),
(306, 'MSG_SEND_CONTACT_US', 'Contact requests send!<br/>Thank you.', 'Richiesta di contatto inviata con successo!<br/>Grazie.', 'frontend', 2),
(307, 'MSG_PAYPAL_NOTETOPAYER', 'The shipping address will remain in the checkout form and not the one indicated in the PayPal payment!', 'L''indirizzo di spedizione resterà quello inserito nel modulo di checkout e non quello indicato nel pagamento PayPal !', 'frontend', 2),
(308, 'MSG_PAYPAL_CANCEL', 'Retry with payment to complete the order !', 'Riprova ad effettuare il pagamemto !', 'frontend', 2),
(309, 'MSG_PAYPAL_ERROR', 'Error during payment operation:', 'Errore durante il pagamento:', 'frontend', 2),
(310, 'MSG_NO_RESULT', 'No results found for  ', 'Nessun risultato per ', 'frontend', 2),
(311, 'MSG_NO_RESULT_FILTER', 'No results found with filters selected.', 'Nessun risultato presente per i filtri selezionati.', 'frontend', 2),
(312, 'MSG_NO_SIZE_FOR_PRODUCTS_COLOR', 'No availables sizes at the moment for this product/color', 'Al momento non ci sono taglie disponibili per questo prodotto/colore', 'frontend', 2),
(313, 'MSG_ORDER_SUCCESS', 'saved successfully! Thank you.', 'inserito con successo ! Grazie.', 'frontend', 2),
(314, 'MSG_ORDER_PAYMENT_ERROR', 'Error during order payment. Please che your order and retry.', 'Errore durante il pagamento ordine. Per favore verifica il tuo ordine e riprova.', 'frontend', 2),
(315, 'MSG_SUCCESS_CONTACT', 'Your message has been sent correctly. Thank you !', 'Il tuo messaggio è stato inviato correttamente. Grazie !', 'frontend', 2),
(316, 'MSG_FAILURE_CONTACT', 'Sorry it seems that our mail server is not responding. Please try again later!', 'Abbiamo riscontrato un problema nell''invio del mesaggio. Riprova!', 'frontend', 2),
(317, 'MSG_SUCCESS_NEWSLETTER', 'Now you are subscribed to the newsletter. Thank you !', 'Adesso sei iscritto alla newsletter. Grazie !', 'frontend', 2),
(318, 'MSG_UNIQUE_NEWSLETTER', 'The email address is already subscribed to the newsletter.', 'L''indirizzo email è già iscritto alla newsletter', 'frontend', 2),
(319, 'MSG_UNSUBSCRIBE_DONE', 'Your email/subscription was removed. Thank you!', 'La tua email/iscrizione è stata rimossa. Grazie!', 'frontend', 2),
(320, 'MSG_UNSUBSCRIBE_NOTFOUND', 'The required contact is not currently registered', 'Il contatto richiesto non è attualmente registrato', 'frontend', 2),
(321, 'MSG_CART_REMOVED', 'Product removed from cart', 'Prodotto rimosso dal carrello', 'frontend', 2),
(322, 'MSG_CART_ADDED', 'Product added to cart', 'Prodotto inserito nel carrello', 'frontend', 2),
(323, 'MSG_CART_UPDATED', 'Product updated into cart', 'Prodotto aggiornato nel carrello', 'frontend', 2),
(324, 'MSG_SERVICE_FAILURE', 'There was an error. Please retry!', 'Si è verificato un errore. Riprova!', 'frontend', 2),
(325, 'MSG_BILLING_ADDRESS_NECESSARY', 'The billing address is required for the purchase process!<br/>Are you sure you want to leave?<br/>You will still be asked for the purchase phase!', 'L''indirizzo di fatturazione è obbligatorio ai fini del processo di acquisto! <br/>Sei sicuro di voler uscire?<br/>Ti sarà comunque chiesto nella fase di acquisto!', 'frontend', 2),
(326, 'MSG_ALTERNATE_ADDRESS_LOGGED', 'enter a new address or select one already in the list', 'inserisci un nuovo indirizzo o selezionane uno già presente nella lista', 'frontend', 2),
(327, 'MSG_ALTERNATE_ADDRESS_NOTLOGGED', 'enter a new shipping address', 'inserisci un nuovo indirizzo per la spedizione', 'frontend', 2),
(328, 'MSG_COUPON_INVALID', 'Coupon invalid or expired', 'Coupon non valido o scaduto', 'frontend', 2),
(329, 'MSG_COUPON_INVALID_OVER', 'The value of the inserted Coupon is greater than the cart!', 'Il valore del Coupon inserito è maggiore del carrello!', 'frontend', 2),
(330, 'LABEL_TP_SALE', 'Sale', 'Offerta', 'frontend', 2),
(331, 'LABEL_TP_BESTSELLER', 'Best seller', 'Più venduti', 'frontend', 2),
(332, 'LABEL_TP_TOPRATED', 'Top rated', 'Più votati', 'frontend', 2),
(333, 'LABEL_TP_STANDARD', 'Standard', 'Standard', 'frontend', 2),
(334, 'Standard', 'New', 'Nuovo', 'frontend', 2),
(335, 'SEND_AREYOUSURE_BTN', 'ARE YOU SURE ?', 'SEI SICURO ?', 'frontend', 2),
(336, 'LABEL_UNSUBSCRIBE', 'Unsubscribe', 'Cancellati', 'frontend', 2),
(337, 'LABEL_MY_ACCOUNT_EMAIL', 'My account', 'Il mio profilo', 'email', 2),
(338, 'LABEL_SEE_EMAIL_ONLINE', 'See contents online', 'Vedi email online', 'email', 2),
(339, 'LABEL_EMAIL_SALES_TITLE', 'Check out our newest sales!', 'Ultime offerte', 'email', 2),
(340, 'LABEL_DETAIL_EMAIL', 'See details', 'Vedi i dettagli', 'email', 2),
(341, 'TEXT_EMAIL_FOOTER_USUBSCRIBE', 'unsubscribe', 'Cancellati footer', 'email', 2),
(342, 'TEXT_EMAIL_FOOTER_RESERVED', 'All rights reserved', 'Tutti i diritti riservati', 'email', 2),
(343, 'TEXT_EMAIL_FOOTER_COPYRIGHT', 'If you no longer wish to receive these emails please', 'Non ricevere più email', 'email', 2),
(344, 'LABEL_EMAIL_SUBJECT_CONTACT', 'Contact from', 'Contatto dal sito', 'email', 2),
(345, 'LABEL_EMAIL_CONTACT_TITLE', 'Thank you for contact us!', 'Ti ringraziamo per il contatto', 'email', 2),
(346, 'LABEL_EMAIL_CONTACT_TEXT', 'We have received your communication and will send you an answer as soon as possible. Thank you!', 'Comunicazione ricevuta', 'email', 2),
(347, 'LABEL_EMAIL_SUBJECT_NEWSLETTER', 'Newsletter signup', 'Iscrizione alla Newsletter', 'email', 2),
(348, 'LABEL_EMAIL_NEWSLETTER_TITLE', 'Thank you for signing up to our newsletter!', 'Grazie newsletter', 'email', 2),
(349, 'LABEL_EMAIL_NEWSLETTER_TEXT', 'You will receive updates, news and exclusive offers to stay in touch with our world.', 'Riceverai gli aggiornamenti', 'email', 2),
(350, 'LABEL_EMAIL_SUBJECT_WELCOME', 'Welcome to Ma Chlò!', 'Benvenuto su', 'email', 2),
(351, 'LABEL_EMAIL_WELCOME_TITLE', 'Welcome to Ma Chlò!', 'Benvenuto su TITOLO', 'email', 2),
(352, 'LABEL_EMAIL_WELCOME_TEXT', 'Thank you for signing up! We hope you enjoy your time with us. Check out some of our newest offers below or the button to view your new account.', 'Grazie per esserti registrato', 'email', 2),
(353, 'LABEL_EMAIL_SUBJECT_NEW_ORDER', 'New order', 'Nuovo ordine', 'email', 2),
(354, 'LABEL_TITLE_INVOICE', 'Order invoice', 'Riepilogo ordine', 'email', 2),
(355, 'LABEL_INVOICE_THANKS', 'Thank you for your order!', 'Ti ringraziamo per il tuo ordine!', 'email', 2),
(356, 'LABEL_INVOICE_THANKS_TEXT', 'We\\''ll let you know as soon as your items have shipped.<br>To change or view your order, please view your account by clicking the button below.', 'Ti faremo sapere USER', 'email', 2),
(357, 'LABEL_INVOICE_THANKS_TEXT_NOUSER', 'We\\''ll let you know as soon as your items have shipped.<br>To change or view your order, please register your account by clicking the button below.', 'Ti faremo sapere NO USER', 'email', 2),
(358, 'LABEL_SHIPPING_ADDRESS_EMAIL', 'Shipping address', 'Indirizzo di spedizione', 'email', 2),
(359, 'LABEL_ORDER_EMAIL', 'Order', 'Ordine', 'email', 2),
(360, 'LABEL_ORDER_DATE_EMAIL', 'Order date', 'Data ordine', 'email', 2),
(361, 'LABEL_ADDRESS_NOTES_EMAIL', 'Address notes', 'Note indirizzo', 'email', 2),
(362, 'LABEL_ORDER_NOTES_EMAIL', 'Order notes', 'Note ordine', 'email', 2),
(363, 'LABEL_DESCRIPTION_EMAIL', 'Description', 'Descrizione', 'email', 2),
(364, 'LABEL_QTY_EMAIL', 'Qty', 'Quantità', 'email', 2),
(365, 'LABEL_SIZE_EMAIL', 'Size', 'Taglia', 'email', 2),
(366, 'LABEL_COLOR_EMAIL', 'Color', 'Colore', 'email', 2),
(367, 'LABEL_SUBTOTAL_ORDER_EMAIL', 'Subtotal', 'Subtotale', 'email', 2),
(368, 'LABEL_COUPON_APPLY_EMAIL', 'Apply coupon', 'Applica coupon', 'email', 2),
(369, 'LABEL_SHIPPING_EMAIL', 'Shipping', 'Spedizione', 'email', 2),
(370, 'LABEL_TOTAL_EMAIL', 'Total', 'Totale', 'email', 2),
(371, 'LABEL_TP_NEW', 'Nuovo', 'Nuovo prodotto', 'frontend', 1),
(372, 'LABEL_TP_NEW', 'New', 'New product', 'frontend', 2),
(373, 'LABEL_FROM', 'from', 'From', 'frontend', 2),
(374, 'LABEL_FROM', 'da', 'Da', 'frontend', 1);

-- --------------------------------------------------------

--
-- Struttura della tabella `login_attempts`
--

CREATE TABLE IF NOT EXISTS `login_attempts` (
  `id` int(11) unsigned NOT NULL,
  `ip_address` varchar(15) NOT NULL,
  `login` varchar(100) NOT NULL,
  `time` int(11) unsigned DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Struttura della tabella `ordini`
--

CREATE TABLE IF NOT EXISTS `ordini` (
  `id_ordine` int(11) NOT NULL,
  `id_cliente` int(11) DEFAULT NULL,
  `data_ordine` date DEFAULT NULL,
  `totale_ordine` double DEFAULT NULL,
  `note_ordine` text NOT NULL,
  `tipo_pagamento` int(11) NOT NULL,
  `stato_pagamento` int(11) NOT NULL,
  `token_pagamento` varchar(250) NOT NULL,
  `stato_ordine` tinyint(4) NOT NULL DEFAULT '1',
  `id_indirizzo_spedizione` int(11) DEFAULT NULL,
  `id_indirizzo_fatturazione_spedizione` int(11) DEFAULT NULL,
  `punti` int(11) NOT NULL DEFAULT '0',
  `coupon_code` varchar(250) NOT NULL,
  `coupon_value` double NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;

--
-- Dump dei dati per la tabella `ordini`
--

INSERT INTO `ordini` (`id_ordine`, `id_cliente`, `data_ordine`, `totale_ordine`, `note_ordine`, `tipo_pagamento`, `stato_pagamento`, `token_pagamento`, `stato_ordine`, `id_indirizzo_spedizione`, `id_indirizzo_fatturazione_spedizione`, `punti`, `coupon_code`, `coupon_value`) VALUES
(8, 2, '2018-03-23', 18, 'note', 2, 1, 'tok_1C8n1pF2tw3BJl6Ov0RPFAZl', 1, 1, 3, 2, '', 0);

-- --------------------------------------------------------

--
-- Struttura della tabella `pagine`
--

CREATE TABLE IF NOT EXISTS `pagine` (
  `id_pagina` int(11) NOT NULL,
  `nome_pagina` varchar(50) NOT NULL,
  `url_pagina` varchar(50) NOT NULL,
  `id_lingua` int(11) NOT NULL,
  `controller` varchar(250) NOT NULL,
  `tipo_pagina` varchar(25) NOT NULL,
  `trad_code` varchar(25) NOT NULL,
  `label_page_url` varchar(250) NOT NULL,
  `ordine_menu` int(5) NOT NULL,
  `nome_menu` varchar(255) NOT NULL,
  `testo_menu` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `pagine`
--

INSERT INTO `pagine` (`id_pagina`, `nome_pagina`, `url_pagina`, `id_lingua`, `controller`, `tipo_pagina`, `trad_code`, `label_page_url`, `ordine_menu`, `nome_menu`, `testo_menu`) VALUES
(1, 'default page', 'default_page', 1, 'frontend/Home', 'statica', '', '', 0, '', ''),
(2, 'Home', 'it/home', 1, 'frontend/Home', 'statica', '', 'PAGE_HOME_URL', 1, 'MENU_HOME', 'Home'),
(3, 'Negozio', 'it/negozio', 1, 'frontend/Home/shop', 'dinamica', 'negozio', 'PAGE_SHOP_URL', 2, 'MENU_SHOP', 'Negozio'),
(4, 'Negozio', 'it/negozio/(:any)', 1, 'frontend/Home/shop/$1', 'dinamica', 'negozio', '', 0, '', ''),
(5, 'Chi siamo', 'it/chisiamo', 1, 'frontend/Home/about', 'statica', '', 'PAGE_ABOUT_URL', 3, 'MENU_ABOUT', 'Chi siamo'),
(6, 'Spedizioni', 'it/spedizioni', 1, 'frontend/Home/shipping', 'statica', '', 'PAGE_SHIPPING_URL', 4, 'MENU_SHIPPING', 'Spedizioni'),
(7, 'Contatti', 'it/contatti', 1, 'frontend/Home/contacts', 'statica', '', 'PAGE_CONTACTS_URL', 8, 'MENU_CONTACTS', 'Contatti'),
(8, 'Regolamento', 'it/regolamento', 1, 'frontend/Home/rules', 'statica', '', 'PAGE_RULES_URL', 5, 'MENU_RULES', 'Regolamento'),
(9, 'Prodotti', 'it/prodotti/(:any)/(:any)', 1, 'frontend/Products/detailcode/$1', 'dinamica', 'prodotti', '', 0, '', ''),
(10, 'Varianti', 'it/prodotti/(:any)/(:any)/(:any)', 1, 'frontend/Products/detailcode/$1/$2', 'dinamica', 'varianti', '', 0, '', ''),
(11, 'Account', 'it/account', 1, 'frontend/Account', 'statica', '', 'PAGE_ACCOUNT_URL', 0, '', ''),
(12, 'Logout', 'it/logout', 1, 'frontend/Account/logout', 'statica', '', 'PAGE_LOGOUT_URL', 0, '', ''),
(13, 'Login', 'it/login', 1, 'frontend/Account/login', 'statica', '', 'PAGE_LOGIN_URL', 0, '', ''),
(14, 'Registrati', 'it/registrati', 1, 'frontend/Account/register', 'statica', '', '', 0, '', ''),
(15, 'Salva account', 'it/salva_account', 1, 'frontend/Account/salvaDatiProfilo', 'statica', '', '', 0, '', ''),
(16, 'Carrello', 'it/carrello', 1, 'frontend/Cart', 'statica', '', 'PAGE_CART_URL', 0, '', ''),
(17, 'Checkout', 'it/checkout', 1, 'frontend/Cart/checkout', 'statica', '', 'PAGE_CHECKOUT_URL', 0, '', ''),
(18, 'Privacy', 'it/privacy', 1, 'frontend/Home/privacy', 'statica', '', 'PAGE_PRIVACY_URL', 7, 'MENU_PRIVACY', 'Privacy'),
(19, 'Home', 'en/home', 2, 'frontend/Home', 'statica', '', 'PAGE_HOME_URL', 1, 'MENU_HOME', 'Home'),
(20, 'Shop', 'en/shop', 2, 'frontend/Home/shop', 'dinamica', 'negozio', 'PAGE_SHOP_URL', 2, 'MENU_SHOP', 'Shop'),
(21, 'Shop', 'en/shop/(:any)', 2, 'frontend/Home/shop/$1', 'dinamica', 'negozio', '', 0, '', ''),
(22, 'About', 'en/about', 2, 'frontend/Home/about', 'statica', '', 'PAGE_ABOUT_URL', 3, 'MENU_ABOUT', 'About us'),
(23, 'Shipping', 'en/shipping', 2, 'frontend/Home/shipping', 'statica', '', 'PAGE_SHIPPING_URL', 4, 'MENU_SHIPPING', 'Shipping'),
(24, 'Contacts', 'en/contacts', 2, 'frontend/Home/contacts', 'statica', '', 'PAGE_CONTACTS_URL', 8, 'MENU_CONTACTS', 'Contacts'),
(25, 'Rules', 'en/rules', 2, 'frontend/Home/rules', 'statica', '', 'PAGE_RULES_URL', 5, 'MENU_RULES', 'Rules'),
(26, 'Products', 'en/products/(:any)/(:any)', 2, 'frontend/Products/detailcode/$1', 'dinamica', 'prodotti', '', 0, '', ''),
(27, 'Variants', 'en/products/(:any)/(:any)/(:any)', 2, 'frontend/Products/detailcode/$1/$2', 'dinamica', 'varianti', '', 0, '', ''),
(28, 'Account', 'en/account', 2, 'frontend/Account', 'statica', '', 'PAGE_ACCOUNT_URL', 0, '', ''),
(29, 'Logout', 'en/logout', 2, 'frontend/Account/logout', 'statica', '', 'PAGE_LOGOUT_URL', 0, '', ''),
(30, 'Login', 'en/login', 2, 'frontend/Account/login', 'statica', '', 'PAGE_LOGIN_URL', 0, '', ''),
(31, 'Register', 'en/register', 2, 'frontend/Account/register', 'statica', '', '', 0, '', ''),
(32, 'Save account', 'en/save_account', 2, 'frontend/Account/salvaDatiProfilo', 'statica', '', '', 0, '', ''),
(33, 'Shopping cart', 'en/cart', 2, 'frontend/Cart', 'statica', '', 'PAGE_CART_URL', 0, '', ''),
(34, 'Checkout', 'en/checkout', 2, 'frontend/Cart/checkout', 'statica', '', 'PAGE_CHECKOUT_URL', 0, '', ''),
(35, 'Privacy', 'en/privacy', 2, 'frontend/Home/privacy', 'statica', '', 'PAGE_PRIVACY_URL', 7, 'MENU_PRIVACY', 'Privacy'),
(36, 'Gallery', 'it/gallery', 1, 'frontend/Home/gallery', 'statica', '', 'PAGE_GALLERY_URL', 6, 'MENU_GALLERY', 'Gallery'),
(37, 'Gallery', 'en/gallery', 2, 'frontend/Home/gallery', 'statica', '', 'PAGE_GALLERY_URL', 6, 'MENU_GALLERY', 'Gallery'),
(38, 'Prodotti', 'it/prodotti', 1, 'frontend/Products', 'dinamica', 'prodotti', 'PAGE_PRODUCTS_URL', 0, '', ''),
(39, 'Products', 'en/products', 2, 'frontend/Products', 'dinamica', 'prodotti', 'PAGE_PRODUCTS_URL', 0, '', '');

-- --------------------------------------------------------

--
-- Struttura della tabella `pagine_contenuti`
--

CREATE TABLE IF NOT EXISTS `pagine_contenuti` (
  `id_pc` int(11) NOT NULL,
  `code` varchar(150) NOT NULL,
  `title` varchar(250) NOT NULL,
  `meta_description` text NOT NULL,
  `description` longtext NOT NULL,
  `image` varchar(250) NOT NULL,
  `id_lingua` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `pagine_contenuti`
--

INSERT INTO `pagine_contenuti` (`id_pc`, `code`, `title`, `meta_description`, `description`, `image`, `id_lingua`) VALUES
(1, 'HOME', 'Abbigliamento artistico', 'Benvenuti sul sito di Ma Chlò vendita di abbigliamento di qualità basato su lavori artistici con spedizione in tutto il mondo.', '', '', 1),
(2, 'SHOP', 'Negozio', 'Qui puoi scoprire tutti i nostri prodotti, le taglie e i colori disponibili per portare le nostre opere a casa tua.', '', '', 1),
(3, 'ABOUT', 'Chi siamo', 'Ma Chlò nasce in Sardegna nel 2017 dall’idea e dalla collaborazione di Stefania, Simona e Francesca che hanno voluto riprodurre su capi di abbigliamento i quadri di una delle fondatrici da sempre appassionata al disegno e all’arte.', 'Ma Chlò nasce in Sardegna nel 2017 dall’idea e dalla collaborazione di Stefania, Simona e Francesca che hanno voluto riprodurre su capi di abbigliamento i quadri di una delle fondatrici da sempre appassionata al disegno e all’arte. Ma Chlò, infatti, non propone delle stampe qualsiasi ma delle vere e proprie opere d’arte uniche e originali che con i loro soggetti e fantasie raffigurano la natura in tutte le sue forme e colori, qualcosa di unico che prende ispirazione anche dai profumi, dalla magia e dai paesaggi della Sardegna. Ogni quadro è ispirato ad un viaggio, un vissuto, un’emozione, un’esperienza che l’artista ha voluto esprimere su tela e nella quale ogni donna può immedesimarsi.<br><br>\r\nOltre alla natura sotto forma di animali, alberi, fiori, Ma Chlò propone come linea principale “Le maschere” che rappresentano le varie tappe di una storia d’amore. «L’idea delle maschere» racconta l’artista «nasce da una passeggiata lungo l’ultimo chilometro e mezzo rimasto in piedi del muro di Berlino.<br><br>Rimasi affascinata da un murales che rappresentava due facce divise da qualcosa di importante, decisi di riportarle su tela giocando con i colori e dopo il primo disegno capii che quella doveva diventare una storia, la storia di un amore che nessun muro avrebbe più potuto fermare». Una storia, dunque, articolata in diverse fasi, l’incontro, lo sguardo, il bacio, la separazione, il silenzio, la rottura, l’infinito, un nuovo inizio che Ma Chlò vuole far indossare ad ogni donna come espressione del proprio vissuto.<br><br>\r\nPer iniziare Ma Chlò propone nella sua linea di abbigliamento t-shirt e vestiti con l’obiettivo di ampliare la sua gamma di prodotti in un’ottica di crescita continua.<h3>Mission</h3>La nostra mission è creare un punto di incontro tra moda e arte.<br>Ciò che ci contraddistingue è l’originalità ed il significato delle stampe.', 'about.jpg', 1),
(4, 'SHIPPING', 'Spedizioni e consegne', 'La spedizione dei prodotti Ma Chlò è gratuita, i tempi di consegna sono di circa 7/10 giorni lavorativi dall’evasione dell’ordine.', '<h3>Regole di spedizione e costi</h3>\r\nLa spedizione dei prodotti Ma Chlò è gratuita, i tempi di consegna sono di circa 7/10 giorni lavorativi dall’evasione dell’ordine, se invece si desidera una spedizione espressa i tempi di consegna sono di 3/5 giorni lavorativi con un costo a carico del cliente di 20 euro.', '99fc4-blog.jpg', 1),
(5, 'RULES', 'Regolamento e condizioni', 'Ma Chlò è marchio registrato, scopri come funziona e quale è il regolamento del servizio.', 'Ma Chlò è marchio registrato, tutti i suoi contenuti, quali, a titolo esemplificativo, le immagini, le fotografie, le opere, i disegni, le figure, i loghi ed ogni altro materiale, in qualsiasi formato, pubblicato su machlo.com , compresi i menu, le pagine web, la grafica, i colori, gli schemi, gli strumenti, i caratteri ed il design del sito web, il layouts, i metodi, i processi, le funzioni ed il software che fanno parte di Ma Chlò , sono protetti dal diritto d\\''autore e da ogni altro diritto di proprietà intellettuale del Gestore e degli altri titolari dei diritti. È vietata la riproduzione, in tutto o in parte, in qualsiasi forma, di Ma Chlò senza il consenso espresso in forma scritta del Gestore.<br><br> Ma Chlò garantisce un utilizzo dei dati personali strettamente legato all’erogazione dei propri servizi, alla gestione del sito e all’evasione degli ordini e non verranno in alcun modo venduti a terzi.<br><br> Ma Chlò non può garantire ai propri utenti che le misure adottate per la sicurezza del sito e della trasmissione dei dati e delle informazioni sul sito siano in grado di limitare o escludere qualsiasi rischio di accesso non consentito o di dispersione dei dati da parte di dispositivi di pertinenza dell’utente. Per tale motivo, suggeriamo agli utenti del sito di assicurarsi che il proprio computer sia dotato di software adeguati per la protezione della trasmissione in rete di dati (ad esempio antivirus aggiornati) e che il proprio Internet provider abbia adottato misure idonee per la sicurezza della trasmissione di dati in rete.<br><br>\r\n<h3>Pagamenti</h3>\r\nIl cliente puo’ scegliere il metodo di pagamento indicato al momento dell’acquisto nel modulo d’ordine.<br> Tutti i nostri prodotti sono made in Usa, i prezzi e le transazioni sono in Euro, eventuali cambi valuta, dazi doganali, commissioni su pagamenti con carta di credito e Paypal sono a carico del cliente.<br><br> Le informazioni relative all’esecuzione della transazione saranno inviate agli enti responsabili ( Circuito Visa/Mastercard, PayPal )  tramite protocollo crittografato, senza che terzi possano aver accesso in alcun modo. Le informazioni non saranno mai visualizzate o memorizzate da parte di Ma Chlò.<br><br>\r\n<h3>Reso</h3>\r\nIl cliente ha 14 giorni per esercitare il diritto di recesso dalla data di ricezione della merce e la spedizione di restituzione  sarà a carico del cliente.<br><br> Non è  possibile cambiare il prodotto scelto con un altro.<br> Per richiedere l’autorizzazione al reso, accedere alla sezione contatti e scrivere un messaggio di posta elettronica  all’indirizzo info@machlo.com  con indicazioni del prodotto.<br><br> Il Diritto di Recesso si intende esercitato correttamente qualora siano interamente rispettate anche le seguenti condizioni:<br> •	I prodotti non devono essere stati danneggiati, indossati, lavati e non devono presentare nessun segno d’uso.<br> •	I resi devono essere spediti all’interno della confezione Ma Chlò entro 14 giorni dalla data di comunicazione, da parte del cliente, del diritto di recesso.<br><br> Il rimborso sarà eseguito con lo stesso mezzo da te utilizzato per il pagamento dopo aver ricevuto il prodotto reso.<br> Ma Chlò si riserva inoltre il diritto di rifiutare resi non autorizzati o comunque non conformi a tutte le condizioni previste.', '', 1),
(6, 'PRIVACY', 'Privacy', 'Scopri in che modo vengo trattati i tuoi dati sensibili, l''utilizzo dei cookie e dei dati di sessione.', '<h3>Cookie policy</h3>\r\nIn questo sito vengono utilizzati alcuni cookie tecnici che servono per la navigazione e per fornire un servizio già richiesto dall''utente come il carrello degli acquisti. Non vengono utilizzati per scopi ulteriori e sono normalmente installati nella maggior parte dei siti web. Un cookie è una piccola particella di informazioni che viene salvata sul dispositivo dell''utente che visita un sito web. Il cookie non contiene dati personali e non può essere utilizzato per identificare l\\''utente all\\''interno di altri siti web, compreso il sito web del provider di analisi. I cookie possono inoltre essere utilizzati per memorizzare le impostazioni preferite, come lingua e paese, in modo da renderle immediatamente disponibili alla visita successiva. Non utilizziamo gli indirizzi IP o i cookie per identificare personalmente gli utenti. Utilizziamo il sistema di analisi web al fine di incrementare l\\''efficienza del nostro portale.<br><br>Per avere maggiori informazioni sui cookies vi suggeriamo di visitare il sito www.allaboutcookies.org che vi fornirà indicazioni su come gestire secondo le vostre preferenze, ed eventualmente cancellare i cookies in funzione del browser che state utilizzando.<br> In questo sito web utilizziamo il sistema di analisi Google Analytics per misurare e analizzare le visite al nostro sito. Utilizziamo gli indirizzi IP al fine di raccogliere dati sul traffico Internet, sul browser e sul computer degli utenti. Tali informazioni vengono esaminate unicamente per fini statistici. L\\''anonimato dell\\''utente viene rispettato. Informazioni sul funzionamento del software open source di analisi web Google Analytics.<br><br> Ribadiamo che sul sito sono operativi esclusivamente cookies tecnici (come quelli sopra elencati) necessari per navigare e che essenziali quali autenticazione, validazione, gestione di una sessione di navigazione e prevenzione delle frodi e consentono ad esempio: di identificare se l’utente ha avuto regolarmente accesso alle aree del sito che richiedono la preventiva autenticazione oppure la validazione dell’utente e la gestione delle sessioni relative ai vari servizi e applicazioni oppure la conservazione dei dati per l’accesso in modalità sicura oppure le funzioni di controllo e prevenzione delle frodi.<br> Non è obbligatorio acquisire il consenso alla operatività dei soli cookies tecnici o di terze parti o analitici assimilati ai cookies tecnici. La loro disattivazione e/o il diniego alla loro operatività comporterà l’impossibilità di una corretta navigazione sul Sito e/o la impossibilità di fruire dei servizi, delle pagine, delle funzionalità o dei contenuti ivi disponibili.<br><br> Tutti i dati inseriti dai nostri clienti all\\''interno di moduli, carrello, ordini e procedure di pagamento verranno utilizzati esclusivamente per gli ordini e le consegne degli stessi. Per questo motivo i clienti sono obbligati a fornire i dati necessari alla compilazione dei moduli richiesti, in caso contrario non sarà possibile utilizzare il nostro servizio.<br> In particolare i dati utilizzati per i pagamenti online non verranno assolutamente trattati e/o memorizzati sui nostri sistemi in quanto passati direttamente ai servizi di pagamento Stripe e/o Paypal tramite transazione sicura SSL (HTTPS).<br>Al momento dell\\''ordine, della richiesta di preventivo o informazioni, l\\''indirizzo email dell\\''utente verrà inserito nel nostro sistema di newsletter e informazioni ai clienti da cui sarà sempre possibile disiscriversi facilmente tramite il link presente in ogni comunicazione inviata.<br> In ogni caso confermiamo che i dati utilizzati e memorizzati al fine del funzionamento del servizio non verranno mai e in nessun caso ceduti a terzi per nessun tipo di finalità od utilizzo.', '', 1),
(7, 'CONTACTS', 'Contatti e recapiti', 'Puoi contattare Ma Chlò in tantissimi modi diversi: email, telefono e socials.', '', '', 1),
(8, 'GALLERY', 'Galleria immagini', 'Ecco una galleria delle nostre immagini più belle.', '', '', 1),
(9, 'HOME', 'Artistic apparel', 'Welcome to Ma Chlò'' s website, selling quality garments based on artwork with worldwide shipping.', '', '', 2),
(10, 'SHOP', 'Our products', 'Here you can find all of our products, sizes and colors available to bring our works to your home.', '', '', 2),
(11, 'ABOUT', 'About us', 'Ma Chlò was born in Sardinia in 2017 by the idea and collaboration of Stefania, Simona and Francesca who wanted to reproduce on dresses the paintings of one of the founders who have always been fond of drawing and art.', 'Ma Chlò was founded in Sardinia in 2017 by the collaboration of Stefania, Simona and Francesca. One of our founders has always been interested in art and we wanted to display her paintings on our clothing. Ma Chlò doesn’t use prints but original and real works of art which represent nature in all its shapes and colors and include different subjects and their imaginations. Our unique offering also takes inspiration from the scents, magic and landscape of Sardinia. Each picture is inspired by a trip, an emotion or an experience that the artist wants to express in her art and which every woman who wears our clothes can feel.<br><br>Apart from nature in the form of animals, trees and flowers, Ma Chlò offers our customers the clothing line: "The Masks" that represents the evolution of a love story. "The idea of the masks," the artist tells us, “arose during a walk along the last kilometre and a half of the remaining section of the Berlin Wall. I was fascinated by a painting on this famous wall that represented two faces which had been divided from each other. After playing with the colours I understood after the first sketch that this was a story, a love story, that no barrier could ever stop." Different phases of the story are represented and include “the meeting”, ”the look”, “the kiss”, “the separation”, “the silence”, “the breakup”, “eternity” and “a new beginning” that Ma Chlò wants every woman to wear as an expression of her experiences. Ma Chlò currently offers t-shirts and dresses and plans to expand its range of products as we continue to grow.<h3>Mission</h3>We would like to create a meeting point between fashion and art.  We believe that our originality and the meaning of our paintings distinguishes us from other clothing companies.', 'about.jpg', 2),
(12, 'SHIPPING', 'Shipping and delivery', 'Welcome to Ma Chlò\\''s website, selling quality garments based on artwork with worldwide shipping.', '<h3>Shipping rules and price</h3>\r\nMa Chlò shipping is free, the delivery time is within 7/10 business days once the order will be fulfilled, or you can choose an express shipping for 20€', '99fc4-blog.jpg', 2),
(13, 'RULES', 'Rules and conditions', 'Ma Chlò is a registered trademark, find out how it works and what the service policy is.', 'Ma Chlò is a registered trade mark, all of its contents, like images, photos, works, sketches, figures, logos and every other material, in any format, published on machlo.com, included menus, web pages, graphics, colors, schemes, tools, characters and the design of the web site, layouts, methods, trials, functions and the software that make part of Ma Chlò, are protected by copyright and by all other intellectual rights of the Owner and the other holders of the rights. All reproduction, in whole or in part, in any form, of Ma Chlò, without written permission of the Owner, is forbidden.<br/><br/> Ma Chlò tightly guarantees an use of the personal data tied up to its own services, to the management of the site and the escape of the orders and they won''t come in some way sold to third.<br/><br/> Ma Chlò cannot guarantee to its own consumers that the measures adopted for the safety of the site and the transmission of the data and the information on the site is able to limit or to exclude any risk of access not allowed or of dispersion of the data from devices of pertinence of the consumer. For such motive, we suggest to the consumers of the site to make sure that his/her own computer both endowed with suitable software for the protection of the transmission online of data (for instance adjourned antivirus) and that the proper Internet provider has adopted online fit measures for the safety of the data transmission.<br>\r\n<h3>Payments</h3>\r\nThe consumer is able to choose the method of suitable payment during the purchase in the form of order. All of our products are made in Usa, the prices and the transactions are in Euro, possible changes currency, customs, errands on payments with credit card and Paypal are to load of the consumer. The financial information (Encircled Visa / Mastercard PayPal) will be cryptographically forwarded , without third parties being able to access said information in any way . Information will never be visualized or you memorize from Ma Chlò.<br/><br/>\r\n<h3>Refund</h3>\r\nThe consumer has the right to withdraw from the contract concluded with the Vendor, within 14 working days from the day of receiving the products purchased on “Ma Chlò”.<br/> Return will be a customer''s charge.<br/> An item cannot be exchanged for another one.<br/><br/> To ask for the authorization to theproduct, you can contact us by our web site on the section “contact” writing an e-mail message to the address info@machlo.com with indications of the product.<br/> The Right of Return is considered correctly followed when the following conditions are also completely met:<br/><br/> •	The products must not have been damaged, worn, washed and must not show any sign of use.<br/> •	the products must be returned in their original packaging to Ma Chlò within 14 days from the date of communication, from the consumer, of the right of recess.<br/><br/> The refund will be performed with the same metod used for the payment after having received the product.<br/><br/> Ma Chlò reserves the right to refuse non authorized refunds or however you doesn''t conform to all the anticipated conditions.', '', 2),
(14, 'PRIVACY', 'Privacy', 'Find out how we treat your sensitive data, use cookies, and sessions data.', '<h3>Cookie policy</h3>\r\nThis site uses some technical cookies that are used to navigate and provide a service already required by the user as the shopping cart. They are not used for further purposes and are normally installed on most websites. A cookie is a small piece of information that is saved on the user\\''s device that visits a website. The cookie does not contain personal data and can not be used to identify the user inside other websites, including the analyst\\''s website. Cookies can also be used to store your favorite settings, such as your language and country, so that you can make them available immediately to your next visit. We do not use IP addresses or cookies to personally identify users. We use the web analytics system to increase the efficiency of our portal. <br> <br> For more information on cookies, please visit www.allaboutcookies.org to provide you with directions on how to handle your cookies Your preferences, and possibly delete cookies as a browser you are using. <br> On this website we use the Google Analytics analysis system to measure and analyze visits to our site. We use IP addresses to collect data on Internet traffic, browsers, and user computers. This information is only examined for statistical purposes. The user\\''s anonymity is respected. About the operation of the open source Google Analytics web analytics software. <br> <br> We reiterate that technical cookies (such as those listed above) are required to navigate and essential, such as authentication, validation, management of a session of Navigation and fraud prevention and allow, for example: to identify whether the user has regularly accessed areas of the site that require prior authentication or user validation and session management for various services and applications or the retention of Data for secure access or the control and prevention of fraud. <br> It is not compulsory to acquire the consent of operating only technical or third-party cookies or analytics similar to technical cookies. Their deactivation and / or denial of their operation will result in the inability to properly navigate the Site and / or the inability to access the services, pages, features or content available there. The data entered by our customers inside forms, shopping carts, orders and payment procedures will only be used for orders and deliveries. For this reason, customers are required to provide the data required to complete the required forms, otherwise we will not be able to use our service. In particular, the data used for online payments will not be treated and / or stored on Our systems as they have passed directly to the Stripe and / or Paypal payment services via Secure SSL Transaction (HTTPS). <br> At the time of ordering the quote or information, the user\\''s email address will come Inserted in our newsletter system and customer information from which it will always be possible to easily disagree with the link in any communication sent. <br> In any case, we confirm that the data used and stored for the purpose of service will never and No case has been transferred to third parties for any purpose or use.', '', 2),
(15, 'CONTACTS', 'Contacts and infos', 'You can contact Ma Chlò in many different ways: email, phone and socials.', '', '', 2),
(16, 'GALLERY', 'Images gallery', 'This is our best images gallery.', '', '', 2);

-- --------------------------------------------------------

--
-- Struttura della tabella `prodotti`
--

CREATE TABLE IF NOT EXISTS `prodotti` (
  `id_prodotti` int(11) NOT NULL,
  `id_tipo_prodotto` int(11) DEFAULT NULL,
  `codice` varchar(100) DEFAULT NULL,
  `nome` varchar(250) DEFAULT NULL,
  `prezzo` double DEFAULT NULL,
  `url_img_piccola` varchar(250) DEFAULT NULL,
  `url_img_grande` varchar(250) DEFAULT NULL,
  `stato` tinyint(1) NOT NULL,
  `varianti` int(11) NOT NULL,
  `ordine` int(11) NOT NULL DEFAULT '0',
  `sync_status` int(2) NOT NULL,
  `sync_time` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `prodotti`
--

INSERT INTO `prodotti` (`id_prodotti`, `id_tipo_prodotto`, `codice`, `nome`, `prezzo`, `url_img_piccola`, `url_img_grande`, `stato`, `varianti`, `ordine`, `sync_status`, `sync_time`) VALUES
(1, 5, '5aa5a386dd8547', 'Blue cat', 14.25, '5aa5a386dd8547.png', '5aa5a386dd8547.png', 3, 8, 1, 1, '2018-03-19 12:21:21'),
(2, 5, '5aa5a2c4598f03', 'Sleeping mask', 14.25, '5aa5a2c4598f03.png', '5aa5a2c4598f03.png', 3, 8, 1, 1, '2018-03-19 12:21:21'),
(3, 5, '5aa599dd2b4a72', 'Colored cats ', 15, '5aa599dd2b4a72.png', '5aa599dd2b4a72.png', 3, 20, 1, 1, '2018-03-19 12:21:21'),
(4, 5, '5aa3013293e052', 'Lips t-shirt', 15, '5aa3013293e052.png', '5aa3013293e052.png', 3, 35, 1, 1, '2018-03-19 12:21:21'),
(5, 5, '5aa2fe0e98fe41', 'Flowers', 14.25, '5aa2fe0e98fe41.png', '5aa2fe0e98fe41.png', 3, 8, 1, 1, '2018-03-19 12:21:21'),
(6, 5, '5aa2fb8b66a114', 'Masks', 14.25, '5aa2fb8b66a114.png', '5aa2fb8b66a114.png', 3, 8, 1, 1, '2018-03-19 12:21:21'),
(7, 3, '5aa2f943e9f697', 'Yellow cat', 15, '5aa2f943e9f697.png', '5aa2f943e9f697.png', 1, 15, 1, 1, '2018-03-19 12:21:21'),
(8, 5, '5aa2f81d3552c7', 'Gallinella sarda', 14.25, '5aa2f81d3552c7.png', '5aa2f81d3552c7.png', 3, 8, 1, 1, '2018-03-19 12:21:21'),
(9, 5, '5aa2a4314a7da4', 'Yellow cat', 7.95, '5aa2a4314a7da4.png', '5aa2a4314a7da4.png', 3, 1, 1, 1, '2018-03-19 12:21:21'),
(10, 5, '5aa2a3db357648', 'Yang', 7.95, '5aa2a3db357648.png', '5aa2a3db357648.png', 3, 1, 1, 1, '2018-03-19 12:21:21'),
(11, 5, '5aa2a35b45f316', 'Universe', 7.95, '5aa2a35b45f316.png', '5aa2a35b45f316.png', 3, 1, 1, 1, '2018-03-19 12:21:21'),
(12, 5, '5aa2a2dc902565', 'Standby', 7.95, '5aa2a2dc902565.png', '5aa2a2dc902565.png', 3, 1, 1, 1, '2018-03-19 12:21:21'),
(13, 5, '5aa2a2244efd56', 'Ma Chlò', 7.95, '5aa2a2244efd56.png', '5aa2a2244efd56.png', 3, 1, 1, 1, '2018-03-19 12:21:21'),
(14, 5, '5aa29e4923eeb6', 'La separazione', 7.95, '5aa29e4923eeb6.png', '5aa29e4923eeb6.png', 3, 1, 1, 1, '2018-03-19 12:21:21'),
(15, 5, '5aa29dff07e579', 'Japan', 7.95, '5aa29dff07e579.png', '5aa29dff07e579.png', 3, 1, 1, 1, '2018-03-19 12:21:21'),
(16, 5, '5aa29cee9d00e1', 'Il silenzio', 7.95, '5aa29cee9d00e1.png', '5aa29cee9d00e1.png', 3, 1, 1, 1, '2018-03-19 12:21:21'),
(17, 5, '5aa29cb78a18b1', 'Un nuovo inizio', 7.95, '5aa29cb78a18b1.png', '5aa29cb78a18b1.png', 3, 1, 1, 1, '2018-03-19 12:21:21'),
(18, 5, '5aa29c681a45f2', 'Il bacio', 7.95, '5aa29c681a45f2.png', '5aa29c681a45f2.png', 3, 1, 1, 1, '2018-03-19 12:21:21'),
(19, 5, '5aa29c1e763eb7', 'Il pensiero', 7.95, '5aa29c1e763eb7.png', '5aa29c1e763eb7.png', 3, 1, 1, 1, '2018-03-19 12:21:21'),
(20, 5, '5aa29bd9d57cd5', 'Jellyfish', 7.95, '5aa29bd9d57cd5.png', '5aa29bd9d57cd5.png', 3, 1, 1, 1, '2018-03-19 12:21:21'),
(21, 5, '5aa29b851d86a1', 'Color tree', 7.95, '5aa29b851d86a1.png', '5aa29b851d86a1.png', 3, 1, 1, 1, '2018-03-19 12:21:21'),
(22, 5, '5aa29b26787987', 'Colorful town', 7.95, '5aa29b26787987.png', '5aa29b26787987.png', 3, 1, 1, 1, '2018-03-19 12:21:21'),
(23, 5, '5aa29ab9c856c2', 'A monkey playing with a snake', 7.95, '5aa29ab9c856c2.png', '5aa29ab9c856c2.png', 3, 1, 1, 1, '2018-03-19 12:21:21'),
(24, 5, '5aa29a3e69b2c4', 'Feel good', 7.95, '5aa29a3e69b2c4.png', '5aa29a3e69b2c4.png', 3, 1, 1, 1, '2018-03-19 12:21:21'),
(25, 5, '5aa299f1a4ad82', 'Talking', 7.95, '5aa299f1a4ad82.png', '5aa299f1a4ad82.png', 3, 1, 1, 1, '2018-03-19 12:21:21'),
(26, 5, '5aa298b3ec6ad5', 'Japan', 36.95, '5aa298b3ec6ad5.png', '5aa298b3ec6ad5.png', 3, 5, 1, 1, '2018-03-19 12:21:21'),
(27, 5, '5aa297d0cfdd44', 'Eclipse', 36.95, '5aa297d0cfdd44.png', '5aa297d0cfdd44.png', 3, 5, 1, 1, '2018-03-19 12:21:21'),
(28, 5, '5aa27b23a9f4a8', 'Yang', 36.95, '5aa27b23a9f4a8.png', '5aa27b23a9f4a8.png', 3, 5, 1, 1, '2018-03-19 12:21:21'),
(29, 5, '5aa27a84d1e238', 'Standby', 36.95, '5aa27a84d1e238.png', '5aa27a84d1e238.png', 3, 5, 1, 1, '2018-03-19 12:21:21'),
(30, 5, '5aa27a09f005c3', 'Universe', 36.95, '5aa27a09f005c3.png', '5aa27a09f005c3.png', 3, 5, 1, 1, '2018-03-19 12:21:21'),
(31, 5, '5aa27975242e84', 'Colored cats', 36.95, '5aa27975242e84.png', '5aa27975242e84.png', 3, 5, 1, 1, '2018-03-19 12:21:21'),
(32, 5, '5aa275790bd952', 'Geometric', 36.95, '5aa275790bd952.png', '5aa275790bd952.png', 3, 5, 1, 1, '2018-03-19 12:21:21'),
(33, 5, '5aa010e062b998', 'Il pensiero', 34.95, '5aa010e062b998.png', '5aa010e062b998.png', 3, 5, 1, 1, '2018-03-19 12:21:21'),
(34, 5, '5aa00f5f1c42e5', 'Jungle flowers dress black', 34.95, '5aa00f5f1c42e5.png', '5aa00f5f1c42e5.png', 3, 5, 1, 1, '2018-03-19 12:21:21'),
(35, 5, '5aa00eb9a04005', 'Jungle flowers dress', 34.95, '5aa00eb9a04005.png', '5aa00eb9a04005.png', 3, 5, 1, 1, '2018-03-19 12:21:21'),
(36, 5, '5aa00df8cceb58', 'La separazione', 34.95, '5aa00df8cceb58.png', '5aa00df8cceb58.png', 3, 5, 1, 1, '2018-03-19 12:21:21'),
(37, 5, '5aa00db6bb9d33', 'Life of a tree', 34.95, '5aa00db6bb9d33.png', '5aa00db6bb9d33.png', 3, 5, 1, 1, '2018-03-19 12:21:21'),
(38, 5, '5aa00c3e2e6184', 'Lips dress', 34.95, '5aa00c3e2e6184.png', '5aa00c3e2e6184.png', 1, 5, 1, 1, '2018-03-19 12:21:21'),
(39, 5, '5aa00bc3dc48f8', 'Monkeys into the jungle', 34.95, '5aa00bc3dc48f8.png', '5aa00bc3dc48f8.png', 3, 5, 1, 1, '2018-03-19 12:21:21'),
(40, 5, '5aa00b9b1fc403', 'Reaching for the moon', 34.95, '5aa00b9b1fc403.png', '5aa00b9b1fc403.png', 3, 5, 1, 1, '2018-03-19 12:21:21'),
(41, 5, '5aa009916da013', 'Un nuovo inizio', 34.95, '5aa009916da013.png', '5aa009916da013.png', 3, 5, 1, 1, '2018-03-19 12:21:21'),
(42, 5, '5aa0058e449ec3', 'Woman''s lips', 34.95, '5aa0058e449ec3.png', '5aa0058e449ec3.png', 3, 5, 1, 1, '2018-03-19 12:21:21'),
(43, 5, '5aa00071b87794', 'Wild girl', 34.95, '5aa00071b87794.png', '5aa00071b87794.png', 3, 5, 1, 1, '2018-03-19 12:21:21'),
(44, 5, '5aa0001c6a2461', 'Snakes dress', 34.95, '5aa0001c6a2461.png', '5aa0001c6a2461.png', 3, 5, 1, 1, '2018-03-19 12:21:21'),
(45, 5, '5a9fffa5692b77', 'Colorful town', 34.95, '5a9fffa5692b77.png', '5a9fffa5692b77.png', 3, 5, 1, 1, '2018-03-19 12:21:21'),
(46, 5, '5a9e7ddd14c8c6', 'Gallinella sarda t-shirt', 21.95, '5a9e7ddd14c8c6.png', '5a9e7ddd14c8c6.png', 3, 4, 1, 1, '2018-03-19 12:21:21'),
(47, 5, '5a9e7dbe639f93', 'Gallinella sarda t-shirt', 21.95, '5a9e7dbe639f93.png', '5a9e7dbe639f93.png', 3, 4, 1, 1, '2018-03-19 12:21:21'),
(48, 5, '5a9e7d71439da1', 'Dreaming of you t-shirt', 21.95, '5a9e7d71439da1.png', '5a9e7d71439da1.png', 3, 4, 1, 1, '2018-03-19 12:21:21'),
(49, 5, '5a9e7cfa97eb11', 'Dreaming of you t-shirt', 21.95, '5a9e7cfa97eb11.png', '5a9e7cfa97eb11.png', 3, 4, 1, 1, '2018-03-19 12:21:21'),
(50, 5, '5a9e7be6ef2c59', 'Il bacio t-shirt', 29.95, '5a9e7be6ef2c59.png', '5a9e7be6ef2c59.png', 3, 6, 1, 1, '2018-03-19 12:21:21'),
(51, 5, '5a9e7b0f14d151', 'Il pensiero t-shirt', 29.95, '5a9e7b0f14d151.png', '5a9e7b0f14d151.png', 3, 6, 1, 1, '2018-03-19 12:21:21'),
(52, 5, '5a9e7a457b0155', 'Il silenzio t-shirt', 29.95, '5a9e7a457b0155.png', '5a9e7a457b0155.png', 3, 6, 1, 1, '2018-03-19 12:21:21'),
(53, 5, '5a9e7938145986', 'Infinito t-shirt', 29.95, '5a9e7938145986.png', '5a9e7938145986.png', 3, 6, 1, 1, '2018-03-19 12:21:21'),
(54, 5, '5a9e784dd4fc14', 'La separazione t-shirt', 29.95, '5a9e784dd4fc14.png', '5a9e784dd4fc14.png', 3, 6, 1, 1, '2018-03-19 12:21:21'),
(55, 5, '5a9e76e177cf98', 'Lo sguardo t-shirt', 29.95, '5a9e76e177cf98.png', '5a9e76e177cf98.png', 3, 6, 1, 1, '2018-03-19 12:21:21'),
(56, 5, '5a8ed76cdfac23', 'Un nuovo inizio T-shirt', 29.95, '5a8ed76cdfac23.png', '5a8ed76cdfac23.png', 3, 6, 1, 1, '2018-03-19 12:21:21'),
(57, 5, '5a8ed67932c228', 'L''incontro T-shirt', 29.95, '5a8ed67932c228.png', '5a8ed67932c228.png', 3, 6, 1, 1, '2018-03-19 12:21:21'),
(58, 5, '5a8b2ba7958098', 'Lips t-shirt', 15, '5a8b2ba7958098.png', '5a8b2ba7958098.png', 3, 30, 1, 1, '2018-03-19 12:21:21'),
(59, 5, '5a8b29f137e8c2', 'Yang', 7.95, '5a8b29f137e8c2.png', '5a8b29f137e8c2.png', 3, 1, 1, 1, '2018-03-19 12:21:21'),
(60, 5, '5a8b2928123a87', 'Jungle Flowers Dress', 34.95, '5a8b2928123a87.png', '5a8b2928123a87.png', 3, 5, 1, 1, '2018-03-19 12:21:21');

-- --------------------------------------------------------

--
-- Struttura della tabella `prodotti_categorie`
--

CREATE TABLE IF NOT EXISTS `prodotti_categorie` (
  `id_prodotti_categorie` int(11) NOT NULL,
  `id_prodotto` int(11) NOT NULL,
  `id_categoria` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `prodotti_categorie`
--

INSERT INTO `prodotti_categorie` (`id_prodotti_categorie`, `id_prodotto`, `id_categoria`) VALUES
(1, 7, 8),
(2, 38, 6);

-- --------------------------------------------------------

--
-- Struttura della tabella `prodotti_traduzioni`
--

CREATE TABLE IF NOT EXISTS `prodotti_traduzioni` (
  `id_prodotti_traduzioni` int(11) NOT NULL,
  `id_prodotti` int(11) NOT NULL,
  `descrizione` text NOT NULL,
  `descrizione_breve` varchar(250) NOT NULL,
  `lingua_traduzione_id` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `prodotti_traduzioni`
--

INSERT INTO `prodotti_traduzioni` (`id_prodotti_traduzioni`, `id_prodotti`, `descrizione`, `descrizione_breve`, `lingua_traduzione_id`) VALUES
(1, 7, 'Maglietta manica corta', 'Maglietta manica corta', 1),
(2, 7, 'Short sleeve tshirt', 'Short sleeve tshirt', 2),
(3, 38, 'Vestito bianco', 'Vestito bianco', 1),
(4, 38, 'White dress', 'White dress', 2);

-- --------------------------------------------------------

--
-- Struttura della tabella `prodotto_taglia`
--

CREATE TABLE IF NOT EXISTS `prodotto_taglia` (
  `fk_prodotto` int(11) NOT NULL,
  `fk_taglia` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `prodotto_taglia`
--

INSERT INTO `prodotto_taglia` (`fk_prodotto`, `fk_taglia`) VALUES
(1, 1),
(1, 2),
(1, 3),
(1, 4),
(1, 5),
(2, 1),
(2, 2),
(2, 3),
(2, 4),
(2, 5),
(3, 1),
(3, 2),
(3, 3),
(3, 4),
(3, 5),
(3, 6),
(4, 1),
(4, 2),
(4, 3),
(4, 4),
(4, 5),
(5, 1),
(5, 2),
(5, 3),
(5, 4),
(5, 5),
(6, 2),
(6, 3),
(6, 4),
(6, 5),
(6, 6),
(7, 2),
(7, 3),
(7, 4),
(7, 5),
(7, 6),
(8, 2),
(8, 3),
(8, 4),
(8, 5),
(8, 6),
(9, 2),
(9, 3),
(9, 4),
(9, 5),
(9, 6),
(10, 2),
(10, 3),
(10, 4),
(10, 5),
(10, 6),
(11, 2),
(11, 3),
(11, 4),
(11, 5),
(11, 6),
(12, 2),
(12, 3),
(12, 4),
(12, 5),
(12, 6),
(13, 2),
(13, 3),
(13, 4),
(13, 5),
(13, 6),
(14, 2),
(14, 3),
(14, 4),
(14, 5),
(14, 6),
(15, 1),
(15, 2),
(15, 3),
(15, 4),
(15, 5),
(15, 6),
(16, 1),
(16, 2),
(16, 3),
(16, 4),
(16, 5),
(16, 6),
(17, 1),
(17, 2),
(17, 3),
(17, 4),
(17, 5),
(17, 6),
(18, 1),
(18, 2),
(18, 3),
(18, 4),
(18, 5),
(18, 6),
(19, 1),
(19, 2),
(19, 3),
(19, 4),
(19, 5),
(19, 6),
(20, 2),
(20, 3),
(20, 4),
(20, 5),
(20, 6),
(21, 2),
(21, 3),
(21, 4),
(21, 5),
(21, 6),
(22, 2),
(22, 3),
(22, 4),
(22, 5),
(22, 6),
(23, 2),
(23, 3),
(23, 4),
(23, 5),
(23, 6),
(24, 2),
(24, 3),
(24, 4),
(24, 5),
(24, 6),
(25, 2),
(25, 3),
(25, 4),
(25, 5),
(25, 6),
(26, 2),
(26, 3),
(26, 4),
(26, 5),
(26, 6),
(27, 2),
(27, 3),
(27, 4),
(27, 5),
(27, 6),
(28, 2),
(28, 3),
(28, 4),
(28, 5),
(28, 6),
(29, 2),
(29, 3),
(29, 4),
(29, 5),
(29, 6),
(30, 2),
(30, 3),
(30, 4),
(30, 5),
(30, 6),
(31, 2),
(31, 3),
(31, 4),
(31, 5),
(31, 6),
(32, 2),
(32, 3),
(32, 4),
(32, 5),
(32, 6),
(33, 2),
(33, 3),
(33, 4),
(33, 5),
(33, 6),
(34, 2),
(34, 3),
(34, 4),
(34, 5),
(34, 6),
(35, 1),
(35, 2),
(35, 3),
(35, 4),
(35, 5),
(35, 6),
(36, 1),
(36, 2),
(36, 3),
(36, 4),
(36, 5),
(36, 6),
(37, 1),
(37, 2),
(37, 3),
(37, 4),
(37, 5),
(37, 6),
(38, 1),
(38, 2),
(38, 3),
(38, 4),
(38, 5),
(38, 6),
(39, 1),
(39, 2),
(39, 3),
(39, 4),
(39, 5),
(39, 6),
(40, 1),
(40, 2),
(40, 3),
(40, 4),
(40, 5),
(40, 6),
(41, 1),
(41, 2),
(41, 3),
(41, 4),
(41, 5),
(42, 1),
(42, 2),
(42, 3),
(42, 4),
(42, 5),
(43, 1),
(43, 2),
(43, 3),
(43, 4),
(43, 5),
(44, 1),
(44, 2),
(44, 3),
(44, 4),
(44, 5),
(45, 1),
(45, 2),
(45, 3),
(45, 4),
(45, 5),
(46, 1),
(46, 2),
(46, 3),
(46, 4),
(46, 5),
(47, 1),
(47, 2),
(47, 3),
(47, 4),
(47, 5),
(48, 1),
(48, 2),
(48, 3),
(48, 4),
(48, 5),
(49, 1),
(49, 2),
(49, 3),
(49, 4),
(49, 5),
(50, 1),
(50, 2),
(50, 3),
(50, 4),
(50, 5),
(51, 1),
(51, 2),
(51, 3),
(51, 4),
(51, 5),
(51, 6),
(52, 1),
(52, 2),
(52, 3),
(52, 4),
(52, 5),
(52, 6),
(53, 1),
(53, 2),
(53, 3),
(53, 4),
(53, 5),
(53, 6),
(54, 1),
(54, 2),
(54, 3),
(54, 4),
(54, 5),
(54, 6),
(55, 2),
(55, 3),
(55, 4),
(55, 5),
(55, 6),
(56, 1),
(56, 2),
(56, 3),
(56, 4),
(56, 5),
(56, 6),
(57, 1),
(57, 2),
(57, 3),
(57, 4),
(57, 5),
(57, 6),
(58, 1),
(58, 2),
(58, 3),
(58, 4),
(58, 5),
(58, 6),
(59, 2),
(59, 3),
(59, 4),
(59, 5),
(59, 6),
(60, 2),
(60, 3),
(60, 4),
(60, 5),
(60, 6),
(61, 2),
(61, 3),
(61, 4),
(61, 5),
(61, 6),
(62, 2),
(62, 3),
(62, 4),
(62, 5),
(62, 6),
(63, 2),
(63, 3),
(63, 4),
(63, 5),
(63, 6),
(64, 2),
(64, 3),
(64, 4),
(64, 5),
(64, 6),
(65, 2),
(65, 3),
(65, 4),
(65, 5),
(65, 6),
(66, 2),
(66, 3),
(66, 4),
(66, 5),
(66, 6),
(67, 2),
(67, 3),
(67, 4),
(67, 5),
(67, 6),
(68, 2),
(68, 3),
(68, 4),
(68, 5),
(68, 6),
(69, 2),
(69, 3),
(69, 4),
(69, 5),
(69, 6),
(70, 2),
(70, 3),
(70, 4),
(70, 5),
(70, 6),
(71, 2),
(71, 3),
(71, 4),
(71, 5),
(71, 6),
(72, 2),
(72, 3),
(72, 4),
(72, 5),
(72, 6),
(73, 2),
(73, 3),
(73, 4),
(73, 5),
(73, 6),
(74, 2),
(74, 3),
(74, 4),
(74, 5),
(74, 6),
(75, 2),
(75, 3),
(75, 4),
(75, 5),
(75, 6),
(76, 2),
(76, 3),
(76, 4),
(76, 5),
(76, 6),
(77, 2),
(77, 3),
(77, 4),
(77, 5),
(77, 6),
(78, 2),
(78, 3),
(78, 4),
(78, 5),
(78, 6),
(79, 2),
(79, 3),
(79, 4),
(79, 5),
(79, 6),
(80, 2),
(80, 3),
(80, 4),
(80, 5),
(80, 6),
(81, 2),
(81, 3),
(81, 4),
(81, 5),
(81, 6),
(82, 2),
(82, 3),
(82, 4),
(82, 5),
(82, 6),
(83, 2),
(83, 3),
(83, 4),
(83, 5),
(83, 6),
(84, 1),
(84, 2),
(84, 3),
(84, 4),
(84, 5),
(85, 1),
(85, 2),
(85, 3),
(85, 4),
(85, 5),
(86, 1),
(86, 2),
(86, 3),
(86, 4),
(86, 5),
(87, 2),
(87, 3),
(87, 4),
(87, 5),
(88, 2),
(88, 3),
(88, 4),
(88, 5),
(89, 2),
(89, 3),
(89, 4),
(89, 5),
(90, 1),
(90, 2),
(90, 3),
(90, 4),
(90, 5),
(91, 2),
(91, 3),
(91, 4),
(91, 5),
(92, 1),
(92, 2),
(92, 3),
(92, 4),
(92, 5),
(93, 1),
(93, 2),
(93, 3),
(93, 4),
(93, 5),
(94, 2),
(94, 3),
(94, 4),
(94, 5),
(95, 2),
(95, 3),
(95, 4),
(95, 5),
(96, 2),
(96, 3),
(96, 4),
(96, 5),
(97, 2),
(97, 3),
(97, 4),
(97, 5),
(98, 2),
(98, 3),
(98, 4),
(98, 5),
(99, 2),
(99, 3),
(99, 4),
(99, 5),
(100, 2),
(100, 3),
(100, 4),
(100, 5),
(101, 2),
(101, 3),
(101, 4),
(101, 5),
(102, 2),
(102, 3),
(102, 4),
(102, 5),
(103, 1),
(103, 2),
(103, 3),
(103, 4),
(103, 5),
(103, 6),
(105, 2),
(105, 3),
(105, 4),
(105, 5),
(105, 6),
(106, 2),
(106, 3),
(106, 4),
(106, 5),
(106, 6),
(107, 2),
(107, 3),
(107, 4),
(107, 5),
(107, 6),
(108, 2),
(108, 3),
(108, 4),
(108, 5),
(108, 6),
(109, 1),
(109, 2),
(109, 3),
(109, 4),
(109, 5),
(110, 1),
(110, 2),
(110, 3),
(110, 4),
(110, 5),
(111, 1),
(111, 2),
(111, 3),
(111, 4),
(111, 5),
(112, 1),
(112, 2),
(112, 3),
(112, 4),
(112, 5),
(113, 1),
(113, 2),
(113, 3),
(113, 4),
(113, 5),
(114, 1),
(114, 2),
(114, 3),
(114, 4),
(114, 5),
(115, 1),
(115, 2),
(115, 3),
(115, 4),
(115, 5),
(116, 1),
(116, 2),
(116, 3),
(116, 4),
(116, 5),
(117, 2),
(117, 3),
(117, 4),
(117, 5),
(117, 6),
(118, 2),
(118, 3),
(118, 4),
(118, 5),
(118, 6),
(119, 2),
(119, 3),
(119, 4),
(119, 5),
(119, 6),
(120, 1),
(120, 2),
(120, 3),
(120, 4),
(120, 5),
(121, 1),
(121, 2),
(121, 3),
(121, 4),
(121, 5),
(122, 1),
(122, 2),
(122, 3),
(122, 4),
(122, 5),
(123, 1),
(123, 2),
(123, 3),
(123, 4),
(123, 5),
(125, 1),
(125, 2),
(125, 3),
(125, 4),
(125, 5),
(126, 7),
(127, 7),
(128, 7),
(129, 7),
(130, 7),
(131, 7),
(132, 7),
(133, 7),
(134, 7),
(135, 7),
(136, 7),
(137, 7),
(138, 7),
(139, 7),
(140, 7),
(141, 7),
(142, 7);

-- --------------------------------------------------------

--
-- Struttura della tabella `stato_coupon`
--

CREATE TABLE IF NOT EXISTS `stato_coupon` (
  `id_stato_coupon` int(11) NOT NULL,
  `desc_stato_coupon` varchar(250) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `stato_coupon`
--

INSERT INTO `stato_coupon` (`id_stato_coupon`, `desc_stato_coupon`) VALUES
(1, 'ATTIVO'),
(2, 'SOSPESO'),
(3, 'UTILIZZATO');

-- --------------------------------------------------------

--
-- Struttura della tabella `stato_descrizione`
--

CREATE TABLE IF NOT EXISTS `stato_descrizione` (
  `id_stato_descrizione` int(11) NOT NULL,
  `testo_stato_descrizione` varchar(250) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `stato_descrizione`
--

INSERT INTO `stato_descrizione` (`id_stato_descrizione`, `testo_stato_descrizione`) VALUES
(1, 'ATTIVO'),
(2, 'SOSPESO');

-- --------------------------------------------------------

--
-- Struttura della tabella `stato_ordine`
--

CREATE TABLE IF NOT EXISTS `stato_ordine` (
  `id_stato_ordine` int(11) NOT NULL,
  `desc_stato_ordine` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `stato_ordine`
--

INSERT INTO `stato_ordine` (`id_stato_ordine`, `desc_stato_ordine`) VALUES
(1, 'IN LAVORAZIONE'),
(2, 'IN CONSEGNA'),
(3, 'CONSEGNATO'),
(4, 'ANNULLATO'),
(5, 'SPEDITO');

-- --------------------------------------------------------

--
-- Struttura della tabella `stato_pagamento`
--

CREATE TABLE IF NOT EXISTS `stato_pagamento` (
  `id_stato_pagamento` int(11) NOT NULL,
  `desc_stato_pagamento` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `stato_pagamento`
--

INSERT INTO `stato_pagamento` (`id_stato_pagamento`, `desc_stato_pagamento`) VALUES
(1, 'ACCETTATO'),
(2, 'SOSPESO'),
(3, 'RIFIUTATO'),
(4, 'ANNULLATO');

-- --------------------------------------------------------

--
-- Struttura della tabella `stato_prodotti`
--

CREATE TABLE IF NOT EXISTS `stato_prodotti` (
  `stato_prodotti_id` int(11) NOT NULL,
  `stato_prodotti_desc` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `stato_prodotti`
--

INSERT INTO `stato_prodotti` (`stato_prodotti_id`, `stato_prodotti_desc`) VALUES
(1, 'ATTIVO'),
(2, 'CANCELLATO'),
(3, 'SOSPESO');

-- --------------------------------------------------------

--
-- Struttura della tabella `storico_carrello`
--

CREATE TABLE IF NOT EXISTS `storico_carrello` (
  `id_storico_carrello` int(11) NOT NULL,
  `id_ordine` int(11) DEFAULT NULL,
  `id_variante` int(11) NOT NULL,
  `id_prodotto` int(11) NOT NULL,
  `data_storicizzazione` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `qty` int(11) NOT NULL,
  `taglia` varchar(15) NOT NULL,
  `tipo_prodotto` varchar(250) NOT NULL,
  `codice_prodotto` varchar(100) NOT NULL,
  `codice_variante` varchar(100) NOT NULL,
  `nome` varchar(250) NOT NULL,
  `prezzo` double NOT NULL DEFAULT '0',
  `prezzo_scontato` double NOT NULL DEFAULT '0',
  `url_immagine` varchar(250) NOT NULL,
  `colore_prodotto` varchar(150) DEFAULT NULL,
  `colore_prodotto_codice` varchar(150) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `storico_carrello`
--

INSERT INTO `storico_carrello` (`id_storico_carrello`, `id_ordine`, `id_variante`, `id_prodotto`, `data_storicizzazione`, `qty`, `taglia`, `tipo_prodotto`, `codice_prodotto`, `codice_variante`, `nome`, `prezzo`, `prezzo_scontato`, `url_immagine`, `colore_prodotto`, `colore_prodotto_codice`) VALUES
(4, 8, 298, 58, '2018-03-23 14:58:59', 1, '', 'LABEL_TP_NEW', '', '5a8b2ba795b2d5', 'Lips t-shirt', 15, 0, '5a8b2ba795b2d5_f.png', 'Black', '232227'),
(5, 8, 118, 16, '2018-03-23 15:00:41', 1, '', 'LABEL_TP_NEW', '', '5aa29cee9d08c7', 'Il silenzio', 7.95, 3, '5aa29cee9d08c7_f.png', 'White', '');

-- --------------------------------------------------------

--
-- Struttura della tabella `storico_clienti`
--

CREATE TABLE IF NOT EXISTS `storico_clienti` (
  `id_storico_clienti` int(11) NOT NULL,
  `id_ordine` int(11) NOT NULL,
  `nome` varchar(250) DEFAULT NULL,
  `cognome` varchar(250) DEFAULT NULL,
  `email` varchar(250) NOT NULL,
  `telefono` int(11) DEFAULT NULL,
  `indirizzo_fatturazione` varchar(250) DEFAULT NULL,
  `indirizzo_spedizione` varchar(250) NOT NULL,
  `note_indirizzo_spedizione` varchar(250) NOT NULL,
  `note_indirizzo_fatturazione` varchar(250) NOT NULL,
  `partita_iva` varchar(11) CHARACTER SET latin1 DEFAULT NULL,
  `codice_fiscale` varchar(16) CHARACTER SET latin1 DEFAULT NULL,
  `id_lingua` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `storico_clienti`
--

INSERT INTO `storico_clienti` (`id_storico_clienti`, `id_ordine`, `nome`, `cognome`, `email`, `telefono`, `indirizzo_fatturazione`, `indirizzo_spedizione`, `note_indirizzo_spedizione`, `note_indirizzo_fatturazione`, `partita_iva`, `codice_fiscale`, `id_lingua`) VALUES
(8, 8, 'Francesca', 'Meloni', 'francescamariameloni@gmail.com', 2147483647, 'Olbia | Viale Aldo Moro, 07026 - 07026 Olbia (Italia)', 'Olbia | Viale Aldo Moro, 07026 - 07026 Olbia (Italia)', '', '', '', '', 1);

-- --------------------------------------------------------

--
-- Struttura della tabella `sync_status`
--

CREATE TABLE IF NOT EXISTS `sync_status` (
  `id_sync_status` int(11) NOT NULL,
  `desc_sync_status` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `sync_status`
--

INSERT INTO `sync_status` (`id_sync_status`, `desc_sync_status`) VALUES
(0, 'RIMOSSO'),
(1, 'NUOVO'),
(2, 'AGGIORNATO'),
(3, 'FORZATO');

-- --------------------------------------------------------

--
-- Struttura della tabella `sync_test`
--

CREATE TABLE IF NOT EXISTS `sync_test` (
  `id_sync_test` int(11) NOT NULL,
  `time_sync_test` date NOT NULL,
  `sync_type` varchar(15) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `sync_test`
--

INSERT INTO `sync_test` (`id_sync_test`, `time_sync_test`, `sync_type`) VALUES
(1, '2018-03-13', 'stockupdate');

-- --------------------------------------------------------

--
-- Struttura della tabella `taglie`
--

CREATE TABLE IF NOT EXISTS `taglie` (
  `id_taglia` int(11) NOT NULL,
  `codice` varchar(20) CHARACTER SET latin1 DEFAULT NULL,
  `descrizione` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `separatore` char(1) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `taglie`
--

INSERT INTO `taglie` (`id_taglia`, `codice`, `descrizione`, `separatore`) VALUES
(1, 'XS', 'Extra Small', '-'),
(2, 'S', 'Small', '-'),
(3, 'M', 'Medium', '-'),
(4, 'L', 'Large', '-'),
(5, 'XL', 'Extra Large', '-'),
(6, '2XL', '2 Extra Large', '-'),
(7, 'One Size', 'Taglia unica', '-');

-- --------------------------------------------------------

--
-- Struttura della tabella `tags`
--

CREATE TABLE IF NOT EXISTS `tags` (
  `id_tag` int(11) NOT NULL,
  `nome_tag` varchar(100) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `tags`
--

INSERT INTO `tags` (`id_tag`, `nome_tag`) VALUES
(1, 'Design'),
(2, 'Estate'),
(3, 'Elegante'),
(4, 'Manica corta'),
(5, 'Smanicato');

-- --------------------------------------------------------

--
-- Struttura della tabella `tags_prodotti`
--

CREATE TABLE IF NOT EXISTS `tags_prodotti` (
  `id_tags_prodotti` int(11) NOT NULL,
  `id_tag` int(11) NOT NULL,
  `id_prodotto` int(11) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `tags_prodotti`
--

INSERT INTO `tags_prodotti` (`id_tags_prodotti`, `id_tag`, `id_prodotto`) VALUES
(1, 2, 1),
(2, 3, 1),
(3, 4, 1),
(4, 4, 8),
(5, 4, 105),
(6, 4, 106),
(7, 1, 104),
(8, 1, 103),
(9, 4, 58),
(10, 4, 57),
(11, 4, 56),
(12, 4, 3),
(13, 4, 53),
(14, 4, 52),
(15, 4, 51),
(16, 4, 107),
(17, 4, 108),
(18, 4, 119);

-- --------------------------------------------------------

--
-- Struttura della tabella `tipo_coupon`
--

CREATE TABLE IF NOT EXISTS `tipo_coupon` (
  `id_tipo_coupon` int(11) NOT NULL,
  `desc_tipo_coupon` varchar(250) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `tipo_coupon`
--

INSERT INTO `tipo_coupon` (`id_tipo_coupon`, `desc_tipo_coupon`) VALUES
(1, 'Sconto sul carrello utilizzo singolo'),
(2, 'Sconto % sul carrello utilizzo singolo'),
(3, 'Sconto sul carrello utilizzo multiplo'),
(4, 'Sconto % sul carrello utilizzo multiplo');

-- --------------------------------------------------------

--
-- Struttura della tabella `tipo_pagamento`
--

CREATE TABLE IF NOT EXISTS `tipo_pagamento` (
  `id_tipo_pagamento` int(11) NOT NULL,
  `desc_tipo_pagamento` varchar(100) NOT NULL,
  `icon_tipo_pagamento` varchar(150) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `tipo_pagamento`
--

INSERT INTO `tipo_pagamento` (`id_tipo_pagamento`, `desc_tipo_pagamento`, `icon_tipo_pagamento`) VALUES
(1, 'Paypal', 'fa-paypal'),
(2, 'Stripe', 'fa-cc-stripe');

-- --------------------------------------------------------

--
-- Struttura della tabella `tipo_prodotto`
--

CREATE TABLE IF NOT EXISTS `tipo_prodotto` (
  `id_tipo_prodotto` int(11) NOT NULL,
  `descrizione_tipo_prodotto` varchar(30) NOT NULL,
  `css_class` varchar(20) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `tipo_prodotto`
--

INSERT INTO `tipo_prodotto` (`id_tipo_prodotto`, `descrizione_tipo_prodotto`, `css_class`) VALUES
(1, 'LABEL_TP_SALE', 'text-danger'),
(2, 'LABEL_TP_BESTSELLER', 'text-warning'),
(3, 'LABEL_TP_TOPRATED', 'top-rated'),
(4, 'LABEL_TP_STANDARD', ''),
(5, 'LABEL_TP_NEW', 'text-success');

-- --------------------------------------------------------

--
-- Struttura della tabella `tipo_template`
--

CREATE TABLE IF NOT EXISTS `tipo_template` (
  `id_tipo_template` int(11) NOT NULL,
  `desc_tipo_template` varchar(250) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `tipo_template`
--

INSERT INTO `tipo_template` (`id_tipo_template`, `desc_tipo_template`) VALUES
(1, 'CONTATTO'),
(2, 'NEWSLETTER'),
(3, 'CUSTOM');

-- --------------------------------------------------------

--
-- Struttura della tabella `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(11) unsigned NOT NULL,
  `ip_address` varchar(45) NOT NULL,
  `username` varchar(100) DEFAULT NULL,
  `password` varchar(255) NOT NULL,
  `salt` varchar(255) DEFAULT NULL,
  `email` varchar(100) NOT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `forgotten_password_time` int(11) unsigned DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `created_on` int(11) unsigned NOT NULL,
  `last_login` int(11) unsigned DEFAULT NULL,
  `active` tinyint(1) unsigned DEFAULT NULL,
  `first_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) DEFAULT NULL,
  `company` varchar(100) DEFAULT NULL,
  `phone` varchar(20) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `users`
--

INSERT INTO `users` (`id`, `ip_address`, `username`, `password`, `salt`, `email`, `activation_code`, `forgotten_password_code`, `forgotten_password_time`, `remember_code`, `created_on`, `last_login`, `active`, `first_name`, `last_name`, `company`, `phone`) VALUES
(1, '127.0.0.1', 'roberto.rossi77@gmail.com', '$2y$08$thYAVQ43pc.dHmdNDhQz/evLOaAKHbthgR/dkfc7MIjpEwCcDYdMy', '', 'roberto.rossi77@gmail.com', '', NULL, NULL, NULL, 1268889823, 1522157611, 1, 'Roberto', 'Rossi', 'ADMIN', '4234234'),
(2, '127.0.0.1', 'info@machlo.com', '$2y$08$VYAfiskG1BgPgTUswBIoaejDT6w6OCenxk9duml7x/hP6ArsaAdb2', NULL, 'info@machlo.com', NULL, NULL, NULL, NULL, 1505121761, 1518011392, 1, 'Stefania', 'Laconi', 'machlo', '060606'),
(5, '93.34.88.220', 'maurizio.maui@gmail.com', '$2y$08$IJj7OXsyviRMup4oDOUIIehrno/frnZSVfmRrOJjPC7yiZBHRKuUy', NULL, 'maurizio.maui@gmail.com', NULL, NULL, NULL, NULL, 1507237096, 1507237125, 1, 'Maurizio', 'Custodi', '0', '0'),
(6, '82.49.28.174', 'stef.laconi@gmail.com', '$2y$08$MXI4PCsr9Dv.wCuVHeJ9buNREPmDsbb43gib47zUFTUFTxo0HggbG', NULL, 'stef.laconi@gmail.com', NULL, NULL, NULL, NULL, 1515429629, 1515429675, 1, 'Stefania', 'Laconi', '0', '0'),
(7, '5.168.40.23', 'francescamariameloni@gmail.com', '$2y$08$wCSW8GUmopQVCT2dRGOPdOEB7nua0ds9qO5hH8jtlnReVe8za4h/.', NULL, 'francescamariameloni@gmail.com', NULL, NULL, NULL, NULL, 1515435667, 1515435723, 1, 'Francesca', 'Meloni', '0', '0'),
(8, '37.77.121.200', 'valentina.deriu92@gmail.com', '$2y$08$0EJ5.DgWgsnkSzCQbMXAo.wmCfuAIAu8NQZmA3uGmWlY3z4q/rr9u', NULL, 'valentina.deriu92@gmail.com', NULL, NULL, NULL, NULL, 1515443214, 1515443353, 1, 'Valentina', 'Deriu', '0', '0'),
(10, '5.168.38.193', 'mikelacossu80@gmail.com', '$2y$08$4nWngAhQOPDBD7Mvgqy8GOr9Ad43CUzw/77fsJp95.4PKhqmgpTRm', NULL, 'mikelacossu80@gmail.com', NULL, NULL, NULL, NULL, 1518004680, 1518010882, 1, 'Michela', 'Cossu', '0', '0');

-- --------------------------------------------------------

--
-- Struttura della tabella `users_groups`
--

CREATE TABLE IF NOT EXISTS `users_groups` (
  `id` int(11) unsigned NOT NULL,
  `user_id` int(11) unsigned NOT NULL,
  `group_id` mediumint(8) unsigned NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=16 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `users_groups`
--

INSERT INTO `users_groups` (`id`, `user_id`, `group_id`) VALUES
(1, 1, 1),
(2, 1, 2),
(3, 2, 1),
(6, 5, 2),
(11, 6, 2),
(12, 7, 2),
(13, 8, 2),
(15, 10, 2);

-- --------------------------------------------------------

--
-- Struttura della tabella `variante_taglia`
--

CREATE TABLE IF NOT EXISTS `variante_taglia` (
  `fk_variante` int(11) NOT NULL,
  `fk_taglia` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `variante_taglia`
--

INSERT INTO `variante_taglia` (`fk_variante`, `fk_taglia`) VALUES
(1, 1),
(1, 2),
(1, 3),
(1, 4),
(1, 5),
(2, 2),
(3, 3),
(3, 4),
(9, 2),
(9, 3),
(9, 4),
(9, 5),
(9, 6),
(15, 2),
(15, 3),
(15, 4),
(15, 5),
(15, 6),
(28, 2),
(28, 3),
(28, 4),
(28, 5),
(28, 6),
(39, 2),
(39, 3),
(39, 4),
(39, 5),
(39, 6),
(41, 2),
(41, 3),
(41, 4),
(41, 5),
(41, 6),
(59, 1),
(59, 2),
(59, 3),
(59, 4),
(59, 5),
(62, 2),
(62, 3),
(62, 4),
(62, 5),
(62, 6),
(63, 2),
(63, 3),
(63, 4),
(63, 5),
(63, 6),
(64, 1),
(64, 2),
(64, 3),
(64, 4),
(64, 5),
(64, 6),
(65, 2),
(65, 3),
(65, 4),
(65, 5),
(65, 6),
(66, 2),
(66, 3),
(66, 4),
(66, 5),
(66, 6),
(67, 2),
(67, 3),
(67, 4),
(67, 5),
(67, 6),
(68, 2),
(68, 3),
(68, 4),
(68, 5),
(68, 6),
(69, 2),
(69, 3),
(69, 4),
(69, 5),
(69, 6),
(70, 2),
(70, 3),
(70, 4),
(70, 5),
(70, 6),
(71, 2),
(71, 3),
(71, 4),
(71, 5),
(71, 6),
(72, 2),
(72, 3),
(72, 4),
(72, 5),
(72, 6),
(73, 2),
(73, 3),
(73, 4),
(73, 5),
(73, 6),
(74, 2),
(74, 3),
(74, 4),
(74, 5),
(74, 6),
(75, 2),
(76, 2);

-- --------------------------------------------------------

--
-- Struttura della tabella `varianti_prodotti`
--

CREATE TABLE IF NOT EXISTS `varianti_prodotti` (
  `id_variante` int(11) NOT NULL,
  `codice_prodotto` varchar(100) NOT NULL,
  `nome` varchar(250) NOT NULL,
  `colore` varchar(15) NOT NULL,
  `colore_codice` varchar(15) NOT NULL,
  `taglia` varchar(10) NOT NULL,
  `codice` varchar(100) NOT NULL,
  `prezzo` double NOT NULL,
  `prezzo_scontato` double NOT NULL,
  `url_img_piccola` varchar(250) NOT NULL,
  `url_img_grande` varchar(250) NOT NULL,
  `url_img_grande_retro` varchar(250) NOT NULL,
  `stato` tinyint(1) NOT NULL,
  `in_stock` tinyint(4) NOT NULL,
  `sync_status` int(2) NOT NULL,
  `sync_time` datetime NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=328 DEFAULT CHARSET=utf8;

--
-- Dump dei dati per la tabella `varianti_prodotti`
--

INSERT INTO `varianti_prodotti` (`id_variante`, `codice_prodotto`, `nome`, `colore`, `colore_codice`, `taglia`, `codice`, `prezzo`, `prezzo_scontato`, `url_img_piccola`, `url_img_grande`, `url_img_grande_retro`, `stato`, `in_stock`, `sync_status`, `sync_time`) VALUES
(1, '5aa5a386dd8547', 'Blue cat - Black / S', 'Black', '050505', 'S', '5aa5a386dd8f31', 14.25, 0, '5aa5a386dd8f31.png', '5aa5a386dd8f31_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(2, '5aa5a386dd8547', 'Blue cat - Black / M', 'Black', '050505', 'M', '5aa5a386dd96d4', 14.25, 0, '5aa5a386dd96d4.png', '5aa5a386dd96d4_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(3, '5aa5a386dd8547', 'Blue cat - Black / L', 'Black', '050505', 'L', '5aa5a386dd9e61', 14.25, 0, '5aa5a386dd9e61.png', '5aa5a386dd9e61_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(4, '5aa5a386dd8547', 'Blue cat - Black / XL', 'Black', '050505', 'XL', '5aa5a386dda604', 14.25, 0, '5aa5a386dda604.png', '5aa5a386dda604_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(5, '5aa5a386dd8547', 'Blue cat - White / S', 'White', 'fdfdff', 'S', '5aa5a386ddada7', 14.25, 0, '5aa5a386ddada7.png', '5aa5a386ddada7_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(6, '5aa5a386dd8547', 'Blue cat - White / M', 'White', 'fdfdff', 'M', '5aa5a386ddb535', 14.25, 0, '5aa5a386ddb535.png', '5aa5a386ddb535_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(7, '5aa5a386dd8547', 'Blue cat - White / L', 'White', 'fdfdff', 'L', '5aa5a386ddbcc4', 14.25, 0, '5aa5a386ddbcc4.png', '5aa5a386ddbcc4_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(8, '5aa5a386dd8547', 'Blue cat - White / XL', 'White', 'fdfdff', 'XL', '5aa5a386ddc456', 14.25, 0, '5aa5a386ddc456.png', '5aa5a386ddc456_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(9, '5aa5a2c4598f03', 'Sleeping mask - Black / S', 'Black', '050505', 'S', '5aa5a2c45996e6', 14.25, 0, '5aa5a2c45996e6.png', '5aa5a2c45996e6_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(10, '5aa5a2c4598f03', 'Sleeping mask - Black / M', 'Black', '050505', 'M', '5aa5a2c4599df6', 14.25, 0, '5aa5a2c4599df6.png', '5aa5a2c4599df6_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(11, '5aa5a2c4598f03', 'Sleeping mask - Black / L', 'Black', '050505', 'L', '5aa5a2c459a504', 14.25, 0, '5aa5a2c459a504.png', '5aa5a2c459a504_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(12, '5aa5a2c4598f03', 'Sleeping mask - Black / XL', 'Black', '050505', 'XL', '5aa5a2c459abf3', 14.25, 0, '5aa5a2c459abf3.png', '5aa5a2c459abf3_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(13, '5aa5a2c4598f03', 'Sleeping mask - White / S', 'White', 'fdfdff', 'S', '5aa5a2c459b2f9', 14.25, 0, '5aa5a2c459b2f9.png', '5aa5a2c459b2f9_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(14, '5aa5a2c4598f03', 'Sleeping mask - White / M', 'White', 'fdfdff', 'M', '5aa5a2c459b9e4', 14.25, 0, '5aa5a2c459b9e4.png', '5aa5a2c459b9e4_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(15, '5aa5a2c4598f03', 'Sleeping mask - White / L', 'White', 'fdfdff', 'L', '5aa5a2c459c0d6', 14.25, 0, '5aa5a2c459c0d6.png', '5aa5a2c459c0d6_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(16, '5aa5a2c4598f03', 'Sleeping mask - White / XL', 'White', 'fdfdff', 'XL', '5aa5a2c459c841', 14.25, 0, '5aa5a2c459c841.png', '5aa5a2c459c841_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(17, '5aa599dd2b4a72', 'Colored cats  - White / S', 'White', 'e5e6e1', 'S', '5aa599dd2b5319', 15, 0, '5aa599dd2b5319.png', '5aa599dd2b5319_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(18, '5aa599dd2b4a72', 'Colored cats  - White / M', 'White', 'e5e6e1', 'M', '5aa599dd2b5b87', 15, 0, '5aa599dd2b5b87.png', '5aa599dd2b5b87_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(19, '5aa599dd2b4a72', 'Colored cats  - White / L', 'White', 'e5e6e1', 'L', '5aa599dd2b63b2', 15, 0, '5aa599dd2b63b2.png', '5aa599dd2b63b2_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(20, '5aa599dd2b4a72', 'Colored cats  - White / XL', 'White', 'e5e6e1', 'XL', '5aa599dd2b6bf2', 15, 0, '5aa599dd2b6bf2.png', '5aa599dd2b6bf2_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(21, '5aa599dd2b4a72', 'Colored cats  - White / 2XL', 'White', 'e5e6e1', '2XL', '5aa599dd2b7422', 16.5, 0, '5aa599dd2b7422.png', '5aa599dd2b7422_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(22, '5aa599dd2b4a72', 'Colored cats  - Black / S', 'Black', '232227', 'S', '5aa599dd2b7c52', 15, 0, '5aa599dd2b7c52.png', '5aa599dd2b7c52_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(23, '5aa599dd2b4a72', 'Colored cats  - Black / M', 'Black', '232227', 'M', '5aa599dd2b8436', 15, 0, '5aa599dd2b8436.png', '5aa599dd2b8436_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(24, '5aa599dd2b4a72', 'Colored cats  - Black / L', 'Black', '232227', 'L', '5aa599dd2b8bb7', 15, 0, '5aa599dd2b8bb7.png', '5aa599dd2b8bb7_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(25, '5aa599dd2b4a72', 'Colored cats  - Black / XL', 'Black', '232227', 'XL', '5aa599dd2b92d9', 15, 0, '5aa599dd2b92d9.png', '5aa599dd2b92d9_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(26, '5aa599dd2b4a72', 'Colored cats  - Black / 2XL', 'Black', '232227', '2XL', '5aa599dd2b99e1', 16.5, 0, '5aa599dd2b99e1.png', '5aa599dd2b99e1_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(27, '5aa599dd2b4a72', 'Colored cats  - Heather Dark Grey / S', 'Heather Dark Gr', '262f36', 'S', '5aa599dd2ba0f1', 15, 0, '5aa599dd2ba0f1.png', '5aa599dd2ba0f1_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(28, '5aa599dd2b4a72', 'Colored cats  - Heather Dark Grey / M', 'Heather Dark Gr', '262f36', 'M', '5aa599dd2ba873', 15, 0, '5aa599dd2ba873.png', '5aa599dd2ba873_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(29, '5aa599dd2b4a72', 'Colored cats  - Heather Dark Grey / L', 'Heather Dark Gr', '262f36', 'L', '5aa599dd2bb055', 15, 0, '5aa599dd2bb055.png', '5aa599dd2bb055_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(30, '5aa599dd2b4a72', 'Colored cats  - Heather Dark Grey / XL', 'Heather Dark Gr', '262f36', 'XL', '5aa599dd2bb857', 15, 0, '5aa599dd2bb857.png', '5aa599dd2bb857_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(31, '5aa599dd2b4a72', 'Colored cats  - Heather Dark Grey / 2XL', 'Heather Dark Gr', '262f36', '2XL', '5aa599dd2bc054', 16.5, 0, '5aa599dd2bc054.png', '5aa599dd2bc054_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(32, '5aa599dd2b4a72', 'Colored cats  - Heather Grey / S', 'Heather Grey', '928d92', 'S', '5aa599dd2bc875', 15, 0, '5aa599dd2bc875.png', '5aa599dd2bc875_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(33, '5aa599dd2b4a72', 'Colored cats  - Heather Grey / M', 'Heather Grey', '928d92', 'M', '5aa599dd2bd098', 15, 0, '5aa599dd2bd098.png', '5aa599dd2bd098_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(34, '5aa599dd2b4a72', 'Colored cats  - Heather Grey / L', 'Heather Grey', '928d92', 'L', '5aa599dd2bd8b8', 15, 0, '5aa599dd2bd8b8.png', '5aa599dd2bd8b8_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(35, '5aa599dd2b4a72', 'Colored cats  - Heather Grey / XL', 'Heather Grey', '928d92', 'XL', '5aa599dd2be0d1', 15, 0, '5aa599dd2be0d1.png', '5aa599dd2be0d1_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(36, '5aa599dd2b4a72', 'Colored cats  - Heather Grey / 2XL', 'Heather Grey', '928d92', '2XL', '5aa599dd2be8e3', 16.5, 0, '5aa599dd2be8e3.png', '5aa599dd2be8e3_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(37, '5aa3013293e052', 'Lips t-shirt - White / S', 'White', 'e5e6e1', 'S', '5aa3013293e907', 15, 0, '5aa3013293e907.png', '5aa3013293e907_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(38, '5aa3013293e052', 'Lips t-shirt - White / M', 'White', 'e5e6e1', 'M', '5aa3013293f098', 15, 0, '5aa3013293f098.png', '5aa3013293f098_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(39, '5aa3013293e052', 'Lips t-shirt - White / L', 'White', 'e5e6e1', 'L', '5aa3013293f802', 15, 0, '5aa3013293f802.png', '5aa3013293f802_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(40, '5aa3013293e052', 'Lips t-shirt - White / XL', 'White', 'e5e6e1', 'XL', '5aa3013293ff78', 15, 0, '5aa3013293ff78.png', '5aa3013293ff78_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(41, '5aa3013293e052', 'Lips t-shirt - White / 2XL', 'White', 'e5e6e1', '2XL', '5aa301329406e2', 16.5, 0, '5aa301329406e2.png', '5aa301329406e2_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(42, '5aa3013293e052', 'Lips t-shirt - Black / S', 'Black', '232227', 'S', '5aa30132940e68', 15, 0, '5aa30132940e68.png', '5aa30132940e68_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(43, '5aa3013293e052', 'Lips t-shirt - Black / M', 'Black', '232227', 'M', '5aa301329415d1', 15, 0, '5aa301329415d1.png', '5aa301329415d1_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(44, '5aa3013293e052', 'Lips t-shirt - Black / L', 'Black', '232227', 'L', '5aa30132941d34', 15, 0, '5aa30132941d34.png', '5aa30132941d34_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(45, '5aa3013293e052', 'Lips t-shirt - Black / XL', 'Black', '232227', 'XL', '5aa301329424a7', 15, 0, '5aa301329424a7.png', '5aa301329424a7_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(46, '5aa3013293e052', 'Lips t-shirt - Black / 2XL', 'Black', '232227', '2XL', '5aa30132942c01', 16.5, 0, '5aa30132942c01.png', '5aa30132942c01_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(47, '5aa3013293e052', 'Lips t-shirt - Heather Dark Grey / S', 'Heather Dark Gr', '262f36', 'S', '5aa30132943362', 15, 0, '5aa30132943362.png', '5aa30132943362_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(48, '5aa3013293e052', 'Lips t-shirt - Heather Dark Grey / M', 'Heather Dark Gr', '262f36', 'M', '5aa30132943ac3', 15, 0, '5aa30132943ac3.png', '5aa30132943ac3_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(49, '5aa3013293e052', 'Lips t-shirt - Heather Dark Grey / L', 'Heather Dark Gr', '262f36', 'L', '5aa30132944212', 15, 0, '5aa30132944212.png', '5aa30132944212_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(50, '5aa3013293e052', 'Lips t-shirt - Heather Dark Grey / XL', 'Heather Dark Gr', '262f36', 'XL', '5aa30132944958', 15, 0, '5aa30132944958.png', '5aa30132944958_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(51, '5aa3013293e052', 'Lips t-shirt - Heather Dark Grey / 2XL', 'Heather Dark Gr', '262f36', '2XL', '5aa301329450b4', 16.5, 0, '5aa301329450b4.png', '5aa301329450b4_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(52, '5aa3013293e052', 'Lips t-shirt - Heather Blue / S', 'Heather Blue', '4a5684', 'S', '5aa301329457f2', 15, 0, '5aa301329457f2.png', '5aa301329457f2_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(53, '5aa3013293e052', 'Lips t-shirt - Heather Blue / M', 'Heather Blue', '4a5684', 'M', '5aa30132945f44', 15, 0, '5aa30132945f44.png', '5aa30132945f44_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(54, '5aa3013293e052', 'Lips t-shirt - Heather Blue / L', 'Heather Blue', '4a5684', 'L', '5aa30132946696', 15, 0, '5aa30132946696.png', '5aa30132946696_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(55, '5aa3013293e052', 'Lips t-shirt - Heather Blue / XL', 'Heather Blue', '4a5684', 'XL', '5aa30132946de4', 15, 0, '5aa30132946de4.png', '5aa30132946de4_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(56, '5aa3013293e052', 'Lips t-shirt - Heather Blue / 2XL', 'Heather Blue', '4a5684', '2XL', '5aa30132947536', 16.5, 0, '5aa30132947536.png', '5aa30132947536_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(57, '5aa3013293e052', 'Lips t-shirt - Heather Grey / S', 'Heather Grey', '928d92', 'S', '5aa30132947c83', 15, 0, '5aa30132947c83.png', '5aa30132947c83_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(58, '5aa3013293e052', 'Lips t-shirt - Heather Grey / M', 'Heather Grey', '928d92', 'M', '5aa301329483f1', 15, 0, '5aa301329483f1.png', '5aa301329483f1_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(59, '5aa3013293e052', 'Lips t-shirt - Heather Grey / L', 'Heather Grey', '928d92', 'L', '5aa30132948b54', 15, 0, '5aa30132948b54.png', '5aa30132948b54_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(60, '5aa3013293e052', 'Lips t-shirt - Heather Grey / XL', 'Heather Grey', '928d92', 'XL', '5aa301329492c3', 15, 0, '5aa301329492c3.png', '5aa301329492c3_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(61, '5aa3013293e052', 'Lips t-shirt - Heather Grey / 2XL', 'Heather Grey', '928d92', '2XL', '5aa30132949a35', 16.5, 0, '5aa30132949a35.png', '5aa30132949a35_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(62, '5aa3013293e052', 'Lips t-shirt - Caribbean Blue / S', 'Caribbean Blue', '008fb7', 'S', '5aa3013294a198', 15, 0, '5aa3013294a198.png', '5aa3013294a198_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(63, '5aa3013293e052', 'Lips t-shirt - Caribbean Blue / M', 'Caribbean Blue', '008fb7', 'M', '5aa3013294a8e3', 15, 0, '5aa3013294a8e3.png', '5aa3013294a8e3_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(64, '5aa3013293e052', 'Lips t-shirt - Caribbean Blue / L', 'Caribbean Blue', '008fb7', 'L', '5aa3013294b048', 15, 0, '5aa3013294b048.png', '5aa3013294b048_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(65, '5aa3013293e052', 'Lips t-shirt - Caribbean Blue / XL', 'Caribbean Blue', '008fb7', 'XL', '5aa3013294b7c8', 15, 0, '5aa3013294b7c8.png', '5aa3013294b7c8_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(66, '5aa3013293e052', 'Lips t-shirt - Caribbean Blue / 2XL', 'Caribbean Blue', '008fb7', '2XL', '5aa3013294bf45', 16.5, 0, '5aa3013294bf45.png', '5aa3013294bf45_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(67, '5aa3013293e052', 'Lips t-shirt - Red / S', 'Red', 'd00f3c', 'S', '5aa3013294c6b1', 15, 0, '5aa3013294c6b1.png', '5aa3013294c6b1_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(68, '5aa3013293e052', 'Lips t-shirt - Red / M', 'Red', 'd00f3c', 'M', '5aa3013294ce36', 15, 0, '5aa3013294ce36.png', '5aa3013294ce36_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(69, '5aa3013293e052', 'Lips t-shirt - Red / L', 'Red', 'd00f3c', 'L', '5aa3013294d5b3', 15, 0, '5aa3013294d5b3.png', '5aa3013294d5b3_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(70, '5aa3013293e052', 'Lips t-shirt - Red / XL', 'Red', 'd00f3c', 'XL', '5aa3013294dd22', 15, 0, '5aa3013294dd22.png', '5aa3013294dd22_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(71, '5aa3013293e052', 'Lips t-shirt - Red / 2XL', 'Red', 'd00f3c', '2XL', '5aa3013294e4c5', 16.5, 0, '5aa3013294e4c5.png', '5aa3013294e4c5_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(72, '5aa2fe0e98fe41', 'Flowers - Black / S', 'Black', '050505', 'S', '5aa2fe0e990725', 14.25, 0, '5aa2fe0e990725.png', '5aa2fe0e990725_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(73, '5aa2fe0e98fe41', 'Flowers - Black / M', 'Black', '050505', 'M', '5aa2fe0e990ed6', 14.25, 0, '5aa2fe0e990ed6.png', '5aa2fe0e990ed6_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(74, '5aa2fe0e98fe41', 'Flowers - Black / L', 'Black', '050505', 'L', '5aa2fe0e991669', 14.25, 0, '5aa2fe0e991669.png', '5aa2fe0e991669_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(75, '5aa2fe0e98fe41', 'Flowers - Black / XL', 'Black', '050505', 'XL', '5aa2fe0e991de9', 14.25, 0, '5aa2fe0e991de9.png', '5aa2fe0e991de9_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(76, '5aa2fe0e98fe41', 'Flowers - White / S', 'White', 'fdfdff', 'S', '5aa2fe0e992565', 14.25, 0, '5aa2fe0e992565.png', '5aa2fe0e992565_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(77, '5aa2fe0e98fe41', 'Flowers - White / M', 'White', 'fdfdff', 'M', '5aa2fe0e992ce5', 14.25, 0, '5aa2fe0e992ce5.png', '5aa2fe0e992ce5_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(78, '5aa2fe0e98fe41', 'Flowers - White / L', 'White', 'fdfdff', 'L', '5aa2fe0e993445', 14.25, 0, '5aa2fe0e993445.png', '5aa2fe0e993445_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(79, '5aa2fe0e98fe41', 'Flowers - White / XL', 'White', 'fdfdff', 'XL', '5aa2fe0e993bc8', 14.25, 0, '5aa2fe0e993bc8.png', '5aa2fe0e993bc8_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(80, '5aa2fb8b66a114', 'Masks - Black / S', 'Black', '050505', 'S', '5aa2fb8b66aa28', 14.25, 0, '5aa2fb8b66aa28.png', '5aa2fb8b66aa28_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(81, '5aa2fb8b66a114', 'Masks - Black / M', 'Black', '050505', 'M', '5aa2fb8b66b257', 14.25, 0, '5aa2fb8b66b257.png', '5aa2fb8b66b257_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(82, '5aa2fb8b66a114', 'Masks - Black / L', 'Black', '050505', 'L', '5aa2fb8b66ba53', 14.25, 0, '5aa2fb8b66ba53.png', '5aa2fb8b66ba53_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(83, '5aa2fb8b66a114', 'Masks - Black / XL', 'Black', '050505', 'XL', '5aa2fb8b66c259', 14.25, 0, '5aa2fb8b66c259.png', '5aa2fb8b66c259_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(84, '5aa2fb8b66a114', 'Masks - White / S', 'White', 'fdfdff', 'S', '5aa2fb8b66ca43', 14.25, 0, '5aa2fb8b66ca43.png', '5aa2fb8b66ca43_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(85, '5aa2fb8b66a114', 'Masks - White / M', 'White', 'fdfdff', 'M', '5aa2fb8b66d237', 14.25, 0, '5aa2fb8b66d237.png', '5aa2fb8b66d237_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(86, '5aa2fb8b66a114', 'Masks - White / L', 'White', 'fdfdff', 'L', '5aa2fb8b66d9f5', 14.25, 0, '5aa2fb8b66d9f5.png', '5aa2fb8b66d9f5_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(87, '5aa2fb8b66a114', 'Masks - White / XL', 'White', 'fdfdff', 'XL', '5aa2fb8b66e1c9', 14.25, 0, '5aa2fb8b66e1c9.png', '5aa2fb8b66e1c9_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(88, '5aa2f943e9f697', 'Yellow cat - White / S', 'White', 'e5e6e1', 'S', '5aa2f943e9ffa8', 15, 5, '5aa2f943e9ffa8.png', '5aa2f943e9ffa8_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(89, '5aa2f943e9f697', 'Yellow cat - White / M', 'White', 'e5e6e1', 'M', '5aa2f943ea07a7', 15, 0, '5aa2f943ea07a7.png', '5aa2f943ea07a7_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(90, '5aa2f943e9f697', 'Yellow cat - White / L', 'White', 'e5e6e1', 'L', '5aa2f943ea0fd8', 15, 0, '5aa2f943ea0fd8.png', '5aa2f943ea0fd8_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(91, '5aa2f943e9f697', 'Yellow cat - White / XL', 'White', 'e5e6e1', 'XL', '5aa2f943ea1822', 15, 0, '5aa2f943ea1822.png', '5aa2f943ea1822_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(92, '5aa2f943e9f697', 'Yellow cat - White / 2XL', 'White', 'e5e6e1', '2XL', '5aa2f943ea2041', 16.5, 0, '5aa2f943ea2041.png', '5aa2f943ea2041_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(93, '5aa2f943e9f697', 'Yellow cat - Black / S', 'Black', '232227', 'S', '5aa2f943ea2826', 15, 0, '5aa2f943ea2826.png', '5aa2f943ea2826_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(94, '5aa2f943e9f697', 'Yellow cat - Black / M', 'Black', '232227', 'M', '5aa2f943ea3033', 15, 0, '5aa2f943ea3033.png', '5aa2f943ea3033_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(95, '5aa2f943e9f697', 'Yellow cat - Black / L', 'Black', '232227', 'L', '5aa2f943ea3855', 15, 0, '5aa2f943ea3855.png', '5aa2f943ea3855_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(96, '5aa2f943e9f697', 'Yellow cat - Black / XL', 'Black', '232227', 'XL', '5aa2f943ea4088', 15, 0, '5aa2f943ea4088.png', '5aa2f943ea4088_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(97, '5aa2f943e9f697', 'Yellow cat - Black / 2XL', 'Black', '232227', '2XL', '5aa2f943ea48d3', 16.5, 0, '5aa2f943ea48d3.png', '5aa2f943ea48d3_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(98, '5aa2f943e9f697', 'Yellow cat - Heather Dark Grey / S', 'Heather Dark Gr', '262f36', 'S', '5aa2f943ea5112', 15, 0, '5aa2f943ea5112.png', '5aa2f943ea5112_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(99, '5aa2f943e9f697', 'Yellow cat - Heather Dark Grey / M', 'Heather Dark Gr', '262f36', 'M', '5aa2f943ea5978', 15, 0, '5aa2f943ea5978.png', '5aa2f943ea5978_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(100, '5aa2f943e9f697', 'Yellow cat - Heather Dark Grey / L', 'Heather Dark Gr', '262f36', 'L', '5aa2f943ea6195', 15, 0, '5aa2f943ea6195.png', '5aa2f943ea6195_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(101, '5aa2f943e9f697', 'Yellow cat - Heather Dark Grey / XL', 'Heather Dark Gr', '262f36', 'XL', '5aa2f943ea69a9', 15, 0, '5aa2f943ea69a9.png', '5aa2f943ea69a9_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(102, '5aa2f943e9f697', 'Yellow cat - Heather Dark Grey / 2XL', 'Heather Dark Gr', '262f36', '2XL', '5aa2f943ea71c1', 16.5, 0, '5aa2f943ea71c1.png', '5aa2f943ea71c1_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(103, '5aa2f81d3552c7', 'Gallinella sarda - Black / S', 'Black', '050505', 'S', '5aa2f81d355ad6', 14.25, 0, '5aa2f81d355ad6.png', '5aa2f81d355ad6_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(104, '5aa2f81d3552c7', 'Gallinella sarda - Black / M', 'Black', '050505', 'M', '5aa2f81d356202', 14.25, 0, '5aa2f81d356202.png', '5aa2f81d356202_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(105, '5aa2f81d3552c7', 'Gallinella sarda - Black / L', 'Black', '050505', 'L', '5aa2f81d356937', 14.25, 0, '5aa2f81d356937.png', '5aa2f81d356937_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(106, '5aa2f81d3552c7', 'Gallinella sarda - Black / XL', 'Black', '050505', 'XL', '5aa2f81d357062', 14.25, 0, '5aa2f81d357062.png', '5aa2f81d357062_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(107, '5aa2f81d3552c7', 'Gallinella sarda - White / S', 'White', 'fdfdff', 'S', '5aa2f81d357781', 14.25, 0, '5aa2f81d357781.png', '5aa2f81d357781_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(108, '5aa2f81d3552c7', 'Gallinella sarda - White / M', 'White', 'fdfdff', 'M', '5aa2f81d357ea1', 14.25, 0, '5aa2f81d357ea1.png', '5aa2f81d357ea1_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(109, '5aa2f81d3552c7', 'Gallinella sarda - White / L', 'White', 'fdfdff', 'L', '5aa2f81d3585b2', 14.25, 0, '5aa2f81d3585b2.png', '5aa2f81d3585b2_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(110, '5aa2f81d3552c7', 'Gallinella sarda - White / XL', 'White', 'fdfdff', 'XL', '5aa2f81d358cc9', 14.25, 0, '5aa2f81d358cc9.png', '5aa2f81d358cc9_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(111, '5aa2a4314a7da4', 'Yellow cat', 'White', '', '11oz', '5aa2a4314a85e1', 7.95, 0, '5aa2a4314a85e1.png', '5aa2a4314a85e1_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(112, '5aa2a3db357648', 'Yang', 'White', '', '11oz', '5aa2a3db357f84', 7.95, 0, '5aa2a3db357f84.png', '5aa2a3db357f84_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(113, '5aa2a35b45f316', 'Universe', 'White', '', '11oz', '5aa2a35b45fc52', 7.95, 0, '5aa2a35b45fc52.png', '5aa2a35b45fc52_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(114, '5aa2a2dc902565', 'Standby', 'White', '', '11oz', '5aa2a2dc903193', 7.95, 0, '5aa2a2dc903193.png', '5aa2a2dc903193_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(115, '5aa2a2244efd56', 'Ma Chlò', 'White', '', '11oz', '5aa2a2244f0586', 7.95, 0, '5aa2a2244f0586.png', '5aa2a2244f0586_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(116, '5aa29e4923eeb6', 'La separazione', 'White', '', '11oz', '5aa29e4923f7c6', 7.95, 0, '5aa29e4923f7c6.png', '5aa29e4923f7c6_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(117, '5aa29dff07e579', 'Japan', 'White', '', '11oz', '5aa29dff07ee65', 7.95, 0, '5aa29dff07ee65.png', '5aa29dff07ee65_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(118, '5aa29cee9d00e1', 'Il silenzio', 'White', '', '11oz', '5aa29cee9d08c7', 7.95, 0, '5aa29cee9d08c7.png', '5aa29cee9d08c7_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(119, '5aa29cb78a18b1', 'Un nuovo inizio', 'White', '', '11oz', '5aa29cb78a1f97', 7.95, 0, '5aa29cb78a1f97.png', '5aa29cb78a1f97_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(120, '5aa29c681a45f2', 'Il bacio', 'White', '', '11oz', '5aa29c681a4e66', 7.95, 0, '5aa29c681a4e66.png', '5aa29c681a4e66_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(121, '5aa29c1e763eb7', 'Il pensiero', 'White', '', '11oz', '5aa29c1e764934', 7.95, 0, '5aa29c1e764934.png', '5aa29c1e764934_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(122, '5aa29bd9d57cd5', 'Jellyfish', 'White', '', '11oz', '5aa29bd9d584c6', 7.95, 0, '5aa29bd9d584c6.png', '5aa29bd9d584c6_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(123, '5aa29b851d86a1', 'Color tree', 'White', '', '11oz', '5aa29b851d8f67', 7.95, 0, '5aa29b851d8f67.png', '5aa29b851d8f67_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(124, '5aa29b26787987', 'Colorful town', 'White', '', '11oz', '5aa29b26788174', 7.95, 0, '5aa29b26788174.png', '5aa29b26788174_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(125, '5aa29ab9c856c2', 'A monkey playing with a snake', 'White', '', '11oz', '5aa29ab9c86045', 7.95, 0, '5aa29ab9c86045.png', '5aa29ab9c86045_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(126, '5aa29a3e69b2c4', 'Feel good', 'White', '', '11oz', '5aa29a3e69bad6', 7.95, 0, '5aa29a3e69bad6.png', '5aa29a3e69bad6_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(127, '5aa299f1a4ad82', 'Talking', 'White', '', '11oz', '5aa299f1a4b653', 7.95, 0, '5aa299f1a4b653.png', '5aa299f1a4b653_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(128, '5aa298b3ec6ad5', 'Japan - XS', 'White', 'ffffff', 'XS', '5aa298b3ec73a4', 36.95, 0, '5aa298b3ec73a4.png', '5aa298b3ec73a4_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(129, '5aa298b3ec6ad5', 'Japan - S', 'White', 'ffffff', 'S', '5aa298b3ec7b32', 36.95, 0, '5aa298b3ec7b32.png', '5aa298b3ec7b32_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(130, '5aa298b3ec6ad5', 'Japan - M', 'White', 'ffffff', 'M', '5aa298b3ec82a3', 36.95, 0, '5aa298b3ec82a3.png', '5aa298b3ec82a3_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(131, '5aa298b3ec6ad5', 'Japan - L', 'White', 'ffffff', 'L', '5aa298b3ec8a01', 36.95, 0, '5aa298b3ec8a01.png', '5aa298b3ec8a01_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(132, '5aa298b3ec6ad5', 'Japan - XL', 'White', 'ffffff', 'XL', '5aa298b3ec9183', 36.95, 0, '5aa298b3ec9183.png', '5aa298b3ec9183_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(133, '5aa297d0cfdd44', 'Eclipse - XS', 'White', 'ffffff', 'XS', '5aa297d0cfe558', 36.95, 0, '5aa297d0cfe558.png', '5aa297d0cfe558_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(134, '5aa297d0cfdd44', 'Eclipse - S', 'White', 'ffffff', 'S', '5aa297d0cfec74', 36.95, 0, '5aa297d0cfec74.png', '5aa297d0cfec74_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(135, '5aa297d0cfdd44', 'Eclipse - M', 'White', 'ffffff', 'M', '5aa297d0cff377', 36.95, 0, '5aa297d0cff377.png', '5aa297d0cff377_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(136, '5aa297d0cfdd44', 'Eclipse - L', 'White', 'ffffff', 'L', '5aa297d0cffa85', 36.95, 0, '5aa297d0cffa85.png', '5aa297d0cffa85_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(137, '5aa297d0cfdd44', 'Eclipse - XL', 'White', 'ffffff', 'XL', '5aa297d0d00173', 36.95, 0, '5aa297d0d00173.png', '5aa297d0d00173_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(138, '5aa27b23a9f4a8', 'Yang - XS', 'White', 'ffffff', 'XS', '5aa27b23a9fca1', 36.95, 0, '5aa27b23a9fca1.png', '5aa27b23a9fca1_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(139, '5aa27b23a9f4a8', 'Yang - S', 'White', 'ffffff', 'S', '5aa27b23aa03d7', 36.95, 0, '5aa27b23aa03d7.png', '5aa27b23aa03d7_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(140, '5aa27b23a9f4a8', 'Yang - M', 'White', 'ffffff', 'M', '5aa27b23aa0b05', 36.95, 0, '5aa27b23aa0b05.png', '5aa27b23aa0b05_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(141, '5aa27b23a9f4a8', 'Yang - L', 'White', 'ffffff', 'L', '5aa27b23aa1215', 36.95, 0, '5aa27b23aa1215.png', '5aa27b23aa1215_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(142, '5aa27b23a9f4a8', 'Yang - XL', 'White', 'ffffff', 'XL', '5aa27b23aa1932', 36.95, 0, '5aa27b23aa1932.png', '5aa27b23aa1932_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(143, '5aa27a84d1e238', 'Standby - XS', 'White', 'ffffff', 'XS', '5aa27a84d1ea75', 36.95, 0, '5aa27a84d1ea75.png', '5aa27a84d1ea75_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(144, '5aa27a84d1e238', 'Standby - S', 'White', 'ffffff', 'S', '5aa27a84d1f193', 36.95, 0, '5aa27a84d1f193.png', '5aa27a84d1f193_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(145, '5aa27a84d1e238', 'Standby - M', 'White', 'ffffff', 'M', '5aa27a84d1f892', 36.95, 0, '5aa27a84d1f892.png', '5aa27a84d1f892_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(146, '5aa27a84d1e238', 'Standby - L', 'White', 'ffffff', 'L', '5aa27a84d1ff84', 36.95, 0, '5aa27a84d1ff84.png', '5aa27a84d1ff84_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(147, '5aa27a84d1e238', 'Standby - XL', 'White', 'ffffff', 'XL', '5aa27a84d20686', 36.95, 0, '5aa27a84d20686.png', '5aa27a84d20686_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(148, '5aa27a09f005c3', 'Universe - XS', 'White', 'ffffff', 'XS', '5aa27a09f00db6', 36.95, 0, '5aa27a09f00db6.png', '5aa27a09f00db6_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(149, '5aa27a09f005c3', 'Universe - S', 'White', 'ffffff', 'S', '5aa27a09f014e8', 36.95, 0, '5aa27a09f014e8.png', '5aa27a09f014e8_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(150, '5aa27a09f005c3', 'Universe - M', 'White', 'ffffff', 'M', '5aa27a09f01bf8', 36.95, 0, '5aa27a09f01bf8.png', '5aa27a09f01bf8_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(151, '5aa27a09f005c3', 'Universe - L', 'White', 'ffffff', 'L', '5aa27a09f022f1', 36.95, 0, '5aa27a09f022f1.png', '5aa27a09f022f1_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(152, '5aa27a09f005c3', 'Universe - XL', 'White', 'ffffff', 'XL', '5aa27a09f029f5', 36.95, 0, '5aa27a09f029f5.png', '5aa27a09f029f5_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(153, '5aa27975242e84', 'Colored cats - XS', 'White', 'ffffff', 'XS', '5aa27975243692', 36.95, 0, '5aa27975243692.png', '5aa27975243692_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(154, '5aa27975242e84', 'Colored cats - S', 'White', 'ffffff', 'S', '5aa27975243cf6', 36.95, 0, '5aa27975243cf6.png', '5aa27975243cf6_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(155, '5aa27975242e84', 'Colored cats - M', 'White', 'ffffff', 'M', '5aa27975244473', 36.95, 0, '5aa27975244473.png', '5aa27975244473_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(156, '5aa27975242e84', 'Colored cats - L', 'White', 'ffffff', 'L', '5aa27975244b92', 36.95, 0, '5aa27975244b92.png', '5aa27975244b92_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(157, '5aa27975242e84', 'Colored cats - XL', 'White', 'ffffff', 'XL', '5aa279752452b5', 36.95, 0, '5aa279752452b5.png', '5aa279752452b5_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(158, '5aa275790bd952', 'Geometric - XS', 'White', 'ffffff', 'XS', '5aa275790be221', 36.95, 0, '5aa275790be221.png', '5aa275790be221_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(159, '5aa275790bd952', 'Geometric - S', 'White', 'ffffff', 'S', '5aa275790bea14', 36.95, 0, '5aa275790bea14.png', '5aa275790bea14_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(160, '5aa275790bd952', 'Geometric - M', 'White', 'ffffff', 'M', '5aa275790bf1e4', 36.95, 0, '5aa275790bf1e4.png', '5aa275790bf1e4_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(161, '5aa275790bd952', 'Geometric - L', 'White', 'ffffff', 'L', '5aa275790bf9a4', 36.95, 0, '5aa275790bf9a4.png', '5aa275790bf9a4_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(162, '5aa275790bd952', 'Geometric - XL', 'White', 'ffffff', 'XL', '5aa275790c0193', 36.95, 0, '5aa275790c0193.png', '5aa275790c0193_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(163, '5aa010e062b998', 'Il pensiero - XS', 'White', 'ffffff', 'XS', '5aa010e062c288', 34.95, 0, '5aa010e062c288.png', '5aa010e062c288_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(164, '5aa010e062b998', 'Il pensiero - S', 'White', 'ffffff', 'S', '5aa010e062ca38', 34.95, 0, '5aa010e062ca38.png', '5aa010e062ca38_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(165, '5aa010e062b998', 'Il pensiero - M', 'White', 'ffffff', 'M', '5aa010e062d1c4', 34.95, 0, '5aa010e062d1c4.png', '5aa010e062d1c4_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(166, '5aa010e062b998', 'Il pensiero - L', 'White', 'ffffff', 'L', '5aa010e062d942', 34.95, 0, '5aa010e062d942.png', '5aa010e062d942_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(167, '5aa010e062b998', 'Il pensiero - XL', 'White', 'ffffff', 'XL', '5aa010e062e0c3', 34.95, 0, '5aa010e062e0c3.png', '5aa010e062e0c3_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(168, '5aa00f5f1c42e5', 'Jungle flowers dress black - XS', 'White', 'ffffff', 'XS', '5aa00f5f1c4b17', 34.95, 0, '5aa00f5f1c4b17.png', '5aa00f5f1c4b17_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(169, '5aa00f5f1c42e5', 'Jungle flowers dress black - S', 'White', 'ffffff', 'S', '5aa00f5f1c5258', 34.95, 0, '5aa00f5f1c5258.png', '5aa00f5f1c5258_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(170, '5aa00f5f1c42e5', 'Jungle flowers dress black - M', 'White', 'ffffff', 'M', '5aa00f5f1c5981', 34.95, 0, '5aa00f5f1c5981.png', '5aa00f5f1c5981_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(171, '5aa00f5f1c42e5', 'Jungle flowers dress black - L', 'White', 'ffffff', 'L', '5aa00f5f1c6091', 34.95, 0, '5aa00f5f1c6091.png', '5aa00f5f1c6091_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(172, '5aa00f5f1c42e5', 'Jungle flowers dress black - XL', 'White', 'ffffff', 'XL', '5aa00f5f1c67a6', 34.95, 0, '5aa00f5f1c67a6.png', '5aa00f5f1c67a6_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(173, '5aa00eb9a04005', 'Jungle flowers dress - XS', 'White', 'ffffff', 'XS', '5aa00eb9a04839', 34.95, 0, '5aa00eb9a04839.png', '5aa00eb9a04839_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(174, '5aa00eb9a04005', 'Jungle flowers dress - S', 'White', 'ffffff', 'S', '5aa00eb9a04f68', 34.95, 0, '5aa00eb9a04f68.png', '5aa00eb9a04f68_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(175, '5aa00eb9a04005', 'Jungle flowers dress - M', 'White', 'ffffff', 'M', '5aa00eb9a05671', 34.95, 0, '5aa00eb9a05671.png', '5aa00eb9a05671_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(176, '5aa00eb9a04005', 'Jungle flowers dress - L', 'White', 'ffffff', 'L', '5aa00eb9a05d84', 34.95, 0, '5aa00eb9a05d84.png', '5aa00eb9a05d84_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(177, '5aa00eb9a04005', 'Jungle flowers dress - XL', 'White', 'ffffff', 'XL', '5aa00eb9a06499', 34.95, 0, '5aa00eb9a06499.png', '5aa00eb9a06499_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(178, '5aa00df8cceb58', 'La separazione - XS', 'White', 'ffffff', 'XS', '5aa00df8ccf352', 34.95, 0, '5aa00df8ccf352.png', '5aa00df8ccf352_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(179, '5aa00df8cceb58', 'La separazione - S', 'White', 'ffffff', 'S', '5aa00df8ccfa88', 34.95, 0, '5aa00df8ccfa88.png', '5aa00df8ccfa88_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(180, '5aa00df8cceb58', 'La separazione - M', 'White', 'ffffff', 'M', '5aa00df8cd01a4', 34.95, 0, '5aa00df8cd01a4.png', '5aa00df8cd01a4_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(181, '5aa00df8cceb58', 'La separazione - L', 'White', 'ffffff', 'L', '5aa00df8cd08b1', 34.95, 0, '5aa00df8cd08b1.png', '5aa00df8cd08b1_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(182, '5aa00df8cceb58', 'La separazione - XL', 'White', 'ffffff', 'XL', '5aa00df8cd0fc9', 34.95, 0, '5aa00df8cd0fc9.png', '5aa00df8cd0fc9_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(183, '5aa00db6bb9d33', 'Life of a tree - XS', 'White', 'ffffff', 'XS', '5aa00db6bba5c1', 34.95, 0, '5aa00db6bba5c1.png', '5aa00db6bba5c1_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(184, '5aa00db6bb9d33', 'Life of a tree - S', 'White', 'ffffff', 'S', '5aa00db6bbad24', 34.95, 0, '5aa00db6bbad24.png', '5aa00db6bbad24_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(185, '5aa00db6bb9d33', 'Life of a tree - M', 'White', 'ffffff', 'M', '5aa00db6bbb435', 34.95, 0, '5aa00db6bbb435.png', '5aa00db6bbb435_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(186, '5aa00db6bb9d33', 'Life of a tree - L', 'White', 'ffffff', 'L', '5aa00db6bbbb41', 34.95, 0, '5aa00db6bbbb41.png', '5aa00db6bbbb41_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(187, '5aa00db6bb9d33', 'Life of a tree - XL', 'White', 'ffffff', 'XL', '5aa00db6bbc254', 34.95, 0, '5aa00db6bbc254.png', '5aa00db6bbc254_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(188, '5aa00c3e2e6184', 'Lips dress - XS', 'White', 'ffffff', 'XS', '5aa00c3e2e6a34', 34.95, 0, '5aa00c3e2e6a34.png', '5aa00c3e2e6a34_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(189, '5aa00c3e2e6184', 'Lips dress - S', 'White', 'ffffff', 'S', '5aa00c3e2e71c5', 34.95, 0, '5aa00c3e2e71c5.png', '5aa00c3e2e71c5_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(190, '5aa00c3e2e6184', 'Lips dress - M', 'White', 'ffffff', 'M', '5aa00c3e2e7937', 34.95, 0, '5aa00c3e2e7937.png', '5aa00c3e2e7937_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(191, '5aa00c3e2e6184', 'Lips dress - L', 'White', 'ffffff', 'L', '5aa00c3e2e80b4', 34.95, 0, '5aa00c3e2e80b4.png', '5aa00c3e2e80b4_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(192, '5aa00c3e2e6184', 'Lips dress - XL', 'White', 'ffffff', 'XL', '5aa00c3e2e8853', 34.95, 0, '5aa00c3e2e8853.png', '5aa00c3e2e8853_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(193, '5aa00bc3dc48f8', 'Monkeys into the jungle - XS', 'White', 'ffffff', 'XS', '5aa00bc3dc51d5', 34.95, 0, '5aa00bc3dc51d5.png', '5aa00bc3dc51d5_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(194, '5aa00bc3dc48f8', 'Monkeys into the jungle - S', 'White', 'ffffff', 'S', '5aa00bc3dc59a7', 34.95, 0, '5aa00bc3dc59a7.png', '5aa00bc3dc59a7_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(195, '5aa00bc3dc48f8', 'Monkeys into the jungle - M', 'White', 'ffffff', 'M', '5aa00bc3dc6206', 34.95, 0, '5aa00bc3dc6206.png', '5aa00bc3dc6206_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(196, '5aa00bc3dc48f8', 'Monkeys into the jungle - L', 'White', 'ffffff', 'L', '5aa00bc3dc6838', 34.95, 0, '5aa00bc3dc6838.png', '5aa00bc3dc6838_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(197, '5aa00bc3dc48f8', 'Monkeys into the jungle - XL', 'White', 'ffffff', 'XL', '5aa00bc3dc6fe1', 34.95, 0, '5aa00bc3dc6fe1.png', '5aa00bc3dc6fe1_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(198, '5aa00b9b1fc403', 'Reaching for the moon - XS', 'White', 'ffffff', 'XS', '5aa00b9b1fccd2', 34.95, 0, '5aa00b9b1fccd2.png', '5aa00b9b1fccd2_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(199, '5aa00b9b1fc403', 'Reaching for the moon - S', 'White', 'ffffff', 'S', '5aa00b9b1fd4b1', 34.95, 0, '5aa00b9b1fd4b1.png', '5aa00b9b1fd4b1_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(200, '5aa00b9b1fc403', 'Reaching for the moon - M', 'White', 'ffffff', 'M', '5aa00b9b1fdc61', 34.95, 0, '5aa00b9b1fdc61.png', '5aa00b9b1fdc61_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(201, '5aa00b9b1fc403', 'Reaching for the moon - L', 'White', 'ffffff', 'L', '5aa00b9b1fe416', 34.95, 0, '5aa00b9b1fe416.png', '5aa00b9b1fe416_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(202, '5aa00b9b1fc403', 'Reaching for the moon - XL', 'White', 'ffffff', 'XL', '5aa00b9b1fed33', 34.95, 0, '5aa00b9b1fed33.png', '5aa00b9b1fed33_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(203, '5aa009916da013', 'Un nuovo inizio - XS', 'White', 'ffffff', 'XS', '5aa009916da824', 34.95, 0, '5aa009916da824.png', '5aa009916da824_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(204, '5aa009916da013', 'Un nuovo inizio - S', 'White', 'ffffff', 'S', '5aa009916daf57', 34.95, 0, '5aa009916daf57.png', '5aa009916daf57_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(205, '5aa009916da013', 'Un nuovo inizio - M', 'White', 'ffffff', 'M', '5aa009916db669', 34.95, 0, '5aa009916db669.png', '5aa009916db669_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(206, '5aa009916da013', 'Un nuovo inizio - L', 'White', 'ffffff', 'L', '5aa009916dbd75', 34.95, 0, '5aa009916dbd75.png', '5aa009916dbd75_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(207, '5aa009916da013', 'Un nuovo inizio - XL', 'White', 'ffffff', 'XL', '5aa009916dc482', 34.95, 0, '5aa009916dc482.png', '5aa009916dc482_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(208, '5aa0058e449ec3', 'Woman''s lips - XS', 'White', 'ffffff', 'XS', '5aa0058e44a6c2', 34.95, 0, '5aa0058e44a6c2.png', '5aa0058e44a6c2_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(209, '5aa0058e449ec3', 'Woman''s lips - S', 'White', 'ffffff', 'S', '5aa0058e44ae05', 34.95, 0, '5aa0058e44ae05.png', '5aa0058e44ae05_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(210, '5aa0058e449ec3', 'Woman''s lips - M', 'White', 'ffffff', 'M', '5aa0058e44b513', 34.95, 0, '5aa0058e44b513.png', '5aa0058e44b513_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(211, '5aa0058e449ec3', 'Woman''s lips - L', 'White', 'ffffff', 'L', '5aa0058e44bc27', 34.95, 0, '5aa0058e44bc27.png', '5aa0058e44bc27_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(212, '5aa0058e449ec3', 'Woman''s lips - XL', 'White', 'ffffff', 'XL', '5aa0058e44c331', 34.95, 0, '5aa0058e44c331.png', '5aa0058e44c331_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(213, '5aa00071b87794', 'Wild girl - XS', 'White', 'ffffff', 'XS', '5aa00071b87f97', 34.95, 0, '5aa00071b87f97.png', '5aa00071b87f97_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(214, '5aa00071b87794', 'Wild girl - S', 'White', 'ffffff', 'S', '5aa00071b886d1', 34.95, 0, '5aa00071b886d1.png', '5aa00071b886d1_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(215, '5aa00071b87794', 'Wild girl - M', 'White', 'ffffff', 'M', '5aa00071b88e02', 34.95, 0, '5aa00071b88e02.png', '5aa00071b88e02_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(216, '5aa00071b87794', 'Wild girl - L', 'White', 'ffffff', 'L', '5aa00071b89516', 34.95, 0, '5aa00071b89516.png', '5aa00071b89516_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(217, '5aa00071b87794', 'Wild girl - XL', 'White', 'ffffff', 'XL', '5aa00071b89c38', 34.95, 0, '5aa00071b89c38.png', '5aa00071b89c38_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(218, '5aa0001c6a2461', 'Snakes dress - XS', 'White', 'ffffff', 'XS', '5aa0001c6a2cc6', 34.95, 0, '5aa0001c6a2cc6.png', '5aa0001c6a2cc6_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(219, '5aa0001c6a2461', 'Snakes dress - S', 'White', 'ffffff', 'S', '5aa0001c6a3457', 34.95, 0, '5aa0001c6a3457.png', '5aa0001c6a3457_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(220, '5aa0001c6a2461', 'Snakes dress - M', 'White', 'ffffff', 'M', '5aa0001c6a3b96', 34.95, 0, '5aa0001c6a3b96.png', '5aa0001c6a3b96_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(221, '5aa0001c6a2461', 'Snakes dress - L', 'White', 'ffffff', 'L', '5aa0001c6a42b2', 34.95, 0, '5aa0001c6a42b2.png', '5aa0001c6a42b2_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(222, '5aa0001c6a2461', 'Snakes dress - XL', 'White', 'ffffff', 'XL', '5aa0001c6a49c1', 34.95, 0, '5aa0001c6a49c1.png', '5aa0001c6a49c1_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(223, '5a9fffa5692b77', 'Colorful town - XS', 'White', 'ffffff', 'XS', '5a9fffa56934f4', 34.95, 0, '5a9fffa56934f4.png', '5a9fffa56934f4_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(224, '5a9fffa5692b77', 'Colorful town - S', 'White', 'ffffff', 'S', '5a9fffa5693c79', 34.95, 0, '5a9fffa5693c79.png', '5a9fffa5693c79_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(225, '5a9fffa5692b77', 'Colorful town - M', 'White', 'ffffff', 'M', '5a9fffa5694389', 34.95, 0, '5a9fffa5694389.png', '5a9fffa5694389_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(226, '5a9fffa5692b77', 'Colorful town - L', 'White', 'ffffff', 'L', '5a9fffa5694a81', 34.95, 0, '5a9fffa5694a81.png', '5a9fffa5694a81_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(227, '5a9fffa5692b77', 'Colorful town - XL', 'White', 'ffffff', 'XL', '5a9fffa5695178', 34.95, 0, '5a9fffa5695178.png', '5a9fffa5695178_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(228, '5a9e7ddd14c8c6', 'Gallinella sarda t-shirt - S', 'Black', '2F2E33', 'S', '5a9e7ddd14cf07', 21.95, 0, '5a9e7ddd14cf07.png', '5a9e7ddd14cf07_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(229, '5a9e7ddd14c8c6', 'Gallinella sarda t-shirt - M', 'Black', '2F2E33', 'M', '5a9e7ddd14d705', 21.95, 0, '5a9e7ddd14d705.png', '5a9e7ddd14d705_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(230, '5a9e7ddd14c8c6', 'Gallinella sarda t-shirt - L', 'Black', '2F2E33', 'L', '5a9e7ddd14de35', 21.95, 0, '5a9e7ddd14de35.png', '5a9e7ddd14de35_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(231, '5a9e7ddd14c8c6', 'Gallinella sarda t-shirt - XL', 'Black', '2F2E33', 'XL', '5a9e7ddd14e531', 21.95, 0, '5a9e7ddd14e531.png', '5a9e7ddd14e531_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(232, '5a9e7dbe639f93', 'Gallinella sarda t-shirt - S', 'Black', '2F2E33', 'S', '5a9e7dbe63a8c7', 21.95, 0, '5a9e7dbe63a8c7.png', '5a9e7dbe63a8c7_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(233, '5a9e7dbe639f93', 'Gallinella sarda t-shirt - M', 'Black', '2F2E33', 'M', '5a9e7dbe63b125', 21.95, 0, '5a9e7dbe63b125.png', '5a9e7dbe63b125_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(234, '5a9e7dbe639f93', 'Gallinella sarda t-shirt - L', 'Black', '2F2E33', 'L', '5a9e7dbe63b975', 21.95, 0, '5a9e7dbe63b975.png', '5a9e7dbe63b975_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(235, '5a9e7dbe639f93', 'Gallinella sarda t-shirt - XL', 'Black', '2F2E33', 'XL', '5a9e7dbe63c1e7', 21.95, 0, '5a9e7dbe63c1e7.png', '5a9e7dbe63c1e7_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(236, '5a9e7d71439da1', 'Dreaming of you t-shirt - S', 'Black', '2F2E33', 'S', '5a9e7d7143a5b8', 21.95, 0, '5a9e7d7143a5b8.png', '5a9e7d7143a5b8_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(237, '5a9e7d71439da1', 'Dreaming of you t-shirt - M', 'Black', '2F2E33', 'M', '5a9e7d7143acf3', 21.95, 0, '5a9e7d7143acf3.png', '5a9e7d7143acf3_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(238, '5a9e7d71439da1', 'Dreaming of you t-shirt - L', 'Black', '2F2E33', 'L', '5a9e7d7143b419', 21.95, 0, '5a9e7d7143b419.png', '5a9e7d7143b419_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(239, '5a9e7d71439da1', 'Dreaming of you t-shirt - XL', 'Black', '2F2E33', 'XL', '5a9e7d7143bb26', 21.95, 0, '5a9e7d7143bb26.png', '5a9e7d7143bb26_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(240, '5a9e7cfa97eb11', 'Dreaming of you t-shirt - S', 'Black', '2F2E33', 'S', '5a9e7cfa97f343', 21.95, 0, '5a9e7cfa97f343.png', '5a9e7cfa97f343_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(241, '5a9e7cfa97eb11', 'Dreaming of you t-shirt - M', 'Black', '2F2E33', 'M', '5a9e7cfa97fa83', 21.95, 0, '5a9e7cfa97fa83.png', '5a9e7cfa97fa83_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(242, '5a9e7cfa97eb11', 'Dreaming of you t-shirt - L', 'Black', '2F2E33', 'L', '5a9e7cfa9801b8', 21.95, 0, '5a9e7cfa9801b8.png', '5a9e7cfa9801b8_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(243, '5a9e7cfa97eb11', 'Dreaming of you t-shirt - XL', 'Black', '2F2E33', 'XL', '5a9e7cfa9808e7', 21.95, 0, '5a9e7cfa9808e7.png', '5a9e7cfa9808e7_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(244, '5a9e7be6ef2c59', 'Il bacio t-shirt - XS', 'White', 'ffffff', 'XS', '5a9e7be6ef3494', 29.95, 0, '5a9e7be6ef3494.png', '5a9e7be6ef3494_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(245, '5a9e7be6ef2c59', 'Il bacio t-shirt - S', 'White', 'ffffff', 'S', '5a9e7be6ef3bf1', 29.95, 0, '5a9e7be6ef3bf1.png', '5a9e7be6ef3bf1_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(246, '5a9e7be6ef2c59', 'Il bacio t-shirt - M', 'White', 'ffffff', 'M', '5a9e7be6ef4326', 29.95, 0, '5a9e7be6ef4326.png', '5a9e7be6ef4326_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(247, '5a9e7be6ef2c59', 'Il bacio t-shirt - L', 'White', 'ffffff', 'L', '5a9e7be6ef4a88', 29.95, 0, '5a9e7be6ef4a88.png', '5a9e7be6ef4a88_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(248, '5a9e7be6ef2c59', 'Il bacio t-shirt - XL', 'White', 'ffffff', 'XL', '5a9e7be6ef51a2', 29.95, 0, '5a9e7be6ef51a2.png', '5a9e7be6ef51a2_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(249, '5a9e7be6ef2c59', 'Il bacio t-shirt - 2XL', 'White', 'ffffff', '2XL', '5a9e7be6ef58c5', 31.45, 0, '5a9e7be6ef58c5.png', '5a9e7be6ef58c5_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(250, '5a9e7b0f14d151', 'Il pensiero t-shirt - XS', 'White', 'ffffff', 'XS', '5a9e7b0f14dae2', 29.95, 0, '5a9e7b0f14dae2.png', '5a9e7b0f14dae2_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(251, '5a9e7b0f14d151', 'Il pensiero t-shirt - S', 'White', 'ffffff', 'S', '5a9e7b0f14e213', 29.95, 0, '5a9e7b0f14e213.png', '5a9e7b0f14e213_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(252, '5a9e7b0f14d151', 'Il pensiero t-shirt - M', 'White', 'ffffff', 'M', '5a9e7b0f14eb92', 29.95, 0, '5a9e7b0f14eb92.png', '5a9e7b0f14eb92_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(253, '5a9e7b0f14d151', 'Il pensiero t-shirt - L', 'White', 'ffffff', 'L', '5a9e7b0f14f167', 29.95, 0, '5a9e7b0f14f167.png', '5a9e7b0f14f167_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(254, '5a9e7b0f14d151', 'Il pensiero t-shirt - XL', 'White', 'ffffff', 'XL', '5a9e7b0f14f8e1', 29.95, 0, '5a9e7b0f14f8e1.png', '5a9e7b0f14f8e1_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(255, '5a9e7b0f14d151', 'Il pensiero t-shirt - 2XL', 'White', 'ffffff', '2XL', '5a9e7b0f14fff4', 31.45, 0, '5a9e7b0f14fff4.png', '5a9e7b0f14fff4_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(256, '5a9e7a457b0155', 'Il silenzio t-shirt - XS', 'White', 'ffffff', 'XS', '5a9e7a457b0983', 29.95, 0, '5a9e7a457b0983.png', '5a9e7a457b0983_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(257, '5a9e7a457b0155', 'Il silenzio t-shirt - S', 'White', 'ffffff', 'S', '5a9e7a457b10c5', 29.95, 0, '5a9e7a457b10c5.png', '5a9e7a457b10c5_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(258, '5a9e7a457b0155', 'Il silenzio t-shirt - M', 'White', 'ffffff', 'M', '5a9e7a457b17f5', 29.95, 0, '5a9e7a457b17f5.png', '5a9e7a457b17f5_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(259, '5a9e7a457b0155', 'Il silenzio t-shirt - L', 'White', 'ffffff', 'L', '5a9e7a457b1f11', 29.95, 0, '5a9e7a457b1f11.png', '5a9e7a457b1f11_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(260, '5a9e7a457b0155', 'Il silenzio t-shirt - XL', 'White', 'ffffff', 'XL', '5a9e7a457b2628', 29.95, 0, '5a9e7a457b2628.png', '5a9e7a457b2628_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(261, '5a9e7a457b0155', 'Il silenzio t-shirt - 2XL', 'White', 'ffffff', '2XL', '5a9e7a457b2d32', 31.45, 0, '5a9e7a457b2d32.png', '5a9e7a457b2d32_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(262, '5a9e7938145986', 'Infinito t-shirt - XS', 'White', 'ffffff', 'XS', '5a9e7938146198', 29.95, 0, '5a9e7938146198.png', '5a9e7938146198_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(263, '5a9e7938145986', 'Infinito t-shirt - S', 'White', 'ffffff', 'S', '5a9e79381468c5', 29.95, 0, '5a9e79381468c5.png', '5a9e79381468c5_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(264, '5a9e7938145986', 'Infinito t-shirt - M', 'White', 'ffffff', 'M', '5a9e7938146fd8', 29.95, 0, '5a9e7938146fd8.png', '5a9e7938146fd8_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(265, '5a9e7938145986', 'Infinito t-shirt - L', 'White', 'ffffff', 'L', '5a9e79381476d7', 29.95, 0, '5a9e79381476d7.png', '5a9e79381476d7_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(266, '5a9e7938145986', 'Infinito t-shirt - XL', 'White', 'ffffff', 'XL', '5a9e7938147de8', 29.95, 0, '5a9e7938147de8.png', '5a9e7938147de8_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(267, '5a9e7938145986', 'Infinito t-shirt - 2XL', 'White', 'ffffff', '2XL', '5a9e79381484e8', 31.45, 0, '5a9e79381484e8.png', '5a9e79381484e8_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(268, '5a9e784dd4fc14', 'La separazione t-shirt - XS', 'White', 'ffffff', 'XS', '5a9e784dd50507', 29.95, 0, '5a9e784dd50507.png', '5a9e784dd50507_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(269, '5a9e784dd4fc14', 'La separazione t-shirt - S', 'White', 'ffffff', 'S', '5a9e784dd50d08', 29.95, 0, '5a9e784dd50d08.png', '5a9e784dd50d08_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(270, '5a9e784dd4fc14', 'La separazione t-shirt - M', 'White', 'ffffff', 'M', '5a9e784dd514f3', 29.95, 0, '5a9e784dd514f3.png', '5a9e784dd514f3_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(271, '5a9e784dd4fc14', 'La separazione t-shirt - L', 'White', 'ffffff', 'L', '5a9e784dd51cd2', 29.95, 0, '5a9e784dd51cd2.png', '5a9e784dd51cd2_f.png', '', 1, 1, 1, '2018-03-19 12:21:21');
INSERT INTO `varianti_prodotti` (`id_variante`, `codice_prodotto`, `nome`, `colore`, `colore_codice`, `taglia`, `codice`, `prezzo`, `prezzo_scontato`, `url_img_piccola`, `url_img_grande`, `url_img_grande_retro`, `stato`, `in_stock`, `sync_status`, `sync_time`) VALUES
(272, '5a9e784dd4fc14', 'La separazione t-shirt - XL', 'White', 'ffffff', 'XL', '5a9e784dd524b2', 29.95, 0, '5a9e784dd524b2.png', '5a9e784dd524b2_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(273, '5a9e784dd4fc14', 'La separazione t-shirt - 2XL', 'White', 'ffffff', '2XL', '5a9e784dd52c88', 31.45, 0, '5a9e784dd52c88.png', '5a9e784dd52c88_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(274, '5a9e76e177cf98', 'Lo sguardo t-shirt - XS', 'White', 'ffffff', 'XS', '5a9e76e177d7a7', 29.95, 0, '5a9e76e177d7a7.png', '5a9e76e177d7a7_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(275, '5a9e76e177cf98', 'Lo sguardo t-shirt - S', 'White', 'ffffff', 'S', '5a9e76e177df18', 29.95, 0, '5a9e76e177df18.png', '5a9e76e177df18_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(276, '5a9e76e177cf98', 'Lo sguardo t-shirt - M', 'White', 'ffffff', 'M', '5a9e76e177e636', 29.95, 0, '5a9e76e177e636.png', '5a9e76e177e636_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(277, '5a9e76e177cf98', 'Lo sguardo t-shirt - L', 'White', 'ffffff', 'L', '5a9e76e177ed45', 29.95, 0, '5a9e76e177ed45.png', '5a9e76e177ed45_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(278, '5a9e76e177cf98', 'Lo sguardo t-shirt - XL', 'White', 'ffffff', 'XL', '5a9e76e177f555', 29.95, 0, '5a9e76e177f555.png', '5a9e76e177f555_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(279, '5a9e76e177cf98', 'Lo sguardo t-shirt - 2XL', 'White', 'ffffff', '2XL', '5a9e76e177fcd1', 31.45, 0, '5a9e76e177fcd1.png', '5a9e76e177fcd1_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(280, '5a8ed76cdfac23', 'Un nuovo inizio T-shirt - XS', 'White', 'ffffff', 'XS', '5a8ed76cdfb427', 29.95, 0, '5a8ed76cdfb427.png', '5a8ed76cdfb427_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(281, '5a8ed76cdfac23', 'Un nuovo inizio T-shirt - S', 'White', 'ffffff', 'S', '5a8ed76cdfbb44', 29.95, 0, '5a8ed76cdfbb44.png', '5a8ed76cdfbb44_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(282, '5a8ed76cdfac23', 'Un nuovo inizio T-shirt - M', 'White', 'ffffff', 'M', '5a8ed76cdfc265', 29.95, 0, '5a8ed76cdfc265.png', '5a8ed76cdfc265_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(283, '5a8ed76cdfac23', 'Un nuovo inizio T-shirt - L', 'White', 'ffffff', 'L', '5a8ed76cdfc973', 29.95, 0, '5a8ed76cdfc973.png', '5a8ed76cdfc973_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(284, '5a8ed76cdfac23', 'Un nuovo inizio T-shirt - XL', 'White', 'ffffff', 'XL', '5a8ed76cdfd073', 29.95, 0, '5a8ed76cdfd073.png', '5a8ed76cdfd073_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(285, '5a8ed76cdfac23', 'Un nuovo inizio T-shirt - 2XL', 'White', 'ffffff', '2XL', '5a8ed76cdfd766', 31.45, 0, '5a8ed76cdfd766.png', '5a8ed76cdfd766_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(286, '5a8ed67932c228', 'L''incontro T-shirt - XS', 'White', 'ffffff', 'XS', '5a8ed67932ca34', 29.95, 0, '5a8ed67932ca34.png', '5a8ed67932ca34_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(287, '5a8ed67932c228', 'L''incontro T-shirt - S', 'White', 'ffffff', 'S', '5a8ed67932d184', 29.95, 0, '5a8ed67932d184.png', '5a8ed67932d184_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(288, '5a8ed67932c228', 'L''incontro T-shirt - M', 'White', 'ffffff', 'M', '5a8ed67932d8a7', 29.95, 0, '5a8ed67932d8a7.png', '5a8ed67932d8a7_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(289, '5a8ed67932c228', 'L''incontro T-shirt - L', 'White', 'ffffff', 'L', '5a8ed67932dfc7', 29.95, 0, '5a8ed67932dfc7.png', '5a8ed67932dfc7_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(290, '5a8ed67932c228', 'L''incontro T-shirt - XL', 'White', 'ffffff', 'XL', '5a8ed67932e715', 29.95, 0, '5a8ed67932e715.png', '5a8ed67932e715_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(291, '5a8ed67932c228', 'L''incontro T-shirt - 2XL', 'White', 'ffffff', '2XL', '5a8ed67932eef9', 31.45, 0, '5a8ed67932eef9.png', '5a8ed67932eef9_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(292, '5a8b2ba7958098', 'Lips t-shirt - White / S', 'White', 'e5e6e1', 'S', '5a8b2ba7958895', 15, 0, '5a8b2ba7958895.png', '5a8b2ba7958895_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(293, '5a8b2ba7958098', 'Lips t-shirt - White / M', 'White', 'e5e6e1', 'M', '5a8b2ba7958fb7', 15, 0, '5a8b2ba7958fb7.png', '5a8b2ba7958fb7_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(294, '5a8b2ba7958098', 'Lips t-shirt - White / L', 'White', 'e5e6e1', 'L', '5a8b2ba79596a6', 15, 0, '5a8b2ba79596a6.png', '5a8b2ba79596a6_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(295, '5a8b2ba7958098', 'Lips t-shirt - White / XL', 'White', 'e5e6e1', 'XL', '5a8b2ba7959da4', 15, 0, '5a8b2ba7959da4.png', '5a8b2ba7959da4_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(296, '5a8b2ba7958098', 'Lips t-shirt - White / 2XL', 'White', 'e5e6e1', '2XL', '5a8b2ba795a491', 16.5, 0, '5a8b2ba795a491.png', '5a8b2ba795a491_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(297, '5a8b2ba7958098', 'Lips t-shirt - Black / S', 'Black', '232227', 'S', '5a8b2ba795abc3', 15, 0, '5a8b2ba795abc3.png', '5a8b2ba795abc3_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(298, '5a8b2ba7958098', 'Lips t-shirt - Black / M', 'Black', '232227', 'M', '5a8b2ba795b2d5', 15, 0, '5a8b2ba795b2d5.png', '5a8b2ba795b2d5_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(299, '5a8b2ba7958098', 'Lips t-shirt - Black / L', 'Black', '232227', 'L', '5a8b2ba795b9c2', 15, 0, '5a8b2ba795b9c2.png', '5a8b2ba795b9c2_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(300, '5a8b2ba7958098', 'Lips t-shirt - Black / XL', 'Black', '232227', 'XL', '5a8b2ba795c0c2', 15, 0, '5a8b2ba795c0c2.png', '5a8b2ba795c0c2_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(301, '5a8b2ba7958098', 'Lips t-shirt - Black / 2XL', 'Black', '232227', '2XL', '5a8b2ba795c7b5', 16.5, 0, '5a8b2ba795c7b5.png', '5a8b2ba795c7b5_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(302, '5a8b2ba7958098', 'Lips t-shirt - Heather Dark Grey / S', 'Heather Dark Gr', '262f36', 'S', '5a8b2ba795ceb3', 15, 0, '5a8b2ba795ceb3.png', '5a8b2ba795ceb3_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(303, '5a8b2ba7958098', 'Lips t-shirt - Heather Dark Grey / M', 'Heather Dark Gr', '262f36', 'M', '5a8b2ba795d5e6', 15, 0, '5a8b2ba795d5e6.png', '5a8b2ba795d5e6_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(304, '5a8b2ba7958098', 'Lips t-shirt - Heather Dark Grey / L', 'Heather Dark Gr', '262f36', 'L', '5a8b2ba795dd86', 15, 0, '5a8b2ba795dd86.png', '5a8b2ba795dd86_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(305, '5a8b2ba7958098', 'Lips t-shirt - Heather Dark Grey / XL', 'Heather Dark Gr', '262f36', 'XL', '5a8b2ba795e4d2', 15, 0, '5a8b2ba795e4d2.png', '5a8b2ba795e4d2_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(306, '5a8b2ba7958098', 'Lips t-shirt - Heather Dark Grey / 2XL', 'Heather Dark Gr', '262f36', '2XL', '5a8b2ba795ebd3', 16.5, 0, '5a8b2ba795ebd3.png', '5a8b2ba795ebd3_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(307, '5a8b2ba7958098', 'Lips t-shirt - Heather Blue / S', 'Heather Blue', '4a5684', 'S', '5a8b2ba795f2f4', 15, 0, '5a8b2ba795f2f4.png', '5a8b2ba795f2f4_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(308, '5a8b2ba7958098', 'Lips t-shirt - Heather Blue / M', 'Heather Blue', '4a5684', 'M', '5a8b2ba795f9f2', 15, 0, '5a8b2ba795f9f2.png', '5a8b2ba795f9f2_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(309, '5a8b2ba7958098', 'Lips t-shirt - Heather Blue / L', 'Heather Blue', '4a5684', 'L', '5a8b2ba79600e6', 15, 0, '5a8b2ba79600e6.png', '5a8b2ba79600e6_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(310, '5a8b2ba7958098', 'Lips t-shirt - Heather Blue / XL', 'Heather Blue', '4a5684', 'XL', '5a8b2ba79607d2', 15, 0, '5a8b2ba79607d2.png', '5a8b2ba79607d2_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(311, '5a8b2ba7958098', 'Lips t-shirt - Heather Blue / 2XL', 'Heather Blue', '4a5684', '2XL', '5a8b2ba7960ec9', 16.5, 0, '5a8b2ba7960ec9.png', '5a8b2ba7960ec9_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(312, '5a8b2ba7958098', 'Lips t-shirt - Heather Grey / S', 'Heather Grey', '928d92', 'S', '5a8b2ba79615b4', 15, 0, '5a8b2ba79615b4.png', '5a8b2ba79615b4_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(313, '5a8b2ba7958098', 'Lips t-shirt - Heather Grey / M', 'Heather Grey', '928d92', 'M', '5a8b2ba7961cb9', 15, 0, '5a8b2ba7961cb9.png', '5a8b2ba7961cb9_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(314, '5a8b2ba7958098', 'Lips t-shirt - Heather Grey / L', 'Heather Grey', '928d92', 'L', '5a8b2ba79623a4', 15, 0, '5a8b2ba79623a4.png', '5a8b2ba79623a4_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(315, '5a8b2ba7958098', 'Lips t-shirt - Heather Grey / XL', 'Heather Grey', '928d92', 'XL', '5a8b2ba7962a98', 15, 0, '5a8b2ba7962a98.png', '5a8b2ba7962a98_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(316, '5a8b2ba7958098', 'Lips t-shirt - Heather Grey / 2XL', 'Heather Grey', '928d92', '2XL', '5a8b2ba7963188', 16.5, 0, '5a8b2ba7963188.png', '5a8b2ba7963188_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(317, '5a8b2ba7958098', 'Lips t-shirt - Caribbean Blue / S', 'Caribbean Blue', '008fb7', 'S', '5a8b2ba7963876', 15, 0, '5a8b2ba7963876.png', '5a8b2ba7963876_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(318, '5a8b2ba7958098', 'Lips t-shirt - Caribbean Blue / M', 'Caribbean Blue', '008fb7', 'M', '5a8b2ba7963f64', 15, 0, '5a8b2ba7963f64.png', '5a8b2ba7963f64_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(319, '5a8b2ba7958098', 'Lips t-shirt - Caribbean Blue / L', 'Caribbean Blue', '008fb7', 'L', '5a8b2ba7964658', 15, 0, '5a8b2ba7964658.png', '5a8b2ba7964658_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(320, '5a8b2ba7958098', 'Lips t-shirt - Caribbean Blue / XL', 'Caribbean Blue', '008fb7', 'XL', '5a8b2ba7964d47', 15, 0, '5a8b2ba7964d47.png', '5a8b2ba7964d47_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(321, '5a8b2ba7958098', 'Lips t-shirt - Caribbean Blue / 2XL', 'Caribbean Blue', '008fb7', '2XL', '5a8b2ba7965432', 16.5, 0, '5a8b2ba7965432.png', '5a8b2ba7965432_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(322, '5a8b29f137e8c2', 'Yang', 'White', '', '11oz', '5a8b29f137ef57', 7.95, 0, '5a8b29f137ef57.png', '5a8b29f137ef57_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(323, '5a8b2928123a87', 'Jungle Flowers Dress - XS', 'White', 'ffffff', 'XS', '5a8b29281242c2', 34.95, 0, '5a8b29281242c2.png', '5a8b29281242c2_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(324, '5a8b2928123a87', 'Jungle Flowers Dress - S', 'White', 'ffffff', 'S', '5a8b2928124a18', 34.95, 0, '5a8b2928124a18.png', '5a8b2928124a18_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(325, '5a8b2928123a87', 'Jungle Flowers Dress - M', 'White', 'ffffff', 'M', '5a8b29281251b2', 34.95, 0, '5a8b29281251b2.png', '5a8b29281251b2_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(326, '5a8b2928123a87', 'Jungle Flowers Dress - L', 'White', 'ffffff', 'L', '5a8b29281259c1', 34.95, 0, '5a8b29281259c1.png', '5a8b29281259c1_f.png', '', 1, 1, 1, '2018-03-19 12:21:21'),
(327, '5a8b2928123a87', 'Jungle Flowers Dress - XL', 'White', 'ffffff', 'XL', '5a8b29281261b6', 34.95, 0, '5a8b29281261b6.png', '5a8b29281261b6_f.png', '', 1, 1, 1, '2018-03-19 12:21:21');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `carrello`
--
ALTER TABLE `carrello`
  ADD PRIMARY KEY (`id_carrello`);

--
-- Indexes for table `categorie`
--
ALTER TABLE `categorie`
  ADD PRIMARY KEY (`id_categorie`);

--
-- Indexes for table `categorie_gallery`
--
ALTER TABLE `categorie_gallery`
  ADD PRIMARY KEY (`id_categoria_gallery`);

--
-- Indexes for table `categorie_gallery_traduzioni`
--
ALTER TABLE `categorie_gallery_traduzioni`
  ADD PRIMARY KEY (`id_categorie_gallery_traduzioni`);

--
-- Indexes for table `categorie_traduzioni`
--
ALTER TABLE `categorie_traduzioni`
  ADD PRIMARY KEY (`id_categoria_traduzioni`);

--
-- Indexes for table `clienti`
--
ALTER TABLE `clienti`
  ADD PRIMARY KEY (`id_cliente`);

--
-- Indexes for table `colori_classi`
--
ALTER TABLE `colori_classi`
  ADD PRIMARY KEY (`id_colore_classe`);

--
-- Indexes for table `colori_prodotti`
--
ALTER TABLE `colori_prodotti`
  ADD PRIMARY KEY (`id_colori_prodotti`);

--
-- Indexes for table `constants_framework`
--
ALTER TABLE `constants_framework`
  ADD PRIMARY KEY (`id_cf`);

--
-- Indexes for table `contatti_moduli`
--
ALTER TABLE `contatti_moduli`
  ADD PRIMARY KEY (`id_contatto`);

--
-- Indexes for table `contatti_newsletter`
--
ALTER TABLE `contatti_newsletter`
  ADD PRIMARY KEY (`id_contatto_newsletter`);

--
-- Indexes for table `coupon`
--
ALTER TABLE `coupon`
  ADD PRIMARY KEY (`id_coupon`);

--
-- Indexes for table `email_templates`
--
ALTER TABLE `email_templates`
  ADD PRIMARY KEY (`id_template`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `home_slider`
--
ALTER TABLE `home_slider`
  ADD PRIMARY KEY (`hs_id`);

--
-- Indexes for table `immagini_gallery`
--
ALTER TABLE `immagini_gallery`
  ADD PRIMARY KEY (`id_ig`);

--
-- Indexes for table `immagini_gallery_traduzioni`
--
ALTER TABLE `immagini_gallery_traduzioni`
  ADD PRIMARY KEY (`id_immagini_gallery_traduzioni`);

--
-- Indexes for table `indirizzo_fatturazione`
--
ALTER TABLE `indirizzo_fatturazione`
  ADD PRIMARY KEY (`id_indirizzo_fatturazione`);

--
-- Indexes for table `indirizzo_spedizione`
--
ALTER TABLE `indirizzo_spedizione`
  ADD PRIMARY KEY (`id_indirizzo_spedizione`), ADD KEY `fk_cliente_idx` (`id_cliente`);

--
-- Indexes for table `lingue`
--
ALTER TABLE `lingue`
  ADD PRIMARY KEY (`id_lingue`);

--
-- Indexes for table `lingue_labels_lang`
--
ALTER TABLE `lingue_labels_lang`
  ADD PRIMARY KEY (`id_lingue_labels_lang`);

--
-- Indexes for table `login_attempts`
--
ALTER TABLE `login_attempts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `ordini`
--
ALTER TABLE `ordini`
  ADD PRIMARY KEY (`id_ordine`), ADD KEY `fk_cliente_idx` (`id_cliente`);

--
-- Indexes for table `pagine`
--
ALTER TABLE `pagine`
  ADD PRIMARY KEY (`id_pagina`);

--
-- Indexes for table `pagine_contenuti`
--
ALTER TABLE `pagine_contenuti`
  ADD PRIMARY KEY (`id_pc`);

--
-- Indexes for table `prodotti`
--
ALTER TABLE `prodotti`
  ADD PRIMARY KEY (`id_prodotti`);

--
-- Indexes for table `prodotti_categorie`
--
ALTER TABLE `prodotti_categorie`
  ADD PRIMARY KEY (`id_prodotti_categorie`);

--
-- Indexes for table `prodotti_traduzioni`
--
ALTER TABLE `prodotti_traduzioni`
  ADD PRIMARY KEY (`id_prodotti_traduzioni`);

--
-- Indexes for table `prodotto_taglia`
--
ALTER TABLE `prodotto_taglia`
  ADD PRIMARY KEY (`fk_prodotto`,`fk_taglia`);

--
-- Indexes for table `stato_coupon`
--
ALTER TABLE `stato_coupon`
  ADD PRIMARY KEY (`id_stato_coupon`);

--
-- Indexes for table `stato_descrizione`
--
ALTER TABLE `stato_descrizione`
  ADD PRIMARY KEY (`id_stato_descrizione`);

--
-- Indexes for table `stato_ordine`
--
ALTER TABLE `stato_ordine`
  ADD PRIMARY KEY (`id_stato_ordine`);

--
-- Indexes for table `stato_pagamento`
--
ALTER TABLE `stato_pagamento`
  ADD PRIMARY KEY (`id_stato_pagamento`);

--
-- Indexes for table `stato_prodotti`
--
ALTER TABLE `stato_prodotti`
  ADD PRIMARY KEY (`stato_prodotti_id`);

--
-- Indexes for table `storico_carrello`
--
ALTER TABLE `storico_carrello`
  ADD PRIMARY KEY (`id_storico_carrello`);

--
-- Indexes for table `storico_clienti`
--
ALTER TABLE `storico_clienti`
  ADD PRIMARY KEY (`id_storico_clienti`);

--
-- Indexes for table `sync_status`
--
ALTER TABLE `sync_status`
  ADD PRIMARY KEY (`id_sync_status`);

--
-- Indexes for table `sync_test`
--
ALTER TABLE `sync_test`
  ADD PRIMARY KEY (`id_sync_test`);

--
-- Indexes for table `taglie`
--
ALTER TABLE `taglie`
  ADD PRIMARY KEY (`id_taglia`);

--
-- Indexes for table `tags`
--
ALTER TABLE `tags`
  ADD PRIMARY KEY (`id_tag`);

--
-- Indexes for table `tags_prodotti`
--
ALTER TABLE `tags_prodotti`
  ADD PRIMARY KEY (`id_tags_prodotti`);

--
-- Indexes for table `tipo_coupon`
--
ALTER TABLE `tipo_coupon`
  ADD PRIMARY KEY (`id_tipo_coupon`);

--
-- Indexes for table `tipo_pagamento`
--
ALTER TABLE `tipo_pagamento`
  ADD PRIMARY KEY (`id_tipo_pagamento`);

--
-- Indexes for table `tipo_prodotto`
--
ALTER TABLE `tipo_prodotto`
  ADD PRIMARY KEY (`id_tipo_prodotto`);

--
-- Indexes for table `tipo_template`
--
ALTER TABLE `tipo_template`
  ADD PRIMARY KEY (`id_tipo_template`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users_groups`
--
ALTER TABLE `users_groups`
  ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `uc_users_groups` (`user_id`,`group_id`), ADD KEY `fk_users_groups_users1_idx` (`user_id`), ADD KEY `fk_users_groups_groups1_idx` (`group_id`);

--
-- Indexes for table `variante_taglia`
--
ALTER TABLE `variante_taglia`
  ADD PRIMARY KEY (`fk_variante`,`fk_taglia`);

--
-- Indexes for table `varianti_prodotti`
--
ALTER TABLE `varianti_prodotti`
  ADD PRIMARY KEY (`id_variante`), ADD KEY `fk_prodotti` (`codice_prodotto`), ADD KEY `fk_colore` (`colore`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `carrello`
--
ALTER TABLE `carrello`
  MODIFY `id_carrello` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `categorie`
--
ALTER TABLE `categorie`
  MODIFY `id_categorie` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `categorie_gallery`
--
ALTER TABLE `categorie_gallery`
  MODIFY `id_categoria_gallery` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `categorie_gallery_traduzioni`
--
ALTER TABLE `categorie_gallery_traduzioni`
  MODIFY `id_categorie_gallery_traduzioni` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `categorie_traduzioni`
--
ALTER TABLE `categorie_traduzioni`
  MODIFY `id_categoria_traduzioni` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `clienti`
--
ALTER TABLE `clienti`
  MODIFY `id_cliente` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `colori_classi`
--
ALTER TABLE `colori_classi`
  MODIFY `id_colore_classe` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `colori_prodotti`
--
ALTER TABLE `colori_prodotti`
  MODIFY `id_colori_prodotti` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=25;
--
-- AUTO_INCREMENT for table `constants_framework`
--
ALTER TABLE `constants_framework`
  MODIFY `id_cf` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=34;
--
-- AUTO_INCREMENT for table `contatti_moduli`
--
ALTER TABLE `contatti_moduli`
  MODIFY `id_contatto` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `contatti_newsletter`
--
ALTER TABLE `contatti_newsletter`
  MODIFY `id_contatto_newsletter` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `coupon`
--
ALTER TABLE `coupon`
  MODIFY `id_coupon` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `email_templates`
--
ALTER TABLE `email_templates`
  MODIFY `id_template` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
  MODIFY `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `home_slider`
--
ALTER TABLE `home_slider`
  MODIFY `hs_id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `immagini_gallery`
--
ALTER TABLE `immagini_gallery`
  MODIFY `id_ig` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=50;
--
-- AUTO_INCREMENT for table `immagini_gallery_traduzioni`
--
ALTER TABLE `immagini_gallery_traduzioni`
  MODIFY `id_immagini_gallery_traduzioni` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=101;
--
-- AUTO_INCREMENT for table `indirizzo_fatturazione`
--
ALTER TABLE `indirizzo_fatturazione`
  MODIFY `id_indirizzo_fatturazione` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=21;
--
-- AUTO_INCREMENT for table `indirizzo_spedizione`
--
ALTER TABLE `indirizzo_spedizione`
  MODIFY `id_indirizzo_spedizione` bigint(20) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `lingue`
--
ALTER TABLE `lingue`
  MODIFY `id_lingue` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `lingue_labels_lang`
--
ALTER TABLE `lingue_labels_lang`
  MODIFY `id_lingue_labels_lang` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=375;
--
-- AUTO_INCREMENT for table `login_attempts`
--
ALTER TABLE `login_attempts`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `ordini`
--
ALTER TABLE `ordini`
  MODIFY `id_ordine` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `pagine`
--
ALTER TABLE `pagine`
  MODIFY `id_pagina` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=40;
--
-- AUTO_INCREMENT for table `pagine_contenuti`
--
ALTER TABLE `pagine_contenuti`
  MODIFY `id_pc` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `prodotti`
--
ALTER TABLE `prodotti`
  MODIFY `id_prodotti` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=61;
--
-- AUTO_INCREMENT for table `prodotti_categorie`
--
ALTER TABLE `prodotti_categorie`
  MODIFY `id_prodotti_categorie` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `prodotti_traduzioni`
--
ALTER TABLE `prodotti_traduzioni`
  MODIFY `id_prodotti_traduzioni` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `stato_coupon`
--
ALTER TABLE `stato_coupon`
  MODIFY `id_stato_coupon` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `stato_descrizione`
--
ALTER TABLE `stato_descrizione`
  MODIFY `id_stato_descrizione` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `stato_ordine`
--
ALTER TABLE `stato_ordine`
  MODIFY `id_stato_ordine` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `stato_pagamento`
--
ALTER TABLE `stato_pagamento`
  MODIFY `id_stato_pagamento` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `storico_carrello`
--
ALTER TABLE `storico_carrello`
  MODIFY `id_storico_carrello` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `storico_clienti`
--
ALTER TABLE `storico_clienti`
  MODIFY `id_storico_clienti` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `sync_status`
--
ALTER TABLE `sync_status`
  MODIFY `id_sync_status` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `sync_test`
--
ALTER TABLE `sync_test`
  MODIFY `id_sync_test` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `taglie`
--
ALTER TABLE `taglie`
  MODIFY `id_taglia` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `tags`
--
ALTER TABLE `tags`
  MODIFY `id_tag` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tags_prodotti`
--
ALTER TABLE `tags_prodotti`
  MODIFY `id_tags_prodotti` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `tipo_coupon`
--
ALTER TABLE `tipo_coupon`
  MODIFY `id_tipo_coupon` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `tipo_pagamento`
--
ALTER TABLE `tipo_pagamento`
  MODIFY `id_tipo_pagamento` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `tipo_prodotto`
--
ALTER TABLE `tipo_prodotto`
  MODIFY `id_tipo_prodotto` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `tipo_template`
--
ALTER TABLE `tipo_template`
  MODIFY `id_tipo_template` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `users_groups`
--
ALTER TABLE `users_groups`
  MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `varianti_prodotti`
--
ALTER TABLE `varianti_prodotti`
  MODIFY `id_variante` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=328;
--
-- Limiti per le tabelle scaricate
--

--
-- Limiti per la tabella `users_groups`
--
ALTER TABLE `users_groups`
ADD CONSTRAINT `fk_users_groups_groups1` FOREIGN KEY (`group_id`) REFERENCES `groups` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
ADD CONSTRAINT `fk_users_groups_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
