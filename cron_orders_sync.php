<?
/**
CALL /admin/Products/sync/update
> /dev/null 2>&1 per non avere email
*/
if (isset($_SERVER['REMOTE_ADDR'])) die('CLI-only access.');

//if(!php_sapi_name() === 'cli') die('CLI-only access.');

$argc = $_SERVER['argc'];
$argv = $_SERVER['argv'];
 
// INTERPRETTING INPUT
if ($argc > 1 && isset($argv[1])) {
	//$_SERVER['HTTP_HOST'] = 'localhost';
	$_SERVER['PATH_INFO']   = $argv[1];
	$_SERVER['REQUEST_URI'] = $argv[1];
} else {
	$_SERVER['HTTP_HOST'] = 'www.machlo.com';
	$_SERVER['PATH_INFO']   = '/admin/Orders/syncPrintfulOrders';
	$_SERVER['REQUEST_URI'] = '/admin/Orders/syncPrintfulOrders';
}

/*
|---------------------------------------------------------------
| PHP SCRIPT EXECUTION TIME ('0' means Unlimited)
|---------------------------------------------------------------
|
*/
ini_set('max_execution_time', 0);
set_time_limit(0);
 
require_once('index.php');
?>